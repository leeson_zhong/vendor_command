;/**************************************************************************//**
; * @file     startup_ARMCM4.s
; * @brief    CMSIS Core Device Startup File for
; *           ARMCM4 Device Series
; * @version  V1.08
; * @date     23. November 2012
; *
; * @note
; *
; ******************************************************************************/
;/* Copyright (c) 2011 - 2012 ARM LIMITED
;
;   All rights reserved.
;   Redistribution and use in source and binary forms, with or without
;   modification, are permitted provided that the following conditions are met:
;   - Redistributions of source code must retain the above copyright
;     notice, this list of conditions and the following disclaimer.
;   - Redistributions in binary form must reproduce the above copyright
;     notice, this list of conditions and the following disclaimer in the
;     documentation and/or other materials provided with the distribution.
;   - Neither the name of ARM nor the names of its contributors may be used
;     to endorse or promote products derived from this software without
;     specific prior written permission.
;   *
;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;   ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDERS AND CONTRIBUTORS BE
;   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
;   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;   POSSIBILITY OF SUCH DAMAGE.
;   ---------------------------------------------------------------------------*/


;
; The modules in this file are included in the libraries, and may be replaced
; by any user-defined modules that define the PUBLIC symbol _program_start or
; a user defined start symbol.
; To override the cstartup defined in the library, simply add your modified
; version to the workbench project.
;
; The vector table is normally located at address 0.
; When debugging in RAM, it can be located in RAM, aligned to at least 2^6.
; The name "__vector_table" has special meaning for C-SPY:
; it is where the SP start value is found, and the NVIC vector
; table register (VTOR) is initialized to this address if != 0.
;
; Cortex-M version
;

        MODULE  ?cstartup

        ;; Forward declaration of sections.
        SECTION CSTACK:DATA:NOROOT(3)

        SECTION .intvec:CODE:NOROOT(2)

        EXTERN  __iar_program_start
        EXTERN  SystemInit
        PUBLIC  __vector_table
        PUBLIC  __vector_table_0x1c
        PUBLIC  __Vectors
        PUBLIC  __Vectors_End
        PUBLIC  __Vectors_Size

        DATA

__vector_table
        DCD     sfe(CSTACK)
        DCD     Reset_Handler

        DCD     NMI_Handler
        DCD     HardFault_Handler
        DCD     MemManage_Handler
        DCD     BusFault_Handler
        DCD     UsageFault_Handler
__vector_table_0x1c
        DCD     0
        DCD     0
        DCD     0
        DCD     0
        DCD     SVC_Handler
        DCD     DebugMon_Handler
        DCD     0
        DCD     PendSV_Handler
        DCD     SysTick_Handler

        ; External Interrupts
        DCD     TIM0_IRQHandler                   ;  0
        DCD     TIM1_IRQHandler                   ;  1
        DCD     TIM2_IRQHandler                   ;  2
        DCD     TIM3_IRQHandler                   ;  3
        DCD     TIM4_IRQHandler                   ;  4
        DCD     TIM5_IRQHandler                   ;  5
        DCD     TIM6_IRQHandler                   ;  6
        DCD     TIM7_IRQHandler                   ;  7
        DCD     TMBA_IRQHandler                   ;  8
        DCD     TMBB_IRQHandler                   ;  9
        DCD     TMBC_IRQHandler                   ; 10
        DCD     EXTA_IRQHandler                   ; 11
        DCD     EXTB_IRQHandler                   ; 12
        DCD     EXTC_IRQHandler                   ; 13
        DCD     UART0_IRQHandler                  ; 14
        DCD     UART1_IRQHandler                  ; 15
        DCD     UART2_IRQHandler                  ; 16
        DCD     I2C0_IRQHandler                   ; 17
        DCD     I2C1_IRQHandler                   ; 18
        DCD     SPI0_IRQHandler                   ; 19
        DCD     SPI1_IRQHandler                   ; 20
        DCD     CANBUS_IRQHandler                 ; 21
        DCD     ALM_IRQHandler                    ; 22
        DCD     SCH_IRQHandler                    ; 23
        DCD     HMS_IRQHandler                    ; 24
        DCD     RTC_IRQHandler                    ; 25
        DCD     KEYCHANGE_IRQHandler              ; 26
        DCD     SRAADC_IRQHandler                 ; 27
        DCD     ADCF_IRQHandler                   ; 28
        DCD     DMA_IRQHandler                    ; 29
        DCD     AUDA_IRQHandler                   ; 30
        DCD     AUDB_IRQHandler                   ; 31
        DCD     I2STX0_IRQHandler                 ; 32
        DCD     I2STX1_IRQHandler                 ; 33
        DCD     I2STX2_IRQHandler                 ; 34
        DCD     I2STX3_IRQHandler                 ; 35
        DCD     I2SRX0_IRQHandler                 ; 36
        DCD     I2SRX1_IRQHandler                 ; 37
        DCD     I2SRX2_IRQHandler                 ; 38
        DCD     I2SRX3_IRQHandler                 ; 39
        DCD     USB20_IRQHandler                  ; 40
        DCD     OHCI_IRQHandler                   ; 41
        DCD     EHCI_IRQHandler                   ; 42
        DCD     CEC_IRQHandler                    ; 43
        DCD     SCALAR0_IRQHandler                ; 44
        DCD     SD0_IRQHandler                    ; 45
        DCD     SD1_IRQHandler                    ; 46
        DCD     NFC_IRQHandler                    ; 47
        DCD     BCH_IRQHandler                    ; 48
        DCD     SPUFIQ_IRQHandler                 ; 49
        DCD     SPUENV_IRQHandler                 ; 50
        DCD     SPUPW_IRQHandler                  ; 51
        DCD     SPUBEAT_IRQHandler                ; 52
        DCD     PPU_IRQHandler                    ; 53
        DCD     MIPI_IRQHandler                   ; 54
        DCD     MIPI_B_IRQHandler                 ; 55
        DCD     CSI_IRQHandler                    ; 56
        DCD     CDSP_IRQHandler                   ; 57
        DCD     JPEG_IRQHandler                   ; 58
        DCD     CONV420TO422_IRQHandler           ; 59
        DCD     HDMI_IRQHandler                   ; 60
        DCD     FIR_IRQHandler                    ; 61
        DCD     PSCALER0_IRQHandler               ; 62
        DCD     PSCALER1_IRQHandler               ; 63
        DCD     MB2SCAN_IRQHandler                ; 64
        DCD     ROTATOR_IRQHandler                ; 65
        DCD     FFT_IRQHandler                    ; 66
        DCD     H264_IRQHandler                   ; 67
        
        
__Vectors_End

__Vectors       EQU   __vector_table
__Vectors_Size  EQU   __Vectors_End - __Vectors


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Default interrupt handlers.
;;
        THUMB

        PUBWEAK Reset_Handler
        SECTION .text:CODE:REORDER:NOROOT(2)
Reset_Handler
        LDR     r0, =__Vectors
        LDR     r1, =0xE000ED08
        STR     r0, [r1]
        LDR     R0, =SystemInit
        BLX     R0
        LDR     R0, =__iar_program_start
        BX      R0

        PUBWEAK NMI_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
NMI_Handler
        B NMI_Handler

        PUBWEAK HardFault_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
HardFault_Handler
        B HardFault_Handler

        PUBWEAK MemManage_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
MemManage_Handler
        B MemManage_Handler

        PUBWEAK BusFault_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
BusFault_Handler
        B BusFault_Handler

        PUBWEAK UsageFault_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
UsageFault_Handler
        B UsageFault_Handler

        PUBWEAK SVC_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
SVC_Handler
        B SVC_Handler

        PUBWEAK DebugMon_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
DebugMon_Handler
        B DebugMon_Handler

        PUBWEAK PendSV_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
PendSV_Handler
        B PendSV_Handler

        PUBWEAK SysTick_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
SysTick_Handler
        B SysTick_Handler

        PUBWEAK TIM0_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
TIM0_IRQHandler
        B TIM0_IRQHandler

        PUBWEAK TIM1_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
TIM1_IRQHandler
        B TIM1_IRQHandler

        PUBWEAK TIM2_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
TIM2_IRQHandler
        B TIM2_IRQHandler

        PUBWEAK TIM3_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
TIM3_IRQHandler
        B TIM3_IRQHandler

        PUBWEAK TIM4_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
TIM4_IRQHandler
        B TIM4_IRQHandler

        PUBWEAK TIM5_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
TIM5_IRQHandler
        B TIM5_IRQHandler

        PUBWEAK TIM6_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
TIM6_IRQHandler
        B TIM6_IRQHandler
	
        PUBWEAK TIM7_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
TIM7_IRQHandler
        B TIM7_IRQHandler	

        PUBWEAK TMBA_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
TMBA_IRQHandler
        B TMBA_IRQHandler

        PUBWEAK TMBB_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
TMBB_IRQHandler
        B TMBB_IRQHandler

        PUBWEAK TMBC_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
TMBC_IRQHandler
        B TMBC_IRQHandler		
	
        PUBWEAK EXTA_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
EXTA_IRQHandler
        B EXTA_IRQHandler

        PUBWEAK EXTB_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
EXTB_IRQHandler
        B EXTB_IRQHandler

        PUBWEAK EXTC_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
EXTC_IRQHandler
        B EXTC_IRQHandler

        PUBWEAK UART0_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
UART0_IRQHandler
        B UART0_IRQHandler

        PUBWEAK UART1_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
UART1_IRQHandler
        B UART1_IRQHandler

        PUBWEAK UART2_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
UART2_IRQHandler
        B UART2_IRQHandler		
		
        PUBWEAK I2C0_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
I2C0_IRQHandler
        B I2C0_IRQHandler	

        PUBWEAK I2C1_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
I2C1_IRQHandler
        B I2C1_IRQHandler	

        PUBWEAK SPI0_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
SPI0_IRQHandler
        B SPI0_IRQHandler	

        PUBWEAK SPI1_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
SPI1_IRQHandler
        B SPI1_IRQHandler	

        PUBWEAK CANBUS_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
CANBUS_IRQHandler
        B CANBUS_IRQHandler	

        PUBWEAK ALM_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
ALM_IRQHandler
        B ALM_IRQHandler	

        PUBWEAK SCH_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
SCH_IRQHandler
        B SCH_IRQHandler	

        PUBWEAK HMS_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
HMS_IRQHandler
        B HMS_IRQHandler	

        PUBWEAK RTC_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
RTC_IRQHandler
        B RTC_IRQHandler	

        PUBWEAK KEYCHANGE_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
KEYCHANGE_IRQHandler
        B KEYCHANGE_IRQHandler	

        PUBWEAK SRAADC_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
SRAADC_IRQHandler
        B SRAADC_IRQHandler	

        PUBWEAK ADCF_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
ADCF_IRQHandler
        B ADCF_IRQHandler	

        PUBWEAK DMA_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
DMA_IRQHandler
        B DMA_IRQHandler	

        PUBWEAK AUDA_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
AUDA_IRQHandler
        B AUDA_IRQHandler	

        PUBWEAK AUDB_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
AUDB_IRQHandler
        B AUDB_IRQHandler	

        PUBWEAK I2STX0_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
I2STX0_IRQHandler
        B I2STX0_IRQHandler	

        PUBWEAK I2STX1_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
I2STX1_IRQHandler
        B I2STX1_IRQHandler	

        PUBWEAK I2STX2_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
I2STX2_IRQHandler
        B I2STX2_IRQHandler	
		
        PUBWEAK I2STX3_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
I2STX3_IRQHandler
        B I2STX3_IRQHandler	

        PUBWEAK I2SRX0_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
I2SRX0_IRQHandler
        B I2SRX0_IRQHandler			
		
        PUBWEAK I2SRX1_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
I2SRX1_IRQHandler
        B I2SRX1_IRQHandler	

        PUBWEAK I2SRX2_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
I2SRX2_IRQHandler
        B I2SRX2_IRQHandler	

        PUBWEAK I2SRX3_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
I2SRX3_IRQHandler
        B I2SRX3_IRQHandler	

        PUBWEAK USB20_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
USB20_IRQHandler
        B USB20_IRQHandler	

        PUBWEAK OHCI_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
OHCI_IRQHandler
        B OHCI_IRQHandler	

        PUBWEAK EHCI_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
EHCI_IRQHandler
        B EHCI_IRQHandler	
		
        PUBWEAK CEC_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
CEC_IRQHandler
        B CEC_IRQHandler	

        PUBWEAK SCALAR0_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
SCALAR0_IRQHandler
        B SCALAR0_IRQHandler	

        PUBWEAK SD0_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
SD0_IRQHandler
        B SD0_IRQHandler	

        PUBWEAK SD1_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
SD1_IRQHandler
        B SD1_IRQHandler	

        PUBWEAK NFC_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
NFC_IRQHandler
        B NFC_IRQHandler	

        PUBWEAK BCH_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
BCH_IRQHandler
        B BCH_IRQHandler			
		
        PUBWEAK SPUFIQ_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
SPUFIQ_IRQHandler
        B SPUFIQ_IRQHandler

        PUBWEAK SPUENV_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
SPUENV_IRQHandler
        B SPUENV_IRQHandler

        PUBWEAK SPUPW_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
SPUPW_IRQHandler
        B SPUPW_IRQHandler

        PUBWEAK SPUBEAT_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
SPUBEAT_IRQHandler
        B SPUBEAT_IRQHandler

        PUBWEAK PPU_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
PPU_IRQHandler
        B PPU_IRQHandler

        PUBWEAK MIPI_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
MIPI_IRQHandler
        B MIPI_IRQHandler

        PUBWEAK MIPI_B_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
MIPI_B_IRQHandler
        B MIPI_B_IRQHandler

        PUBWEAK CSI_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
CSI_IRQHandler
        B CSI_IRQHandler

        PUBWEAK CDSP_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
CDSP_IRQHandler
        B CDSP_IRQHandler

        PUBWEAK JPEG_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
JPEG_IRQHandler
        B JPEG_IRQHandler

        PUBWEAK CONV420TO422_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
CONV420TO422_IRQHandler
        B CONV420TO422_IRQHandler	

        PUBWEAK HDMI_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
HDMI_IRQHandler
        B HDMI_IRQHandler

        PUBWEAK FIR_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
FIR_IRQHandler
        B FIR_IRQHandler		

        PUBWEAK PSCALER0_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
PSCALER0_IRQHandler
        B PSCALER0_IRQHandler		

        PUBWEAK PSCALER1_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
PSCALER1_IRQHandler
        B PSCALER1_IRQHandler	

        PUBWEAK MB2SCAN_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
MB2SCAN_IRQHandler
        B MB2SCAN_IRQHandler	
        
        PUBWEAK ROTATOR_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
ROTATOR_IRQHandler
        B ROTATOR_IRQHandler
        
        PUBWEAK FFT_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
FFT_IRQHandler
        B FFT_IRQHandler
        
        PUBWEAK MIPIB_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
MIPIB_IRQHandler
        B MIPIB_IRQHandler
        
        PUBWEAK H264_IRQHandler
        SECTION .text:CODE:REORDER:NOROOT(1)
H264_IRQHandler
        B H264_IRQHandler
        
        END

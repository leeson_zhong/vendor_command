;/**************************************************************************//**
; * @file     startup_ARMCM4.s
; * @brief    CMSIS Core Device Startup File for
; *           ARMCM4 Device Series
; * @version  V1.08
; * @date     23. November 2012
; *
; * @note
; *
; ******************************************************************************/
;/* Copyright (c) 2011 - 2012 ARM LIMITED
;
;   All rights reserved.
;   Redistribution and use in source and binary forms, with or without
;   modification, are permitted provided that the following conditions are met:
;   - Redistributions of source code must retain the above copyright
;     notice, this list of conditions and the following disclaimer.
;   - Redistributions in binary form must reproduce the above copyright
;     notice, this list of conditions and the following disclaimer in the
;     documentation and/or other materials provided with the distribution.
;   - Neither the name of ARM nor the names of its contributors may be used
;     to endorse or promote products derived from this software without
;     specific prior written permission.
;   *
;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;   ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDERS AND CONTRIBUTORS BE
;   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
;   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;   POSSIBILITY OF SUCH DAMAGE.
;   ---------------------------------------------------------------------------*/
;/*
;//-------- <<< Use Configuration Wizard in Context Menu >>> ------------------
;*/


; <h> Stack Configuration
;   <o> Stack Size (in Bytes) <0x0-0xFFFFFFFF:8>
; </h>

Stack_Size      EQU     0x00020000

                AREA    STACK, NOINIT, READWRITE, ALIGN=3
Stack_Mem       SPACE   Stack_Size
__initial_sp


; <h> Heap Configuration
;   <o>  Heap Size (in Bytes) <0x0-0xFFFFFFFF:8>
; </h>

Heap_Size       EQU     0x00100000

                AREA    HEAP, NOINIT, READWRITE, ALIGN=3
__heap_base
Heap_Mem        SPACE   Heap_Size
__heap_limit


                PRESERVE8
                THUMB


; Vector Table Mapped to Address 0 at Reset

                AREA    RESET, DATA, READONLY
                EXPORT  __Vectors
                EXPORT  __Vectors_End
                EXPORT  __Vectors_Size

__Vectors       DCD     __initial_sp              ; Top of Stack
                DCD     Reset_Handler             ; Reset Handler
                DCD     NMI_Handler               ; NMI Handler
                DCD     HardFault_Handler         ; Hard Fault Handler
                DCD     MemManage_Handler         ; MPU Fault Handler
                DCD     BusFault_Handler          ; Bus Fault Handler
                DCD     UsageFault_Handler        ; Usage Fault Handler
                DCD     0                         ; Reserved
                DCD     0                         ; Reserved
                DCD     0                         ; Reserved
                DCD     0                         ; Reserved
                DCD     SVC_Handler               ; SVCall Handler
                DCD     DebugMon_Handler          ; Debug Monitor Handler
                DCD     0                         ; Reserved
                DCD     PendSV_Handler            ; PendSV Handler
                DCD     SysTick_Handler           ; SysTick Handler

                ; External Interrupts
                DCD     TIM0_IRQHandler           ;  0
                DCD     TIM1_IRQHandler           ;  1
                DCD     TIM2_IRQHandler           ;  2
                DCD     TIM3_IRQHandler           ;  3
                DCD     TIM4_IRQHandler           ;  4
                DCD     TIM5_IRQHandler           ;  5
                DCD     TIM6_IRQHandler           ;  6
                DCD     TIM7_IRQHandler           ;  7
                DCD     TMBA_IRQHandler           ;  8
                DCD     TMBB_IRQHandler           ;  9
                DCD     TMBC_IRQHandler           ; 10
                DCD     EXTA_IRQHandler           ; 11
                DCD     EXTB_IRQHandler           ; 12
                DCD     EXTC_IRQHandler           ; 13
                DCD     UART0_IRQHandler          ; 14
                DCD     UART1_IRQHandler          ; 15
                DCD     UART2_IRQHandler          ; 16
                DCD     I2C0_IRQHandler           ; 17
                DCD     I2C1_IRQHandler           ; 18
                DCD     SPI0_IRQHandler           ; 19
                DCD     SPI1_IRQHandler           ; 20
                DCD     CANBUS_IRQHandler         ; 21
                DCD     ALM_IRQHandler            ; 22
                DCD     SCH_IRQHandler            ; 23
                DCD     HMS_IRQHandler            ; 24
                DCD     RTC_IRQHandler            ; 25
                DCD     KEYCHANGE_IRQHandler      ; 26
                DCD     SRAADC_IRQHandler         ; 27
                DCD     ADCF_IRQHandler           ; 28
                DCD     DMA_IRQHandler            ; 29
                DCD     AUDA_IRQHandler           ; 30
                DCD     AUDB_IRQHandler           ; 31
                DCD     I2STX0_IRQHandler         ; 32
                DCD     I2STX1_IRQHandler         ; 33
                DCD     I2STX2_IRQHandler         ; 34
                DCD     I2STX3_IRQHandler         ; 35
                DCD     I2SRX0_IRQHandler         ; 36
                DCD     I2SRX1_IRQHandler         ; 37
                DCD     I2SRX2_IRQHandler         ; 38
                DCD     I2SRX3_IRQHandler         ; 39
                DCD     USB20_IRQHandler          ; 40
                DCD     OHCI_IRQHandler           ; 41
                DCD     EHCI_IRQHandler           ; 42
                DCD     CEC_IRQHandler            ; 43
                DCD     SCALAR0_IRQHandler        ; 44
                DCD     SCALAR1_IRQHandler        ; 45
                DCD     SD0_IRQHandler            ; 46
                DCD     SD1_IRQHandler            ; 47
                DCD     NFC_IRQHandler            ; 48
                DCD     BCH_IRQHandler            ; 49
                DCD     SPUFIQ_IRQHandler         ; 50
                DCD     SPUENV_IRQHandler         ; 51
                DCD     SPUPW_IRQHandler          ; 52
                DCD     SPUBEAT_IRQHandler        ; 53
                DCD     PPU_IRQHandler            ; 54
                DCD     MIPI_IRQHandler           ; 55
                DCD     CSI_IRQHandler            ; 56
                DCD     CDSP_IRQHandler           ; 57
                DCD     JPEG_IRQHandler           ; 58
                DCD     CONV420TO422_IRQHandler   ; 59
                DCD     CSI420TO422_IRQHandler    ; 60
                DCD     HDMI_IRQHandler           ; 61
                DCD     FIR_IRQHandler            ; 62
__Vectors_End

__Vectors_Size  EQU     __Vectors_End - __Vectors

                AREA    |.text|, CODE, READONLY


; Reset Handler

Reset_Handler   PROC
                EXPORT  Reset_Handler             [WEAK]
                IMPORT  SystemInit
                IMPORT  __main
				LDR     R0, =SystemInit
                BLX     R0
                LDR     R0, =__main
				BX      R0
                ENDP


; Dummy Exception Handlers (infinite loops which can be modified)

NMI_Handler     PROC
                EXPORT  NMI_Handler               [WEAK]
                B       .
                ENDP
HardFault_Handler\
                PROC
                EXPORT  HardFault_Handler         [WEAK]
                B       .
                ENDP
MemManage_Handler\
                PROC
                EXPORT  MemManage_Handler         [WEAK]
                B       .
                ENDP
BusFault_Handler\
                PROC
                EXPORT  BusFault_Handler          [WEAK]
                B       .
                ENDP
UsageFault_Handler\
                PROC
                EXPORT  UsageFault_Handler        [WEAK]
                B       .
                ENDP
SVC_Handler     PROC
                EXPORT  SVC_Handler               [WEAK]
                B       .
                ENDP
DebugMon_Handler\
                PROC
                EXPORT  DebugMon_Handler          [WEAK]
                B       .
                ENDP
PendSV_Handler  PROC
                EXPORT  PendSV_Handler            [WEAK]
                B       .
                ENDP
SysTick_Handler PROC
                EXPORT  SysTick_Handler           [WEAK]
                B       .
                ENDP

Default_Handler PROC
				
				EXPORT  TIM0_IRQHandler           [WEAK]
                EXPORT  TIM1_IRQHandler           [WEAK]
                EXPORT  TIM2_IRQHandler           [WEAK]
                EXPORT  TIM3_IRQHandler           [WEAK]
                EXPORT  TIM4_IRQHandler           [WEAK]
                EXPORT  TIM5_IRQHandler           [WEAK]
                EXPORT  TIM6_IRQHandler           [WEAK]
                EXPORT  TIM7_IRQHandler           [WEAK]
                EXPORT  TMBA_IRQHandler           [WEAK]
                EXPORT  TMBB_IRQHandler           [WEAK]
                EXPORT  TMBC_IRQHandler           [WEAK]
                EXPORT  EXTA_IRQHandler           [WEAK]
                EXPORT  EXTB_IRQHandler           [WEAK]
                EXPORT  EXTC_IRQHandler           [WEAK]
                EXPORT  UART0_IRQHandler          [WEAK]
                EXPORT  UART1_IRQHandler          [WEAK]
                EXPORT  UART2_IRQHandler          [WEAK]
                EXPORT  I2C0_IRQHandler           [WEAK]
                EXPORT  I2C1_IRQHandler           [WEAK]
                EXPORT  SPI0_IRQHandler           [WEAK]
                EXPORT  SPI1_IRQHandler           [WEAK]
                EXPORT  CANBUS_IRQHandler         [WEAK]
                EXPORT  ALM_IRQHandler            [WEAK]
                EXPORT  SCH_IRQHandler            [WEAK]
                EXPORT  HMS_IRQHandler            [WEAK]
                EXPORT  RTC_IRQHandler            [WEAK]
                EXPORT  KEYCHANGE_IRQHandler      [WEAK]
                EXPORT  SRAADC_IRQHandler         [WEAK]
                EXPORT  ADCF_IRQHandler           [WEAK]
                EXPORT  DMA_IRQHandler            [WEAK]
                EXPORT  AUDA_IRQHandler           [WEAK]
                EXPORT  AUDB_IRQHandler           [WEAK]
                EXPORT  I2STX0_IRQHandler         [WEAK]
                EXPORT  I2STX1_IRQHandler         [WEAK]
                EXPORT  I2STX2_IRQHandler         [WEAK]
                EXPORT  I2STX3_IRQHandler         [WEAK]
                EXPORT  I2SRX0_IRQHandler         [WEAK]
                EXPORT  I2SRX1_IRQHandler         [WEAK]
                EXPORT  I2SRX2_IRQHandler         [WEAK]
                EXPORT  I2SRX3_IRQHandler         [WEAK]
                EXPORT  USB20_IRQHandler          [WEAK]
                EXPORT  OHCI_IRQHandler           [WEAK]
                EXPORT  EHCI_IRQHandler           [WEAK]
                EXPORT  CEC_IRQHandler            [WEAK]
                EXPORT  SCALAR0_IRQHandler        [WEAK]
                EXPORT  SCALAR1_IRQHandler        [WEAK]
                EXPORT  SD0_IRQHandler            [WEAK]
                EXPORT  SD1_IRQHandler            [WEAK]
                EXPORT  NFC_IRQHandler            [WEAK]
                EXPORT  BCH_IRQHandler            [WEAK]
                EXPORT  SPUFIQ_IRQHandler         [WEAK]
                EXPORT  SPUENV_IRQHandler         [WEAK]
                EXPORT  SPUPW_IRQHandler          [WEAK]
                EXPORT  SPUBEAT_IRQHandler        [WEAK]
                EXPORT  PPU_IRQHandler            [WEAK]
                EXPORT  MIPI_IRQHandler           [WEAK]
                EXPORT  CSI_IRQHandler            [WEAK]
                EXPORT  CDSP_IRQHandler           [WEAK]
                EXPORT  JPEG_IRQHandler           [WEAK]
                EXPORT  CONV420TO422_IRQHandler   [WEAK]
                EXPORT  CSI420TO422_IRQHandler    [WEAK]
                EXPORT  HDMI_IRQHandler           [WEAK]
                EXPORT  FIR_IRQHandler            [WEAK]
							
TIM0_IRQHandler
TIM1_IRQHandler
TIM2_IRQHandler
TIM3_IRQHandler
TIM4_IRQHandler
TIM5_IRQHandler
TIM6_IRQHandler
TIM7_IRQHandler
TMBA_IRQHandler
TMBB_IRQHandler
TMBC_IRQHandler
EXTA_IRQHandler
EXTB_IRQHandler
EXTC_IRQHandler
UART0_IRQHandler
UART1_IRQHandler
UART2_IRQHandler
I2C0_IRQHandler
I2C1_IRQHandler
SPI0_IRQHandler
SPI1_IRQHandler
CANBUS_IRQHandler
ALM_IRQHandler
SCH_IRQHandler
HMS_IRQHandler
RTC_IRQHandler
KEYCHANGE_IRQHandler
SRAADC_IRQHandler
ADCF_IRQHandler
DMA_IRQHandler
AUDA_IRQHandler
AUDB_IRQHandler
I2STX0_IRQHandler
I2STX1_IRQHandler
I2STX2_IRQHandler
I2STX3_IRQHandler
I2SRX0_IRQHandler
I2SRX1_IRQHandler
I2SRX2_IRQHandler
I2SRX3_IRQHandler
USB20_IRQHandler
OHCI_IRQHandler
EHCI_IRQHandler
CEC_IRQHandler
SCALAR0_IRQHandler
SCALAR1_IRQHandler
SD0_IRQHandler
SD1_IRQHandler
NFC_IRQHandler
BCH_IRQHandler
SPUFIQ_IRQHandler
SPUENV_IRQHandler
SPUPW_IRQHandler
SPUBEAT_IRQHandler
PPU_IRQHandler
MIPI_IRQHandler
CSI_IRQHandler
CDSP_IRQHandler
JPEG_IRQHandler
CONV420TO422_IRQHandler
CSI420TO422_IRQHandler
HDMI_IRQHandler
FIR_IRQHandler				
                B       .

                ENDP


                ALIGN


; User Initial Stack & Heap

                IF      :DEF:__MICROLIB

                EXPORT  __initial_sp
                EXPORT  __heap_base
                EXPORT  __heap_limit

                ELSE

                IMPORT  __use_two_region_memory
                EXPORT  __user_initial_stackheap

__user_initial_stackheap PROC
                LDR     R0, =  Heap_Mem
                LDR     R1, =(Stack_Mem + Stack_Size)
                LDR     R2, = (Heap_Mem +  Heap_Size)
                LDR     R3, = Stack_Mem
                BX      LR
                ENDP

                ALIGN

                ENDIF


                END

/**************************************************************************//**
 * @file     ARMCM4_FP.h
 * @brief    CMSIS Core Peripheral Access Layer Header File for
 *           ARMCM4 Device Series
 * @version  V2.02
 * @date     10. September 2014
 *
 * @note     configured for CM4 with FPU
 *
 ******************************************************************************/
/* Copyright (c) 2011 - 2014 ARM LIMITED

   All rights reserved.
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:
   - Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   - Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
   - Neither the name of ARM nor the names of its contributors may be used
     to endorse or promote products derived from this software without
     specific prior written permission.
   *
   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDERS AND CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
   ---------------------------------------------------------------------------*/


#ifndef ARMCM4_FP_H
#define ARMCM4_FP_H

#ifdef __cplusplus
extern "C" {
#endif


/* -------------------------  Interrupt Number Definition  ------------------------ */

typedef enum IRQn
{
/* -------------------  Cortex-M4 Processor Exceptions Numbers  ------------------- */
  NonMaskableInt_IRQn           = -14,      /*!<  2 Non Maskable Interrupt          */
  HardFault_IRQn                = -13,      /*!<  3 HardFault Interrupt             */
  MemoryManagement_IRQn         = -12,      /*!<  4 Memory Management Interrupt     */
  BusFault_IRQn                 = -11,      /*!<  5 Bus Fault Interrupt             */
  UsageFault_IRQn               = -10,      /*!<  6 Usage Fault Interrupt           */
  SVCall_IRQn                   =  -5,      /*!< 11 SV Call Interrupt               */
  DebugMonitor_IRQn             =  -4,      /*!< 12 Debug Monitor Interrupt         */
  PendSV_IRQn                   =  -2,      /*!< 14 Pend SV Interrupt               */
  SysTick_IRQn                  =  -1,      /*!< 15 System Tick Interrupt           */

/* ----------------------  ARMCM4 Specific Interrupt Numbers  --------------------- */
  TIM0_IRQn                     =   0,      /*!< Timer0 Interrupt                   */
  TIM1_IRQn                     =   1,      /*!< Timer1 Interrupt                   */
  TIM2_IRQn                     =   2,      /*!< Timer2 Interrupt                   */
  TIM3_IRQn                     =   3,      /*!< Timer3 Interrupt                   */
  TIM4_IRQn                     =   4,      /*!< Timer4 Interrupt                   */
  TIM5_IRQn                     =   5,      /*!< Timer5 Interrupt                   */
  TIM6_IRQn                     =   6,      /*!< Timer6 Interrupt                   */
  TIM7_IRQn                     =   7,      /*!< Timer7 Interrupt                   */
  TMBA_IRQn                     =   8,      /*!< Time Base A Interrupt              */
  TMBB_IRQn                     =   9,      /*!< Time Base B Interrupt              */
  TMBC_IRQn                     =  10,      /*!< Time Base C Interrupt              */
  EXTA_IRQn                     =  11,      /*!< External A Interrupt               */
  EXTB_IRQn                     =  12,      /*!< External B Interrupt               */
  EXTC_IRQn                     =  13,      /*!< External C Interrupt               */
  UART0_IRQn                    =  14,      /*!< UART0 Interrupt                    */
  UART1_IRQn                    =  15,      /*!< UART1 Interrupt                    */
  UART2_IRQn                    =  16,      /*!< UART2 Interrupt                    */
  I2C0_IRQn                     =  17,      /*!< I2C0 Interrupt                     */
  I2C1_IRQn                     =  18,      /*!< I2C1 Interrupt                     */
  SPI0_IRQn                     =  19,      /*!< SPI0 Interrupt                     */
  SPI1_IRQn                     =  20,      /*!< SPI1 Interrupt                     */
  CANBUS_IRQn                   =  21,      /*!< CANBUS Interrupt                   */
  ALM_IRQn                      =  22,      /*!< Alarm Interrupt                    */
  SCH_IRQn                      =  23,      /*!< Schedule Interrupt                 */
  HMS_IRQn                      =  24,      /*!< HMS Interrupt                      */
  RTC_IRQn                      =  25,      /*!< RTC Interrupt                      */
  KEYCHANGE_IRQn                =  26,      /*!< Key Change Interrupt               */
  SARADC_IRQn                   =  27,      /*!< SAR ADC Interrupt                  */
  ADCF_IRQn                     =  28,      /*!< ADCF Interrupt                     */
  DMA_IRQn                      =  29,      /*!< DMA Interrupt                      */
  AUDA_IRQn                     =  30,      /*!< AudioA Interrupt                   */
  AUDB_IRQn                     =  31,      /*!< AudioB Interrupt                   */
  I2STX0_IRQn                   =  32,      /*!< I2S TX0 Interrupt                  */
  I2STX1_IRQn                   =  33,      /*!< I2S TX1 Interrupt                  */
  I2STX2_IRQn                   =  34,      /*!< I2S TX2 Interrupt                  */
  I2STX3_IRQn                   =  35,      /*!< I2S TX3 Interrupt                  */
  I2SRX0_IRQn                   =  36,      /*!< I2S RX0 Interrupt                  */
  I2SRX1_IRQn                   =  37,      /*!< I2S RX1 Interrupt                  */
  I2SRX2_IRQn                   =  38,      /*!< I2S RX2 Interrupt                  */
  I2SRX3_IRQn                   =  39,      /*!< I2S RX3 Interrupt                  */
  USB20_IRQn                    =  40,      /*!< USB 2.0 Interrupt                  */
  OHCI_IRQn                     =  41,      /*!< OHCI Interrupt                     */
  EHCI_IRQn                     =  42,      /*!< EHCI Interrupt                     */
  CEC_IRQn                      =  43,      /*!< CEC Interrupt                      */
  SCALAR0_IRQn                  =  44,      /*!< Scalar0 Interrupt                  */
  SD0_IRQn                      =  45,      /*!< SD0 Interrupt                      */
  SD1_IRQn                      =  46,      /*!< SD1 Interrupt                      */
  NFC_IRQn                      =  47,      /*!< NFC Interrupt                      */
  BCH_IRQn                      =  48,      /*!< BCH Interrupt                      */
  SPUFIQ_IRQn                   =  49,      /*!< SPU FIQ Interrupt                  */
  SPUENV_IRQn                   =  50,      /*!< SPU ENV Interrupt                  */
  SPUPW_IRQn                    =  51,      /*!< SPU PW Interrupt                   */
  SPUBEAT_IRQn                  =  52,      /*!< SPU Beat Interrupt                 */
  PPU_IRQn                      =  53,      /*!< PPU Interrupt                      */
  MIPI_IRQn                     =  54,      /*!< MIPI Interrupt                     */
  MIPI_B_IRQn                   =  55,      /*!< MIPI B Interrupt                     */
  CSI_IRQn                      =  56,      /*!< CSI Interrupt                      */
  CDSP_IRQn                     =  57,      /*!< CDSP Interrupt                     */
  JPEG_IRQn                     =  58,      /*!< JPEG Interrupt                     */
  CONV420TO422_IRQn             =  59,      /*!< CONV 420 to 422 Interrupt          */
  HDMI_IRQn                     =  60,      /*!< HDMI Interrupt                     */
  FIR_IRQn                      =  61,      /*!< FIR Interrupt                      */
  PSCALER_A_IRQn                =  62,      /*!< PIPE-SCALER A Interrupt            */
  PSCALER_B_IRQn                =  63,      /*!< PIPE-SCALER B Interrupt            */
  MB2SCAN_IRQn                  =  64,      /*!< Macro Block to Scan Line Interrupt            */
  ROTATOR_IRQn                  =  65,      /*!< ROTATOR Interrupt                  */
  FFT_IRQn                      =  66,      /*!< FFT Interrupt                      */
  H264_IRQn                     =  67,      /*!< H264 Interrupt                     */
} IRQn_Type;


/* ================================================================================ */
/* ================      Processor and Core Peripheral Section     ================ */
/* ================================================================================ */

/* --------  Configuration of the Cortex-M4 Processor and Core Peripherals  ------- */
#define __CM4_REV                 0x0001    /*!< Core revision r0p1                              */
#define __MPU_PRESENT             1         /*!< MPU present or not                              */
#define __NVIC_PRIO_BITS          3         /*!< Number of Bits used for Priority Levels         */
#define __Vendor_SysTickConfig    0         /*!< Set to 1 if different SysTick Config is used    */
#define __FPU_PRESENT             1         /*!< FPU present                                     */

#include "core_cm4.h"                       /* Processor and core peripherals                    */
#include "system_ARMCM4.h"                  /* System Header                                     */


/* ================================================================================ */
/* ================       Device Specific Peripheral Section       ================ */
/* ================================================================================ */

/* -------------------  Start of section using anonymous unions  ------------------ */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif



/* ================================================================================ */
/* ================            CPU FPGA System (CPU_SYS)           ================ */
/* ================================================================================ */
typedef struct
{
  __I  uint32_t ID;               /* Offset: 0x000 (R/ )  Board and FPGA Identifier */
  __IO uint32_t MEMCFG;           /* Offset: 0x004 (R/W)  Remap and Alias Memory Control */
  __I  uint32_t SW;               /* Offset: 0x008 (R/ )  Switch States */
  __IO uint32_t LED;              /* Offset: 0x00C (R/W)  LED Output States */
  __I  uint32_t TS;               /* Offset: 0x010 (R/ )  Touchscreen Register */
  __IO uint32_t CTRL1;            /* Offset: 0x014 (R/W)  Misc Control Functions */
       uint32_t RESERVED0[2];
  __IO uint32_t CLKCFG;           /* Offset: 0x020 (R/W)  System Clock Configuration */
  __IO uint32_t WSCFG;            /* Offset: 0x024 (R/W)  Flash Waitstate Configuration */
  __IO uint32_t CPUCFG;           /* Offset: 0x028 (R/W)  Processor Configuration */
       uint32_t RESERVED1[3];
  __IO uint32_t BASE;             /* Offset: 0x038 (R/W)  ROM Table base Address */
  __IO uint32_t ID2;              /* Offset: 0x03C (R/W)  Secondary Identification Register */
} ARM_CPU_SYS_TypeDef;


/* ================================================================================ */
/* ================            DUT FPGA System (DUT_SYS)           ================ */
/* ================================================================================ */
typedef struct
{
  __I  uint32_t ID;               /* Offset: 0x000 (R/ )  Board and FPGA Identifier */
  __IO uint32_t PERCFG;           /* Offset: 0x004 (R/W)  Peripheral Control Signals */
  __I  uint32_t SW;               /* Offset: 0x008 (R/ )  Switch States */
  __IO uint32_t LED;              /* Offset: 0x00C (R/W)  LED Output States */
  __IO uint32_t SEG7;             /* Offset: 0x010 (R/W)  7-segment LED Output States */
  __I  uint32_t CNT25MHz;         /* Offset: 0x014 (R/ )  Freerunning counter incrementing at 25MHz */
  __I  uint32_t CNT100Hz;         /* Offset: 0x018 (R/ )  Freerunning counter incrementing at 100Hz */
} ARM_DUT_SYS_TypeDef;


/* ================================================================================ */
/* ================                   Timer (TIM)                  ================ */
/* ================================================================================ */
typedef struct
{
  __IO uint32_t Timer1Load;       /* Offset: 0x000 (R/W)  Timer 1 Load */
  __I  uint32_t Timer1Value;      /* Offset: 0x004 (R/ )  Timer 1 Counter Current Value */
  __IO uint32_t Timer1Control;    /* Offset: 0x008 (R/W)  Timer 1 Control */
  __O  uint32_t Timer1IntClr;     /* Offset: 0x00C ( /W)  Timer 1 Interrupt Clear */
  __I  uint32_t Timer1RIS;        /* Offset: 0x010 (R/ )  Timer 1 Raw Interrupt Status */
  __I  uint32_t Timer1MIS;        /* Offset: 0x014 (R/ )  Timer 1 Masked Interrupt Status */
  __IO uint32_t Timer1BGLoad;     /* Offset: 0x018 (R/W)  Background Load Register */
       uint32_t RESERVED0[1];
  __IO uint32_t Timer2Load;       /* Offset: 0x020 (R/W)  Timer 2 Load */
  __I  uint32_t Timer2Value;      /* Offset: 0x024 (R/ )  Timer 2 Counter Current Value */
  __IO uint32_t Timer2Control;    /* Offset: 0x028 (R/W)  Timer 2 Control */
  __O  uint32_t Timer2IntClr;     /* Offset: 0x02C ( /W)  Timer 2 Interrupt Clear */
  __I  uint32_t Timer2RIS;        /* Offset: 0x030 (R/ )  Timer 2 Raw Interrupt Status */
  __I  uint32_t Timer2MIS;        /* Offset: 0x034 (R/ )  Timer 2 Masked Interrupt Status */
  __IO uint32_t Timer2BGLoad;     /* Offset: 0x038 (R/W)  Background Load Register */
} ARM_TIM_TypeDef;


/* ================================================================================ */
/* ============== Universal Asyncronous Receiver / Transmitter (UART) ============= */
/* ================================================================================ */
typedef struct
{
  __IO uint32_t DR;               /* Offset: 0x000 (R/W)  Data */
  union {
  __I  uint32_t RSR;              /* Offset: 0x000 (R/ )  Receive Status */
  __O  uint32_t ECR;              /* Offset: 0x000 ( /W)  Error Clear */
  };
       uint32_t RESERVED0[4];
  __IO uint32_t FR;               /* Offset: 0x018 (R/W)  Flags */
       uint32_t RESERVED1[1];
  __IO uint32_t ILPR;             /* Offset: 0x020 (R/W)  IrDA Low-power Counter */
  __IO uint32_t IBRD;             /* Offset: 0x024 (R/W)  Interger Baud Rate */
  __IO uint32_t FBRD;             /* Offset: 0x028 (R/W)  Fractional Baud Rate */
  __IO uint32_t LCR_H;            /* Offset: 0x02C (R/W)  Line Control */
  __IO uint32_t CR;               /* Offset: 0x030 (R/W)  Control */
  __IO uint32_t IFLS;             /* Offset: 0x034 (R/W)  Interrupt FIFO Level Select */
  __IO uint32_t IMSC;             /* Offset: 0x038 (R/W)  Interrupt Mask Set / Clear */
  __IO uint32_t RIS;              /* Offset: 0x03C (R/W)  Raw Interrupt Status */
  __IO uint32_t MIS;              /* Offset: 0x040 (R/W)  Masked Interrupt Status */
  __O  uint32_t ICR;              /* Offset: 0x044 ( /W)  Interrupt Clear */
  __IO uint32_t DMACR;            /* Offset: 0x048 (R/W)  DMA Control */
} ARM_UART_TypeDef;


/* --------------------  End of section using anonymous unions  ------------------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif




/* ================================================================================ */
/* ================              Peripheral memory map             ================ */
/* ================================================================================ */
/* --------------------------  CPU FPGA memory map  ------------------------------- */
#define ARM_FLASH_BASE            (0x00000000UL)
#define ARM_RAM_BASE              (0x20000000UL)
#define ARM_RAM_FPGA_BASE         (0x1EFF0000UL)
#define ARM_CPU_CFG_BASE          (0xDFFF0000UL)

#define ARM_CPU_SYS_BASE          (ARM_CPU_CFG_BASE  + 0x00000)
#define ARM_UART3_BASE            (ARM_CPU_CFG_BASE  + 0x05000)

/* --------------------------  DUT FPGA memory map  ------------------------------- */
#define ARM_APB_BASE              (0x40000000UL)
#define ARM_AHB_BASE              (0x4FF00000UL)
#define ARM_DMC_BASE              (0x60000000UL)
#define ARM_SMC_BASE              (0xA0000000UL)

#define ARM_TIM0_BASE             (ARM_APB_BASE      + 0x02000)
#define ARM_TIM2_BASE             (ARM_APB_BASE      + 0x03000)
#define ARM_DUT_SYS_BASE          (ARM_APB_BASE      + 0x04000)
#define ARM_UART0_BASE            (ARM_APB_BASE      + 0x06000)
#define ARM_UART1_BASE            (ARM_APB_BASE      + 0x07000)
#define ARM_UART2_BASE            (ARM_APB_BASE      + 0x08000)
#define ARM_UART4_BASE            (ARM_APB_BASE      + 0x09000)


/* ================================================================================ */
/* ================             Peripheral declaration             ================ */
/* ================================================================================ */
/* --------------------------  CPU FPGA Peripherals  ------------------------------ */
#define ARM_CPU_SYS               ((ARM_CPU_SYS_TypeDef *)  ARM_CPU_SYS_BASE)
#define ARM_UART3                 ((   ARM_UART_TypeDef *)    ARM_UART3_BASE)

/* --------------------------  DUT FPGA Peripherals  ------------------------------ */
#define ARM_DUT_SYS               ((ARM_DUT_SYS_TypeDef *)  ARM_DUT_SYS_BASE)
#define ARM_TIM0                  ((    ARM_TIM_TypeDef *)     ARM_TIM0_BASE)
#define ARM_TIM2                  ((    ARM_TIM_TypeDef *)     ARM_TIM2_BASE)
#define ARM_UART0                 ((   ARM_UART_TypeDef *)    ARM_UART0_BASE)
#define ARM_UART1                 ((   ARM_UART_TypeDef *)    ARM_UART1_BASE)
#define ARM_UART2                 ((   ARM_UART_TypeDef *)    ARM_UART2_BASE)
#define ARM_UART4                 ((   ARM_UART_TypeDef *)    ARM_UART4_BASE)


#ifdef __cplusplus
}
#endif

#endif  /* ARMCM4_FP_H */

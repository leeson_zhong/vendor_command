#ifndef __COLOR_TRACKER_AP_H__
#define __COLOR_TRACKER_AP_H__

/**************************************************************************/
// Color Tracker Header File
// v2100 (for sensor 0308)
/**************************************************************************/

#include "define.h"

#define MAXBALL_CH	                        (10)
#define MAX_CB_RESULT	                    (MAXBALL_CH*3)

typedef struct {
	unsigned char* y_array; // y domain of the captured image (YUV444)
	unsigned char* u_array; // u domain of the captured image (YUV444)
	unsigned char* v_array; // v domain of the captured image (YUV444)
	void* WorkMem; // the allocated memory and the assigned memory address for this func
	unsigned short MAX_RESULT; //buffer size
	unsigned short minWnd; // minimum searching window
	unsigned short maxWnd; // maximum searching window
} ColorTracker_Input;

typedef struct {
	unsigned char* y_array; // y domain of the captured image (YUV444)
	unsigned char* u_array; // u domain of the captured image (YUV444)
	unsigned char* v_array; // v domain of the captured image (YUV444)
	unsigned short MAX_RESULT_CH; // buffer size for each color
	int ballNum; // number of balls
	int scoreTh; // threshold of confidence score of color balls
	gpRect* ballRect; // positions of balls
} CBallDetect_Input;

/**
* @brief Get Color Tracker AP version
* @return source version
*/
const char *ColorTracker_GetVersion(void);

/**
* @brief Calculate needed memory
* @return needed memory size for color tracker
*/
int ColorTracker_MemCalc(void);

/**
* @brief Color tracking
* @param[in] ColorTracker_Input: input description of color tracker
* @param[out] objNum: detected numbers of three colors, maximum is 3
* @param[out] objRect: buffer for temporary storing roi info, outputs detected rois of three colors, | color1 color1 color1 | color2 color2 color2 | color3 color3 color3
*/
void ColorTracker(ColorTracker_Input* ctInput, int* objNum, gpRect* objRect);

/**
* @brief Color ball detection
* @param[in] CBallDetect_Input: input description of color ball
* @param[out] objNum: numbers of color balls
* @param[out] objRect: color balls positions
* @param[out] score: confidence scores of color balls
*/
void CBallDetect(CBallDetect_Input* cbInput, int* objNum, gpRect* objRect, int* score);

#endif

#ifndef __PFMST_H__
#define __PFMST_H__

#include "define.h"
//#include <string.h>
//#include <math.h>
//#include <stdio.h>

#define IMG_FMT_GRAY	0
#define IMG_FMT_YUYV	1
#define IMG_FMT_YCbYCr	2
#define IMG_FMT_RGB		3
#define IMG_FMT_UYVY	4
#define IMG_FMT_CbYCrY	5


#define  GP_PEDESTRIAN   0
#define  GP_FACE  1
#define  GP_HAND  2
#define  GP_FIST  3
#define  GP_SETPOINT  4


#define PFMST_CROP_HW

#define MAX_HIST_SIZE    4096 //8*8*8,    4096 // 16 * 16 * 16
#define MAX_PATCH_SIZE_W 16   // have HW limit
#define MAX_PATCH_SIZE_H 16
#define maxCropWH 300
#define minCropWH 16
#define maxClustery 10
#define maxBinDist 5

#define maxPatchPixels maxW*maxH   //max track image patch size  W*H = 400*400



#define  MAX_Np  9//1//5//9    ///< Number of particles
#define  LI  1    ///< Online learning interval (number of frames)

#define ABS(x)		((x) >= 0 ? (x) : -(x))
#define MAX(x, y)	((x) >= (y) ? (x) : (y))
#define MIN(x, y)	((x) <  (y) ? (x) : (y))


#define ot_max(a,b) ((a)>(b))? a : b
#define ot_min(a,b) ((a)>(b))? b : a

#define PI                  (3.14159265)

#define LIST_COUNT_NUM 200//150

#define MaxModelWidth 24
#define MaxModelHeight 24





/*
typedef struct
{
	short width;
	short height;
    short widthStep;
	char  ch;
	char  format;
	unsigned char *ptr;
} gpImage;

typedef struct
{
    short x;
    short y;
    short width;
    short height;
} gpRect;

typedef struct
{
    short width;
    short height;
} gpSize;

typedef struct
{
	short x, y;
} gpPoint;


typedef struct obj_s {
	int id;
	int x;
	int y;
	//int z;
	int width;
	int height;
	int pose;
	//float confidence;
} GPobj;
*/

typedef struct
{
	signed int delta_x;
	signed int delta_y;
	signed int delta_z;
	signed int delta_r;
	signed int drone_control_baseW;  //for face detect zoom
	signed int drone_control_baseCenterX;
	signed int drone_control_baseCenterY;
	signed int MAX_x;
	signed int MAX_y;
	signed int MAX_z;
	signed int MAX_r;
	signed int MIN_x;
	signed int MIN_y;
	signed int MIN_z;
	signed int MIN_r;
	unsigned int mode; //mode 0: finger fly,  mode 1: face fly follow, mode 2: face fly fix point
	signed int torrence_X;
	signed int torrence_Z;
	signed int torrence_Y;
	signed int torrence_R;

}DRONE_CONTROL;

/// Rectangle
typedef struct
{
	unsigned int x;
	unsigned int y;          ///< left-top corner coordinate of the rectangle
    unsigned int width;
    unsigned int height; ///< width and height of the rectangle
}TRect;

/// 2D point
typedef struct
{
	signed int x;
	signed int y;
	float distWeight;
}TPoint2D;

/// 3D point
typedef struct
{
	float x;
	float y;
	float z;
}TPoint3D;

/// Rotated rectangle
typedef struct
{
	TPoint2D center;   ///< The center coordinates
	unsigned int width;
	unsigned int height; ///< Width and height of the rectangle
	float angle;       ///< The rotation angle (unit: radian)
}TRotRect;



//// Parameters of the Tracker
typedef struct
{
	unsigned char rGamma;        // Reciprocal gamma
	TRotRect stopTh;             // Convergence threshold
	unsigned char maxIteration;  // Maximum iteration times
	float maxObjDiffRatio;       // Ratio of maximum differences on width, height and angle of the object box

}GPTrackerParam;

/// Context of the Tracker
typedef struct
{
	//float modelHist[HIST_SIZE];  // Target model histogram
	TRotRect objBox;             // Object bounding box
	float *pixelWeight_ptr;
	TPoint2D *rotPointLUT_ptr;

}GPTrackerContext;

/// Context of the PFMS tracking
typedef struct
{
	float *hist_target_ptr;  ///< The target histogram derive from the detected object
	float *hist_candidate_ptr;
	//float best_obj_histogram[HIST_SIZE];    ///< The histogram of the tracked object with the best similarity score in the current interval

	unsigned int modelBinListCnt;
	unsigned int candidateBinListCnt;

	unsigned int maxIteration;

	GPTrackerContext  msContext;
	TRotRect msTrackWindow;
// Color Space Kernel
	unsigned int UV_SIZE;
	unsigned int Y_SIZE;

	unsigned int UV_SHIFT_SIZE;
	unsigned int Y_SHIFT_SIZE;
	unsigned int YUV_SHIFT_SIZE;

	unsigned int HIST_SIZE;
	float HIST_THRE;

	unsigned int SAMPLE_STEP;
	float MODEL_NONEZERO_THRE;
	float CANDI_NONZERO_THRE;

// Zoom Parameters
	int ZOOMINOUT_FLG; // use Zoom of Not: 0:Not use, 1: use ZoomInOut
	float ZOOM_WIN_UPDATE_STEP; // after check, extend box, or shrink box
	float ZOOM_DIFF; // distance threshold
	float ZOOM_BIN_LIMIT; // bin number threshold
	float ZOOM_MIN_ERR_THRE; // if minima dist is bigger than value, don't zoom in out

	unsigned int modelHistBinList[LIST_COUNT_NUM];
	unsigned int candidateHistBinList[LIST_COUNT_NUM];
	unsigned int termination_flag;
	TRotRect particle[MAX_Np];         ///< Bounding boxes of particles
	float particle_weighting[MAX_Np];  ///< Weightings of particles
	unsigned int bestParticalX;
	unsigned int bestParticalY;
	unsigned int bestParticalW;
	unsigned int bestParticalH;
	TRotRect tracked_obj;          ///< The bounding box of tracked object
	TRotRect Original_obj; // record original tracking box
	gpRect trackObj_srcIMG;              //src image ROI obj position
	gpRect ROI;                          //src image ROI
	gpRect SRCIMG;                          //src image
	gpRect ZOOM_RECT[ 3 ];
	//TRotRect particle_tmp[Np];     ///< Temporary particles buffer
	signed int delta_x, delta_y;          ///< Motion in x and y directions
	signed int particalOffset_x, particalOffset_y;          ///< partical offset



	/* Online learning related variables */
	unsigned int frame_count;                        ///< Frame counter
	//TPoint2D past_obj_corner[(LI + 1) * 4]; ///< Corner coordinates of the past tracked objects
	//float past_obj_similarity[LI + 1];      ///< Bhattacharyya coefficient of the past tracked objects
	//unsigned char best_obj_idx;             ///< The index of the tracked object with the best similarity score in the current interval (0 ~ LI - 1)
		// Flow control par
	unsigned short detect_total;
	int initial_flag;
	int tracking_flag;
	int trackBackCnt;
	int terminationCnt;
	float fixSquareRatio;
	int frameCnt;
	int frameCnt_Max;
	float terminatedThre;
	float  dist_square;
	float hw_ratio;
	int FarOrClose;
	int detectMode;
	unsigned int nonDetectCnt;
	int dynamicROIFlg;
	unsigned patch_Size;
	float respondMotionPar;   // 0.02 - 0.1

	unsigned char* kernelBuffer;
	unsigned int ROI_720P2VGAFlg;


	unsigned int prediction_x;
	unsigned int prediction_y;


	unsigned int modelUpdateFlg;
	unsigned int modelUpdateCnt;
	float regionGrowThre;

	unsigned int dynamic_NP;
	unsigned int trackingBackFlg;
	signed int lastGoodPos_x, lastGoodPos_y;          ///< last good object position
	unsigned int alreadyTrackBackFlg;




}TPFMSContext;

typedef struct
{
	unsigned char *workMem;

}PFMSTWorkMem;

/**
* Initialize the Tracker.
* @param[in]  image     Input image.
* @param[in]  imageW    Input image width.
* @param[in]  imageH    Input image height.
* @param[in]  pObjBox   The bounding box of the object to be tracked.
* @param[out] pContext  Context of a tracking task.
*/

//void initGPTracker(unsigned char *image, const int imageW, int const imageH,
//						  const TRotRect *pObjBox,
//						  GPTrackerContext *pContext);

/**
* The kernel function of Tracker
* @param[in]     image     Input image.
* @param[in]     imageW    Input image width.
* @param[in]     imageH    Input image height.
* @param[in]     pParam    The parameters.
* @param[in,out] pContext  Context of a tracking task.
*/
void GPTracker(unsigned char *image, const int imageW, const int imageH,
					  TPFMSContext *pContext,
					  float* modelHist,
					  int termination_flag );

void Zoom_InOut(unsigned char *img, int imgW, int imgH, TRotRect bestPartical, float dist_square_best, TPFMSContext *pContext,int* FarOrClose );



/**
* Calculate the histogram from the ROI in the input image.
* @param[in]  image          Input image.
* @param[in]  imageW         Image width.
* @param[in]  imageH         Image height.
* @param[in]  pROI           ROI for calculating histogram.
* @param[out] hist           Histogram of the ROI.
* @param[out] rotPointLUT    Receives a look-up table for offsets of the rotated points within ROI. Set NULL if it won't be used later.
* @param[out] dist           Receives an array of squares of Mahalanobis distance for all points within ROI. Set NULL if it won't be used later.
*/
void modelHistogram( unsigned char *image,  int imageW,  int imageH,
					TRotRect *pROI, TPFMSContext *pContext);


int calcHistogram( unsigned char *image,  int imageW,  int imageH,
				  TRotRect *pROI, TPFMSContext* pContext );




/// Context of the PFMS parameters
typedef struct
{
	GPTrackerParam msParam; ///< Parameters of the mean shift tracking
	int T1;                  ///< Distance threshold for online learning (= 20)
	float T2;                ///< Similarity threshold for online learning (= 0.9)
	float learningRate;      ///< Learning rate (= 0.1)
	int   delta_x_thd;
	int   delta_y_thd;

}TPFMSParam;



////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//                        GLOBAL FUNCTION PROTOTYPES                          //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////


const  char *PFMSTracking_GetVersion(void);
unsigned int getPFMSTrackingMemorySize( void );
void initGPTrack(unsigned char *WorkMem, TPFMSContext* pContext, unsigned int SRCIMG_W, unsigned int SRCIMG_H);
void weighting_calculation(unsigned char *img, int imgW, int imgH, TRotRect particle[], float particle_weighting[], int Np_mux, TPFMSContext *pContext);
/**
* Initilization of PFMS (Particle Filter Mean Shift) tracking: used in detection mode once the object is detected.
* @param[in]  img                   Input image.
* @param[in]  imgW                  Width of input image.
* @param[in]  imgH                  Height of input image.
* @param[in]  deteced_obj           The bounding box of detected object.
* @param[out] pContext              The context of a PFMS tracking task.
*/
//extern void initPFMSTracking(unsigned char *img, int imageW, int imageH, TRotRect* detected_obj, TPFMSContext* pContext);
float initPFMSTracking(unsigned char *img, int imageW, int imageH, TPFMSContext* pContext);


/**
* PFMS (Particle Filter Mean Shift) tracking: used in tracking mode.
* @param[in]      img                 Input image.
* @param[in]      imgW                Width of input image.
* @param[in]      imgH                Height of input image.
* @param[in]      pParam              The parameters of a PFMS tracking task.
* @param[in,out]  pContext            The context of a PFMS tracking task.
*/
//extern void PFMSTracking(unsigned char *img, int imageW, int imageH, const TPFMSParam *pParam, TPFMSContext *pContext, int frame_cnt);
void PFMSTracking(unsigned char *img, int imageW, int imageH, TPFMSContext *pContext);

void PFMSTracker_dynamicROI( TPFMSContext* pContext, gpRect* ROI);

void PFMSTracker_framework( unsigned char *img, int imgW, int imgH, TPFMSContext *pContext, gpRect *faceSet );

void CopyTracker(TPFMSContext* pContext, TPFMSContext* pCopyContext);

void ROI_720P2VGA( TPFMSContext* pContext, gpRect* Target, gpRect* output, unsigned int imgW, unsigned int imgH );

void ROI_VGA2QVGA( TPFMSContext* pContext, gpRect* Target, gpRect* output );

void ROI_720P2QVGA( TPFMSContext* pContext,gpRect* Target, gpRect* output );

void ShiftTarget_720P2VGA( int mode, gpRect* Target,  gpRect* VGACoordi, unsigned int imgW, unsigned int imgH );

void ShiftTarget_VGA2QVGA( int mode, gpRect* Target,  gpRect* VGACoordi );

void ShiftTarget_720P2QVGA( int mode, gpRect* Target,  gpRect* VGACoordi );


TRotRect initRegion( TPFMSContext* pContext, unsigned char *image,  int imageW,  int imageH, signed int startPoint_x , signed int startPoint_y, signed int endPoint_x , signed int endPoint_y);

signed int YCC_2_BIN_IDX( TPFMSContext* pContext, unsigned char Y, unsigned char U, unsigned char V );

//void GetScrImgRatio( unsigned short ScrWidth, unsigned short ScrHeight,
//    unsigned short ImgHeight, unsigned short ImgWidth,
//    float* ScrImgRatio, float* ImgScrRatio,
//    unsigned int* ScrImgWidth, unsigned int* ScrImgHeight, unsigned int* WidthLimit );
//
//void CoordiModify( unsigned short ScrImgWidth, unsigned short ScrImgHeight, float ImgScrRatio, unsigned int WidthLimit, unsigned short* x, unsigned short* y, unsigned short* imW, unsigned short* imHH );

#ifdef PFMST_CROP_HW
int PFMST_CROP_hw_set( int (*pfunc)(unsigned int in_buffer, unsigned short in_w, unsigned short in_h, unsigned int kernel_buffer, unsigned short out_w, unsigned short out_h, unsigned short crop_x, unsigned short crop_y, unsigned short crop_w, unsigned short crop_h));
#endif

void drone_control_report( gpRect *obj_result ,DRONE_CONTROL* drone_parm, TPFMSContext* pContext);

//********************************
// mode: 0, finger fly
// mode: 1, face fly follow
// mode: 2, face fly fix point
//********************************
void gesture_moving_control( unsigned char *img, unsigned int imgW, unsigned int imgH , TPFMSContext *pContext, gpRect *movingROI, unsigned int mode );

float errorDistCompute(TPFMSContext *pContext);

void regionGrow( unsigned char *img, unsigned int imgW, unsigned int imgH , TPFMSContext *pContext, gpPoint fingerPoint, gpRect* growResult );

#endif // __PFMST_H__

#ifndef __FLANDMARK_H__
#define __FLANDMARK_H__

/**************************************************************************/
// Facial Landmark Detection Header File
// v1008 (for sensor H42)
/**************************************************************************/

#include "define.h"

#define landmarksCount                      21
#define floutCount                          (landmarksCount + 1)
#define glassesCount                        8
#define FL_SIZE_HPIXEL                      160
#define FL_SIZE_VPIXEL                      120

typedef struct {
	int fl_stable_buflen; 		// Stabilizing buffer length for facial appearance
	int flout_stable_buflen; 	// Stabilizing buffer length for scaling, resizing and moving
	int g_stable_buflen; 		// Stabilizing buffer length for glasses motions
	int fl_init; 				// Initialization flag for stabilization of landmark detection
	int fl_state;				// Number of valid faces, 0: face not found, 1: one face, 2: more than one face
	int mouth_open;				// 0: closed mouth, 1: opened mouth
	gpPoint* fl_stable_buf; 	// Stabilizing buffer for facial appearance,
    gpPoint* flout_stable_buf; 	// Stabilizing buffer for scaling, resizing and moving
	gpPoint* g_stable_buf; 		// Stabilizing buffer for glasses motions
	gpImage* gray_qqvga; 		// Input image in qqvga & gray-level format
	gpRect* face_roi; 			// Facial region of interest
	void* flandmark_mem; 		// The allocated memory and the assigned memory address for this func
} FLandmark_Input;


/**
* @brief Initialization of Facial Landmark Detection
* @param[in] FLandmark_Input::fl_stable_buflen: stabilizing buffer length for facial appearance
* @param[in] FLandmark_Input::flout_stable_buflen: stabilizing buffer length for scaling, resizing and moving
* @param[in] FLandmark_Input::g_stable_buflen: stabilizing buffer length for glasses motions
* @param[in, out] FLandmark_Input::fl_stable_buf: stabilizing buffer for facial appearance,
* @param[in, out] FLandmark_Input::flout_stable_buf: stabilizing buffer for scaling, resizing and moving
* @param[in, out] FLandmark_Input::g_stable_buf: stabilizing buffer for glasses motions
* @param[in, out] FLandmark_Input::flandmarkMem: the allocated memory and the assigned memory address for this func
* @param[out] FLandmark_Input::fl_init: initialization flag for stabilization of landmark detection
* @param[out] FLandmark_Input::fl_state: Number of valid faces, 0: face not found, 1: one face, 2: more than one face
* @param[out] FLandmark_Input::mouth_open: 0: closed mouth, 1: opened mouth
*/
void FLandmark_Init(FLandmark_Input* flandmark_in);

/**
* @brief Get Facial Landmark Detection Version
* @return source version
*/
const char *FLandmark_GetVersion(void);

/**
* @brief Kernel of Facial Landmark Detection
* @param[in] FLandmark_Input::fl_stable_buflen: stabilizing buffer length for facial appearance
* @param[in] FLandmark_Input::flout_stable_buflen: stabilizing buffer length for scaling, resizing and moving
* @param[in] FLandmark_Input::g_stable_buflen: stabilizing buffer length for glasses motions
* @param[in] FLandmark_Input::gray_qqvga: input image in qqvga & gray-level format
* @param[in] FLandmark_Input::face_roi: facial region of interest
* @param[in] FLandmark_Input::fl_state: Number of valid faces, 0: face not found, 1: one face, 2: more than one face
* @param[in, out] FLandmark_Input::fl_stable_buf: stabilizing buffer for facial appearance,
* @param[in, out] FLandmark_Input::flout_stable_buf: stabilizing buffer for scaling, resizing and moving
* @param[in, out] FLandmark_Input::g_stable_buf: stabilizing buffer for glasses motions
* @param[in, out] FLandmark_Input::flandmarkMem: the allocated memory and the assigned memory address for this func
* @param[in, out] FLandmark_Input::fl_init: initialization flag for stabilization of landmark detection
* @param[in, out] flandmarks_qvga: facial landmark detection result for QVGA display
* @param[in, out] glasses_qvga: glasses estimation result for QVGA display
* @param[out] FLandmark_Input::mouth_open: 0: closed mouth, 1: opened mouth
*/
int FLandmark_Detect(FLandmark_Input* flandmark_in, gpPoint* flandmarks_qvga, gpPoint* glasses_qvga);

/**
* @brief Calculate needed memory
* @return needed memory size for facial landmark detection
*/
int FLandmark_MemCalc();

#endif

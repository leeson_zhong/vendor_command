#ifndef _KOT_H_
#define _KOT_H_

#ifdef __cplusplus
extern "C"{
#endif

#include "define.h"


#define KOT_HW
//#define GP_SETPOINT_ENABLE   //**********  enable set point tracking flag  ***********




//#define PC_SIM
//#define VALIDATION


#define MAX_FACE (256)


#define PATCH_SZ  12 //20
#define PA1 12 //24



#define BasicOne  1
#define V0P81_INT20P12  3317




#define  GP_PEDESTRIAN   0
#define  GP_FACE  1
#define  GP_HAND  2



	/* Wavelet size at first layer of first octave. */
#define HAAR_SIZE0  9

	/* Radius of the circle in which to sample gradients to assign an
	orientation */
#define ORI_RADIUS  3//6

#define max_ori_samples  (2*ORI_RADIUS+1)*(2*ORI_RADIUS+1)
	static int nangle0 = 0;
	static int doOnecFlg = 0;

	/* Wavelet size increment between layers. This should be an even number,
	such that the wavelet sizes in an octave are either all even or all odd.
	This ensures that when looking for the neighbours of a sample, the layers
	above and below are aligned correctly. */
#define HAAR_SIZE_INC  6


#define trackerDatabaseLimitBuffNum  3  //3
#define target_KOT_NUM 70
#define target_KOT_NUM_MIN 20
#define target_Matching_NUM 70//30
#define DATABASE_KOT_NUM_LIMIT 200
#define DETECT_KOT_NUM_LIMIT 200
#define MATCHING_KOT_NUM_LIMIT 200
#define MaxUV 50

#define maxImageW 320
#define maxImageH 240




	typedef struct
	{
		signed short x;
		signed short y;
	}ImgPoint2D;

	/*typedef struct
	{
	short x;
	short y;
	}DataPoint2D;*/

	typedef struct
	{
		ImgPoint2D pt;
		signed char laplacian;
		short size;
		int dir_INT;
		//float dir;  //float check
		//unsigned short  hessian;

	} KOTPoint_INT;


	typedef struct
	{
		//int descriptor_INT[64];
		signed int descriptor_short[32];
	} KOTDescriptor;



	typedef struct
	{
		unsigned short hessianThreshold;
		unsigned char nOctaves;
		unsigned char nOctaveLayers;
	}KOTParams;


	typedef struct
	{
		unsigned int p0, p1, p2, p3;
		unsigned int w_INT17P15;   //for dx, dy
		unsigned int w_INT20P12;   //for dxx, dyy, dxy
	}KOTHF;

	typedef struct
	{
		unsigned short lapNegCnt;
		unsigned short lapPosCnt;
	}KOTLAP;



	typedef struct
	{
		KOTLAP *cardsRFNArr;  //record cards RF points number.
		KOTPoint_INT *objKeypoints;
		KOTDescriptor *objDescriptors;

	} objDatabase;

	typedef struct
	{

		//unsigned short *cardsRFNArr;  //record cards RF points number.
		unsigned int *cardsMatchingCnt;  //cards Matching counter.

	}cardsInformation;


/*
typedef struct DataArr
	{

		unsigned short width;
		unsigned short height;
		unsigned short step;
		unsigned int *i;

	} DataArr;
*/
	typedef struct
	{
		//extraction section
		DataArr sum;         //integral image
		DataArr detsResponse[2];   // det response
		//DataPoint2D apt[max_ori_samples];
		//int apt_w_INT2P30[max_ori_samples];
		//max 400 featrue point
		//KOTPoint_INT imgKeypoints[DETECT_KOT_NUM_LIMIT];
		//KOTDescriptor imgDescriptors[DETECT_KOT_NUM_LIMIT];
		objDatabase objData;
		/*KOTPoint_INT objKeypoints[cardsN*DATABASE_KOT_NUM_LIMIT];
		KOTDescriptor objDescriptors[cardsN*DATABASE_KOT_NUM_LIMIT];*/
		cardsInformation cardsData;
		KOTParams params;
		KOTHF Dxx_opt[16];
		KOTHF Dyy_opt[16];
		KOTHF Dxy[32];
		KOTHF Dx[8];
		KOTHF Dy[8];
		//KOTDescriptor *objDes_ptr;
		unsigned char PATCH[144];   // 12*12 >>2
		signed int buff_INT_vec[64];
		unsigned int cardsN;



		//matching section
		//DataPoint2D rotationMatchingP[MATCHING_KOT_NUM_LIMIT];
		unsigned short recordMIT[MATCHING_KOT_NUM_LIMIT];
		unsigned short patternWidth;
		unsigned short patternHeight;
		unsigned int RMCnt;
		unsigned int matchingCnt;
		unsigned int maxCnt;
		unsigned int matchingThre;
		unsigned int matchingThre_max;
		unsigned int matchingThre_min;
		unsigned int defultMatchingThre;
		unsigned int matchingThreRangeV;
		unsigned int extractionThre;
		unsigned int minExtractionThre;
		unsigned int maxExtractionThre;
		unsigned int incExtractionThre;
		unsigned int decExtractionThre;
		unsigned int startMatchingPointN;
		unsigned int trackingFlg;
		unsigned int trackingTimerCnt;
		unsigned int trackTerminateFlg;
		unsigned int trackKernealInitFlg;
		unsigned int trackTerminateCnt;
		unsigned int trackRestFlg;
		float ROI_ScaleRate;

		unsigned int faceUpdateFromDetectionFlg;
		unsigned int keepLastGoodIndex;
		unsigned int modelAlreadyFlg;
		signed int lastFaceCenterX;
		signed int lastFaceCenterY;
		signed int lastFaceCenterW;
		gpRect InitObject, updateObject;
		unsigned int KOT_initFlg;
		gpRect* objSet;
		unsigned short* objCount;
		signed int G_X;
		signed int G_Y;
		signed int OBJ_X;
		signed int OBJ_Y;
		float mapShiftMotionScale;


		//unsigned char *win_bufs_arr_ptr;

		//find paire array
		unsigned short matchingIndexTable[MATCHING_KOT_NUM_LIMIT];

		unsigned char matchingDatabaseUI[100];
		unsigned char u[MaxUV*2+1];
		unsigned char v[MaxUV*2+1];

		unsigned int modelingIndex;
		unsigned int dx_dy_size;


	}globalData_KOT;




	void GPSmoothImage(unsigned char *inputImg, unsigned char *outputImg, int KOT_ImgW, int KOT_ImgH);
	const signed char *KOT_GetVersion(void);






	//========================================================
	// Function Name :  KOT_get_memory_size
	// Syntax : unsigned int KOT_get_memory_size(int width, int height);
	// Purpose :  compute total working memory size
	// Parameters : int width: detect image width
	//              int height: detect image height
	// Return : working memory size (bytes)
	//========================================================
	unsigned int KOT_get_memory_size(int width, int height);

	//========================================================
	// Function Name :  KOT_init
	// Syntax : void KOT_init(int MaxWidth, int MaxHeight, int *workmem, int cardsN, int *cardsData, unsigned short patternWidth, unsigned short patternHeight);
	// Purpose :  init system
	// Parameters : int MaxWidth:    detect image width
	//              int MaxHeight:   detect image height
	//              int *workmem:    allocated working memory pointer
	//              unsigned short patternWidth:     detect object model image width
	//              unsigned short patternHeight:    detect object model image height
	//              int cardsN:      database object number
	// Return : void
	//========================================================
	void KOT_init_HW(int MaxWidth, int MaxHeight, int *workmem);
	void KOT_kernel_update(int MaxWidth, int MaxHeight, int *workmem);


	//========================================================
	// Function Name :  KOT_ParmSet
	// Syntax : void KOT_ParamSet(int *workmem, unsigned int matchingThre, unsigned int matchingThreRangeV, unsigned int minExtractionThre,
	//			                  int incExtractionThre, int decExtractionThre);
	// Purpose :  set system parm
	// Parameters : int *workmem:                        working memory pointer
	//              unsigned int matchingThre:           set defult matching threshold value
	//              unsigned int matchingThreRangeV:     set defult matching threshold range
	//              unsigned int minExtractionThre:      set min extract point threshold value
	//              int incExtractionThre:               set incerease extract point threshold value
	//              int decExtractionThre:               set decerease extract point threshold value
	//              int startMatchingPointN:             set start matching's point number
	// Return : void
	//========================================================
	void KOT_ParamSet(int *workmem, unsigned int extractionThre, unsigned int matchingThre, unsigned int matchingThreRangeV, unsigned int minExtractionThre,
		int incExtractionThre, int decExtractionThre, int startMatchingPointN);



	//========================================================
	// Function Name :  KOT_extractPoint
	// Syntax : int KOT_extractPoint(void* workmem, gpImage *inputImage);
	// Purpose :  extraction KOT feature point include point position and size
	// Parameters :
	//              int *workmem:    allocated working memory pointer
	//              gpImage *inputImage:    inpute detect gray image
	// Return : KOT point total number
	//========================================================
	int KOT_extractPoint(void* workmem, gpImage *inputImage);



	int KOT_fftobj_hw_set(int (*pfunc)(unsigned int KOT_max, unsigned int total_num, unsigned int object_Des, unsigned int image_Des, unsigned int *min_error, unsigned int *min_error_id));

	void  keypointsObjectTracker(void* workmem, gpImage *inputImage, int objectDetectCnt);


#ifdef __cplusplus
}
#endif

#endif   //_KOT_H_



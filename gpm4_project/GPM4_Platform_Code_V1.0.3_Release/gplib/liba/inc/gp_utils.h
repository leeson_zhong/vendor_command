#ifndef __GP_UTILS_H__
#define __GP_UTILS_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "define.h"

#ifdef __cplusplus
}
#endif

#define Q3	   3
#define Q12    12
#define Q13    13
#define Q15    15
#define Q_FLT  6

#define TWO_FLT 64
#define TWO_12  4096
#define TWO_15  32768

/* Constants for color conversion */
enum
{
	GP_BGR2GRAY    =6
};

/****************************************************************************************\
*          Array allocation, deallocation, initialization and access to elements         *
\****************************************************************************************/

/* dst = src */
void gpCopy( const gpImage* src, gpImage* dst, const gpRect* srcROI, const gpRect* dstROI );

/****************************************************************************************\
*                                    Image Processing                                    *
\****************************************************************************************/
/* Resizes image */
void gpResize( const gpImage* src, gpImage* dst, const gpRect* srcROI );

/* Normalizes image based on eyes position */
void gpNorm(const gpImage* src, gpImage* dst, const gpRect* _faceROI, const gpRect* lEyeROI, const gpRect* rEyeROI);

/****************************************************************************************\
*                                  Histogram functions                                   *
\****************************************************************************************/
/* equalizes histogram of 8-bit single-channel image */
void gpEqualizeHist( const gpImage* src, gpImage* dst, const gpRect* roiRec );

/****************************************************************************************\
*                             Common macros and inline functions                         *
\****************************************************************************************/
static int gpRound15(int value)
{	
	return (value + (value >= 0 ? 16384 : 16383)) >> Q15;
}

static int gpRound13(int value)
{	
	return (value + (value >= 0 ? 4096 : 4095)) >> Q13;
}

static int gpRound12(int value)
{	
	return (value + (value >= 0 ? 2048 : 2047)) >> Q12;
}

static int gpRoundFLT(int value)
{		
	return (value + (value >= 0 ? 32 : 31)) >> Q_FLT;	
}

/****************************************************************************************\
*                      Other supplementary data type definitions                         *
\****************************************************************************************/
/******************************* GPPoint and variants ***********************************/
static gpPoint GPPoint(int _x, int _y)
{
	gpPoint pt;
	pt.x = _x;
	pt.y = _y;
	return pt;
}

//=========== matrix operation ===========//
// if image is empty, return true otherwise return false
static int gpIsEmpty( const gpImage* image )
{
	return ((!image)||(!image->ptr)||(image->height*image->width <= 0));
}

// create rect
static gpRect GPRect(int _x, int _y, int _width, int _height)
{
	gpRect rect;
	rect.x = _x;
	rect.y = _y;
	rect.width = _width;
	rect.height = _height;
	return rect;
}

// create size
static gpSize GPSize(int _width, int _height)
{
	gpSize sz;
	sz.width = _width;
	sz.height = _height;
	return sz;
}

//=========== division operation ===========//
int div32(int r0, int x0);

//=========== memory set =============//
void* gpMemset(void *s, int c, unsigned int count);

//=========== sqroot =============//
int sqrootFLT(const int x); // x*(2^6)

int sqroot12(const int x); // x*(2^12)

int sqroot15(const int x); // x*(2^15)

#endif
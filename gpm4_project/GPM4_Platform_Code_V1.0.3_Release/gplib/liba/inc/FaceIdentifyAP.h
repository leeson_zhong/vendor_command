#ifndef __FACE_IDENTIFY_AP_H__
#define __FACE_IDENTIFY_AP_H__

/**************************************************************************/
// Face ID Header File
// v3101 (for sensor 0308)
/**************************************************************************/

#include "define.h"

#define SZ_ONEDATA        7168 // (IMAGE_SIZE<<1)
#define SZ_ULBP           SZ_ONEDATA

/**** Data Structure ****/
// Take MAX_SUBJECT_NUM = 5, subject_num = 3 for example
//---------------------------------------------------------------------------------------------------------
// Subject0 |           TRAIN_NUM            |       update_num[0]       | MAX_UPDATE_NUM - update_num[0] |
//---------------------------------------------------------------------------------------------------------
// Subject1 |           TRAIN_NUM            |       update_num[1]       | MAX_UPDATE_NUM - update_num[1] |
//---------------------------------------------------------------------------------------------------------
// Subject2 |           TRAIN_NUM            |       update_num[2]       | MAX_UPDATE_NUM - update_num[2] |
//---------------------------------------------------------------------------------------------------------
// xxxxxxxx |           xxxxxxxxx            |       update_num[3]       | MAX_UPDATE_NUM - update_num[3] |
//--------------------------------------------------------------------------------------------------------------------
// xxxxxxxx |           xxxxxxxxx            |       update_num[4]       | MAX_UPDATE_NUM - update_num[4] | TestData |
//--------------------------------------------------------------------------------------------------------------------


typedef struct {
	// training
	void* userDB; // @param[out] user database
	void* fiMem; // @param[in] the allocated memory and the assigned memory address for this func
	int MAX_SUBJECT_NUM; // @param[in] maximum subject number
	int subject_num; // @param[in] valid subject number
	int TRAIN_NUM; // @param[in] training number for each subject
	int MAX_UPDATE_NUM; // @param[in] maximum updating number for each subject
	int subject_index; // @param[in] subject index, which subject is for training or verification or updating or elimination
	int train_index; // @param[in] training index for each person
	int SAMPLE_NUM_ONE; // @param[in] sample number for each person, equals to (TRAIN_NUM + MAX_UPDATE_NUM)
	int MAX_SAMPLE_NUM; // @param[in] max sample number in total, equals to (SAMPLE_NUM_ONE*MAX_SUBJECT_NUM)

	// identification
	int scoreHiTh; // @param[in] high threshold of confidence score,
	               // If a subject has a confidence score higher than this threshold, FaceIdentify_Verify_Shift_MP reports this subject as a genuine immediately
	int scoreLoTh; // @param[in] low threshold of confidence score, still needs to compare to other subjects to confirm the genuine
				   // If a subject has a confidence score higher than this threshold, FaceIdentify_Verify_Shift_MP still needs to check the similarities of other subjects to confirm the identity.
				   // @param[in] confidence score threshold for single person identification
	int* update_num; // @param[in] an array for recording updating numbers for all subjects
	int* update_index; // @param[in] an array for recording updating indexs of all subjects

	int best_subject_index; // @param[out] the subject index with the highest confidence score
	int best_templ_index[3]; // @param[in] user index, if best_user_index[0] is - 1, check all templates, else check designated 3 subjects
							 // @param[out] the user index with the highest confidence score
	int score; // @param[out] score: best confidence score, not an integer array
	int score2; // @param[out] score2: second best confidence score, not an integer array

	// second best
	int best_subject_index2; // @param[out] the subject index with the second-highest confidence score
	int best_templ_index2[3]; // @param[in] user index, if best_user_index[0] is - 1, check all templates, else check designated 3 subjects
							  // @param[out] the user index with the highest confidence score

} FID_Info;

/**
* @brief Get Face ID AP version
* @return source version
*/
const char *FaceIdentify_GetVersion(void);

/**
* @brief Calculate needed memory
* @return needed memory size for face identification
*/
int FaceIdentify_MemCalc();

/**
* @brief Train user's feature
* @param[in] gray: captured image
* @param[in] userDB: user database
* @param[in] train_i: index of the training data
* @param[in] fiMem: the allocated memory and the assigned memory address for this func
* @param[in] userROI[0], faceROI: detected face region
* @param[in] userROI[1], lEyeROI: detected left eye region
* @param[in] userROI[2], rEyeROI: detected right eye region
*/
void FaceIdentify_Train(const gpImage* gray, const gpRect* userROI, void* userDB, const int train_i, void* fiMem);

// Multiple People
/**
* @brief Train multi-users' features
* @param[in] gray: captured image
* @param[in] userROI[0], faceROI: detected face region
* @param[in] userROI[1], lEyeROI: detected left eye region
* @param[in] userROI[2], rEyeROI: detected right eye region
* @param[in] fidInfo.SAMPLE_NUM_ONE: sample number for each person, equals to (TRAIN_NUM + MAX_UPDATE_NUM)
* @param[in] fidInfo.fiMem: the allocated memory and the assigned memory address for this func
* @param[in] fidInfo.userDB: user database
* @param[in] fidInfo.subject_num: subject number of user database
* @param[in] fidInfo.subject_index: subject index for training
* @param[in] fidInfo.train_index: training index for each person
*/
void FaceIdentify_Train_MP(const gpImage* gray, const gpRect* userROI, FID_Info* fidInfo);

/**
* @brief Verify user's identity
* @param[in] gray: captured image
* @param[in] userDB: user database
* @param[in] scoreThreshold: threshold of confidence score
* @param[in] fiMem: the allocated memory and the assigned memory address for this func
* @param[in] templ_num: template number, which is a const number, maximum sample number
* @param[in] valid_num: valid sample number = train_number + update_num
* @param[in] userROI[0], faceROI: detected face region
* @param[in] userROI[1], lEyeROI: detected left eye region
* @param[in] userROI[2], rEyeROI: detected right eye region
* @param[out] templ_index: template index
* @param[out] score: confidence score, not an integer array
* @return 1 if the user is classified as a genuine user, 0 if classified as imposter
*/
int FaceIdentify_Verify(gpImage* gray, const gpRect* userROI, void* userDB, int scoreThreshold, void* fiMem, const int templ_num, int valid_num, int* templ_index, int* score);

/**
* @brief Verify user's identity, for debug usage
* @param[in] gray: captured image
* @param[in] userDB: user database
* @param[in] scoreThreshold: threshold of confidence score
* @param[in] fiMem: the allocated memory and the assigned memory address for this func
* @param[in] templ_num: template number
* @param[in] userROI[0], faceROI: detected face region
* @param[in] userROI[1], lEyeROI: detected left eye region
* @param[in] userROI[2], rEyeROI: detected right eye region
* @param[in] templ_index: template index, if temp_index is -1, check all templates, else check designated index
* @param[out] templ_index: the template index with the highest confidence score
* @param[out] score: highest score if the subject is rejected, the score of passed template if accepted
* @return 1 if the user is classified as a genuine user, 0 if classified as imposter
*/
int FaceIdentify_Verify_Shift(gpImage* gray, const gpRect* userROI, void* userDB, int scoreThreshold, void* fiMem, const int templ_num, int valid_num, int* templ_index, int* score);

/**
* @brief Identification of multi-users' identities
* @param[in] gray: captured image
* @param[in] userROI[0], faceROI: detected face region
* @param[in] userROI[1], lEyeROI: detected left eye region
* @param[in] userROI[2], rEyeROI: detected right eye region
* @param[in] fidInfo.TRAIN_NUM; training number for each subject
* @param[in] fidInfo.MAX_SAMPLE_NUM: max sample number in total, equals to (SAMPLE_NUM_ONE*MAX_SUBJECT_NUM)
* @param[in] fidInfo.SAMPLE_NUM_ONE: sample number for each person, equals to (TRAIN_NUM + MAX_UPDATE_NUM)
* @param[in] fidInfo.fiMem: the allocated memory and the assigned memory address for this func
* @param[in] fidInfo.userDB: user database
* @param[in] fidInfo.subject_num: subject number of user database
* @param[in] fidInfo.update_num: an array for recording updating numbers for all subjects
* @param[in] fidInfo.scoreHiTh: high threshold of confidence score,
	                If a subject has a confidence score higher than this threshold, FaceIdentify_Verify_Shift_MP reports this subject as a genuine immediately
* @param[in] fidInfo.scoreLoTh: low threshold of confidence score, still needs to compare to other subjects to confirm the genuine
				    If a subject has a confidence score higher than this threshold, FaceIdentify_Verify_Shift_MP still needs to check the similarities of other subjects to confirm the identity.
					Besides, this is the confidence score threshold for single person identification
* @param[out] fidInfo.best_subject_index: the subject index with the highest confidence score
* @param[out] fidInfo.best_subject_index2: the subject index with the second-highest confidence score
* @param[out] fidInfo.score: best confidence score, not an integer array
* @param[out] fidInfo.score2: second best confidence score, not an integer array
* @param[in, out] fidInfo.best_templ_index[3]: @param[in] user index, if best_user_index[0] is - 1, check all templates, else check designated 3 subjects
					                           @param[out] the user index with the highest confidence score
* @return 1 if the user is classified as a genuine user in user database, 0 if classified as imposter
*/
int FaceIdentify_Verify_Shift_MP(gpImage* gray, const gpRect* userROI, FID_Info* fidInfo);

/**
* @brief Verify user's identity, for debug usage
* @param[in] gray: captured image
* @param[in] userDB: user database
* @param[in] scoreThreshold: threshold of confidence score
* @param[in] fiMem: the allocated memory and the assigned memory address for this func
* @param[in] templ_num: template number
* @param[in] userROI[0], faceROI: detected face region
* @param[in] userROI[1], lEyeROI: detected left eye region
* @param[in] userROI[2], rEyeROI: detected right eye region
* @param[out] templ_index: template index
* @param[out] score: confidence score, an integer array
* @return 1 if the user is classified as a genuine user, 0 if classified as imposter
*/
int FaceIdentify_Verify_Debug(gpImage* gray, const gpRect* userROI, void* userDB, int scoreThreshold, void* fiMem, const int templ_num, int* templ_index, int* score);

/**
* @brief Update user's data
* @param[in, out] userDB: user database
* @param[in] templ_num: template number, which is a const number
* @param[in] train_num: training number, which is a const number
* @param[in, out] update_num: update number after training
*/
void FaceIdentify_Update(void* userDB, const int templ_num, const int train_num, int* update_num);

/**
* @brief Update multi-users' data
* @param[in] fidInfo.subject_index: subject index for updating
* @param[in] fidInfo.update_index[subject_index]: updating index for designated subject
*/
void FaceIdentify_Update_MP(FID_Info* fidInfo);


// return useless sample index
/**
* @brief Return useless sample index
* @param[in] fidInfo.subject_index: subject index for elimination
* @return the template index of the designated subject
*/
int FaceIdentify_Eliminate_MP(FID_Info* fidInfo);

/**
* @brief Delete designated subject
* @param[in, out] fidInfo.userDB: user database
* @param[in] del_subject: delete subject, which is a const number
* @param[in, out] fidInfo.subject_num: valid subject number
* @param[in, out] fidInfo.update_index: template index for updating
* @param[in, out] fidInfo.update_num: update number after training
*/
void FaceIdentify_Delete(FID_Info* fidInfo, const int del_subject);

#endif

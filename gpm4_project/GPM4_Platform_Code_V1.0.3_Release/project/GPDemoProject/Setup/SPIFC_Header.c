#include "project.h"
/*
Byte 0	1	2	3	4	5	6	7
     NV TAG		SourceAddr
     S	P	I	I	0x1E000200
Byte 8	9	10	11	12	13	14	15
     TargetAddr		RunTimeAddr
     0x1E000200		0x00000000
Byte 16	17	18	19	20	21	22	23
     CodeSize		SPIFC Checksum
    0x00000000		0x00000000
Byte 24	     25	        26	    27	  28	29	  30	31
     HdrVer	 PreRegCnt  Remap	RFU	  RFU	RFU	  RFU	RFU
     0x10	 0x00	    0x01	0x00  0x00	0x00  0x00	0x00
Byte 32~511
     Pre Registers
     0x0
*/
#define TAG_SPINOR      0x49495053   //SPII
#define SRC_ADDR		0x1E000200
#define TAR_ADDR		0x1E000200
#define RUN_ADDR		0x1E000200
#define CODE_SIZE       0x100000
#define HEADER_VER      0x10        //SPIFC bootheader version

#define REMAP_FLAG      0x01        //remap flag to run on spi or copy code to ram to run
#define BYTE27          0x00        //reserved
#define BYTE27_24       (BYTE27<<24) | (REMAP_FLAG<<16) | (PREREG_CNT<<8) | HEADER_VER//0x00010110
#define BYTE31_28       0
#if 0//SYSPLL: 387MHz SPIFC: 48MHz
#define PREREG_CNT      0x01        //preRegister count
#define PREREG_ADD01    0xD000005C
#define PREREG_VAL01    0x00008628
#define PREREG_ADD02    0x00000000
#define PREREG_VAL02    0x00000000
#define PREREG_ADD03    0x00000000
#define PREREG_VAL03    0x00000000
#define PREREG_ADD04    0x00000000
#define PREREG_VAL04    0X00000000

#elif 1//other SYSPLL:387MHz CPUPLL:576MHz SPIFC: 72MHz
#define PREREG_CNT      0x03        //preRegister count
#define PREREG_ADD01    0xD000005C
#define PREREG_VAL01    0x000086A8
#define PREREG_ADD02    0xC0010024
#define PREREG_VAL02    0x00000702
#define PREREG_ADD03    0xD000007C
#define PREREG_VAL03    0x00005D02      //CPUPLL: 576MHz
//#define PREREG_ADD03    0xD000007C
//#define PREREG_VAL03    0x00006402    //CPUPLL: 684MHz
#define PREREG_ADD04    0xC0000000
#define PREREG_VAL04    0X00000000

#else//for GPM47XXA EMU external SPIFC on IOD[0:5]
#define PREREG_CNT      0x01        //preRegister count
#define PREREG_ADD01    0xD000005C
#define PREREG_VAL01    0x00008828
#define PREREG_ADD02    0x00000000
#define PREREG_VAL02    0x00000000
#define PREREG_ADD03    0x00000000
#define PREREG_VAL03    0x00000000
#define PREREG_ADD04    0x00000000
#define PREREG_VAL04    0x00000000

#endif


const INT32U spifc_header[128] __attribute__ ((section(".header_section"))) =
{
	TAG_SPINOR,
	SRC_ADDR,
	TAR_ADDR,
	RUN_ADDR,
	CODE_SIZE,
	TAG_SPINOR ^ RUN_ADDR,
	BYTE27_24,
	BYTE31_28,
	PREREG_ADD01,
	PREREG_VAL01,
	PREREG_ADD02,
	PREREG_VAL02,
	PREREG_ADD03,
	PREREG_VAL03,
	PREREG_ADD04,
	PREREG_VAL04
};

#                                                                                                        
#   GPM4130A 
#

#SYSPLL: 387MHz SPIFC: 48MHz
#set *((int *) 0xC0000108) = 0x40000000
#set *((int *) 0xD000005C) = 0x8828
#SYSPLL:387MHz CPUPLL:288MHz SPIFC: 72MHz
set *((int *) 0x1FFD0000) = 0x55aa0000
set *((int *) 0xC0000108) = 0x40000000
set *((int *) 0xD000005C) = 0x86A8
set *((int *) 0xC0010024) = 0x00000702
set *((int *) 0xD000007C) = 0x00005D02

if $IDEResetWithNoLoadCommand == 1
echo "=== set $pc and $xPSR ==="

	flushregs
	
	set $ResetHandler = (*(int *)(0x1e000204)) 
	#with spifc header
	set $thumb_start  = ($ResetHandler &1)
	
	if $thumb_start == 0x1
		#set execution of instructions in Thumb mode
		set $pc =   $ResetHandler - 1
		set	$xPSR = 0x01000000
	else
		set $pc =   $ResetHandler
	end

end

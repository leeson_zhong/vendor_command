/**************************************************************************
 *                                                                        *
 *         Copyright (c) 2014 by Generalplus Inc.                         *
 *                                                                        *
 *  This software is copyrighted by and is the property of Generalplus    *
 *  Inc. All rights are reserved by Generalplus Inc.                      *
 *  This software may only be used in accordance with the                 *
 *  corresponding license agreement. Any unauthorized use, duplication,   *
 *  distribution, or disclosure of this software is expressly forbidden.  *
 *                                                                        *
 *  This Copyright notice MUST not be removed or modified without prior   *
 *  written consent of Generalplus Technology Co., Ltd.                   *
 *                                                                        *
 *  Generalplus Inc. reserves the right to modify this software           *
 *  without notice.                                                       *
 *                                                                        *
 *  Generalplus Inc.                                                      *
 *  No.19, Industry E. Rd. IV, Hsinchu Science Park                       *
 *  Hsinchu City 30078, Taiwan, R.O.C.                                    *
 *                                                                        *
 **************************************************************************/
/*******************************************************
    Include file
*******************************************************/
#include "drv_l1_sfr.h"
#include "drv_l1_timer.h"
#include "drv_l1_uart.h"
#include "application.h"
//#include "avi_encoder_app.h"
#include "drv_l1_usbd.h"
#include "drv_l2_usbd.h"
#include "drv_l2_usbd_uvc_uc.h"
#include "project.h"
#include "drv_l1_clock.h"
//#include "drv_l2_display.h"
//#include "drv_l2_ad_key_scan.h"

#include "usbd_uac.h"
#include "usbd_uvc.h"
#include "drv_l1_pscaler.h"

#include "drv_l2_usbd_msdc.h"
/******************************************************
    Definition and variable declaration
*******************************************************/
extern xQueueHandle hUSBD_UVC_TaskQ;

typedef struct USBD_BUF_S
{
	INT32U bufptr;
	struct USBD_BUF_S* next;
	INT32U state;
	INT32U size;
} UVC_BUF_T;

#define USBD_STORAGE_NO_WPROTECT	0
#define USBD_STORAGE_WPROTECT		1
//#define USBD_UVC_MIC_PKT_SIZE	(16)
//#define UVC_WIDTH	    640
//#define UVC_HEIGHT	480
#define UVCSTACKSIZE	2048
#define UVC_Q_MAX_SIZE	8

osMessageQId UVCTaskQ = NULL;
osThreadId UVCTaskID  = NULL;

extern uac_aud_dec_t usb_uac_aud_ctlblk;
extern osThreadId UACEncTaskID;

extern INT32U send_video;
extern INT32U send_audio;

extern INT8U USB_UVC_UC_DeviceDescriptor[];
extern INT8U USB_UVC_UC_Qualifier_Descriptor_TBL[];
extern INT8U USB_UVC_UC_ConfigDescriptor[];
extern INT8U UVC_UC_String0_Descriptor[];
extern INT8U UVC_UC_String1_Descriptor[];
extern INT8U UVC_UC_String2_Descriptor[];
extern INT8U UVC_UC_String3_Descriptor[];
extern INT8U UVC_UC_String4_Descriptor[];
extern INT8U UVC_UC_String5_Descriptor[];
//static INT32U yuvidx = 1;
extern INT8U connect_2_host;
extern INT8U USBD_Task_Qhandle_type;
extern INT8U connect_2_host;
extern INT16S *mic_data_buffer[MAX_BUFFER_NUM];
extern MDSC_LUN_STORAGE_DRV const gp_msdc_sd1;
extern INT8U *scsi_inquirydata_ptr[];
extern INT8U Default_scsi_inquirydata[];
extern INT8U Default_scsi_inquirydata_CDROM[];
/******************************************************
    Functions declaration
*******************************************************/
extern void uac_aud_enc_task_entry(void const *para);
extern void UVCTask(void const *param);
extern void usbd_state_uac_iso_in(INT32U event);
extern void _usbd_ep5_send_done_cbk(void);
extern void _usbd_ep7_send_done_cbk(void);
extern void _usbd_get_set_interface(INT32U video, INT32U audio);
extern void usbd_detect_tmrisr(void);
//=========================================================================================================

void usbd_uvc_uc_init(void)
{
	INT8U lun_type;

    osThreadDef_t uac_enc_task = {"uac_enc_task", uac_aud_enc_task_entry, osPriorityHigh, 1, USBD_UAC_ENC_STATE_STACK_SIZE};
	osThreadDef_t uvc_task = {"uvc_task", UVCTask, osPriorityNormal, 1, UVCSTACKSIZE};
	osMessageQDef_t uac_enc_q_def = {USBD_UAC_ENC_QUEUE_MAX_LEN, sizeof(INT32U), 0};
	osMessageQDef_t uvc_q = {UVC_Q_MAX_SIZE, sizeof(INT32U), 0};
	INT32S ret;

    usb_uac_aud_ctlblk.uac_enc_aud_q = osMessageCreate(&uac_enc_q_def, NULL);
    UACEncTaskID = osThreadCreate(&uac_enc_task, NULL);
	if(UACEncTaskID == NULL)
	{
        DBG_PRINT("uac_enc_task thread create failed\r\n");
    }

    /* uphy resume */
    drv_l1_usbd_uphy_suspend(0);
	/* switch to USB device mode  bit8 = 0 */
 	rSYS_CTRL_NEW &= ~(1 << 8);
    drv_l1_usbd_uphy_keep_from_io_floating();

	/* Init USBD L2 protocol layer first, including control/bulk/ISO/interrupt transfers */
    /******************************* Control transfer ************************************/
    ret = drv_l2_usbd_ctl_init();
    if(ret == STATUS_FAIL)
    {
        DBG_PRINT("drv_l2_usbd_ctl_init failed!\r\n");
        return;
    }

    /* Register new descriptor table here, this action must be done after drv_l2_usbd_ctl_init() */
    drv_l2_usbd_register_descriptor(REG_DEVICE_DESCRIPTOR_TYPE, (INT8U*) USB_UVC_UC_DeviceDescriptor);
    drv_l2_usbd_register_descriptor(REG_CONFIG_DESCRIPTOR_TYPE, (INT8U*) USB_UVC_UC_ConfigDescriptor);
    drv_l2_usbd_register_descriptor(REG_DEVICE_QUALIFIER_DESCRIPTOR_TYPE, (INT8U*) USB_UVC_UC_Qualifier_Descriptor_TBL);
    drv_l2_usbd_register_descriptor(REG_STRING0_DESCRIPTOR_TYPE, (INT8U*) UVC_UC_String0_Descriptor);
    drv_l2_usbd_register_descriptor(REG_STRING1_DESCRIPTOR_TYPE, (INT8U*) UVC_UC_String1_Descriptor);
    drv_l2_usbd_register_descriptor(REG_STRING2_DESCRIPTOR_TYPE, (INT8U*) UVC_UC_String2_Descriptor);
	drv_l2_usbd_register_descriptor(REG_STRING3_DESCRIPTOR_TYPE, (INT8U*) UVC_UC_String3_Descriptor);
	drv_l2_usbd_register_descriptor(REG_STRING4_DESCRIPTOR_TYPE, (INT8U*) UVC_UC_String4_Descriptor);
	drv_l2_usbd_register_descriptor(REG_STRING5_DESCRIPTOR_TYPE, (INT8U*) UVC_UC_String5_Descriptor);

    lun_type = LUN_SDC_TYPE;
    drv_l2_usbd_msdc_set_lun(LUN_SDC_TYPE, LUN_NUM_0, USBD_STORAGE_NO_WPROTECT, &gp_msdc_sd1);

    /* Init USBD L1 register layer */
    ret = drv_l1_usbd_init();
    if(ret == STATUS_FAIL)
    {
        DBG_PRINT("drv_l1_usbd_init failed!\r\n");
        return;
    }

   	/* Init USBD L2 of UVC */
    ret = drv_l2_usbd_uvc_init();
    if(ret == STATUS_FAIL)
    {
        DBG_PRINT("drv_l2_usbd_uvc_init failed!\r\n");
        return;
    }

    /* Register EP5 sending done call back function */
    drv_l2_usbd_resigter_uvc_frame_done_cbk(_usbd_ep5_send_done_cbk);

    /* Register EP7 sending done call back function */
    //drv_l2_usbd_resigter_uac_frame_done_cbk(_usbd_ep7_send_done_cbk);
    drv_l2_usbd_resigter_uac_frame_done_cbk(usbd_state_uac_iso_in);
    /* Register set interface call back */
	drv_l2_usbd_resigter_set_interface_cbk(_usbd_get_set_interface);

	UVCTaskID = osThreadCreate(&uvc_task, (void*)NULL);

	if(UVCTaskID == NULL)
	{
        DBG_PRINT("uvc_task thread create failed\r\n");
    }

	UVCTaskQ = osMessageCreate(&uvc_q, NULL);

	if(UVCTaskQ == NULL)
	{
		DBG_PRINT("UVCTaskQ create failed\r\n");
	}

    /* Init USBD L1 register layer */
    ret = drv_l1_usbd_uvc_init();
     if(ret == STATUS_FAIL)
    {
        DBG_PRINT("drv_l1_usbd_uvc_init failed!\r\n");
        return;
    }

	/* Init MSDC driver */
    ret = drv_l2_usbd_msdc_init();
    if(ret == STATUS_FAIL)
    {
        /* Init failed, do uninit procedures */
        drv_l2_usbd_msdc_uninit();
        DBG_PRINT("drv_l2_usbd_msdc_uninit failed!\r\n");
        return;
    }
	scsi_inquirydata_ptr[0] = (INT8U *)Default_scsi_inquirydata;

	/* Register SCSI inquiry data pointer, it must be done after drv_l2_usbd_msdc_init() */
	drv_l2_usbd_msdc_register_scsi_inquiry_data(scsi_inquirydata_ptr, (INT8U*)Default_scsi_inquirydata_CDROM);


	/* register USBD ISR handler */
	drv_l1_usbd_enable_isr();

    DBG_PRINT("USB UVC device init completed\r\n");
}

void usbd_uvc_uc_uninit(void)
{
    INT8U i;

	drv_l2_usbd_ctl_uninit();
	drv_l2_usbd_uvc_uninit();
	drv_l1_clock_set_system_clk_en(CLK_EN1_USBD20, 0);

    for(i=0;i<MAX_BUFFER_NUM;i++)
    {
        if(mic_data_buffer[i] != NULL)
        {
            gp_free(mic_data_buffer[i]);
            mic_data_buffer[i] = NULL;
        }
    }

	send_video = 0;
	send_audio = 0;
}

void usbd_uvc_simple_demo(void)
{
    drv_l1_clock_set_system_clk_en(CLK_EN1_USBD20, 1);
    drv_l1_clock_set_system_clk_en(CLK_EN0_ISP, 1);
    drv_l1_clock_set_system_clk_en(CLK_EN0_CODEC_ADC_I2SRX, 1);
    drv_l1_clock_set_system_clk_en(CLK_EN1_DISPAY_OUT, 1);

    usbd_uvc_uc_init();

}

//==================================================================================
//         USB DEVICE UVC Entry
//==================================================================================
void usbd_uvc_demo(void)
{
    INT32U msg_id;
    osStatus status=STATUS_OK;

    DBG_PRINT("/**********************************************/\r\n");
    DBG_PRINT("/* 1. USBD Q handle should be configured as C_USBD_TASK_QHDL_UVC  /\r\n");
    DBG_PRINT("/* in order to run corrponding demo UAC/ UVC/ MSDC                /\r\n");
    DBG_PRINT("/* 2. plug in detection pin is on IO_A12 by default               /\r\n");
    DBG_PRINT("/**********************************************/\r\n");


    hUSBD_UVC_TaskQ = xQueueCreate(USB_DEVICE_TASK_QUEUE_MAX, sizeof(INT32U));
    drv_l2_usbd_register_userpara((INT8U *)USBD_USER_PARA);
    drv_l2_usbd_detio_init();
    timer_freq_setup(USBD_USING_TIMER, C_5MS, 0, drv_l2_usbd_user_tmrisr);
    usbd_uvc_simple_demo();
    while(1)
    {
        if(hUSBD_UVC_TaskQ && (xQueueReceive(hUSBD_UVC_TaskQ, &msg_id, 0) == pdTRUE))
        {
            switch(msg_id)
            {
                case MSG_USB_DEVICE_INSERT:
                    connect_2_host = 1;
                    usbd_userpara.tmrflagOnOff = 1;
                    uvc_video_start();
                break;

                case MSG_USB_DEVICE_REMOVE:
                    msg_id = USBD_STOP_VIDEO_EVENT;
                    status = osMessagePut(UVCTaskQ, (INT32U)&msg_id, osWaitForever);
                    if(status != osOK)
                    {
                        DBG_PRINT("uvc-demo send event to UVCTaskQ failed\r\n");
                    }
                    uvc_video_stop();
                    usbd_userpara.tmrflagOnOff = 0;
                    connect_2_host = 0;
                break;

                case MSG_USB_DEVICE_TMR_TIMEOUT:

                break;

                default:

                break;
            }
        }
    }
}

#include "application.h"
#include "drv_l2_ad_key_scan.h"
#include "drv_l1_gpio.h"
#include "vr_demo_global.h"

#define TTS_LIB_EXIST   1
#define VR_LIB_EXIST    1
//#define KEY_TEST_DEF    1

#if TTS_LIB_EXIST
#include "ptts.h"
#endif

#define USE_ADKEY 1
#define USE_DISK FS_SD1
#define MAX_BUFFER_NUM 6 // Number of buffers
#define MAX_TASKQ_NUM 3
#define C_STACK_SIZE 768
#define DAC_LENGTH 1*1024*2 //buffer size 8*1024 point
#define DAC_MONO 1 //1: MONO, 2: Stereo

#define SAMPLE_RATE 16000
#define PITCH_VALUE 60
#define VELOCITY_VALUE 60
#define DAC_PGA 32  //volume range[0:63]
#define PRELOAD_BUFFER_COUNT 3 //must PRELOAD_BUFFER_COUNT < MAX_BUFFER_NUM
#define DATA_SIGNED 1

#define WRITE_2_SDC		0

INT32U *buffer_tts[MAX_BUFFER_NUM];

xQueueHandle empty_buffer_q;
xQueueHandle play_buffer_q;
xQueueHandle aud_out_dma_q;

struct sfn_info file_text_info_bg;
struct sfn_info file_text_info_fg;

int r_int_flag = 0;
int l_int_flag = 0;
int flag_dac_play = 0;
int dac_flag = 0xff;
INT16S file_text_read_fg,file_text_read_bg;
INT32U dac_q_r_idx=0;
INT32U dac_q_w_idx=0;
INT32U spu_q_idx=0;
INT32U dac_index_file=0;
INT32U end_size = 0;

void play_file (INT16S *buf_addr);

// TTS related defines
#define TTS_MAX_SAMPLES (DAC_LENGTH / sizeof(short))

int flag_tts_play = 0;
INT16S file_text_write;
char* tts_script;
char* test_string;

// Counter used to trim the last buffer. Is only covers the case where the last buffer is not full.
// If other buffers are not full a more elaborate system is required.
int buff_count = 0;

int song_flag = 0;


void play_tts (INT16S *buf_addr);
void load_tts_script(const char* file_name);

static INT32U total_t = 0;
static INT32U tts_time = 0;

void show_menu(void)
{
	DBG_PRINT("KEY1: Play TTS script 1 on SD card \r\n");
	DBG_PRINT("KEY2: Play TTS script 1 on SD card \r\n");
	DBG_PRINT("KEY3: Stop TTS \r\n");
	DBG_PRINT("KEY4: Start VR + TTS \r\n");
	DBG_PRINT("KEY5: Stop VR + TTS \r\n");
	DBG_PRINT("KEY6: Recording a VR command \r\n");
	DBG_PRINT("KEY7: Training a VR command \r\n");
	DBG_PRINT("KEY8: Erase trained VR commands \r\n");
}

void vr2tts_callback(INT32U idx, const char* string)
{
	INT8U err;
	INT32U i;


	if((dac_flag==0xff)&(flag_dac_play == 0))
	{
		xQueueReset(empty_buffer_q);
		xQueueReset(play_buffer_q);

		song_flag = 2;
		if(idx == 17){
#if TTS_LIB_EXIST
			load_tts_script("F:\\song.txt");
			p_tts_StartTTS(tts_script, 0, P_TTS_PLAIN_TEXT);
#endif
		}
		else if(idx == 18){
#if TTS_LIB_EXIST
			p_tts_SetCharacter("robot");
			p_tts_StartTTS(string, 0, P_TTS_PLAIN_TEXT);
#endif
		}
		else{
#if TTS_LIB_EXIST
			if((idx == 0) ||(idx == 4) ||(idx == 5) ||(idx == 11) ||(idx == 14) || (idx >= 20)){
				p_tts_SetCharacter("Default");
				p_tts_SetEmotion("Excited");
			}
			else if((idx == 1) ||(idx == 3) ||(idx == 6) ||(idx == 7) ||(idx == 8)){
				p_tts_SetCharacter("younggirl");
				p_tts_SetEmotion("Emotional");
			}
			else if((idx == 2) ||(idx == 12) ||(idx == 13)){
				p_tts_SetCharacter("giant");
				p_tts_SetEmotion("Neutral");
			}
			else {
				p_tts_SetCharacter("boy");
				p_tts_SetEmotion("Surprised");
			}
			p_tts_StartTTS(string, 0, P_TTS_PLAIN_TEXT);
#endif
		}
		dac_index_file = 0;
		dac_q_r_idx = 0;
		dac_q_w_idx = 0;

		flag_dac_play = 1;
		flag_tts_play = 1;

		for(i = 0; i < MAX_BUFFER_NUM; i++)
		{
			xQueueSend(empty_buffer_q, &buffer_tts[i], portMAX_DELAY);
		}
		buff_count = 0;
}

}

void show_tts_char_menu(void)
{
	DBG_PRINT("select a character from 1 to 5\r\n");
	DBG_PRINT("1: man \r\n");
	DBG_PRINT("2: woman \r\n");
	DBG_PRINT("3: young girl \r\n");
	DBG_PRINT("4: robot \r\n");
	DBG_PRINT("5: giant \r\n");
	DBG_PRINT("other: default \r\n");
}


INT16U signed_to_unsigned(INT16S di)
{
	#if DATA_SIGNED == 1
		di^= 0x8000;
	if(di==0xffff)
		di--;
	#endif

	return di;
}

INT32U cvt_dbf_idx(INT32U idx_in)
{
	INT32U ret_idx = idx_in;
	INT32U ct2 = 0;

DEC:
	if(ret_idx <= 0)
	{
		ret_idx = MAX_BUFFER_NUM - 1;

	}
	else
	{
		ret_idx--;
	}
	return ret_idx;

}

void cnt_idx_r_dac(void)
{
	dac_q_r_idx++;

	if(dac_q_r_idx >= MAX_BUFFER_NUM)
	{
		dac_q_r_idx = 0;
	}
}

void cnt_idx_w_dac(void)
{
	dac_q_w_idx++;

	if(dac_q_w_idx >= MAX_BUFFER_NUM)
	{
		dac_q_w_idx = 0;
	}
}

void inc_cnt_w_dac(void)
{
	dac_q_w_idx++;
}

void dec_cnt_w_dac(void)
{
	if(dac_q_w_idx>0)
		dac_q_w_idx--;

}

void load_tts_script(const char* file_name)
{
	// Open and stat the file.
	file_text_read_fg = fs_open((char *)file_name, O_RDONLY);
	sfn_stat(file_text_read_fg, &file_text_info_fg);

	// Allocate file size bytes + 1 for the NULL char.
	tts_script = gp_malloc(file_text_info_fg.f_size + 1);

	// Read the entire file into memory for simplicity and close the handle.
	fs_read(file_text_read_fg, (unsigned int)tts_script, file_text_info_fg.f_size);
	fs_close(file_text_read_fg);

	// Make sure the buffer ends with a NULL char.
	tts_script[file_text_info_fg.f_size] = '\0';

	//DBG_PRINT("%s ",tts_script);
}

void stop_play_tts(void)
{
    if((dac_flag == 0x02)&(flag_dac_play == 1))//check DAC is playing
    {
        flag_tts_play = 0;
    }
    osDelay(500);

}

void File_to_buffer_task_entry(void *p_arg)
{
	INT16S *buf_addr;
	INT32S ret;
	INT8U err;
	int i;
	INT32U flag_tts=0;
	char path[24]="A:\\tts_in_ggg.txt";
	INT32U index_file=0;
	int testfd;
	INT16U vr_is_on=0;

	DBG_PRINT("File_to_buffer_task_entry\r\n");
	test_string = gp_malloc(2048);

	show_menu();

	while(1)
	{
		if(_devicemount(USE_DISK))					// Mount device
		{
			DBG_PRINT("Mount Disk Fail[%d]\r\n", USE_DISK);
		}
		else
		{
			DBG_PRINT("Mount Disk success[%d]\r\n", USE_DISK);

			// Initialize tts_db access after SD card is mounted if library is read from SD card.
#if TTS_LIB_EXIST
			ret = EXTFLASH_Init();
			if(ret < 0)
				DBG_PRINT("TTS engine error \r\n");
			(void) p_tts_InitTTS( NULL, NULL );
#endif
			break;
		}
	}

#if LED_EXIST
    gpio_init_io(IO_A15, GPIO_OUTPUT);
    gpio_write_io(IO_A15, DATA_HIGH);
#endif
	//CSpotter
#if VR_LIB_EXIST
	VrDemoGlobalInit();
	VrTtsEnable(VR_TTS);
	HardwareInitialize();
#endif

	adc_key_scan_init();

    //p_tts_StartTTS("This is T T S and voice recognition demo on G P M 4 \0", 0, P_TTS_PLAIN_TEXT);
#if TTS_LIB_EXIST
	p_tts_InitTTS( NULL, NULL );
    vr2tts_callback(20,"This is T T S and voice recognition demo on G P M 4 \0");
#endif


	while(1)
	{
#ifndef KEY_TEST_DEF
		adc_key_scan();
#endif
		if(ADKEY_IO1)
		{
			stop_play_tts();
#if VR_LIB_EXIST
			TestVR_Stop();
			vr_is_on = 0;
#endif
			if((dac_flag==0xff)&(flag_dac_play == 0))
			{
                ADKEY_IO1 = 0;
                DBG_PRINT("Play TTS script 1 on sd card \r\n");
				xQueueReset(empty_buffer_q);
				xQueueReset(play_buffer_q);
				#if TTS_LIB_EXIST
				p_tts_InitTTS( NULL, NULL );
				#endif

				// Load TTS text in one go. Other approaches may also be used.
#if TTS_LIB_EXIST
				load_tts_script("F:\\tts_in.txt");
#endif
#if 0
                file_text_read_fg = fs_open(path, O_RDONLY);
                fs_sfn_stat(file_text_read_fg, &file_text_info_fg);
                DBG_PRINT("file_text_info_fg %d\r\n",file_text_info_fg.f_size);
#endif
				// Provide the text to the TTS engine.
#if TTS_LIB_EXIST
				tts_time = xTaskGetTickCount();
				p_tts_StartTTS(tts_script, 0, P_TTS_PLAIN_TEXT);
#endif
				// Create a file to also output the TTS sound to a file to verify quality.
#if WRITE_2_SDC == 1
				file_text_write = fs_open("F:\\tts.drm", O_CREAT | O_TRUNC | O_RDWR);
#endif
				song_flag = 0;
				DBG_PRINT("open tts_in.txt \r\n");
				dac_index_file = 0;
				dac_q_r_idx = 0;
				dac_q_w_idx = 0;

				flag_dac_play = 1;
				flag_tts_play = 1;
				//flag_tts_play = 0;//play file

				for(i = 0; i < MAX_BUFFER_NUM; i++)
				{
					gp_memset((INT8S *)buffer_tts[i],0x80,DAC_LENGTH);
					xQueueSend(empty_buffer_q, &buffer_tts[i], portMAX_DELAY);
				}
				buff_count = 0;
			}


		}
		if(ADKEY_IO2)
		{
			DBG_PRINT("Play TTS script 2 on sd card \r\n");
			stop_play_tts();
#if VR_LIB_EXIST
			TestVR_Stop();
			vr_is_on = 0;
#endif
			if((dac_flag==0xff)&(flag_dac_play == 0))
			{
				xQueueReset(empty_buffer_q);
				xQueueReset(play_buffer_q);
#if TTS_LIB_EXIST
				p_tts_InitTTS( NULL, NULL );
#endif
				// Load TTS text in one go. Other approaches may also be used.
				load_tts_script("F:\\song.txt");

				// Provide the text to the TTS engine.
#if TTS_LIB_EXIST
				p_tts_StartTTS(tts_script, 0, P_TTS_PLAIN_TEXT);
#endif
				// Create a file to also output the TTS sound to a file to verify quality.
#if WRITE_2_SDC == 1
				file_text_write = fs_open("F:\\tts.drm", O_CREAT | O_TRUNC | O_RDWR);
#endif
				song_flag = 0;
				DBG_PRINT("open song.txt \r\n");
				dac_index_file = 0;
				dac_q_r_idx = 0;
				dac_q_w_idx = 0;

				flag_dac_play = 1;
				flag_tts_play = 1;

				for(i = 0; i < MAX_BUFFER_NUM; i++)
				{
					gp_memset((INT8S *)buffer_tts[i],0x80,DAC_LENGTH);
					xQueueSend(empty_buffer_q, &buffer_tts[i], portMAX_DELAY);
				}
				buff_count = 0;
			}


		}
		if(ADKEY_IO3)
		{

			DBG_PRINT("Stop VR + TTS \r\n");
            stop_play_tts();
			vr2tts_callback(20,"Stop T T S and Voice recognition demo \0");
#if VR_LIB_EXIST
			TestVR_Stop();
			vr_is_on = 0;
#endif
		}

		if(ADKEY_IO4)
		{
			if(vr_is_on == 0)
			{
                DBG_PRINT("Start VR + TTS \r\n");
                stop_play_tts();
                vr2tts_callback(20,"start voice recognition \0");
                osDelay(1800);
#if VR_LIB_EXIST
                TestVR_Start() ;
                vr_is_on = 1;
#endif
            }
            else
            {
#if VR_LIB_EXIST
                TestVR_Stop();
                vr_is_on = 0;
#endif
                DBG_PRINT("Stop VR + TTS \r\n");
                stop_play_tts();
                vr2tts_callback(20,"stop voice recognition \0");
            }

		}

		if(ADKEY_IO5)
		{
			DBG_PRINT("Stop VR + TTS \r\n");
#if VR_LIB_EXIST
			TestVR_Stop();
			vr_is_on = 0;
#endif
            stop_play_tts();
            vr2tts_callback(20,"Stop T T S and Voice recognition demo \0");
		}
		if(ADKEY_IO6)
		{
			DBG_PRINT("Recording a command \r\n");
#if VR_LIB_EXIST
			TestVR_Stop();
			vr_is_on = 0;
#endif
            stop_play_tts();
            vr2tts_callback(20,"Start Recording a command \0");
            osDelay(2000);
			TrainSD_Start();
			while(vr_status_polling())
				osDelay(1);
			//ADKEY_IO7 = 1;
			osDelay(500);
			TrainSD_Stop();
			vr2tts_callback(20,"Command is trained\0");
			gpio_write_io(IO_A15,DATA_HIGH);
		}
		if(ADKEY_IO7)
		{
			DBG_PRINT("Training a command \r\n");
#if VR_LIB_EXIST
			//TrainSD_Stop();
#endif
		}
		if(ADKEY_IO8)
		{
#if VR_LIB_EXIST
			TestVR_Stop();
			vr_is_on = 0;
#endif
            stop_play_tts();
            vr2tts_callback(20,"Erase all trained commands \0");
			DBG_PRINT("Erase all trained commands \r\n");
#if VR_LIB_EXIST
			TrainSD_Erase();
#endif
		}
	}
}


void DAC_to_buffer_task_entry(void *p_arg)
{
	INT16S *buf_addr;
	INT8U err;

	//DBG_PRINT("DAC_to_buffer_task_entry\r\n");

	while(1)
	{

		//check buffer
		err = (INT8U) xQueueReceive(empty_buffer_q, &buf_addr, portMAX_DELAY);
        if(err != pdPASS) {
            DBG_PRINT("%x ",err);
        }
		else
		{
			if(flag_dac_play == 1)
			{
				play_tts(buf_addr);
				//play_file(buf_addr);
#if 0
				if (flag_tts_play == 1)
				{
					play_tts(buf_addr);
				}
				else
				{
					DBG_PRINT("play file \r\n");
					//play_file(buf_addr);
				}
#endif
			}
			else
			{
				//err = OSQPost(empty_buffer_q, (void *) buf_addr);
				osDelay(1);
			}
		}
	}
    osDelay(1);
}

void play_file (INT16S *buf_addr)
{
	INT32S ret;
	INT8U err;

	//read file
	if(file_text_info_fg.f_size < DAC_LENGTH)
	{
		ret = (INT32S)fs_read(file_text_read_fg, (INT32U)(buf_addr), file_text_info_fg.f_size);
		dac_index_file += file_text_info_fg.f_size;//add index offset to next block
		DBG_PRINT("s fsize 0x%x \r\n",file_text_info_fg.f_size);
		end_size = file_text_info_fg.f_size;
	}
	else
	{
		if((dac_index_file + DAC_LENGTH) < file_text_info_fg.f_size)
		{
			ret = (INT32S)fs_read(file_text_read_fg, (INT32U)(buf_addr), DAC_LENGTH);
		}
		else
		{
			ret = (INT32S)fs_read(file_text_read_fg, (INT32U)(buf_addr), file_text_info_fg.f_size-dac_index_file );
			end_size = file_text_info_fg.f_size-dac_index_file;
		}

		dac_index_file += DAC_LENGTH; //add index offset to next block
	}


		inc_cnt_w_dac(); // count number of ready data in play buffer
		xQueueSend(play_buffer_q, &buf_addr, portMAX_DELAY);


	buff_count++;

	if (dac_index_file == DAC_LENGTH * PRELOAD_BUFFER_COUNT)
	{
		dac_flag = 0x0;
		xQueueSend(aud_out_dma_q, &buf_addr, portMAX_DELAY);
	}

	if (dac_index_file >= file_text_info_fg.f_size) //end of file
	{
		flag_dac_play = 0;
		dac_index_file = 0;
		DBG_PRINT("end fg\r\n");
		ret = fs_close(file_text_read_fg);

		if(ret < 0)
		{
			DBG_PRINT("end fg failed[%d]\r\n", _getfserrcode());
		}
	}
}

void stop_tts(void)
{
	flag_dac_play = 0;
	dac_index_file = 0;
	flag_tts_play = 0;
#if TTS_LIB_EXIST
	p_tts_StopTTS();
#endif
    DBG_PRINT("total_t = %d \r\n",total_t);
    total_t = 0;
    tts_time = xTaskGetTickCount() - tts_time;
    DBG_PRINT("total_t = %d \r\n",tts_time);
    tts_time = 0;
}

void play_tts (INT16S *buf_addr)
{
	INT8U err;
	int samples = 0;
	unsigned int events = 0;

    INT32U t1;


	// Flush the previous TTS samples and collect samples into the buffer.
	if(flag_tts_play == 1){
#if TTS_LIB_EXIST
		t1 = xTaskGetTickCount();
		p_tts_FlushSamples();
		p_tts_CollectSamples(buf_addr, TTS_MAX_SAMPLES);

		samples = p_tts_GetNumSamples(&events);
		t1 = xTaskGetTickCount() - t1;
		total_t += t1;
#endif

	}
	else{
		// Stop and free resources.
		stop_tts();
		xQueueReset(play_buffer_q);
		dac_q_w_idx = 0;
		return;
	}

#if TTS_LIB_EXIST
	if (samples > 0 || !p_tts_IsFinished())
#else
    if (samples > 0 )
#endif
	{
		// It might be that TTS engine has not finished yet, but has no samples available.
		// If there are samples, post the buffer as full.
		if (samples > 0)
		{
#if WRITE_2_SDC == 1
			// Dump samples to the file.
			fs_write(file_text_write, buf_addr, samples * sizeof(short));
			// Or use the frame size to check for buffer underrun on the recording.
			//write(file_text_write, buf_addr, DAC_LENGTH);
#endif
			// Update the data counters.

			buff_count++;
			inc_cnt_w_dac();
			xQueueSend(play_buffer_q, &buf_addr, portMAX_DELAY);

			end_size = samples * sizeof(short);
			dac_index_file += end_size;

			// Set the flag indicating the buffer pre-filling is ready.

			if (dac_index_file == DAC_LENGTH * PRELOAD_BUFFER_COUNT)
			{
				dac_flag = 0x0;
				DBG_PRINT("$");
				xQueueSend(aud_out_dma_q, &buf_addr, portMAX_DELAY);

			}
		}
	}
	else
	{
		// Stop and free resources.
		DBG_PRINT("end of TTS \r\n");
		DBG_PRINT("\n");
		stop_tts();

#if WRITE_2_SDC == 1
		fs_close(file_text_write);
#endif
		if(song_flag != 2)
			gp_free(tts_script);
	}
}

void Audio_DAC_task_entry(void *p_arg)
{
	INT16S *play_addr;
	INT8U err;
	INT8S dac_notify[2]={1};
	INT32U write_end = 0;
	INT16S tri[16];
	INT32U flag_last_end;
	#if DAC_MONO == 2
	INT16S triB[16];
	INT32U idx[2]={0};
	#endif
	INT32U index_buf=0;
	INT32U msg_id;
	int i;

	DBG_PRINT("Play_file_task_entry \r\n");

	drv_l1_dac_pga_set(DAC_PGA);
	DBG_PRINT("PGA gain 0x%x\r\n",drv_l1_dac_pga_get());

	drv_l1_dac_data_signed_set(1);
	drv_l1_dac_sample_rate_set(SAMPLE_RATE);
	#if DAC_MONO == 1
	drv_l1_dac_mono_set();
	#else
	drv_l1_dac_stereo_set();
	#endif
	drv_l1_dac_enable_set(1);



	while(1)
	{

        err = (INT8U) xQueueReceive(aud_out_dma_q, &msg_id, portMAX_DELAY);
        if(err != pdPASS) {
            DBG_PRINT("%x ",err);
        }
		if(dac_flag == 0)
		{
			err = (INT8U) xQueueReceive(play_buffer_q, &play_addr, portMAX_DELAY);
            if(err != pdPASS) {
                DBG_PRINT("%x ",err);
            }
			dac_flag = 1;
			flag_last_end = 0;
			drv_l1_dac_cha_dbf_put((INT16S *)play_addr, DAC_LENGTH/2, aud_out_dma_q);
			cnt_idx_r_dac();
			dec_cnt_w_dac();
		} else if(dac_flag == 1) {
			err = (INT8U) xQueueReceive(play_buffer_q, &play_addr, portMAX_DELAY);
            if(err != pdPASS) {
                DBG_PRINT("%x ",err);
            }
			dac_flag = 2;
			// dac start
			drv_l1_dac_cha_dbf_set((INT16S *)play_addr, DAC_LENGTH/2);
			err = xQueueSend(empty_buffer_q, &buffer_tts[cvt_dbf_idx(dac_q_r_idx)], portMAX_DELAY);
			if(err != pdPASS)
			{
				DBG_PRINT("return buffer error\r\n");
			}
			cnt_idx_r_dac();
			dec_cnt_w_dac();
			if(song_flag == 1)
				DBG_PRINT("%s",tts_script);
//			else if (song_flag == 2)
//				DBG_PRINT("%s",test_string);
		}
		else if(dac_flag == 2)
		{
			if (msg_id == 1)
			{
				err = xQueueSend(empty_buffer_q, &buffer_tts[cvt_dbf_idx(dac_q_r_idx)], portMAX_DELAY);
				if(err != pdPASS)
				{
					DBG_PRINT("return buffer error\r\n");
				}

				buff_count--;

				if((flag_dac_play == 0)&( dac_q_w_idx == 0))
				{
					while(drv_l1_dac_dma_status_get() == 1) {
						osDelay(2);
					}
					drv_l1_dac_dbf_free(); /* release dma channel */
					dac_flag = 0xFF;
				}
				else{

					err = (INT8U) xQueueReceive(play_buffer_q, &play_addr, portMAX_DELAY);
                    if(err != pdPASS) {
                        DBG_PRINT("%x ",err);
                    }
				}
				if(err != pdPASS || play_addr == 0)
				{
					DBG_PRINT("N");
					if(flag_dac_play == 0)
					{
						while(drv_l1_dac_dma_status_get() == 1)
						{
							osDelay(2);
						}

						drv_l1_dac_dbf_free(); /* release dma channel */
						dac_flag = 0xFF;
						err = pdPASS;
					}
				}
				else
				{
					cnt_idx_r_dac();
					dec_cnt_w_dac();
					// Trim before switching to the last buffer.
					if((flag_dac_play == 0)&( dac_q_w_idx == 0))
					{
						drv_l1_dac_cha_dbf_set((INT16S *)play_addr, end_size / 2);
					}
					else
					{
						drv_l1_dac_cha_dbf_set((INT16S *)play_addr, DAC_LENGTH / 2);
					}
				}
			}

		}
		else
		{
			dac_flag = 0xFF;
			DBG_PRINT("dac_flag = %x \r\n",dac_flag);
		}

	}
}

#if KEY_TEST_DEF
void Key_test_task_entry(void)
{
    DBG_PRINT("Key test task is running \r\n");
    while(1)
    {
        if((dac_flag==0xff)&(flag_dac_play == 0))
            ADKEY_IO1 = 1;
        osDelay(10);
    }
}
#endif
void TTS_VR_Demo(void)
{
	INT32S err,i;
	DBG_PRINT("TTS Demo\r\n");

	aud_out_dma_q = xQueueCreate(MAX_BUFFER_NUM, sizeof(INT32U));
	empty_buffer_q = xQueueCreate(MAX_BUFFER_NUM, sizeof(INT32U));
	play_buffer_q = xQueueCreate(MAX_BUFFER_NUM, sizeof(INT32U));

	for (i=0; i < MAX_BUFFER_NUM; i++)
	{
		buffer_tts[i] = (INT32U*) gp_malloc_align(DAC_LENGTH, 4);
		DBG_PRINT("buffer_tts[%d]=0x%x\r\n", i ,buffer_tts[i]);
	}

	err = xTaskCreate(File_to_buffer_task_entry, "File_to_Buf_Task", C_STACK_SIZE, NULL, 1, NULL);
	if(err != pdPASS) return;

	err = xTaskCreate(Audio_DAC_task_entry, "Aud_Dac_Task", C_STACK_SIZE, NULL, 3, NULL);
	if(err != pdPASS) return;

	err = xTaskCreate(DAC_to_buffer_task_entry, "DAC_to_Buf_Task", C_STACK_SIZE, NULL, 3, NULL);
	if(err != pdPASS) return;

#if KEY_TEST_DEF
	err = xTaskCreate(Key_test_task_entry, "Key_test_Task", C_STACK_SIZE, NULL, 0, NULL);
	if(err != pdPASS) return;
#endif
}



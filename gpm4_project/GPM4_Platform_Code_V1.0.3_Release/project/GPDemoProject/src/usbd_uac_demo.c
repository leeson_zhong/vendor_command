/******************************************************
* usbd_uac_demo.c
*
* Purpose: USBD demo for UAC
*
* Author: Eugene Hsu
*
* Date: 2013/03/10
*
* Copyright Generalplus Corp. ALL RIGHTS RESERVED.
*
* Version :
* History :
*
*******************************************************/
/*******************************************************
    Include file
*******************************************************/
#include "string.h"
#include "drv_l1_sfr.h"
#include "project.h"
#include "gplib.h"
#include "drv_l1_timer.h"
#include "drv_l1_uart.h"
#include "drv_l2_ad_key_scan.h"
#include "drv_l1_usbd.h"
#include "drv_l2_usbd.h"

#include "drv_l1_dac.h"
#include "usbd_uac.h"

#include "drv_l1_clock.h"

/******************************************************
    Definition and variable declaration
*******************************************************/
osThreadId UACEncTaskID = NULL;
osThreadId UACDecTaskID = NULL;
extern xQueueHandle hUSBD_UAC_TaskQ;
extern const INT8U uac_vol_max_descriptor[];
extern INT8U connect_2_host;
extern INT8U USBD_Task_Qhandle_type;
extern INT16S *mic_data_buffer[MAX_BUFFER_NUM];
extern xQueueHandle KeyTaskQ;
/******************************************************
    Extern functions declaration
*******************************************************/
extern INT32S drv_l2_usbd_ctl_uac_init(void);
extern INT32S drv_l1_usbd_uac_init(void);
extern INT32S drv_l2_usbd_uac_uninit(void);
extern void drv_l2_usbd_register_uac_gain_cbk(USBD_L2_UAC_SET_GAIN_CALLBACK_FUN fun);
extern void drv_l2_usbd_register_hid_set_idle_id_cbk(USBD_L2_HID_SET_IDLE_CALLBACK_FUN fun);
extern void drv_l2_usbd_uac_keyscan(INT32U key_released);

extern void uac_aud_enc_task_entry(void const *para);
extern void uac_aud_dec_task_entry(void const *para);
extern void uac_aud_ctl_task_entry(void const *para);
extern uac_aud_dec_t usb_uac_aud_ctlblk;
extern void usbd_state_uac_iso_in(INT32U event);
extern void usbd_state_uac_iso_out(INT32U event);
extern void usbd_uac_misc_handle(INT32U event);
extern void usbd_state_uac_int_in(INT32U event);
extern void usbd_state_uac_int_out(INT32U event);
extern void uac_set_gain_cbk(INT32U cs, INT32S gain);
extern void hid_set_idle_cbk(INT32U id);
extern INT8U uac_get_gain_level(void);
extern void usbd_detect_tmrisr(void);

/*****************************************************
    USBD API
    Purpose: For upper layer application
		or other system module.
*****************************************************/
INT32U USBD_Detect_UAC_IO(void)
{
	return 1;
}

void USBD_UAC_Reinit(void)
{
	INT32S ret = STATUS_OK;

	ret = drv_l1_usbd_uac_init();

    if(ret == STATUS_FAIL)
    {
        DBG_PRINT("drv_l1_usbd_uac_init failed!\r\n");
        return;
    }

	DBG_PRINT("USBD_UAC_Reinit\r\n");
}

void USBD_UAC_Suspend(void)
{

	DBG_PRINT("USBD_UAC_Suspend\r\n");
}

INT32S USBD_Audio_Init(void)
{
	osThreadDef_t uac_enc_task = {"uac_enc_task", uac_aud_enc_task_entry, osPriorityHigh, 1, USBD_UAC_ENC_STATE_STACK_SIZE};
	osThreadDef_t uac_dec_task = {"uac_dec_task", uac_aud_dec_task_entry, osPriorityHigh, 1, USBD_UAC_DEC_STATE_STACK_SIZE};
	osMessageQDef_t uac_enc_q_def = {USBD_UAC_ENC_QUEUE_MAX_LEN, sizeof(INT32U), 0};
	osMessageQDef_t uac_dec_q_def = {USBD_UAC_DEC_QUEUE_MAX_LEN, sizeof(INT32U), 0};

	/* reset HID report status */
	usb_uac_aud_ctlblk.hidstatus = 0;

	/* Init microphone related data & hardware*/
	usb_uac_aud_ctlblk.pmicbuf = (INT8S*)gp_malloc_align(USBD_UAC_MIC_RING_BUFF_SIZE, 16);

	if(usb_uac_aud_ctlblk.pmicbuf == NULL)
	{
		DBG_PRINT("Allocate ISO IN ring buffer failed...");
		return STATUS_FAIL;
	}
	/* reset buffers */
	memset(usb_uac_aud_ctlblk.pmicbuf, 0, USBD_UAC_MIC_RING_BUFF_SIZE);

	usb_uac_aud_ctlblk.micframesize = USBD_UAC_MIC_PKT_SIZE;
	usb_uac_aud_ctlblk.microphone_sample_rate = USBD_UAC_MICROPHONE_SAMPLE_RATE;	/* 8000 Hz */
	usb_uac_aud_ctlblk.mic_read_index = 0;
	usb_uac_aud_ctlblk.mic_write_index = 0;

    /* Init UAC speaker audio Queue */
	usb_uac_aud_ctlblk.uac_enc_aud_q = osMessageCreate(&uac_enc_q_def, NULL);
	if(usb_uac_aud_ctlblk.uac_enc_aud_q == NULL)
	{
		DBG_PRINT("usb_uac_aud_ctlblk.uac_enc_aud_q create failed\r\n");
	}

	/* Create UAC audio encode task */
	UACEncTaskID = osThreadCreate(&uac_enc_task, NULL);

	if(UACEncTaskID == NULL)
	{
        DBG_PRINT("uac_enc_task thread create failed\r\n");
    }

	/* Init speaker related data & hardware */
	/* Alloc ISO OUT ring buffer, INT IN buffer */
	usb_uac_aud_ctlblk.ppcmbuf = (INT8S*)gp_malloc_align(USBD_UAC_SPEAKER_RING_BUFF_SIZE, 16);
	if(usb_uac_aud_ctlblk.ppcmbuf == NULL)
	{
		DBG_PRINT("Allocate ISO OUT ring buffer failed...");
		return STATUS_FAIL;
	}
	DBG_PRINT("spk buf 0x%x size 0x%x\r\n", usb_uac_aud_ctlblk.ppcmbuf, USBD_UAC_SPEAKER_RING_BUFF_SIZE);
	DBG_PRINT("mic buf 0x%x size 0x%x\r\n",usb_uac_aud_ctlblk.pmicbuf, USBD_UAC_MIC_RING_BUFF_SIZE);
	/* reset buffers */
	memset(usb_uac_aud_ctlblk.ppcmbuf, 0, USBD_UAC_SPEAKER_RING_BUFF_SIZE);

	usb_uac_aud_ctlblk.pcmframesize = USBD_UAC_SPEAKER_PKT_SIZE;				/* 192 bytes per ISO OUT packet */
	usb_uac_aud_ctlblk.channel = 2;												/* Support 2 channel */
	usb_uac_aud_ctlblk.speaker_sample_rate = USBD_UAC_SPEAKER_SAMPLE_RATE;		/* 48000 Hz */
	usb_uac_aud_ctlblk.read_index = 0;
	usb_uac_aud_ctlblk.write_index = 0;

	usb_uac_aud_ctlblk.altinterface1 = 0;
	usb_uac_aud_ctlblk.altinterface2 = 0;
	usb_uac_aud_ctlblk.uac_level = UAC_GAIN_TO_DAC_VOL(*(INT32U*)uac_vol_max_descriptor);
	/* Init UAC speaker audio Queue */
	usb_uac_aud_ctlblk.uac_dec_aud_q = osMessageCreate(&uac_dec_q_def, NULL);
	if(usb_uac_aud_ctlblk.uac_dec_aud_q == NULL)
	{
		DBG_PRINT("usb_uac_aud_ctlblk.uac_dec_aud_q create failed\r\n");
	}

	/* Create UAC audio decode task */
	UACDecTaskID = osThreadCreate(&uac_dec_task, NULL);

	if(UACDecTaskID == NULL)
	{
        DBG_PRINT("uac_dec_task thread create failed\r\n");
    }

	drv_l1_dac_pga_set(0x10);

	drv_l1_dac_fifo_level_set(1, 1);

	drv_l1_dac_sample_rate_set(usb_uac_aud_ctlblk.speaker_sample_rate);

	drv_l1_dac_stereo_set();

	return STATUS_OK;
}

INT32S USBD_Audio_Uninit(void)
{
	INT32S ret = STATUS_OK;
	INT8U i;

	if(usb_uac_aud_ctlblk.pmicbuf != NULL)
	{
		gp_free(usb_uac_aud_ctlblk.pmicbuf);
		usb_uac_aud_ctlblk.pmicbuf = NULL;
	}

	if(usb_uac_aud_ctlblk.ppcmbuf != NULL)
	{
		gp_free(usb_uac_aud_ctlblk.ppcmbuf);
		usb_uac_aud_ctlblk.ppcmbuf = NULL;
	}

    for(i=0;i<MAX_BUFFER_NUM;i++)
    {
        if(mic_data_buffer[i] != NULL)
        {
            gp_free(mic_data_buffer[i]);
            mic_data_buffer[i] = NULL;
        }
    }

	drv_l2_usbd_register_uac_gain_cbk(NULL);
	drv_l2_usbd_register_hid_set_idle_id_cbk(NULL);

	return ret;
}

void USBD_UAC_Init(void)
{
    INT32S ret;

    /* uphy resume */
    drv_l1_usbd_uphy_suspend(0);
 	/* switch to USB device mode  bit8 = 0 */
 	rSYS_CTRL_NEW &= ~(1 << 8);
    drv_l1_usbd_uphy_keep_from_io_floating();
 	DBG_PRINT("USBD_UAC_Init start...\r\n");

 	/* Init audio codec first */
 	ret = USBD_Audio_Init();
 	if(ret == STATUS_FAIL)
    {
        DBG_PRINT("USBD_Audio_Init failed!\r\n");
        return;
    }

    /* Init USBD L2 protocol layer first,control transfers */
    /******************************* Control transfer ************************************/
    ret = drv_l2_usbd_ctl_uac_init();
    if(ret == STATUS_FAIL)
    {
        DBG_PRINT("drv_l2_usbd_ctl_uac_init failed!\r\n");
        return;
    }

    /* Init ISO state handler */
    drv_l2_usbd_register_state_handler(USBD_XFER_ISO, NULL, usbd_state_uac_iso_in, usbd_state_uac_iso_out);

    /* Init interrupt state handler */
    drv_l2_usbd_register_state_handler(USBD_XFER_INT, NULL, usbd_state_uac_int_in, NULL);

    /* Init miscellaneous function handle*/
    drv_l2_usbd_register_state_handler(USBD_XFER_MISC, usbd_uac_misc_handle, NULL, NULL);

    /* Register UAC gain control call back function */
    drv_l2_usbd_register_uac_gain_cbk(uac_set_gain_cbk);

    /* Register HID set idle call back function */
    drv_l2_usbd_register_hid_set_idle_id_cbk(hid_set_idle_cbk);

	ret = drv_l1_usbd_uac_init();

    if(ret == STATUS_FAIL)
    {
        DBG_PRINT("drv_l1_usbd_uac_init failed!\r\n");
        return;
    }

	 /* Register USB device interrupt handle */
    ret = drv_l1_usbd_enable_isr();
    if(ret == STATUS_FAIL)
    {
        DBG_PRINT("register USBD interrupt handle failed!\r\n");
        return;
    }

    DBG_PRINT("USB UAC device init completed!\r\n");

    return;
}

INT32S USBD_UAC_Uninit(void)
{
    INT32S ret;

	/* Unit init audio module */
	USBD_Audio_Uninit();

    /* Uninit Layer 1 */
    ret = drv_l1_usbd_uninit();
    if(ret == STATUS_FAIL)
        return STATUS_FAIL;

    return STATUS_OK;
}


void usbd_uac_simple_demo(void)
{
//    INT32U keyValue;
//    INT32U key_released=1;

    drv_l1_clock_set_system_clk_en(CLK_EN1_USBD20, 1);
    USBD_UAC_Init();

//	DBG_PRINT("**********************************************************************\r\n");
//	DBG_PRINT("*                        This is UAC iso in/out demo                 *\r\n");
//	DBG_PRINT("**********************************************************************\r\n");
    DBG_PRINT("/**********************************************/\r\n");
    DBG_PRINT("/* 1. USBD Q handle should be configured as C_USBD_TASK_QHDL_UAC  /\r\n");
    DBG_PRINT("/* in order to run corrponding demo UAC/ UVC/ MSDC                /\r\n");
    DBG_PRINT("/* 2. plug in detection pin is on IO_A12 by default               /\r\n");
    DBG_PRINT("/**********************************************/\r\n");

#if BOARD_TYPE == BOARD_GPM41XXA_EMU_V1_0
	DBG_PRINT("KEY1    Pause/Resume\r\n");
	DBG_PRINT("KEY2    Next\r\n");
	DBG_PRINT("KEY3    Prev\r\n");
	DBG_PRINT("KEY4    Earphone mute\r\n");
#else
	DBG_PRINT("KEY1    Pause/Resume\r\n");
	DBG_PRINT("KEY2    Next\r\n");
	DBG_PRINT("KEY3    Prev\r\n");
	DBG_PRINT("KEY4    Volume up\r\n");
	DBG_PRINT("KEY5    Volume down\r\n");
	DBG_PRINT("KEY6    Earphone mute\r\n");
#endif
	DBG_PRINT("\r\n");
	DBG_PRINT("\r\n");
    adc_key_scan_init();
/*
    while(1){
        adc_key_scan();
        if(ADKEY_IO1) {
            DBG_PRINT("ADKEY 01- play/pause\r\n");
            keyValue = 0x08;
            drv_l1_usbd_send_int_ep3_in(&keyValue,1);
            key_released = 0;
        }
        else if(ADKEY_IO2){
            DBG_PRINT("ADKEY 02-next\r\n");
            keyValue = 0x10;
            drv_l1_usbd_send_int_ep3_in(&keyValue,1);
            key_released = 0;
        }
        else if(ADKEY_IO3){
            DBG_PRINT("ADKEY 03-prev\r\n");
            keyValue = 0x20;
            drv_l1_usbd_send_int_ep3_in(&keyValue,1);
            key_released = 0;
        }
#if BOARD_TYPE == BOARD_GPM41XXA_EMU_V1_0
        else if(ADKEY_IO4){
            DBG_PRINT("ADKEY 04-Earphone Mute\r\n");
            keyValue = 0x04;
            drv_l1_usbd_send_int_ep3_in(&keyValue,1);
            key_released = 0;
        }
#else
        else if(ADKEY_IO4){
            DBG_PRINT("ADKEY 04-Vol Up\r\n");
            keyValue = 0x01;
            drv_l1_usbd_send_int_ep3_in(&keyValue,1);
            key_released = 0;
        }
        else if(ADKEY_IO5){
            DBG_PRINT("ADKEY 05-Vol Down\r\n");
            keyValue = 0x02;
            drv_l1_usbd_send_int_ep3_in(&keyValue,1);
            key_released = 0;
        }
        else if(ADKEY_IO6){
            DBG_PRINT("ADKEY 06-Earphone Mute\r\n");
            keyValue = 0x04;
            drv_l1_usbd_send_int_ep3_in(&keyValue,1);
            key_released = 0;
        }
#endif
        else{
            if(key_released == 0){
                DBG_PRINT("ADKEY release\r\n");
                keyValue = 0;
                drv_l1_usbd_send_int_ep3_in(&keyValue,1);
                key_released = 1;
            }

        }
    }
*/
}

INT32U usbd_adc_key_scan(void)
{
	INT8U  KeyPara[2];
    INT32S error;

    //error = (INT32S)xQueueReceive(KeyTaskQ, &KeyPara, portMAX_DELAY);
    if(KeyTaskQ && (xQueueReceive(KeyTaskQ, &KeyPara, portMAX_DELAY) == pdTRUE))
    {
        if(KeyPara[1] == RB_KEY_DOWN)
        {
            switch(KeyPara[0])
            {
                case AD_KEY_1:	ADKEY_IO1 = 1;	break;
                case AD_KEY_2:	ADKEY_IO2 = 1;	break;
                case AD_KEY_3:	ADKEY_IO3 = 1;	break;
                case AD_KEY_4:	ADKEY_IO4 = 1;	break;
                case AD_KEY_5:	ADKEY_IO5 = 1;	break;
                case AD_KEY_6:	ADKEY_IO6 = 1;	break;
                case AD_KEY_7:	ADKEY_IO7 = 1;	break;
                case AD_KEY_8:	ADKEY_IO8 = 1;	break;
            }
            //DBG_PRINT("PD = %d\r\n", KeyPara[0]);
        }
        else if(KeyPara[1] == RB_KEY_REPEAT)
        {
            switch(KeyPara[0])
            {
                case AD_KEY_1:	ADKEY_IO1_C = 1;	break;
                case AD_KEY_2:	ADKEY_IO2_C = 1;	break;
                case AD_KEY_3:	ADKEY_IO3_C = 1;	break;
                case AD_KEY_4:	ADKEY_IO4_C = 1;	break;
                case AD_KEY_5:	ADKEY_IO5_C = 1;	break;
                case AD_KEY_6:	ADKEY_IO6_C = 1;	break;
                case AD_KEY_7:	ADKEY_IO7_C = 1;	break;
                case AD_KEY_8:	ADKEY_IO8_C = 1;	break;
            }
            //DBG_PRINT("PR = %d\r\n", KeyPara[0]);
        }
        else if(KeyPara[1] == RB_KEY_UP)
        {
            ADKEY_IO1 = 0;
            ADKEY_IO2 = 0;
            ADKEY_IO3 = 0;
            ADKEY_IO4 = 0;
            ADKEY_IO5 = 0;
            ADKEY_IO6 = 0;
            ADKEY_IO7 = 0;
            ADKEY_IO8 = 0;

            ADKEY_IO1_C = 0;
            ADKEY_IO2_C = 0;
            ADKEY_IO3_C = 0;
            ADKEY_IO4_C = 0;
            ADKEY_IO5_C = 0;
            ADKEY_IO6_C = 0;
            ADKEY_IO7_C = 0;
            ADKEY_IO8_C = 0;
            //DBG_PRINT("PU\r\n");
        }
        return 0;
    }
}

void usbd_uac_keyscan(INT32U key_released)
{
    INT32U keyValue;

    usbd_adc_key_scan();
    if(ADKEY_IO1) {
        DBG_PRINT("ADKEY 01- play/pause\r\n");
        keyValue = 0x08;
        drv_l1_usbd_send_int_ep3_in(&keyValue,1);
        key_released = 0;
    }
    else if(ADKEY_IO2){
        DBG_PRINT("ADKEY 02-next\r\n");
        keyValue = 0x10;
        drv_l1_usbd_send_int_ep3_in(&keyValue,1);
        key_released = 0;
    }
    else if(ADKEY_IO3){
        DBG_PRINT("ADKEY 03-prev\r\n");
        keyValue = 0x20;
        drv_l1_usbd_send_int_ep3_in(&keyValue,1);
        key_released = 0;
    }
#if BOARD_TYPE == BOARD_GPM41XXA_EMU_V1_0
    else if(ADKEY_IO4){
        DBG_PRINT("ADKEY 04-Earphone Mute\r\n");
        keyValue = 0x04;
        drv_l1_usbd_send_int_ep3_in(&keyValue,1);
        key_released = 0;
    }
#else
    else if(ADKEY_IO4){
        DBG_PRINT("ADKEY 04-Vol Up\r\n");
        keyValue = 0x01;
        drv_l1_usbd_send_int_ep3_in(&keyValue,1);
        key_released = 0;
    }
    else if(ADKEY_IO5){
        DBG_PRINT("ADKEY 05-Vol Down\r\n");
        keyValue = 0x02;
        drv_l1_usbd_send_int_ep3_in(&keyValue,1);
        key_released = 0;
    }
    else if(ADKEY_IO6){
        DBG_PRINT("ADKEY 06-Earphone Mute\r\n");
        keyValue = 0x04;
        drv_l1_usbd_send_int_ep3_in(&keyValue,1);
        key_released = 0;
    }
#endif
    else{
        if(key_released == 0){
            DBG_PRINT("ADKEY release\r\n");
            keyValue = 0;
            drv_l1_usbd_send_int_ep3_in(&keyValue,1);
            key_released = 1;
        }
    }
}

//==================================================================================
//         USB DEVICE UAC Entry
//==================================================================================
void usbd_uac_demo(void)
{
    INT32U msg_id;
    INT32U keyValue;
    INT32U key_rlse=0;

    hUSBD_UAC_TaskQ = xQueueCreate(USB_DEVICE_TASK_QUEUE_MAX, sizeof(INT32U));
    drv_l2_usbd_register_userpara((INT8U *)USBD_USER_PARA);
    drv_l2_usbd_detio_init();
    timer_freq_setup(USBD_USING_TIMER, C_5MS, 0, drv_l2_usbd_user_tmrisr);
    while(1)
    {
        if(hUSBD_UAC_TaskQ && (xQueueReceive(hUSBD_UAC_TaskQ, &msg_id, 0) == pdTRUE))
        {
            switch(msg_id)
            {
                case MSG_USB_DEVICE_INSERT:
                    connect_2_host = 1;
                    usbd_userpara.tmrflagOnOff = 1;
                    usbd_uac_simple_demo();
                break;

                case MSG_USB_DEVICE_REMOVE:
                    USBD_UAC_Uninit();
                    usbd_userpara.tmrflagOnOff = 0;
                    connect_2_host = 0;
                break;

                case MSG_USB_DEVICE_TMR_TIMEOUT:

                break;

                default:

                break;
            }
        }
        if(connect_2_host)
            usbd_uac_keyscan(key_rlse);
    }
}

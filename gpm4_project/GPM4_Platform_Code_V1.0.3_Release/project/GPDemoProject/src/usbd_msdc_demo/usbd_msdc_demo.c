/**************************************************************************
 *                                                                        *
 *         Copyright (c) 2014 by Generalplus Inc.                         *
 *                                                                        *
 *  This software is copyrighted by and is the property of Generalplus    *
 *  Inc. All rights are reserved by Generalplus Inc.                      *
 *  This software may only be used in accordance with the                 *
 *  corresponding license agreement. Any unauthorized use, duplication,   *
 *  distribution, or disclosure of this software is expressly forbidden.  *
 *                                                                        *
 *  This Copyright notice MUST not be removed or modified without prior   *
 *  written consent of Generalplus Technology Co., Ltd.                   *
 *                                                                        *
 *  Generalplus Inc. reserves the right to modify this software           *
 *  without notice.                                                       *
 *                                                                        *
 *  Generalplus Inc.                                                      *
 *  No.19, Industry E. Rd. IV, Hsinchu Science Park                       *
 *  Hsinchu City 30078, Taiwan, R.O.C.                                    *
 *                                                                        *
 **************************************************************************/
/*******************************************************
    Include file
*******************************************************/
#include "drv_l1_sfr.h"
#include "drv_l1_uart.h"
#include "drv_l1_usbd.h"
#include "drv_l2_usbd.h"
#include "drv_l2_usbd_msdc.h"
#include "drv_l2_usbd_msdc_vendor.h"

/******************************************************
    External variables & functions declaration
*******************************************************/
extern MDSC_LUN_STORAGE_DRV const gp_msdc_ramdisk;
extern MDSC_LUN_STORAGE_DRV const gp_msdc_nand0;
extern MDSC_LUN_STORAGE_DRV const gp_msdc_nandapp0;
extern MDSC_LUN_STORAGE_DRV const gp_msdc_sd0;
extern MDSC_LUN_STORAGE_DRV const gp_msdc_sd1;

extern INT8U Default_Device_Descriptor_TBL[];
extern INT8U Default_Qualifier_Descriptor_TBL[];
extern INT8U Default_Config_Descriptor_TBL[];
extern INT8U Default_String0_Descriptor[];
extern INT8U Default_String1_Descriptor[];
extern INT8U Default_String2_Descriptor[];
extern INT8U Default_String3_Descriptor[];
extern INT8U Default_scsi_inquirydata[];
extern INT8U Default_scsi_inquirydata_CDROM[];
extern INT8U connect_2_host;
extern vread_info_t vread_info;
extern vwrite_info_t vwrite_info;

/******************************************************
    Definition and variable declaration
*******************************************************/
#define USBD_STORAGE_NO_WPROTECT	0
#define USBD_STORAGE_WPROTECT		1

#define USBD_MSDC_DMA_BUF_ORDER	4 // 6 // should not set more than 6
#define USBD_MSDC_DMA_BUF_SIZE	((1 << USBD_MSDC_DMA_BUF_ORDER)*512)	/* Support 512/1K/2K/4K/8K/16K/32K/64K/128K data length */
#define MSDC_MULTI_LUN          0

#if MSDC_MULTI_LUN == 1
extern INT8U Default_scsi_inquirydata2[];
#endif
extern xQueueHandle hUSBD_MSDC_TaskQ;
extern xQueueHandle hUSBD_MSDC_BUSRSTQ;
//xQueueHandle hUSBD_TimerQ;
//INT8U connect_2_host = 0;
INT8U USBD_Task_Qhandle_type = 0;
INT8U *scsi_inquirydata_ptr[MAX_MSDC_LUN_NUM] = {NULL};
/******************************************************
    Functions declaration
*******************************************************/
void _usbd_reset_issued(void)
{
    INT32U msg;

    if(hUSBD_MSDC_BUSRSTQ)
    {
        msg = MSG_USBBUS_RESET_ISSUED;
        xQueueSend(hUSBD_MSDC_BUSRSTQ, &msg, 0);
    }
}

void usbd_msdc_init(void)
{
    INT32S ret;
    INT8U lun_type;
    INT32U dma_buf_size = USBD_MSDC_DMA_BUF_SIZE;

    /* switch to USB device mode  bit8 = 0 */
 	rSYS_CTRL_NEW &= ~(1 << 8);

    drv_l2_usbd_register_detect_cbk(_usbd_reset_issued);
    /* Init USBD L2 protocol layer first, including control/bulk/ISO/interrupt transfers */
    /******************************* Control transfer ************************************/
    ret = drv_l2_usbd_ctl_init();
    if(ret == STATUS_FAIL)
    {
        DBG_PRINT("drv_l2_usbd_ctl_init failed!\r\n");
        return;
    }

    /* Register new descriptor table here, this action must be done after drv_l2_usbd_ctl_init() */
    drv_l2_usbd_register_descriptor(REG_DEVICE_DESCRIPTOR_TYPE, (INT8U*)Default_Device_Descriptor_TBL);
    drv_l2_usbd_register_descriptor(REG_CONFIG_DESCRIPTOR_TYPE, (INT8U*)Default_Config_Descriptor_TBL);
    drv_l2_usbd_register_descriptor(REG_DEVICE_QUALIFIER_DESCRIPTOR_TYPE, (INT8U*)Default_Qualifier_Descriptor_TBL);
    drv_l2_usbd_register_descriptor(REG_STRING0_DESCRIPTOR_TYPE, (INT8U*)Default_String0_Descriptor);
    drv_l2_usbd_register_descriptor(REG_STRING1_DESCRIPTOR_TYPE, (INT8U*)Default_String1_Descriptor);
    drv_l2_usbd_register_descriptor(REG_STRING2_DESCRIPTOR_TYPE, (INT8U*)Default_String2_Descriptor);
    drv_l2_usbd_register_descriptor(REG_STRING3_DESCRIPTOR_TYPE, (INT8U*)Default_String3_Descriptor);

    /* Register RAM disk storage to MSDC */

    //lun_type = LUN_RAM_DISK_TYPE;
    //drv_l2_usbd_msdc_set_lun(LUN_RAM_DISK_TYPE, LUN_NUM_0, USBD_STORAGE_NO_WPROTECT, &gp_msdc_ramdisk);
#if BOARD_TYPE == BOARD_GPM41XXA_EMU_V1_0
    lun_type = LUN_SDC_TYPE;
    drv_l2_usbd_msdc_set_lun(LUN_SDC_TYPE, LUN_NUM_0, USBD_STORAGE_NO_WPROTECT, &gp_msdc_sd0);
    //drv_l2_usbd_msdc_set_lun(LUN_SDC_TYPE, LUN_NUM_0, USBD_STORAGE_NO_WPROTECT, &gp_msdc_sd1);
#else
    lun_type = LUN_NF_TYPE;
    drv_l2_usbd_msdc_set_lun(LUN_NF_TYPE, LUN_NUM_0, USBD_STORAGE_NO_WPROTECT, &gp_msdc_nand0);
#if MSDC_MULTI_LUN == 1
    drv_l2_usbd_msdc_set_lun(LUN_SDC_TYPE, LUN_NUM_1, USBD_STORAGE_NO_WPROTECT, &gp_msdc_sd1);//(JC)2 lun demo
#endif
    #if 0 && (defined NAND_APP_EN) && (NAND_APP_EN == 1)
        #if (defined NAND_APP_WRITE_EN) && (NAND_APP_WRITE_EN == 1)
        drv_l2_usbd_msdc_set_lun(LUN_NF_TYPE, LUN_NUM_0, USBD_STORAGE_NO_WPROTECT, &gp_msdc_nandapp0);
        #else
        drv_l2_usbd_msdc_set_lun(LUN_NF_TYPE, LUN_NUM_0, USBD_STORAGE_WPROTECT, &gp_msdc_nandapp0);
        #endif
    #endif
#endif
	/* Set MSDC A/B buffer size */
	if (lun_type == LUN_NF_TYPE)
	{
        // when NF, for better performance, use 32K buffer size
        if (dma_buf_size < 32*1024)
            dma_buf_size = 32*1024;
	}
	drv_l2_usbd_msdc_set_dma_buffer_size(dma_buf_size);

   	/* Init MSDC driver */
    ret = drv_l2_usbd_msdc_init();
    if(ret == STATUS_FAIL)
    {
        /* Init failed, do uninit procedures */
        drv_l2_usbd_msdc_uninit();
        DBG_PRINT("drv_l2_usbd_msdc_uninit failed!\r\n");
        return;
    }
    scsi_inquirydata_ptr[0] = (INT8U *)Default_scsi_inquirydata;
#if MSDC_MULTI_LUN == 1
    scsi_inquirydata_ptr[1] = (INT8U *)Default_scsi_inquirydata2;
#endif
    /* Register SCSI inquiry data pointer, it must be done after drv_l2_usbd_msdc_init() */
    drv_l2_usbd_msdc_register_scsi_inquiry_data(scsi_inquirydata_ptr, (INT8U*)Default_scsi_inquirydata_CDROM);

    /* Init USBD L1 register layer */
    ret = drv_l1_usbd_init();
    if(ret == STATUS_FAIL)
    {
        DBG_PRINT("drv_l1_usbd_init failed!\r\n");
        return;
    }

	/* register USBD ISR handler */
	drv_l1_usbd_enable_isr();
    DBG_PRINT("USB MSDC device init completed!\r\n");
}

void usbd_msdc_uninit(void)
{
	drv_l2_usbd_ctl_uninit();
	drv_l2_usbd_msdc_uninit();
	xQueueReset(hUSBD_MSDC_BUSRSTQ);
}

//==================================================================================
//         USB DEVICE MSDC Entry
//==================================================================================
INT32S usbd_msdc_demo(void)
{
    INT32U msg_id, msgno;

    hUSBD_MSDC_TaskQ = xQueueCreate(USB_DEVICE_TASK_QUEUE_MAX, sizeof(INT32U));
    hUSBD_MSDC_BUSRSTQ = xQueueCreate(1, sizeof(INT32U));
//    hUSBD_TimerQ = xQueueCreate(USB_DEVICE_TASK_QUEUE_MAX, sizeof(INT32U));
    drv_l2_usbd_register_userpara((INT8U *)USBD_USER_PARA);
    drv_l2_usbd_detio_init();
    timer_freq_setup(USBD_USING_TIMER, C_5MS, 0, drv_l2_usbd_user_tmrisr);
    while(1)
    {
        if(hUSBD_MSDC_TaskQ && (xQueueReceive(hUSBD_MSDC_TaskQ, &msg_id, 0) == pdTRUE))
        {
            switch(msg_id)
            {
                case MSG_USB_DEVICE_INSERT:
                    connect_2_host = 1;
                    usbd_userpara.tmrflagOnOff = 1;
                    usbd_msdc_init();
                    if(hUSBD_MSDC_BUSRSTQ && (xQueueReceive(hUSBD_MSDC_BUSRSTQ, &msgno, 1000) == pdFALSE))
                    {
                        msg_id = MSG_USB_DEVICE_PLUGIN_CHARGER;
                        xQueueSend(hUSBD_MSDC_TaskQ, &msg_id, 0);
                    }
                break;

                case MSG_USB_DEVICE_REMOVE:
                    usbd_msdc_uninit();
                    usbd_userpara.tmrflagOnOff = 0;
                    connect_2_host = 0;
                break;

                case MSG_USB_DEVICE_TMR_TIMEOUT:

                break;

                case MSG_USB_DEVICE_PLUGIN_CHARGER:
                    DBG_PRINT("Plug into charger!!\r\n");
                break;

                default:

                break;
            }
        }
    }
}

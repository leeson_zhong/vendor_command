#include "application.h"
#include "vr_demo_global.h"

INT DataFlash_Write( BYTE* dest , BYTE* src , INT nSize )
{
	gp_memcpy( (INT8S*)dest , (INT8S*)src , nSize ) ;
	return 0 ;
}

INT DataFlash_Erase( BYTE* dest , INT nSize )
{
	gp_memset( (INT8S*)dest , 0xFF , nSize ) ;
	return 0;
}

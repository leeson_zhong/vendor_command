#include "application.h"
#include <stdlib.h>
#include "drv_l1_sfr.h"
#include "drv_l1_gpio.h"
#include "drv_l2_ad_key_scan.h"
#include "audio_encoder.h"
#include "vr_demo_global.h"

#include "base_types.h"
#include "CSpotterSDKApi.h"
#include "CSpotterSDTrainApi.h"
#include "CES2015_pack_formosa.h"

#define USE_ADKEY ( 1 )

#define VR_KEYWORD  1   //keyword: formosa

#if VR_KEYWORD == 1
#define LED_IO  IO_A15

INT g_keyword = 0;
int gcnt=0;
#endif

CSpotterDemoGlobal gVrDemo ;

INT sdId[SD_MAX_WORD] = {0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF};
INT g_index =0;
INT16U g_tts_callback_en = VR_ONLY;

#if VR_DEMO_OPTION == VR_ONLY//normal vr demo
static char *command[] = {
		"Happy New Year \0",
		"Welcome to the show \0",
		"What time is it \0",
		"新年快樂 \0",
		"恭喜發財 \0",
		"Tell me a joke \0",
		"Introduce yourself \0",
		"Goto sleep \0",
		"Merry Christmas \0",
		"Show me the location \0",
		"How is the weather \0",
		"Dance with me \0",
		"I want to be a JEDI \0",
		"May the Force be with you \0",
		"Generalplus \0",
		"Technology \0",
		"Formosa \0",
		"User Defined Commnad 1 \0",
		"User Defined Commnad 2 \0",
		"User Defined Commnad 3 \0",
		"User Defined Commnad 4 \0",
		"User Defined Commnad 5 \0"
};
#else
char *command[] = {
		"Happy New Year to you, too! \0",
		"Glad to be here! \0",
		"It's 9 o'clock! \0",
		"Happy New Year! \0",
		"Money Money Come to me. \0",
		"How does a computer tell you it needs more memory? It says: byte me!!! \0",
		"Hi! I am Formosa. Your intelligent assistant!\0",
		"Good night \0",
		"Merry Christmas! Ready to open my gift \0",
		"You are at Hsinchu Taiwan. \0",
		"It is a beautiful day. Let's take a walk \0",
		"Okie Dokie! \0",
		"You are out of your mind! \0",
		"Force awakened! \0",
		"That's the right choice! \0",
		"What kind of technology do you need? interactive toy, electronic-pet, wearable devices, dashboard camera, drone camera, wireless charger, calculator, kids learning aid, touch-control appliance? \0",
        "Yes sir! \0",
		"I am a T T S robot. Nice to meet you! \0",
		"User Defined Commnad 3 \0",
		"User Defined Commnad 4 \0",
		"User Defined Commnad 5 \0"

};

static char *command2[] = {
		"Happy two thousand seventeen \0",
		"That's my pleasure to be here! \0",
		"check your smart phone \0",
		"I don't understand chinese \0",
		"Thank you! But I only need 3.3 volt \0",
		"How do astronomers organize a party?    They planet.\0",
		"I am the Answer to your question!\0",
		"time to get some rest \0",
		"Do you have any gift for me? \0",
		"Google map is all you need! \0",
		"That's pretty good for a micro controller \0",
		"I am all yours~  \0",
		"You will be in star wars episode 100 \0",
		"May the Generalplus be with you \0",
		"There is an old slogan. Technology for easy living. \0",
		"Computer Vision is hot. Order some G P M 4 series now? \0",
		"I am here! \0",
		"As you know, I am not a toy",
		"User Defined Commnad 2 \0",
		"User Defined Commnad 3 \0",
		"User Defined Commnad 4 \0"
};

static char *command3[] = {
		"Party is on! \0",
		"Nice to meet you, Ladies and gentlemen \0",
		"That's just about time to demo T T S \0",
		"Thank you! Happy New Year to all of you \0",
		"Gong Xi Fa Cai \0",
		"A boy asks his father, Dad, are bugs good to eat? That's disgusting. Don't talk about things like that over dinner, the dad replies. After dinner the father asks, Now, son, what did you want to ask me? Oh, nothing, the boy says. There was a bug in your soup, but now it’s gone. \0",
		"I am the solution to your interactive electronics projects !\0",
		"Sorry guys. Allow me to take a short nap. I will be right back. \0",
		"May the light of love shine upon you, and may your life be filled with blessings in this Christmas season \0",
		"I can show you how to display it on the L C D panel if you buy a set of emulation board. \0",
		"It's getting cold now. I suggest you stay inside with me and have some fun demos \0",
		"I am able to speak and listen now. Unless you put some motors on me! \0",
		"I guess you are a big fan of Star Wars. Do you need a light saber? \0",
		"Luke, you can destroy the Emperor. He has foreseen this. It is your destiny. Join me, and together we can rule the galaxy as father and son \0",
		"Your best partner in smart toy industry\0",
		"Voice recognition with T T S is very interesting combination. \0",
		"Hi master! \0",
		"Nice to meet you. Say sing a song then I will surprise you! \0",
		"User Defined Commnad 2 \0",
		"User Defined Commnad 3 \0",
		"User Defined Commnad 4 \0"
};

static char *command4[] = {
		"Let's celebrate the year 2017! \0",
		"Get yourself ready. The show is ready to start.  \0",
		"I am sorry. Can you say that again? \0",
		"People look forward to a new year to start afresh and turn over a new leaf. All the best for that and have a wonderful year ahead! \0",
		"May this New Year equip you with good luck, good health, good fortune and good times! Happy New Year! \0",
		"What does an air conditioner have in common with a computer? They both lose efficiency as soon as you open windows. \0",
		"Well, my name is G P M 4 7 3 0 A. I am running 193.5MHz. Do you want more for information about me? Our sales person will have some data sheet for you later!\0",
		"I don't want sleep. Let's keep playing! \0",
		"There’s nothing sadder in this world than to awake Christmas morning and not be a child. \0",
		"I can show you how to display it on the L C D panel if you buy a set of emulation board. \0",
		"That's getting hot inside. Do you want some cold drinks? \0",
		"I usually don't do this. I am afraid of not this time either  \0",
		"The Force is strong with you. A powerful Sith you will become. Henceforth, you shall be known as Darth Vader.”\0",
		"Fear is the path to the dark side \0",
		//"Generalplus Technology, established in 2004 and listed number 4 9 5 2 on TPEx stock market in 2011, has become one of the major integrated circuit providers worldwide, specializing in voice IC and multi-media product solutions. In addition to our headquarters in Taiwan, we’ve also established many branches and offices around the world, where includes Shenzhen-China, Chenghai-China, HongKong, the United States, and Japan, to provide comprehensive supports, engineering and business, to our partners and customers.\0",
		"We can add value to your product! \0",
		"Audio Video streaming? I am no problem with that! \0",
		"I can hear you \0",
		"Nice to meet you. Say sing a song then I will suprise you! \0",
		"User Defined Commnad 2 \0",
		"User Defined Commnad 3 \0",
		"User Defined Commnad 4 \0"
};
#endif

INT32S sv_level=0;

INT16U vr_count=0;
void show_result_over_uart(INT16U index)
{
	INT32U ret;
    if(g_tts_callback_en == VR_ONLY)
        CYB_PRINT("Idx: %d == %s \r\n", index+1, command[index]);
    else{
        ret = xTaskGetTickCount();
        ret = ret%4;

        DBG_PRINT("ret = %d \r\n",ret);

        if(ret == 0){
            CYB_PRINT("Idx: %d == %s \r\n", index+1, command[index]);
            vr2tts_callback(index,command[index]);
        } else if(ret == 1){
            CYB_PRINT("Idx: %d == %s \r\n", index+1, command2[index]);
            vr2tts_callback(index,command2[index]);
        } else if(ret == 2){
            CYB_PRINT("Idx: %d == %s \r\n", index+1, command3[index]);
            vr2tts_callback(index,command3[index]);
        } else if(ret == 3){
            CYB_PRINT("Idx: %d == %s \r\n", index+1, command4[index]);
            vr2tts_callback(index,command4[index]);
        }
    }
}


INT32S vr_write_callback(INT32U buf_addr, INT32U cblen, INT32U file_offset)
{
	//TODO: implement recording to media
	return 0;
}

BOOL HardwareInitialize(void)
{
#if (SAVE_PCM_DATA == 1)
	// Mount Disk
	mount_disk( C_DISK ) ;

	#if( C_DISK == FS_NAND1 )
		chdir( "A:\\" ) ;
	#elif( C_DISK == FS_SD1 )
		chdir( "B:\\" ) ;
	#elif( C_DISK == FS_SD2 )
		chdir( "C:\\" ) ;
	#endif
#endif
	adc_key_scan_init();

	audio_encode_entrance();

#if VR_KEYWORD == 1
    gpio_init_io(LED_IO, GPIO_OUTPUT);
    gpio_write_io(LED_IO, DATA_LOW);
#endif
	return TRUE ;
}

void HardwareRelease(void)
{
}

int GetKey(void)
{
	static INT32U* keys[8] =
	{
		#if( USE_ADKEY == 1 )
			&ADKEY_IO1 , &ADKEY_IO2 , &ADKEY_IO3 , &ADKEY_IO4 , &ADKEY_IO5 , &ADKEY_IO6 , &ADKEY_IO7 , &ADKEY_IO8
		#else
			&IO1 , &IO2 , &IO3 , &IO4 , &IO5 , &IO6 , &IO7 , &IO8
		#endif
	} ;

	int id , numKey = 8 ;

	// Scan
	#if( USE_ADKEY == 1 )
		adc_key_scan() ;
	#else
		key_Scan() ;
	#endif

	for( id = 1 ; id <= numKey ; ++id )
	{
		if( *( keys[ id- 1 ] ) == 1 )
			break ;
	}

	return ( id <= numKey )? id : -1 ;
}

void VrDemoGlobalInit(void)
{
	INT32S size_baseVR, size_VR;
	BYTE* lpbyVRModel;
	BYTE* lpbyCYBase;
	BYTE* lpbyModel;

	lpbyVRModel = (BYTE*)g_lpdwCES2015_pack;//g_lpdwCN_demo_32_pack;//pointer to the VR model
	size_baseVR = *(INT32U*)(lpbyVRModel+4);
	size_VR = *(INT32U*)(lpbyVRModel+8);
	lpbyCYBase = lpbyVRModel+12;
	lpbyModel = lpbyCYBase+size_baseVR;

	gVrDemo.state            = CSPOTTER_IDLE ;
	gVrDemo.baseModel        = (BYTE*)lpbyCYBase ;
	gVrDemo.commnadModel     = (BYTE*)lpbyModel ;
	gVrDemo.sdModel          = NULL ;
	gVrDemo.sdWordBuffer     = NULL ;
	gVrDemo.sdUttrBuffer     = NULL ;
	gVrDemo.engineBuffer     = NULL ;
	gVrDemo.sizeSDModel      = SD_WORD_SIZE * SD_MAX_WORD + SD_INFO_SIZE ;
	gVrDemo.sizeSDWordBuffer = SD_WORD_SIZE ;
	gVrDemo.sizeSDUttrBuffer = SD_UTTR_SIZE ;
	gVrDemo.cspotter         = NULL ;
	gVrDemo.sd_uttr_index    = -1 ;
	gVrDemo.sd_identity_enable = FALSE;

	// Reset State
	gp_memset( (INT8S*)gVrDemo.cspooterState , 0 , sizeof(gVrDemo.cspooterState) ) ;

}

void VrTtsEnable(INT16U tts_en )
{
   g_tts_callback_en = tts_en;  //0: vr_only 1: vr_tts
}

void VrDemoGlobalInit_SDSV(void)
{
	INT32S size_baseVR, size_VR;
	BYTE* lpbyVRModel;
	BYTE* lpbyCYBase;
	BYTE* lpbyModel;

	lpbyVRModel = (BYTE*)g_lpdwCES2015_pack;//pointer to the VR model
	size_baseVR = *(INT32U*)(lpbyVRModel+4);
	size_VR = *(INT32U*)(lpbyVRModel+8);
	lpbyCYBase = lpbyVRModel+12;
	lpbyModel = lpbyCYBase+size_baseVR;

	gVrDemo.state            = CSPOTTER_IDLE ;
	gVrDemo.baseModel        = (BYTE*)lpbyCYBase ;
	gVrDemo.commnadModel     = (BYTE*)lpbyModel ;
	gVrDemo.sdModel          = NULL ;
	gVrDemo.sdWordBuffer     = NULL ;
	gVrDemo.sdUttrBuffer     = NULL ;
	gVrDemo.engineBuffer     = NULL ;
	gVrDemo.sizeSDModel      = SD_WORD_SIZE * 2 * SD_MAX_WORD + SD_INFO_SIZE ;
	gVrDemo.sizeSDWordBuffer = SD_WORD_SIZE * 2 ;
	gVrDemo.sizeSDUttrBuffer = SD_UTTR_SIZE ;
	gVrDemo.cspotter         = NULL ;
	gVrDemo.sd_uttr_index    = -1 ;
	gVrDemo.sd_identity_enable = TRUE;

	// Reset State
	gp_memset( (INT8S*)gVrDemo.cspooterState , 0 , sizeof(gVrDemo.cspooterState) ) ;

}

void VrDemoGlobalRelease(void)
{
	audio_encode_exit();// Release the resources needed by audio encoder
}

void TestVR_Start(void)
{
	int size , ret ;
	MEDIA_SOURCE	media_source;

	CYB_PRINT( "gVrDemo.sdModel = %x.\r\n" , gVrDemo.sdModel) ;

	do
	{
		if( gVrDemo.state != CSPOTTER_IDLE )
			break ;

		CYB_NEWLINE() ;

		FREE( gVrDemo.engineBuffer ) ;

		// Init CSpotter Engine
		size = CSpotter_GetMemoryUsage_Multi( gVrDemo.baseModel , &gVrDemo.commnadModel , ( gVrDemo.sdModel == NULL )? 1 : 2 , 1000 ) ;
		DBG_PRINT("CSPotter Init Mem Size = 0x%x \r\n",size);
		gVrDemo.engineBuffer = gp_malloc_align( size , sizeof(int) ) ;
		if( gVrDemo.engineBuffer == NULL )
		{
			CYB_PRINT( "CSpotterVR buffer alloc failed.\r\n" ) ;
			break ;
		}
		gVrDemo.cspotter = CSpotter_Init_Multi( gVrDemo.baseModel , &gVrDemo.commnadModel , ( gVrDemo.sdModel == NULL )? 1 : 2 , 1000 , gVrDemo.engineBuffer , size , gVrDemo.cspooterState , sizeof(gVrDemo.cspooterState) , &ret ) ;
		if( gVrDemo.cspotter == NULL )
		{
			CYB_PRINT( "CSpotterVR engine init failed(error code:%d).\r\n" , ret ) ;
			break ;
		}

		CYB_PRINT( "SD : %d\r\n", ( gVrDemo.sdModel == NULL )? 1 : 2 );

		//set shortest response time for demo purpose
		CSpotter_SetResponseTime(gVrDemo.cspotter, 10);


		// Start Audio
		gVrDemo.state = CSPOTTER_PROCESS_VR ;

		if(audio_encode_status() == AUDIO_CODEC_PROCESS_END)
		{
			media_source.type = SOURCE_TYPE_USER_DEFINE;

			media_source.Format.AudioFormat = VR_PCM;
			//ret = audio_encode_start(media_source, 16000, 0);
			ret = audio_encode_start(media_source, BUILD_IN_MIC , 16000, 0);
			if (ret != 0)
			{
				CYB_PRINT( "Audio start failed(error code:%d).\r\n" , ret ) ;
				continue;
			}
		}


		CYB_PRINT( "CSpotterVR start.\r\n" ) ;

		return ;

	} while(0) ;

	gVrDemo.state = CSPOTTER_IDLE ;

	FREE( gVrDemo.engineBuffer ) ;
}

void TestVR_Stop(void)
{
//	int ret ;

	do
	{
		CYB_NEWLINE() ;

		// Stop Audio
		audio_encode_stop();

		FREE( gVrDemo.engineBuffer ) ;

		gVrDemo.state = CSPOTTER_IDLE ;

		CYB_PRINT( "CSpotterVR stop.\r\n" ) ;

	} while(0) ;
}



void TrainSD_Start(void)
{
	int size , ret ;
	MEDIA_SOURCE	media_source;


	// Init SD Model
	// SD model memory

	if( gVrDemo.sdModel == NULL ){
		gVrDemo.sdModel = gp_malloc_align( gVrDemo.sizeSDModel , sizeof(int) ) ;
		if( gVrDemo.sdModel == NULL )
		{
			CYB_PRINT( "CSpotterSD model alloc failed.\r\n" ) ;
			return;//break ;
		}
		gp_memset( (INT8S*)gVrDemo.sdModel , 0xFF , gVrDemo.sizeSDModel ) ;
	}

	do
	{
		if( gVrDemo.state != CSPOTTER_IDLE &&
		    gVrDemo.state != CSPOTTER_PROCESS_SD_PAUSE )
		{
			break ;
		}

		CYB_NEWLINE() ;

		if( gVrDemo.sd_uttr_index == -1 )
		{
//			FREE( gVrDemo.sdModel ) ;
			FREE( gVrDemo.sdWordBuffer ) ;
			FREE( gVrDemo.sdUttrBuffer ) ;
			FREE( gVrDemo.engineBuffer ) ;

			// Init SD Model
//			gVrDemo.sdModel = gp_malloc_align( gVrDemo.sizeSDModel , sizeof(int) ) ;
//			if( gVrDemo.sdModel == NULL )
//			{
//				CYB_PRINT( "CSpotterSD model alloc failed(memort:%d/%d).\r\n" , size , mm_free_get() ) ;
//				break ;
//			}
//			gp_memset( (INT8S*)gVrDemo.sdModel , 0xFF , gVrDemo.sizeSDModel ) ;

			// Init SD Buffer
			gVrDemo.sdWordBuffer = gp_malloc_align( gVrDemo.sizeSDWordBuffer , sizeof(int) ) ;
			if( gVrDemo.sdWordBuffer == NULL )
			{
				CYB_PRINT( "CSpotterSD word buffer memory alloc failed.\r\n" ) ;
				break ;
			}
			gVrDemo.sdUttrBuffer = gp_malloc_align( gVrDemo.sizeSDUttrBuffer * SD_MAX_UTTR , sizeof(int) ) ;
			if( gVrDemo.sdUttrBuffer == NULL )
			{
				CYB_PRINT( "CSpotterSD uttr buffer memory alloc failed\r\n" ) ;
				break ;
			}

			// Init SD Engine
			if(gVrDemo.sd_identity_enable == TRUE)
			{
				CYB_PRINT( "Enable Idenetity Mode");
			}
			size = CSpotterSD_GetMemoryUsage( (BYTE*)gVrDemo.baseModel, gVrDemo.sd_identity_enable ) ;
			gVrDemo.engineBuffer = gp_malloc_align( size , sizeof(int) ) ;
			if( gVrDemo.engineBuffer == NULL )
			{
				CYB_PRINT( "CSpotterSD engine memory alloc failed.\r\n" ) ;
				break ;
			}
			gVrDemo.cspotterSD = CSpotterSD_Init( (BYTE*)gVrDemo.baseModel , gVrDemo.engineBuffer , size , &ret ) ;
			if( gVrDemo.cspotterSD == NULL )
			{
				CYB_PRINT( "CSpotterSD engine init failed(error code:%d).\r\n" , ret ) ;
				break ;
			}

			CYB_NEWLINE() ;

			if(audio_encode_status() == AUDIO_CODEC_PROCESS_END)
			{
				media_source.type = SOURCE_TYPE_USER_DEFINE;

				media_source.Format.AudioFormat = VR_PCM;
				ret = audio_encode_start(media_source, BUILD_IN_MIC , 16000, 0);
				if (ret != 0)
				{
					CYB_PRINT( "Audio start failed(error code:%d).\r\n" , ret ) ;
					continue;
				}
			}

			gVrDemo.sd_uttr_index = 0 ;
			ret = CSpotterSD_AddUttrStart( gVrDemo.cspotterSD , gVrDemo.sd_uttr_index , (char*)( gVrDemo.sdUttrBuffer + gVrDemo.sd_uttr_index * gVrDemo.sizeSDUttrBuffer ) , gVrDemo.sizeSDUttrBuffer ) ;
			if( ret != 0 )
			{
				CYB_PRINT( "CSpotterSD add uttr start failed(error code:%d).\r\n" , ret ) ;
				break ;
			}

			gVrDemo.state = CSPOTTER_PROCESS_SD ;

			CYB_PRINT( "CSpotterSD start\r\n" ) ;
			CYB_PRINT( "Speak for SD(%d/%d)...\r\n" , gVrDemo.sd_uttr_index + 1 , SD_MAX_UTTR ) ;
		}
		else
		{
			if( gVrDemo.state != CSPOTTER_PROCESS_SD_PAUSE )
				break ;

			CYB_NEWLINE() ;

			if( gVrDemo.engineBuffer == NULL ||
			    gVrDemo.sdWordBuffer == NULL ||
			    gVrDemo.sdUttrBuffer == NULL ||
			    gVrDemo.sdModel      == NULL )
			{
				CYB_PRINT( "CSpotterSD error.\r\n" ) ;
				break ;
			}

			ret = CSpotterSD_AddUttrStart( gVrDemo.cspotterSD , gVrDemo.sd_uttr_index , (char*)gVrDemo.sdUttrBuffer + gVrDemo.sd_uttr_index * gVrDemo.sizeSDUttrBuffer , gVrDemo.sizeSDUttrBuffer ) ;
			if( ret != 0 )
			{
				CYB_PRINT( "CSpotterSD add uttr start failed(error code:%d).\r\n" , ret ) ;
				break ;
			}

			gVrDemo.state = CSPOTTER_PROCESS_SD ;

			CYB_PRINT( "Speak for SD(%d/%d)...\r\n" , gVrDemo.sd_uttr_index + 1 , SD_MAX_UTTR ) ;
		}

		return ;

	} while(0) ;

	FREE( gVrDemo.engineBuffer ) ;
	FREE( gVrDemo.sdWordBuffer ) ;
	FREE( gVrDemo.sdUttrBuffer ) ;
	FREE( gVrDemo.sdModel ) ;

	gVrDemo.sd_uttr_index = -1 ;
	gVrDemo.state         = CSPOTTER_IDLE ;

	CYB_PRINT( "CSpotter SD Stop\r\n" ) ;
}

INT nTagID = 0;

void TrainSD_Stop(void)
{
	int size , ret , id ;


	do
	{
		CYB_NEWLINE() ;

		// Stop Audio
		audio_encode_stop();

		CYB_NEWLINE() ;

		if( gVrDemo.sd_uttr_index == -1 )
		{
			CYB_PRINT( "CSpotterSD not init.\r\n" ) ;
			break ;
		}
		if( gVrDemo.sd_uttr_index == 0 )
		{
			CYB_PRINT( "CSpotterSD not finished.\r\n" ) ;
			break ;
		}

		do
		{
			CYB_PRINT( "CSpotterSD training...\r\n" ) ;

			ret = CSpotterSD_TrainWord( gVrDemo.cspotterSD , CSPOTTERSD_DIFFCHK_NORMAL , (char*)gVrDemo.sdWordBuffer , gVrDemo.sizeSDWordBuffer ) ;
			if( ret != CSPOTTER_SUCCESS )
			{
				CYB_PRINT( "CSpotterSD train word failed(error code:%d).\r\n" , ret ) ;
				break ;
			}
			id = CSpotterSD_AddWord( gVrDemo.cspotterSD , (char*)gVrDemo.sdModel , gVrDemo.sizeSDModel , nTagID, (char*)gVrDemo.sdWordBuffer , &size ) ;
			CYB_PRINT("nTagID = %d \r\n",nTagID);

			if( id < 0 )
			{
				nTagID -= 1;
				CYB_PRINT( "CSpotterSD add word failed [%d] size = [%d].\r\n", id, size ) ;
				break ;
			}

			nTagID++;
			if(nTagID > 32767)
			{
				CYB_PRINT("nTagID is out of range \r\n");
				nTagID = 0;

			}


			CYB_PRINT( "CSpotterSD finished.\r\n" ) ;

		} while(0) ;

		FREE( gVrDemo.engineBuffer ) ;
		FREE( gVrDemo.sdWordBuffer ) ;
		FREE( gVrDemo.sdUttrBuffer ) ;

		gVrDemo.sd_uttr_index = -1 ;
		gVrDemo.state         = CSPOTTER_IDLE ;

		CYB_PRINT( "CSpotterSD stop.\r\n" ) ;

	} while(0) ;
}
void TrainSD_Erase(void)
{
	if( gVrDemo.state != CSPOTTER_IDLE )
		return ;
	nTagID = 0;
	FREE( gVrDemo.sdModel ) ;
	CYB_NEWLINE() ;
	CYB_PRINT( "CSpotterSD erase.\r\n" ) ;
}


void TestSDVR_Start(void)
{
	int size , ret ;
	MEDIA_SOURCE	media_source;

	CYB_PRINT( "gVrDemo.sdModel = %x.\r\n" , gVrDemo.sdModel) ;

	do
	{
		if( gVrDemo.state != CSPOTTER_IDLE )
			break ;

		CYB_NEWLINE() ;

		FREE( gVrDemo.engineBuffer ) ;

		// Init CSpotter Engine
		size = CSpotter_GetMemoryUsage_Multi( gVrDemo.baseModel , &gVrDemo.sdModel , 1 , 1000 ) ;
		DBG_PRINT("CSPotter Init Mem Size = 0x%x \r\n",size);
		gVrDemo.engineBuffer = gp_malloc_align( size , sizeof(int) ) ;
		if( gVrDemo.engineBuffer == NULL )
		{
			CYB_PRINT( "CSpotterVR buffer alloc failed\r\n" ) ;
			break ;
		}
		gVrDemo.cspotter = CSpotter_Init_Multi( gVrDemo.baseModel , &gVrDemo.sdModel , 1 , 1000 , gVrDemo.engineBuffer , size , gVrDemo.cspooterState , sizeof(gVrDemo.cspooterState) , &ret ) ;
		if( gVrDemo.cspotter == NULL )
		{
			CYB_PRINT( "CSpotterVR engine init failed(error code:%d).\r\n" , ret ) ;
			break ;
		}

		//CYB_PRINT( "SD : %d\r\n", ( gVrDemo.sdModel == NULL )? 1 : 2 );
		CYB_PRINT("SV level = %d",sv_level);
		CSpotter_SetSpeakerIdentLevel(gVrDemo.cspotter, sv_level);

		//set shortest response time for demo purpose
//		CSpotter_SetResponseTime(gVrDemo.cspotter, 0);

		// Start Audio
		gVrDemo.state = CSPOTTER_PROCESS_VR ;

		if(audio_encode_status() == AUDIO_CODEC_PROCESS_END)
		{
			media_source.type = SOURCE_TYPE_USER_DEFINE;

			media_source.Format.AudioFormat = VR_PCM;
			ret = audio_encode_start(media_source, BUILD_IN_MIC , 16000, 0);
			if (ret != 0)
			{
				CYB_PRINT( "Audio start failed(error code:%d).\r\n" , ret ) ;
				continue;
			}
		}


		CYB_PRINT( "CSpotterVR start.\r\n" ) ;

		return ;

	} while(0) ;

	gVrDemo.state = CSPOTTER_IDLE ;

	FREE( gVrDemo.engineBuffer ) ;
}

void TestSDVR_StartLow(void)
{
	int size , ret ;
	MEDIA_SOURCE	media_source;

	CYB_PRINT( "gVrDemo.sdModel = %x.\r\n" , gVrDemo.sdModel) ;

	do
	{
		if( gVrDemo.state != CSPOTTER_IDLE )
			break ;

		CYB_NEWLINE() ;

		FREE( gVrDemo.engineBuffer ) ;

		// Init CSpotter Engine
		size = CSpotter_GetMemoryUsage_Multi( gVrDemo.baseModel , &gVrDemo.sdModel , 1 , 1000 ) ;
		DBG_PRINT("CSPotter Init Mem Size = 0x%x \r\n",size);
		gVrDemo.engineBuffer = gp_malloc_align( size , sizeof(int) ) ;
		if( gVrDemo.engineBuffer == NULL )
		{
			CYB_PRINT( "CSpotterVR buffer alloc failed.\r\n" ) ;
			break ;
		}
		gVrDemo.cspotter = CSpotter_Init_Multi( gVrDemo.baseModel , &gVrDemo.sdModel , 1 , 1000 , gVrDemo.engineBuffer , size , gVrDemo.cspooterState , sizeof(gVrDemo.cspooterState) , &ret ) ;
		if( gVrDemo.cspotter == NULL )
		{
			CYB_PRINT( "CSpotterVR engine init failed(error code:%d).\r\n" , ret ) ;
			break ;
		}

		//CYB_PRINT( "SD : %d\r\n", ( gVrDemo.sdModel == NULL )? 1 : 2 );

		//set shortest response time for demo purpose
//		CSpotter_SetResponseTime(gVrDemo.cspotter, 0);

		// Start Audio
		gVrDemo.state = CSPOTTER_PROCESS_VR ;

		if(audio_encode_status() == AUDIO_CODEC_PROCESS_END)
		{
			media_source.type = SOURCE_TYPE_USER_DEFINE;

			media_source.Format.AudioFormat = VR_PCM;
			ret = audio_encode_start(media_source, BUILD_IN_MIC , 16000, 0);
			if (ret != 0)
			{
				CYB_PRINT( "Audio start failed(error code:%d).\r\n" , ret ) ;
				continue;
			}
		}


		CYB_PRINT( "CSpotterVR start.\r\n" ) ;

		return ;

	} while(0) ;

	gVrDemo.state = CSPOTTER_IDLE ;

	FREE( gVrDemo.engineBuffer ) ;
}


void IncSV_Value(void)
{
	INT32S temp;
	sv_level += 100;
	if(sv_level > 1000)
		sv_level = 1000;
	CYB_PRINT("SV level = %d \r\n",sv_level);

}

void DecSV_Value(void)
{
	sv_level -= 100;
	if(sv_level < -1000)
		sv_level = -1000;
	CYB_PRINT("SV level = %d \r\n",sv_level);

}

void CSpotter_ProcessOnce( short* samples , int n )
{
	int ret , id ;

	switch( gVrDemo.state )
	{
		case CSPOTTER_PROCESS_VR:
#if(VR_KEYWORD == 1)
			ret = CSpotter_AddSample( gVrDemo.cspotter , samples , n ) ;
			if( ret == CSPOTTER_SUCCESS )
			{
				id = CSpotter_GetResult( gVrDemo.cspotter ) ;
				if( id >= 0 )
				{
					if(id == 16){
                        show_result_over_uart(id);
                        g_keyword = 1;//keyword detected
                        gpio_write_io(IO_A15, DATA_HIGH);
                        gcnt = 0;
                    }
                    else{

                        if(g_keyword == 1){
                             g_keyword = 0;
                             show_result_over_uart(id);
                             gpio_write_io(IO_A15, DATA_LOW);
                        }

                    }

					CYB_PRINT( "ID=%d\r\n" , id ) ;
					CYB_PRINT( "ID score =%d\r\n",CSpotter_GetResultScore(gVrDemo.cspotter));
				}
			}
			// one dma buffer is 512 sampels on 16KHz sample rate
			// which is 32ms per DMA done.
            gcnt++;
            if((gcnt >= 70) && (g_keyword == 1))
            {
                gpio_write_io(IO_A15, DATA_LOW);
                g_keyword = 0;
                gcnt = 0;
            }
#else
			ret = CSpotter_AddSample( gVrDemo.cspotter , samples , n ) ;
			if( ret == CSPOTTER_SUCCESS )
			{
				id = CSpotter_GetResult( gVrDemo.cspotter ) ;
				if( id >= 0 )
				{
					CYB_NEWLINE() ;
					ret = CSpotterSD_GetTagID((char*)gVrDemo.sdModel,id);
					if(ret >= 0)
						id = ret;

					CYB_PRINT( "ID=%d\r\n" , id ) ;
					if(gVrDemo.sd_identity_enable == FALSE)
						show_result_over_uart(id);
					CYB_PRINT( "ID score =%d\r\n",CSpotter_GetResultScore(gVrDemo.cspotter));
				}
			}

#endif
			break ;

		case CSPOTTER_PROCESS_SD:
			ret = CSpotterSD_AddSample(  gVrDemo.cspotterSD , samples , n ) ;
			if( ret != CSPOTTER_ERR_NeedMoreSample )
			{
				CYB_NEWLINE() ;

				ret = CSpotterSD_AddUttrEnd( gVrDemo.cspotterSD ) ;
				if( ret != CSPOTTER_SUCCESS )
				{
					gVrDemo.state = CSPOTTER_PROCESS_ERROR ;

					CYB_PRINT( "Uttr %d failed.\r\n" , gVrDemo.sd_uttr_index + 1 ) ;
					CYB_PRINT( "Parse button to stop...\r\n" ) ;
					break ;
				}
				gVrDemo.state = CSPOTTER_PROCESS_SD_PAUSE ;

				CYB_PRINT( "Uttr %d end.\r\n" , gVrDemo.sd_uttr_index + 1 ) ;
				if( gVrDemo.sd_uttr_index + 1 < SD_MAX_UTTR )
				{
					CYB_PRINT( "Parse button for next uttr...\r\n"  ) ; // or auto stop
				}
				else
				{
					CYB_PRINT( "Parse button to start training...\r\n"  ) ; // or auto stop
				}

				gVrDemo.sd_uttr_index += 1 ;
				break ;
			}

			break ;

		default:
			CYB_BACKSPACE() ; // replace '.' when not processing ( or some state can auto stop audio )
			break ;
	}
}


INT16U vr_status_polling(void){
	if (gVrDemo.state == CSPOTTER_PROCESS_SD_PAUSE)
		return 0;
	return 1;
}


#ifndef __CYBERON_DEMO_GLOBAL_H
#define __CYBERON_DEMO_GLOBAL_H

//#include "board_config.h"
//#include "audio_encode.h"

#ifdef __GPEL_PLATFORM__
#	include "gplib.h"
#	include "gp_stdlib.h"
#endif

#include "base_types.h"
#include "CSpotterSDKApi.h"
#include "CSpotterSDTrainApi.h"

#define SD_INFO_SIZE ( 1024 )
#define SD_WORD_SIZE (2048)
#define SD_UTTR_SIZE (8192)//( 8192 )
#define SD_MAX_WORD  ( 5 ) // limit 1 , code not support
#define SD_MAX_UTTR  ( 3 )

#define SAVE_PCM_DATA	FALSE

#define VR_ONLY     0
#define VR_TTS      1
#define VR_DEMO_OPTION    VR_TTS//VR_ONLY

#define FREE(x) \
	do \
	{ \
		if( x != NULL ) \
		{ \
			gp_free(x) ; \
			x = NULL ; \
		} \
	} while(0)

#define CYB_NEWLINE() \
	do \
	{ \
		DBG_PRINT( "\r\n" ) ; \
	} while(0)

#define CYB_BACKSPACE() \
	do \
	{ \
		DBG_PRINT( "\b" ) ; \
	} while(0)

#define CYB_PRINT(...) \
	do \
	{ \
		DBG_PRINT( "|||||||||| DEMO |||||||||| " ) ; \
		DBG_PRINT( __VA_ARGS__ ) ; \
	} while(0)

typedef enum _CSpotterState
{
	CSPOTTER_IDLE ,
	CSPOTTER_PROCESS_VR ,
	CSPOTTER_PROCESS_SD ,
	CSPOTTER_PROCESS_SD_PAUSE ,
	CSPOTTER_PROCESS_ERROR
} CSpotterState ;

typedef struct _CSpotterDemoGlobal
{
	CSpotterState state ;
	BYTE*         baseModel ;
	BYTE*         commnadModel ;
	BYTE*         sdModel ;
	BYTE*         sdWordBuffer ;
	BYTE*         sdUttrBuffer ;
	BYTE*         engineBuffer ;
	int           sizeSDModel ;
	int           sizeSDWordBuffer ;
	int           sizeSDUttrBuffer ;
	HANDLE        cspotter ;
	HANDLE		  cspotterSD;
	BYTE          cspooterState[96] ;
	int           sd_uttr_index ;
	int			  sd_identity_enable;
} CSpotterDemoGlobal ;

extern CSpotterDemoGlobal gVrDemo ;

BOOL HardwareInitialize(void) ;
void HardwareRelease(void) ;
int GetKey(void) ;

void VrDemoGlobalInit(void) ;
void VrDemoGlobalRelease(void) ;

void TestVR_Start(void) ;
void TestVR_Stop(void) ;
void TrainSD_Start(void) ;
void TrainSD_Stop(void) ;
void TrainSD_Erase(void) ;
void CSpotter_ProcessOnce( short* samples , int n ) ;
void VrTtsEnable(INT16U tts_en );
#endif//__CYBERON_DEMO_GLOBAL_H

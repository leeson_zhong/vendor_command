#include "project.h"
#include "vr_demo_global.h"


#if MCU_VERSION == GPM41XXA
static void ShowMenu(void)
{
	CYB_NEWLINE() ;
	CYB_PRINT( "Key1: CSpotterVR Start\r\n" ) ;
	CYB_PRINT( "Key2: CSpotterVR Stop\r\n" ) ;
	CYB_PRINT( "Key3: Menu\r\n" ) ;
	CYB_PRINT( "Key4: Exit\r\n" ) ;
}
#else
static void ShowMenu(void)
{
	CYB_NEWLINE() ;
	CYB_PRINT( "Key1: CSpotterVR Start\r\n" ) ;
	CYB_PRINT( "Key2: CSpotterVR Stop\r\n" ) ;
	CYB_PRINT( "Key3: (RESERVED)\r\n" ) ;
	CYB_PRINT( "Key4: (RESERVED)\r\n" ) ;
	CYB_PRINT( "Key5: (RESERVED)\r\n" ) ;
	CYB_PRINT( "Key6: (RESERVED)\r\n" ) ;
	CYB_PRINT( "Key7: Menu\r\n" ) ;
	CYB_PRINT( "Key8: Exit\r\n" ) ;
}
#endif

void VrDemo(void)
{
	BOOL exit = FALSE ;

	CYB_NEWLINE() ;
	CYB_PRINT( "\t[ CSpotter Demo Entry ]\r\n" ) ;

	do
	{
		VrDemoGlobalInit() ;
		VrTtsEnable(VR_ONLY);
		if( HardwareInitialize() == FALSE )
			break ;

		while( !exit )
		{
			switch( GetKey() )
			{
#if MCU_VERSION == GPM41XXA
				case 1:
					TestVR_Start() ;
					break ;
				case 2:
					TestVR_Stop() ;
					break ;
				case 3:
					ShowMenu() ;
					break ;
				case 4:
					exit = TRUE ;
					break ;
#else
				case 1:
					TestVR_Start() ;
					break ;
				case 2:
					TestVR_Stop() ;
					break ;
				case 3:
					TrainSD_Start();
					break;
				case 4:
					TrainSD_Stop();
					break ;
				case 5:
					TrainSD_Erase();
					break;
				case 6:
					CYB_PRINT( "\t[ This Key is reserved ]\r\n" ) ;
					break;
				case 7:
					ShowMenu() ;
					break ;

				case 8:
					exit = TRUE ;
					break ;
#endif
			}
		}

	} while(0) ;

	HardwareRelease() ;
	VrDemoGlobalRelease() ;

	CYB_NEWLINE() ;
	CYB_PRINT( "\t[ CSpotter Demo Exit ]\r\n" ) ;
}

#include "project.h"
#include "vr_demo_global.h"



static void ShowMenu(void)
{
	CYB_NEWLINE() ;
	CYB_PRINT( "Key1: SDSV Test\r\n" ) ;
	CYB_PRINT( "Key2: CSpotterVR Stop\r\n" ) ;
	CYB_PRINT( "Key3: Recording a Traning command\r\n" ) ;
	CYB_PRINT( "Key4: Training a command\r\n" ) ;
	CYB_PRINT( "Key5: Erase Trained Commands\r\n" ) ;
	CYB_PRINT( "Key6: Increase SV value\r\n" ) ;
	CYB_PRINT( "Key7: Decrease SV value\r\n" ) ;
	CYB_PRINT( "Key8: Exit\r\n" ) ;
}

void VrSDSVDemo(void)
{
	BOOL exit = FALSE ;

	ShowMenu();
	CYB_NEWLINE() ;
	CYB_PRINT( "\t[ CSpotter Demo Entry ]\r\n" ) ;

	do
	{
		VrDemoGlobalInit_SDSV() ;
		if( HardwareInitialize() == FALSE )
			break ;

		while( !exit )
		{
			switch( GetKey() )
			{
				case 1:
					TestSDVR_Start();
					break ;

				case 2:
					TestVR_Stop() ;
					break ;

				case 3:
					TrainSD_Start();
					break;
				case 4:
					TrainSD_Stop();
					break ;
				case 5:
					TrainSD_Erase();
					break;
				case 6:
					IncSV_Value();
					break;
				case 7:
					DecSV_Value() ;
					break ;

				case 8:
					ShowMenu();
					//exit = TRUE ;
					break ;
			}
		}

	} while(0) ;

	HardwareRelease() ;
	VrDemoGlobalRelease() ;

	CYB_NEWLINE() ;
	CYB_PRINT( "\t[ CSpotter Demo Exit ]\r\n" ) ;
}

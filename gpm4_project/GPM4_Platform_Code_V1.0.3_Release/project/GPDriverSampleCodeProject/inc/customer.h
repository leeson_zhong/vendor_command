#ifndef __CUSTOMER_H__
#define __CUSTOMER_H__

/* Leading Define Start */

#define CUSTOM_ON       1
#define CUSTOM_OFF      0

#define CUSTOMER_H_ENABLE CUSTOM_OFF  /* decision the customer define mode on/off */

//#if CUSTOMER_H_ENABLE == CUSTOM_ON
//#define CUSTOMER_DEFINE
//#endif

#if 1

/*=== IC Version definition choices ===*/
//---------------------------------------------------------------------------
#define	GPL32_A                     0x1                                    //
#define	GPL32_B                     0x2                                    //
#define	GPL32_C                     0x3                                    //
#define	GPL326XX                    0x10                                   //
#define GPL326XX_B                  0x11                                   //
#define GPL326XX_C                  0x12                                   //
#define GPL326XXB                   0x17                                   //
#define GP326XXXA                   0x18                                   //
#define GPL327XX                    0x20                                   //
#define GP327XXXA                   0x21                                   //
#define MCU_VERSION                 GPL326XXB                              //
//---------------------------------------------------------------------------


/*=== Board ID Config ===*/
//---------------------------------------------------------------------------
#define BOARD_TURNKEY_BASE      0x10000000                                 //
#define BOARD_EMU_BASE          0x00000000                                 //
                                                                           //
#define BOARD_EMU_V1_0          (BOARD_EMU_BASE + 0x10)                    //
#define BOARD_EMU_V2_0          (BOARD_EMU_BASE + 0x20)                    //
#define BOARD_DEMO_GPL32XXX     (BOARD_EMU_BASE + 0x30)                    //
#define BOARD_DEMO_GPL326XX     (BOARD_EMU_BASE + 0x40)                    //
#define BOARD_DEMO_GPL327XX     (BOARD_EMU_BASE + 0x50)                    //
#define BOARD_DEMO_GPL326XXB    (BOARD_EMU_BASE + 0x60)                    //
#define BOARD_DEMO_GP326XXXA    (BOARD_EMU_BASE + 0x70)                    //
#define BOARD_DEMO_GP327XXXA    (BOARD_EMU_BASE + 0x80)                    //
#define BOARD_TK35_V1_0         (BOARD_TURNKEY_BASE + 0x10)                //
#define BOARD_TK_V1_0           (BOARD_TURNKEY_BASE + 0x20)                //
#define BOARD_TK_V2_0           (BOARD_TURNKEY_BASE + 0x30)                //
#define BOARD_TK_V3_0           (BOARD_TURNKEY_BASE + 0x40)                //
#define BOARD_TK_V4_0           (BOARD_TURNKEY_BASE + 0x50)                //
#define BOARD_TK_V4_1           (BOARD_TURNKEY_BASE + 0x60)                //
#define BOARD_TK_V4_3           (BOARD_TURNKEY_BASE + 0x70)                //
#define BOARD_TK_A_V1_0         (BOARD_TURNKEY_BASE + 0x80)                //
#define BOARD_GPY0200_EMU_V2_0  (BOARD_TURNKEY_BASE + 0x90)                //
#define BOARD_TK_V4_4           (BOARD_TURNKEY_BASE + 0xA0)                //
#define BOARD_TK_A_V2_0         (BOARD_TURNKEY_BASE + 0xB0)                //
#define BOARD_TK_A_V2_1_5KEY    (BOARD_TURNKEY_BASE + 0xC0)                //
#define BOARD_TK_7D_V1_0_7KEY   (BOARD_TURNKEY_BASE + 0xD0)                //
#define BOARD_TK_8D_V1_0        (BOARD_TURNKEY_BASE + 0xE0)                //
#define BOARD_TK_8D_V2_0        (BOARD_TURNKEY_BASE + 0xF0)                //
#define BOARD_TK_32600_V1_0     (BOARD_TURNKEY_BASE + 0x100)               //
#define BOARD_TK_32600_COMBO    (BOARD_TURNKEY_BASE + 0x110)               //
#define BOARD_TK_32600_7A_V1_0  (BOARD_TURNKEY_BASE + 0x120)               //
#define BOARD_TYPE              BOARD_DEMO_GPL326XXB
//---------------------------------------------------------------------------

/*=== software ID Config ===*/
//---------------------------------------------------------------------------
#define SOFTWARE_VER            "v1.6.0.0000.0000"                        //
//---------------------------------------------------------------------------

/*=== Key support ===*/
//---------------------------------------------------------------------------
#define KEY_AD_NONE             0                                          //
#define KEY_AD_4_MODE           4                                          //
#define KEY_AD_5_MODE           5                                          //
#define KEY_AD_6_MODE           6                                          //
#define KEY_AD_8_MODE           8                                          //
#define KEY_IO_MODE             2                                          //
#define KEY_USER_MODE           3                                          //
#define SUPPORT_AD_KEY          KEY_AD_8_MODE                              //
//---------------------------------------------------------------------------

/*=== web cam ===*/
//---------------------------------------------------------------------------
#define C_UVC						CUSTOM_OFF
#define C_USB_AUDIO					CUSTOM_OFF
//---------------------------------------------------------------------------

#endif //CUSTOMER_DEFINE


#endif //__CUSTOMER_H__

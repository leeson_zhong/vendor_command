#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "project.h"

#if (_DRV_L1_I2S_RX == 1)

#include "drv_l2.h"
#include "drv_l1_cfg.h"
#include "drv_l1_sfr.h"
#include "drv_l1_i2c.h"
#include "drv_l1_dma.h"
#include "drv_l1_gpio.h"
#include "gplib.h"
#include "gplib_cfg.h"
#include "drv_l2_sccb.h"
#include "i2s_rx_demo.h"

#define USE_I2C_GPIO            0

#define K_I2S_USE_REAL_BOARD    1

#define K_HAVE_FILE_SYSTEM      GPLIB_FILE_SYSTEM_EN

#define K_RX_DMA_BULK_SIZE              4096 // 153600 // 153600 // 256 // word

#define K_INPUT_MICRO_PHONE             1
#define K_INPUT_LINE_IN                 2

#define K_DATA_AT_BUFFER                0
#define K_DATA_AT_DISK                  1

#if (K_HAVE_FILE_SYSTEM)
#define K_AUDIO_OUTPUT_TARGET           K_DATA_AT_DISK //K_DATA_AT_DISK
#else
#define K_AUDIO_OUTPUT_TARGET           K_DATA_AT_BUFFER
#endif

#define K_AUDIO_STEREO                   0
#define K_AUDIO_MONO                     1

#define K_I2S_NONMERGE_MODE              0
#define K_I2S_MERGE_MODE                 1

#define K_AUDIO_CODEC_BCM_CLK_MODE       1
#define K_AUDIO_CODEC_DEFO_CLK_MODE      0

#define K_I2S_RX_USE_INTERRUPT           0

#define I2S_RX_SIG            0xbeafbeaf
#define I2S_TX_SIG            0xfaebfaeb

extern int merge_2ch(short *p, int times);
extern int merge_2ch_size_in_word(int times);

#define MAX_I2X_RX_NUM 		4
#define MAX_PCM_BUF_NUM 	20

typedef struct pcm_buf_entry_s
{
    INT16S *buf;
    volatile INT32U buf_len;
    volatile INT8U filled;
    volatile INT8U stopped;
} pcm_buf_entry_t;

static pcm_buf_entry_t pcm_buf_list[MAX_I2X_RX_NUM][MAX_PCM_BUF_NUM];
static volatile INT32U consumer_buf_idx_list[MAX_I2X_RX_NUM];
static volatile INT32U producer_buf_idx_list[MAX_I2X_RX_NUM];
static INT8U startup_done_flag[MAX_I2X_RX_NUM];

void i2s_rx_demo_buf_init(INT32U ch);

i2s_rx_demo_t **prx_demo = NULL;

#define SLAVE_ID_0 0x34
#define SLAVE_ID_1 0x36

typedef drv_l1_i2c_bus_handle_t i2c_bus_handle_t;

typedef union i2s_i2c_config_def
{
    drv_l1_i2c_bus_handle_t    i2c_hw;
    sccb_config_t              i2c_gpio;
} i2s_i2c_config_t;

typedef struct i2s_i2c_def
{
    i2s_i2c_config_t config;
    INT8U use_gpio;
    void *handle;
} i2s_i2c_t;

typedef union _WM8988
{
    short info;
    char  data[2];
}WM8988;

typedef struct i2s_profile_s
{
    INT32U frame_size; // 16(0), 24(1), 32(2), 48(3), 64(4), 96(5), 128(6), 176(7), 192(8), 32(15, slave use)
    INT32U MCLK_clock_rate; // 12300000 = 12.3MHz
    INT32U sample_rate; // ex 8K=8000,48K=48000
                        // 目前FPGA MCLK=12.3MHz
                        // 所以1T BCLK :12.3M/4=3.075MHz
                        // 1T LRCK: 3.075/64=48KHz
                        // MCLK/1   BCLK = 12.3MHz,   LRCK = 192KHz
                        // MCLK/2,  BCLK = 6.15MHz,   LRCK =  96KHz
                        // MCLK/3,  BCLK = 4.1MHz,    LRCK =  64KHz
                        // MCLK/4,  BCLK = 3.075MHz,  LRCK =  48KHz
                        // MCLK/6,  BCLK = 2.05MHz,   LRCK =  32KHz
                        // MCLK/8,  BCLK = 1.5375MHz, LRCK =  24KHz
    INT32 MCLK_div;
    INT32U data_size;   // 16(0), 18(1), 20(2), 22(3), 24(4), 32(5), 32(6), 32(7)


    INT8U merge;
    INT8U mono;
    INT8U r_lsb;
    INT8U normal_mode_aligned;
    INT8U send_mode;
    INT8U edge_mode;
    INT8U frame_polar;
    INT8U first_frame_LR;
    INT8U framingmode; // 0 is I2S mode, 1 is Normal Mode, 2 is DSP mode, 3 is DSP mode

} i2s_profile_t;

static sccb_config_t i2c_0_2_config =
{   // I2C0 # 2
    .scl_port = IO_A8,
    .scl_drv = IOD_DRV_4mA,
    .sda_port = IO_A9,
    .sda_drv = IOD_DRV_4mA,
    .pwdn_port = (GPIO_ENUM)0, // when have_pwdn is 0, this filed dont care
    .pwdn_drv = IOD_DRV_4mA,   // not yet implemented
    .have_pwdn = 0,
    .RegBits = 8,
    .DataBits = 8,
    .slaveAddr = 0,
    .timeout = 2000,            // in ms unit, not yet implemented
    .clock_rate = 20            // in kHz unit, not yet implemented
};

static sccb_config_t i2c_2_2_config =
{   // I2C2 # 2
    .scl_port = IO_A10,
    .scl_drv = IOD_DRV_4mA,
    .sda_port = IO_A11,
    .sda_drv = IOD_DRV_4mA,
    .pwdn_port = (GPIO_ENUM)0, // when have_pwdn is 0, this filed dont care
    .pwdn_drv = IOD_DRV_4mA,   // not yet implemented
    .have_pwdn = 0,
    .RegBits = 8,
    .DataBits = 8,
    .slaveAddr = 0,
    .timeout = 2000,            // in ms unit, not yet implemented
    .clock_rate = 20            // in kHz unit, not yet implemented
};

static sccb_config_t i2c_1_0_config =
{   // I2C1 # 0
    .scl_port = IO_C0,
    .scl_drv = IOD_DRV_4mA,
    .sda_port = IO_C1,
    .sda_drv = IOD_DRV_4mA,
    .pwdn_port = (GPIO_ENUM)0, // when have_pwdn is 0, this filed dont care
    .pwdn_drv = IOD_DRV_4mA,   // not yet implemented
    .have_pwdn = 0,
    .RegBits = 8,
    .DataBits = 8,
    .slaveAddr = 0,
    .timeout = 2000,            // in ms unit, not yet implemented
    .clock_rate = 20            // in kHz unit, not yet implemented
};

static sccb_config_t i2c_IOA15_IOA14_config =
{
    .scl_port = IO_A15,
    .scl_drv = IOD_DRV_4mA,
    .sda_port = IO_A14,
    .sda_drv = IOD_DRV_4mA,
    .pwdn_port = (GPIO_ENUM)0, // when have_pwdn is 0, this filed dont care
    .pwdn_drv = IOD_DRV_4mA,   // not yet implemented
    .have_pwdn = 0,
    .RegBits = 8,
    .DataBits = 8,
    .slaveAddr = 0,
    .timeout = 2000,            // in ms unit, not yet implemented
    .clock_rate = 20            // in kHz unit, not yet implemented
};

static void i2s_delay(unsigned int num)
{
    volatile int i;

    for (i=0; i<num; ++i)
    {
        // nonsense , just delay
        //R_RANDOM0 = i;
        __asm("nop");
    }
}

static int i2c_write(i2s_i2c_t *pi2s_i2c, INT8U reg, INT8U value)
{
    int ret;
    int i=0;

    for (i=0;i<100;++i)
    {
        if (pi2s_i2c->use_gpio)
            ret = drv_l2_sccb_write(pi2s_i2c->handle , reg, value);
        else
            ret = drv_l1_reg_1byte_data_1byte_write(pi2s_i2c->handle, reg, value);
        if (ret != -1)
        {
          break;
        }
        else
        {
          DBG_PRINT("drv_l1_reg_1byte_data_1byte_write fail. reg=%d value=%0x02x\r\n", reg, value);
          i2s_delay(0x10000); // 0x4FFFF
        }
    }
    if ( ret == -1 ) {
      while (1) R_RANDOM0 = i;
    }

    //i2s_delay(0x10000); // 0x4FFFF

    return ret;
}

static short i2c_wolfson_WM8988(int addr, int data)
{
   int hi = addr << (8+1);
   int lo = data;
   int cmd_swap = (hi|lo);
   int cmd = ( ((cmd_swap>>8) & 0x000000ff) |  ((cmd_swap<<8) & 0x0000ff00) );

   return (short)cmd;
}

static void wolfson_WM8988_power_down(i2s_i2c_t *pi2s_i2c)
{
    WM8988 pack;

    pack.info = i2c_wolfson_WM8988(25,0x00);
    i2c_write(pi2s_i2c, pack.data[0] ,pack.data[1]);
}

static void wolfson_WM8988_init(i2s_i2c_t *pi2s_i2c,  INT8U audio_input_source, i2s_profile_t *prx_profile)
{
    WM8988 pack;
    INT32U R, boost, mic_pga_l, mic_pga_r;
    INT32U alc_max = 0x7, alc_target = 0xB;
    INT16U bcm = 0;
    INT16U WL;
    INT16U monomix;
    INT16U datsel;
    INT16U BCLKINV = 0;
    INT16U LRP = 0;
    INT16U format = 2; // I2S mode
    INT16U clkdiv2 = 0;
    INT16U mclk_44K = 0;
    INT16U higher_mclk = 0;
    INT16U SR;

    if (audio_input_source == K_INPUT_LINE_IN)
    {
        boost = 0x0;
        mic_pga_l = 0x2A; // default 010111=0x17
        mic_pga_r = 0x2A; // default 010111=0x17
    }
    else
    {
        boost = 0x3;
        mic_pga_l = 0x20;
        mic_pga_r = 0x7;
    }

    //pack.info = i2c_wolfson_WM8988(15,0x0);	//reset
    //reg_1byte_data_1byte_write(pi2s_i2c,pack.data[0] ,pack.data[1]);

    pack.info = i2c_wolfson_WM8988(67,0x0); // Low Power Playback
    i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);
    pack.info = i2c_wolfson_WM8988(24,0x0); // Additional control(2)
    i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);
    pack.info = i2c_wolfson_WM8988(25,0x0FC); // Pwr Mgmt(1) // 0xEC not work, 0x0FC work.
    i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);
    pack.info = i2c_wolfson_WM8988(26,0x1F8); // Pwr Mgmt(2)
    i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);

    if (prx_profile->data_size == 32)
        WL = 3;
    else if (prx_profile->data_size == 24)
        WL = 2;
    else if (prx_profile->data_size == 20)
        WL = 1;
    else if (prx_profile->data_size == 16)
        WL = 0; // data_size 16 bit
    else
        WL = 3; // data_size 32 bit

    if (prx_profile->edge_mode)
    {
      BCLKINV = 1;
      LRP = 0;
    }

    if (prx_profile->framingmode == 1)
      format = 1; // left justified

    pack.info = i2c_wolfson_WM8988(7,format | (1<<6) | (WL<<2) | (BCLKINV << 7) | (LRP << 4));  // Audio Interface,
                                                     //  [7]BCLKINV, 0 BLCK not inverted, 1 BLCK onverted
                                                     //  [6]MS, 1 Enable Master Mode, 0 Slave Mode
                                                     //  [5]LRSWAP 1 swap left and right, 0 no swap
                                                     //  [4]LRP 1 invert LRCLK polarity
                                                     //  [3:2] WL Audio Data word length, 00 16 bits, 01 20 bits, 10 24 bits, 11 32 bits
                                                     //  [1:0] Format Audio Data Format, 00 reserve, 01 Left justified, 10 I2S format 11 DSP Mode
    i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);

#if 0
     pack.info = i2c_wolfson_WM8988(8, (1<<0)); // Audio Interface,
                                                // [0]=1 usb mode, [5:1]=0 48MHz, [6]=0, MCLK is not devided
     i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);
#endif

    if (prx_profile->mono == K_AUDIO_MONO)
    {
        monomix = 0;  //0 stereo
                     // 1 analog mono mix using left ADC
                     // 2 analog mono mix using right ADC
                     // 3 digital mono mix
        datsel = 0;  // 0 left data = left ADC, right data = right ADC
                     // 1 left data = left ADC, right data = left ADC, for mono mix using left ADC
                     // 2 left data = right ADC, right data = right ADC, for mono mix using right ADC
                     // 3 left data = right ADC, right data = left ADC, swap
    }
    else
    {
        monomix = 0;
        datsel = 0;
    }
    if (audio_input_source == K_INPUT_LINE_IN)
    {
        pack.info = i2c_wolfson_WM8988(31,monomix << 6); // ADC input mode, LINPUT1 - RINUT1, 8=0 DS=0, 7:6=0x00 stereo, 5=0 4=0 Normal Operation PGA enable,
        i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);
        pack.info = i2c_wolfson_WM8988(23,datsel << 2);
        i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);
    }
    else
    {
        pack.info = i2c_wolfson_WM8988(31,0x100 | (monomix << 6)); // ADC input mode, LINPUT2 - RINUT2, 8=0 DS=1, 7:6=0x00 stereo, 5=0 4=0 Normal Operation PGA enable,
        i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);
        pack.info = i2c_wolfson_WM8988(23,datsel << 2);
        i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);
    }

    if (audio_input_source == K_INPUT_LINE_IN)
    {
        pack.info = i2c_wolfson_WM8988(32,0x00|(boost<<4)); // ADC signal path control Left
                                                            // [7:6] 00 input1, 01 input2, 11 diffrential
                                                            // input1 is line in, input2 is MicroPhone
        i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);
        pack.info = i2c_wolfson_WM8988(33,0x00|(boost<<4)); // ADC signal path control Right
        i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);
    }
    else
    {
        pack.info = i2c_wolfson_WM8988(32,0xC0|(boost<<4)); // ADC signal path control Left
                                                            // [7:6] 00 input1, 01 input2, 11 diffrential
                                                            // input1 is line in, input2 is MicroPhone
        i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);
        pack.info = i2c_wolfson_WM8988(33,0xC0|(boost<<4)); // ADC signal path control Right
        i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);
    }

    {
        R = 0x97;
        R &= ~0x3F;
        R &= ~0x80; 		//Disable Mute
        R |= 0x100;
        R |= mic_pga_l;
        pack.info = i2c_wolfson_WM8988(0,R); // left channel pga
        i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);
    }
    {
        R = 0x97;
        R &= ~0x3F;
        R &= ~0x80;		 //Disable Mute
        R |= 0x100;
        R |= mic_pga_r;
        pack.info = i2c_wolfson_WM8988(1,R); // right channel pga
        i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);
    }
    {
        R  = 0x7B;
        R &= ~0x7;
        R |= alc_max << 4;
        R |= alc_target;
        if (audio_input_source == K_INPUT_LINE_IN)
          //R |= 0x180; //turn on ALC
          R = 0; // turn off ALCA
        else
          R = 0; // turn off ALCA
        pack.info = i2c_wolfson_WM8988(17,R); //  ALC Control 1
        i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);
    }

    if (audio_input_source == K_INPUT_LINE_IN)
    {
        pack.info = i2c_wolfson_WM8988(21,0x1D0); // Left ADC Volumn, default is 0x1C3, 0x18B
        i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);
        pack.info = i2c_wolfson_WM8988(22,0x1D0); // Right ADC Volumn, default is 0x1C3, 0x18B
        i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);
    }
    else
    {
        pack.info = i2c_wolfson_WM8988(21,0x1D0); // Left ADC Volumn
        i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);
        pack.info = i2c_wolfson_WM8988(22,0x1D0); // Right ADC Volumn
        i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);
    }

#if 0
    if ( (prx_profile->data_size == 32) || (prx_profile->frame_size == 0) || \
        ((prx_profile->data_size == 16) && (prx_profile->mono == K_AUDIO_STEREO) && \
        (prx_profile->merge == K_I2S_MERGE_MODE) && (prx_profile->frame_size == 32)))
        bcm = 0; // 0 BCM function disable
#endif

#if K_I2S_USE_REAL_BOARD == 1
#else
    if (prx_profile->frame_size == prx_profile->data_size)
    {
        if (prx_profile->sample_rate <= 16000 || prx_profile->sample_rate >= 96000)
          bcm = 1; // when sample rate, wolfson codec fixed the bck to MCLK/4, frame size bck count will more than expected by our I2C
    }
#endif
          // 1, MCLK/4
          // 2, MCLK/8
          // 3, MCLK/16
    if (prx_profile->MCLK_clock_rate == 24576000 || \
        prx_profile->MCLK_clock_rate == 22579000 || \
        prx_profile->MCLK_clock_rate == 36864000 || \
        prx_profile->MCLK_clock_rate == 33869000 )
        clkdiv2 = 1;

    if (prx_profile->MCLK_clock_rate == 18432000 || \
        prx_profile->MCLK_clock_rate == 36864000 || \
        prx_profile->MCLK_clock_rate == 16934000 || \
        prx_profile->MCLK_clock_rate == 33869000)
        higher_mclk = 1; // higher than

    if (prx_profile->MCLK_clock_rate == 11289000 || \
        prx_profile->MCLK_clock_rate == 22579000 || \
        prx_profile->MCLK_clock_rate == 16934000 || \
        prx_profile->MCLK_clock_rate == 33869000 )
        mclk_44K = 1; // 44K freq series

    if (prx_profile->sample_rate == 24000)
    {
        SR = 0x1C; // SR[4:0]=11100=0x1C
        if (mclk_44K != 0)
          DBG_PRINT("sample rate 24K, mclk freq wrong. %u\r\n", prx_profile->MCLK_clock_rate);
    }
    else if (prx_profile->sample_rate == 32000)
    {
        SR = 0x0C; // SR[4:0]=01100=0x0C
        if (mclk_44K != 0)
          DBG_PRINT("sample rate 32K, mclk freq wrong. %u\r\n", prx_profile->MCLK_clock_rate);
    }
    else if (prx_profile->sample_rate == 96000)
    {
        SR = 0x0E; // SR[4:0]=01110=0x0E
        if (mclk_44K != 0)
          DBG_PRINT("sample rate 96K, mclk freq wrong. %u\r\n", prx_profile->MCLK_clock_rate);
       if (clkdiv2 == 0)
          DBG_PRINT("sample rate 96K, mclk freq must at >= 24576000 because mclk freq must >= 4xbclk freq. %u\r\n", prx_profile->MCLK_clock_rate);
    }
    else if (prx_profile->sample_rate == 88000)
    {
        SR = 0x0E; // SR[4:0]=01110=0x0E
        if (mclk_44K == 0)
          DBG_PRINT("sample rate 88.2K, mclk freq wrong. %u\r\n", prx_profile->MCLK_clock_rate);
       if (clkdiv2 == 0)
          DBG_PRINT("sample rate 88.2K, mclk freq must at >= 22579000 because mclk freq must >= 4xbclk freq. %u\r\n", prx_profile->MCLK_clock_rate);
    }
    else if (prx_profile->sample_rate == 16000)
    {
        SR = 0x0A; // SR[4:0]=01010=0x0A
        if (mclk_44K != 0)
          DBG_PRINT("sample rate 16K, mclk freq wrong. %u\r\n", prx_profile->MCLK_clock_rate);
    }
    else if (prx_profile->sample_rate == 22000)
    {
        SR = 0x0A; // SR[4:0]=01010=0x0A
        if (mclk_44K == 0)
          DBG_PRINT("sample rate 22.05K, mclk freq wrong. %u\r\n", prx_profile->MCLK_clock_rate);
    }
    else if (prx_profile->sample_rate == 8000)
    {
        SR = 0x06; // SR[4:0]=00110=0x06
        if (mclk_44K != 0)
          DBG_PRINT("sample rate 8K, mclk freq wrong. %u\r\n", prx_profile->MCLK_clock_rate);
    }
    else if (prx_profile->sample_rate == 8017)
    {
        SR = 0x06; // SR[4:0]=00110=0x06
        if (mclk_44K == 0)
          DBG_PRINT("sample rate 8.0182K, mclk freq wrong. %u\r\n", prx_profile->MCLK_clock_rate);
    }
    else if (prx_profile->sample_rate == 12000)
    {
        SR = 0x08; // SR[4:0]=01000=0x08
        if (mclk_44K != 0)
          DBG_PRINT("sample rate 12K, mclk freq wrong. %u\r\n", prx_profile->MCLK_clock_rate);
    }
    else if (prx_profile->sample_rate == 11000)
    {
        SR = 0x08; // SR[4:0]=01000=0x08
        if (mclk_44K == 0)
          DBG_PRINT("sample rate 11.025K, mclk freq wrong. %u\r\n", prx_profile->MCLK_clock_rate);
    }
    else if (prx_profile->sample_rate == 44000)
    {
        SR = 0;   // SR[4:0]=00000=0x00
        if (mclk_44K == 0)
          DBG_PRINT("sample rate 44.1K, mclk freq wrong. %u\r\n", prx_profile->MCLK_clock_rate);
    }
    else // 48K
    {
        SR = 0;   // SR[4:0]=00000=0x00
        if (mclk_44K != 0)
          DBG_PRINT("sample rate 48K, mclk freq wrong. %u\r\n", prx_profile->MCLK_clock_rate);
    }

    pack.info = i2c_wolfson_WM8988(8, ((SR | (mclk_44K<<4) | higher_mclk)<<1) | (bcm << 7) | (clkdiv2 << 6) );

    i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);
}

#if (K_I2S_RX_USE_INTERRUPT == 1)

static void i2s_app_wait_rx_fifo_half_full(INT32U channel, INT32 fifo_count_to_wait)
{
    INT32U fifo_count;

    fifo_count = drv_l1_i2s_rx_get_fifo_count(channel);

    while (fifo_count < fifo_count_to_wait)
    {
        fifo_count = drv_l1_i2s_rx_get_fifo_count(channel);
    }
    DBG_PRINT("rx fifo_count=%d\r\n", fifo_count);

    return;
}
#endif

static char *out_folder_name = "audio_output";
static char *out_file_name[4] =
    {
        "tx_rx_ch_00.pcm",
        "tx_rx_ch_01.pcm",
        "tx_rx_ch_02.pcm",
        "tx_rx_ch_03.pcm"
    };

static char *out_original_folder_name = "audio_output_original";

static char *out_original_file_name[4] =
    {
        "tx_rx_ch_00_original.pcm",
        "tx_rx_ch_01_original.pcm",
        "tx_rx_ch_02_original.pcm",
        "tx_rx_ch_03_original.pcm"
    };

static INT32U pcm_rx_audio_buf_size;
static INT8U *pcm_rx_audio_buf;
static INT32U pcm_rx_audio_buf2_size;
static INT8U *pcm_rx_audio_buf2;
static INT32U pcm_rx_audio_buf3_size;
static INT8U *pcm_rx_audio_buf3;
static INT32U pcm_rx_audio_buf4_size;
static INT8U *pcm_rx_audio_buf4;

#define K_MCLK_RATE         12300000 //  目前FPGA MCLK=12.3MHz

typedef struct i2s_dma_s
{
    INT32U buf_len; // word unit, buffer len
    short *buffer;
    INT32U dma_bulk_size;
    volatile  INT8S notify_flag;
    INT32U dma_done_count;
    INT8S complete_flag;
    INT32U acc_dma_size;
    INT32U overrun_count; // rx
    INT32U ovf_count;
    INT32U underrun_count; // tx
    INT32U half_full_count;
} i2s_dma_t;

typedef struct i2s_app_s
{
    INT32U run_number;
    INT32U channel;
    unsigned int TestState;
    INT32U loop_max;
    int ret;
    INT8U input_source;
    INT8U output_target;     // K_AUDIO_OUTPUT_TARGET
    volatile INT8U running;

    i2s_i2c_t i2s_i2c;

    i2s_dma_t rx_dma;
    i2s_profile_t rx_profile;

    INT16S fd_in;
    INT32S read_size;
    INT32S read_ret;
    char *in_file_name;
    char *buffer_file_name;

    INT16S fd_out;
    INT32S write_size;
    INT32S write_ret;
    INT8U drv;
    char out_file_name[256];
    char out_original_file_name[256];
    INT16S fd_out_original;

    INT64U f_size;
	INT64U acc_write_size;

} i2s_app_t;

static i2s_app_t i2s_apps[4];

#if (K_I2S_RX_USE_INTERRUPT == 1)

static INT32U i2s_rx_sts_check(INT32U channel, INT32U bit_mask, char *msg)
{
    volatile i2sRxReg_t *i2s = drv_l1_i2s_rx_get_register_base_addr(channel);
    INT32U status;

    status = i2s->RX_STATUS;

    if ((status & bit_mask) == bit_mask)
    {
        if (msg != NULL)
            DBG_PRINT("%s, sts=%08x\r\n", msg, status);

        return 1;
    }
    else
        return 0;
}

static INT32U i2s_rx_ctl_check(INT32U channel, INT32U bit_mask, char *msg, INT32U bit_clear)
{
    volatile i2sRxReg_t *i2s = drv_l1_i2s_rx_get_register_base_addr(channel);
    INT32U ctl, ctl_org;

    ctl = i2s->RX_CTRL;
    if ((ctl & bit_mask) == bit_mask)
    {
        ctl_org = ctl;
        if (bit_clear == bit_mask)
        {
            ctl |= bit_clear;
            i2s->RX_CTRL = ctl;
            if (msg != NULL)
                DBG_PRINT("%s, ctl org=%08x clr=%08x, aft=%08x\r\n", msg, ctl_org,  ctl, i2s->RX_CTRL);
        }
        else if (bit_clear == (~bit_mask))
        {
            ctl &= bit_clear;
            i2s->RX_CTRL = ctl;
            if (msg != NULL)
                DBG_PRINT("%s, ctl org=%08x clr=%08x, aft=%08x\r\n", msg, ctl_org,  ctl, i2s->RX_CTRL);
        }
        else
        {
            if (msg != NULL)
                DBG_PRINT("%s, ctl_org=%08x\r\n", msg, ctl_org);
        }

        return 1;
    }
    else
        return 0;
}

static void i2s_rx_isr(INT32U channel, INT32U global)
{
    volatile i2sRxReg_t *i2s = drv_l1_i2s_rx_get_register_base_addr(channel);
    INT32U ctl, ctl_org, ctl_aft;
    INT32U sts;
    i2s_app_t *pi2s_app;
    i2s_dma_t *prx_dma;

    sts = i2s->RX_STATUS;
    ctl_org = i2s->RX_CTRL;

    ctl = ctl_org | (1<< K_I2S_RX_CTL_BIT_IRT_PEND); //clear interrupt
    i2s->RX_CTRL = ctl;
    ctl_aft = i2s->RX_CTRL;

    pi2s_app = &i2s_apps[channel];
    prx_dma = &pi2s_app->rx_dma;
    prx_dma->half_full_count++;

#if 0
    if ((ctl_aft & (1<< K_I2S_RX_CTL_BIT_IRT_PEND)) != 0)
    {
        // Although clear, if fifo still over half, the bit is still 1.
        DBG_PRINT("1 fatal:rx interrupt still 1.%08x\r\n", ctl_aft);
        while (1);
    }
#endif

#if 1
    if (i2s_rx_ctl_check(channel, (1<<K_I2S_RX_CTL_BIT_OVF), 0, (1<<K_I2S_RX_CTL_BIT_OVF)))
    {
        prx_dma->ovf_count++;
        //DBG_PRINT("rxch=%d, sts=%08x ctrl=%08x, %d\r\n", channel, sts, i2s->RX_CTRL, prx_dma->half_full_count);
    }
#endif

    #if 1 // TEST
    //if ((sts & (1<<1)) != 0) // check full
    if ((ctl_aft & (1<< K_I2S_RX_CTL_BIT_IRT_PEND)) != 0) // check half full
    {
        // it is full or half full
        drv_l1_i2s_rx_clear_fifo(channel);
        if ((i2s->RX_CTRL & (1<< K_I2S_RX_CTL_BIT_IRT_PEND)) != 0)
        {
            DBG_PRINT("2 fatal:rx interrupt still 1.%08x\r\n", i2s->RX_CTRL);
            while (1);
        }
    }
    #endif

    if (prx_dma->half_full_count < 5)
    {
        DBG_PRINT("rxch=%d, sts=%08x ctrl=%08x, %d\r\n", channel, sts, i2s->RX_CTRL, prx_dma->half_full_count);
    }
}

#endif

static int i2s_app_rx_buf_init(INT32U channel, i2s_dma_t *prx_dma, INT32U aligned_size)
{
    INT32U buf_aligned_size = 4;

    if (channel == 1)
    {
		if (pcm_rx_audio_buf2 == NULL)
		{
			pcm_rx_audio_buf2_size = K_AUDIO_BUF_SIZE;
			pcm_rx_audio_buf2 = pvPortMalloc(pcm_rx_audio_buf2_size + buf_aligned_size);
			if (pcm_rx_audio_buf2 == NULL)
				return -1;
		}
        prx_dma->buffer = (short *)(((INT32U)pcm_rx_audio_buf2 + (buf_aligned_size - 1)) & (~(buf_aligned_size - 1))); // aligned to 4 byte multiple
        prx_dma->buf_len = pcm_rx_audio_buf2_size / 4;
    }
    else if (channel == 2)
    {
		if (pcm_rx_audio_buf3 == NULL)
		{
			pcm_rx_audio_buf3_size = K_AUDIO_BUF_SIZE;
			pcm_rx_audio_buf3 = pvPortMalloc(pcm_rx_audio_buf3_size + buf_aligned_size);
			if (pcm_rx_audio_buf3 == NULL)
				return -1;
		}
        prx_dma->buffer = (short *)(((INT32U)pcm_rx_audio_buf3 + (buf_aligned_size - 1)) & (~(buf_aligned_size - 1))); // aligned to 4 byte multiple
        prx_dma->buf_len = pcm_rx_audio_buf3_size / 4;
    }
    else if (channel == 3)
    {
		if (pcm_rx_audio_buf4 == NULL)
		{
			pcm_rx_audio_buf4_size = K_AUDIO_BUF_SIZE;
			pcm_rx_audio_buf4 = pvPortMalloc(pcm_rx_audio_buf4_size + buf_aligned_size);
			if (pcm_rx_audio_buf4 == NULL)
				return -1;
		}
        prx_dma->buffer = (short *)(((INT32U)pcm_rx_audio_buf4 + (buf_aligned_size - 1)) & (~(buf_aligned_size - 1))); // aligned to 4 byte multiple
        prx_dma->buf_len = pcm_rx_audio_buf4_size / 4;
    }
    else
    {
		if (pcm_rx_audio_buf == NULL)
		{
			pcm_rx_audio_buf_size = K_AUDIO_BUF_SIZE;
			pcm_rx_audio_buf = pvPortMalloc(pcm_rx_audio_buf_size + buf_aligned_size);
			if (pcm_rx_audio_buf == NULL)
				return -1;
		}
        prx_dma->buffer = (short *)(((INT32U)pcm_rx_audio_buf + (buf_aligned_size - 1)) & (~(buf_aligned_size - 1))); // aligned to 4 byte multiple
        prx_dma->buf_len = pcm_rx_audio_buf_size / 4;
    }

	if (aligned_size != 0 && aligned_size < prx_dma->buf_len)
        prx_dma->dma_bulk_size = aligned_size;
    else
        prx_dma->dma_bulk_size = prx_dma->buf_len;

    // rounded to aligned_size bulk size multiple
    prx_dma->buf_len = (prx_dma->buf_len / prx_dma->dma_bulk_size) * prx_dma->dma_bulk_size;

    prx_dma->acc_dma_size = 0;

    return 0;
}

static void i2s_print_input_out_source(i2s_app_t *pi2s_app)
{
    if (pi2s_app->input_source == K_INPUT_LINE_IN)
        DBG_PRINT("channel %d input from line in\r\n", pi2s_app->channel);
    else if (pi2s_app->input_source == K_INPUT_MICRO_PHONE)
        DBG_PRINT("channel %d input from micro phone in\r\n", pi2s_app->channel);
    else
        DBG_PRINT("channel %d input from UNKNOWN!\r\n", pi2s_app->channel);

    if (pi2s_app->output_target == K_DATA_AT_DISK)
        DBG_PRINT("channel %d record to file\r\n", pi2s_app->channel);
    else
        DBG_PRINT("channel %d record to buffer\r\n", pi2s_app->channel);
}

static int i2s_i2c_init(INT32U devNumber, INT16U slaveAddr, i2s_i2c_t *pi2s_i2c)
{
    if (pi2s_i2c->use_gpio)
    {
        pi2s_i2c->config.i2c_gpio.slaveAddr = slaveAddr;
        pi2s_i2c->config.i2c_gpio.clock_rate = 20;
        pi2s_i2c->handle = drv_l2_sccb_open_ext(&pi2s_i2c->config.i2c_gpio);
    }
    else
    {
        pi2s_i2c->config.i2c_hw.slaveAddr = slaveAddr;
        pi2s_i2c->config.i2c_hw.clkRate = 20; // 20; // from diag_i2C, 100
        pi2s_i2c->config.i2c_hw.devNumber = devNumber;
        drv_l1_i2c_init(pi2s_i2c->config.i2c_hw.devNumber);
        pi2s_i2c->handle = &pi2s_i2c->config.i2c_hw;
    }

    return 0;
}

static int i2s_i2c_close(i2s_i2c_t *pi2s_i2c)
{
    if (pi2s_i2c->use_gpio)
        drv_l2_sccb_close(pi2s_i2c->handle);
    else
        drv_l1_i2c_uninit(pi2s_i2c->config.i2c_hw.devNumber);
    pi2s_i2c->handle = NULL;

    return 0;
}

static char *get_fs_root_path(INT8U drv)
{
    if (drv == FS_SD)
        return "C:\\";
    else if (drv == FS_SD2)
        return "K:\\";
    else if (drv == FS_NAND1)
        return "A:\\";
    else
    {
        DBG_PRINT("Unknown drive %u.\r\n", drv);
        return NULL;
    }
}

static int i2s_app_channel_prepare(INT32U channel, INT8U input_source, INT32U run_number, INT8U drv)
{
	volatile i2sRxReg_t *i2s = drv_l1_i2s_rx_get_register_base_addr(channel);
    i2s_app_t *pi2s_app;
    i2s_dma_t *prx_dma;
    i2s_profile_t *prx_profile;
    INT32U devNumber;
    INT16U slaveAddr;
    char num_str[32];
    char ext_str[5];

    pi2s_app = &i2s_apps[channel];

    memset(pi2s_app, 0, sizeof(i2s_app_t));

    pi2s_app->run_number = run_number;
    pi2s_app->channel = channel;
    pi2s_app->input_source = input_source;
    pi2s_app->drv = drv;

    pi2s_app->output_target = K_AUDIO_OUTPUT_TARGET;
    i2s_print_input_out_source(pi2s_app);

    prx_dma = &pi2s_app->rx_dma;
    i2s_app_rx_buf_init(channel, prx_dma, K_RX_DMA_BULK_SIZE);
    pi2s_app->loop_max = prx_dma->buf_len / prx_dma->dma_bulk_size;
    if (pi2s_app->loop_max == 0)
    {
        pi2s_app->loop_max = 1;
        prx_dma->dma_bulk_size = prx_dma->buf_len;
    }
    else if ((pi2s_app->loop_max * prx_dma->dma_bulk_size) !=  prx_dma->buf_len)
    {
        prx_dma->buf_len = pi2s_app->loop_max * prx_dma->dma_bulk_size;
    }
	i2s_rx_demo_buf_init(channel);

    prx_profile = &pi2s_app->rx_profile;
    prx_profile->MCLK_clock_rate = K_MCLK_RATE;

    #if (K_I2S_USE_REAL_BOARD == 1)
    drv_l1_clock_enable_apll_clock();

    //drv_l1_clock_set_i2s_main_mclk_freq(I2S_MAIN_FREQ_12288MHZ); // max 48K, limited by RX 4X of mclk freq
    DBG_PRINT("1 i2s main clock initial frequency=%u\r\n", drv_l1_clock_get_i2s_main_mclk_freq());
    //drv_l1_clock_set_i2s_main_mclk_freq(I2S_MAIN_FREQ_18432MHZ);
    //drv_l1_clock_set_i2s_main_mclk_freq(I2S_MAIN_FREQ_24576MHZ);   // could more than 96K, limited by wolfson wm8988
    //drv_l1_clock_set_i2s_main_mclk_freq(I2S_MAIN_FREQ_36864MHZ);   // could more than 96K, limited by wolfson wm8988

    //drv_l1_clock_set_i2s_main_mclk_freq(I2S_MAIN_FREQ_11289MHZ); // max 44.1k, limited by RX 4X of mclk freq
    //drv_l1_clock_set_i2s_main_mclk_freq(I2S_MAIN_FREQ_16934MHZ);
    //drv_l1_clock_set_i2s_main_mclk_freq(I2S_MAIN_FREQ_22579MHZ);   // could more than 88.2K, limited by wolfson wm8988
    //drv_l1_clock_set_i2s_main_mclk_freq(I2S_MAIN_FREQ_33869MHZ); // could more than 88.2K, limited by wolfson wm8988

    //DBG_PRINT("2 i2s main clock initial frequency=%u\r\n", drv_l1_clock_get_i2s_main_mclk_freq());
    prx_profile->MCLK_clock_rate = drv_l1_clock_get_i2s_main_mclk_freq()*1000;
    #endif

    prx_profile->merge = K_I2S_MERGE_MODE;
    prx_profile->mono = K_AUDIO_STEREO;
    prx_profile->r_lsb = 0; // 0 is mosted
    prx_profile->normal_mode_aligned = 1;//1 is most tested, and must 1 for i2s. This flag have effect only when framingmode is Normal Mode
    prx_profile->send_mode = 0; // 0 is default, MSB first, Bit polarity
    prx_profile->edge_mode = 0; // 0 is most tested, 1 have problem now
    prx_profile->first_frame_LR = 0; // 0 is most tested
    prx_profile->framingmode = 0; // 0 is I2S mode, most tested, 1 is Normal Mode, 2 is DSP mode, 3 is DSP mode
    if (prx_profile->framingmode == 1)
        prx_profile->frame_polar = 0; // so that match with Wolfson's left justified define that LRCK high is left.
    else
        prx_profile->frame_polar = 1; // 1 is most tested

    if (channel == 3)
    {
        prx_profile->mono = K_AUDIO_STEREO;
        prx_profile->merge = K_I2S_MERGE_MODE;
        prx_profile->data_size = 16;
        prx_profile->frame_size = 0; //0;
        prx_profile->sample_rate = 48000; // 88000 // 24000; // 8000
    }
    else if (channel == 2)
    {
        prx_profile->mono = K_AUDIO_STEREO;
        prx_profile->merge = K_I2S_MERGE_MODE;
        prx_profile->data_size = 16;
        prx_profile->frame_size = 0;
        prx_profile->sample_rate = 48000; // 16000
    }
    else if (channel == 1)
    {
        prx_profile->mono = K_AUDIO_STEREO;
        prx_profile->merge = K_I2S_MERGE_MODE;
        prx_profile->data_size = 16;
        prx_profile->frame_size = 0;
        prx_profile->sample_rate = 48000;
    }
    else
    {
#if 0
        prx_profile->mono = K_AUDIO_STEREO;
        prx_profile->merge = K_I2S_MERGE_MODE;
        prx_profile->data_size = 16;
        prx_profile->frame_size = 0; // when 0, frame size not predictable, only used in slave mode
        prx_profile->sample_rate = 16000;
#else
        prx_profile->mono = K_AUDIO_STEREO;
        prx_profile->merge = K_I2S_MERGE_MODE;
        prx_profile->data_size = 16;
        prx_profile->frame_size = 0;
        prx_profile->sample_rate = 48000;
#endif

    }

    DBG_PRINT("rx samplerate=%u\r\n", prx_profile->sample_rate);

    memset(prx_dma->buffer, 0x55, prx_dma->buf_len*4); // DEBUG USE, clear rx buffer to make sure tx data received
    DBG_PRINT("rx buf addr=%08x, len=%d, loop_max=%d, bulk=%d\r\n", (INT32U)(prx_dma->buffer), prx_dma->buf_len, pi2s_app->loop_max, prx_dma->dma_bulk_size);

    if (pi2s_app->output_target == K_DATA_AT_DISK)
    {
        char *root_path;

        root_path = get_fs_root_path(pi2s_app->drv);
        if (root_path != NULL)
            strcpy(pi2s_app->out_file_name, root_path);
        else
            strcpy(pi2s_app->out_file_name, "C:\\");

		strcat(pi2s_app->out_file_name, out_folder_name);
		mkdir(pi2s_app->out_file_name); // make output folder
		strcat(pi2s_app->out_file_name, "\\");
        strcat(pi2s_app->out_file_name, out_file_name[pi2s_app->channel]);
        if (pi2s_app->run_number > 0)
        {
            int len = strlen(pi2s_app->out_file_name);

            strcpy(ext_str, &(pi2s_app->out_file_name[len - 4]));
            pi2s_app->out_file_name[len - 4] = '\0';
            sprintf(num_str, "_%05d", pi2s_app->run_number);
            strcat(pi2s_app->out_file_name, num_str);
            strcat(pi2s_app->out_file_name, ext_str);
        }

        pi2s_app->fd_out = fs_open(pi2s_app->out_file_name, O_CREAT|O_TRUNC|O_WRONLY);
        if (pi2s_app->fd_out < 0)
        {
            DBG_PRINT("fail to create output file %s\r\n", pi2s_app->out_file_name);
            return -1;
        }

        if (root_path != NULL)
            strcpy(pi2s_app->out_original_file_name, root_path);
        else
            strcpy(pi2s_app->out_original_file_name, "C:\\");
		strcat(pi2s_app->out_original_file_name, out_original_folder_name);
		mkdir(pi2s_app->out_original_file_name); // make output folder
		strcat(pi2s_app->out_original_file_name, "\\");
        strcat(pi2s_app->out_original_file_name, out_original_file_name[pi2s_app->channel]);
        if (pi2s_app->run_number > 0)
        {
            int len = strlen(pi2s_app->out_original_file_name);

            strcpy(ext_str, &(pi2s_app->out_original_file_name[len - 4]));
            pi2s_app->out_original_file_name[len - 4] = '\0';
            sprintf(num_str, "_%05d", pi2s_app->run_number);
            strcat(pi2s_app->out_original_file_name, num_str);
            strcat(pi2s_app->out_original_file_name, ext_str);
        }
        pi2s_app->fd_out_original = fs_open(pi2s_app->out_original_file_name, O_CREAT|O_TRUNC|O_WRONLY);
        if (pi2s_app->fd_out_original < 0)
        {
            DBG_PRINT("fail to create output original file %s\r\n", pi2s_app->out_original_file_name);
            return -1;
        }
    }

    pi2s_app->i2s_i2c.use_gpio = 0;

    {
        if (channel == 1)
        {
            #if K_I2S_USE_REAL_BOARD == 1
            pi2s_app->i2s_i2c.use_gpio = 1;
            pi2s_app->i2s_i2c.config.i2c_gpio = i2c_0_2_config;
            R_FUNPOS0 = (R_FUNPOS0 & (~(0x3 << 25))) | (2 << 25); // williamyeo, for I2S board I2C0 #2
            devNumber = I2C_0;
            slaveAddr = SLAVE_ID_0;

            #else
            devNumber = I2C_0;
            slaveAddr = SLAVE_ID_1;
            #endif
        }
        else if (channel == 2)
        {
            #if K_I2S_USE_REAL_BOARD == 1
            pi2s_app->i2s_i2c.use_gpio = 1;
            pi2s_app->i2s_i2c.config.i2c_gpio = i2c_2_2_config;
            devNumber = I2C_2;
            slaveAddr = SLAVE_ID_0;
            R_FUNPOS1 = (R_FUNPOS1 & (~(0x3 << 16))) | (2 << 16); // williamyeo, for I2S board I2C2 #2
            #else
            devNumber = I2C_1;
            slaveAddr = SLAVE_ID_0;
            #endif
        }
        else if (channel == 3)
        {
            #if K_I2S_USE_REAL_BOARD == 1
            pi2s_app->i2s_i2c.use_gpio = 1;
            pi2s_app->i2s_i2c.config.i2c_gpio = i2c_IOA15_IOA14_config;
            devNumber = I2C_1;
            slaveAddr = SLAVE_ID_0;
            #else
            devNumber = I2C_1;
            slaveAddr = SLAVE_ID_1;
            #endif
        }
        else
        {
            #if K_I2S_USE_REAL_BOARD == 1
            pi2s_app->i2s_i2c.use_gpio = 1;
            pi2s_app->i2s_i2c.config.i2c_gpio = i2c_1_0_config;
            devNumber = I2C_1;
            slaveAddr = SLAVE_ID_0;

            #else
            devNumber = I2C_0;
            slaveAddr = SLAVE_ID_0;
            #endif
        }

        i2s_i2c_init(devNumber, slaveAddr, &(pi2s_app->i2s_i2c));
        wolfson_WM8988_init(&(pi2s_app->i2s_i2c), pi2s_app->input_source, prx_profile);
    }

    DBG_PRINT("0 TestState=%d TX_CTRL=0x%08x RX_CTRL=0x%08x\r\n", pi2s_app->TestState, i2s->TX_CTRL, i2s->RX_CTRL);

    // RX register 設定（請設 slave mode，再由跳線把 BCLK, LRCK 接進來)
    drv_l1_i2s_rx_init(pi2s_app->channel);
    drv_l1_i2s_rx_set_framing_mode(pi2s_app->channel, prx_profile->framingmode);
    drv_l1_i2s_rx_set_frame_size(pi2s_app->channel, prx_profile->frame_size);
    drv_l1_i2s_rx_set_data_bit_length(pi2s_app->channel, prx_profile->data_size);
    drv_l1_i2s_rx_set_merge_mode(pi2s_app->channel, prx_profile->merge);
    drv_l1_i2s_rx_set_mono(pi2s_app->channel, prx_profile->mono);
    drv_l1_i2s_rx_set_right_channel_at_lsb_byte(pi2s_app->channel, prx_profile->r_lsb);
    drv_l1_i2s_rx_set_normal_mode_bit_align(pi2s_app->channel, prx_profile->normal_mode_aligned);
    //if ((prx_profile->merge == K_I2S_MERGE_MODE) && (prx_profile->data_size == 16))
        drv_l1_i2s_rx_set_bit_receive_mode(pi2s_app->channel, prx_profile->send_mode);
    drv_l1_i2s_rx_set_edge_mode(pi2s_app->channel, prx_profile->edge_mode);
    drv_l1_i2s_rx_set_frame_polar(pi2s_app->channel, prx_profile->frame_polar);
    drv_l1_i2s_rx_set_first_frame_left_right(pi2s_app->channel, prx_profile->first_frame_LR);

    drv_l1_i2s_rx_clear_fifo(pi2s_app->channel);
    drv_l1_i2s_rx_dma_transfer(pi2s_app->channel, (INT8U*)prx_dma->buffer + prx_dma->acc_dma_size*4, prx_dma->dma_bulk_size, &prx_dma->notify_flag);
    //drv_l1_i2s_rx_enable(pi2s_app->channel, 1); // remark because just 1 pin for MCLK, when 2 line in together, turn on here will cause another walson codedc i2c become fail

    return 0;
}

static void i2s_app_convert_32_to_24_bit(INT32U *src_buf, INT32U count, INT32U valid_bit)
{
    INT32U k;
    INT8U *dst_buf = (INT8U *)src_buf;
    INT32S temp;
    INT32S amp = 1;

    if (valid_bit == 20)
      amp = 16; // (24-20) = 4, 2 ^4=16
    else  if (valid_bit == 18)
      amp = 64; // (24-18) = 6, 2 ^6=64
    else  if (valid_bit == 22)
      amp = 4; // (24-22) = 2, 2 ^2=4

    for (k = 0; k < count; k++)
    {
        temp = src_buf[k];
        temp *= amp;
        *dst_buf++ = temp;
        *dst_buf++ = temp >> 8;
        *dst_buf++ = temp >> 16;
    }

    return;
}

static void i2s_app_convert_32_to_16_bit(INT32U *src_buf, INT32U count)
{
    INT32U k;
    INT8U *dst_buf = (INT8U *)src_buf;
    INT32U temp;

    for (k = 0; k < count; k++)
    {
        temp = src_buf[k];
        *dst_buf++ = temp;
        *dst_buf++ = temp >> 8;
    }

    return;
}

static void i2s_app_truncate_32_to_24_bit(INT32U *src_buf, INT32U count)
{
    INT32U k;
    INT8U *dst_buf = (INT8U *)src_buf;
    INT32U temp;

    for (k = 0; k < count; k++)
    {
        temp = src_buf[k];
        *dst_buf++ = temp >> 8;
        *dst_buf++ = temp >> 16;
        *dst_buf++ = temp >> 24;
    }

    return;
}

static void traverse_16_bits(INT16U *psample, INT32U count)
{
  INT32U k;
  INT16U j, orig, result;

  for (k = 0; k < count; k++)
  {
      orig = psample[k];
      result = 0;
      for (j = 0; j < 16; j++)
      {
          if ((orig & (1<<j)) != 0)
            result |= (1<< (15 - j));
      }
      psample[k] = result;
  }
}

static void traverse_32_bits(INT32U *psample, INT32U count, INT32U data_size)
{
  INT32U j, k, sample_bit_count;
  INT32U orig, result;

  sample_bit_count = data_size - 1;
  for (k = 0; k < count; k++)
  {
      orig = psample[k];
      result = 0;
      for (j = 0; j <= sample_bit_count; j++)
      {
          if ((orig & (1<<j)) != 0)
            result |= (1<< (sample_bit_count - j));
      }

      if ((data_size < 32) && ((result & (1 << sample_bit_count)) != 0))
      {
          // sign extension
          for (j = data_size; j < 32; j++)
            result |= (1<<j);
      }
      psample[k] = result;
  }
}

static char *input_source_name[3] =
  {
      "TX",
      "MicroPhone",
      "LineIn"
  };

static INT8U i2s_completed_channel_count(INT8U *input_source_list, INT8U *input_source_list_copy)
{
    int j, k;

    for (j = 0, k = 0; j < 4; j++)
    {
        if ((input_source_list[j] != 0xff) && (input_source_list_copy[j] == 0xff))
            k++;
    } // end for

    return k;
}

static INT8U input_source_list_copy[4];
static INT8U input_source_list[4];

static INT32S i2s_rx_demo_data_consumer(INT32U ch, INT32U fill_loop);

void i2s_rx_demo_stop_run(void)
{
    INT32U channel;

    for (channel = 0; channel < 4; channel++)
        input_source_list_copy[channel] = 0xff;
}

static int i2s_rx_demo_running(void)
{
    i2s_app_t *pi2s_app;
    i2s_dma_t *prx_dma;
    INT32U channel;

    INT8U stop_running = 0;
    INT8U ch_total = 0;

	for (channel = 0, ch_total = 0; channel < 4; channel++)
    {
        input_source_list_copy[channel] = input_source_list[channel];
        if (input_source_list_copy[channel] != 0xff)
        {
            pi2s_app = &i2s_apps[channel];
            ch_total++;

            #if (K_I2S_RX_USE_INTERRUPT == 1)
            drv_l1_i2s_rx_resgiter_isr(channel, &i2s_rx_isr, 0);
            drv_l1_i2s_rx_enable_interrupt(pi2s_app->channel);
            #endif

            pi2s_app->running = 1;
        }
    }

    channel = 3;

    while (!stop_running)
    {
        channel = (channel + 1) % 4;

		if (i2s_completed_channel_count(input_source_list, input_source_list_copy) == ch_total)
        {
            stop_running = 1;
			DBG_PRINT("RX ALL STOP RUNNING!!!\r\n");
            break;
        }

        if (input_source_list_copy[channel] != 0xff)
        {
            INT32S ret;

            pi2s_app = &i2s_apps[channel];
            prx_dma = &pi2s_app->rx_dma;

            ret = i2s_rx_demo_data_consumer(channel, 1);
            if(ret < 0)
			{
				input_source_list_copy[channel] = 0xff;
            }
        }
    } // end while

    for (channel = 0; channel < 4; channel++)
    {
        if (input_source_list[channel] != 0xff)
        {
            pi2s_app = &i2s_apps[channel];

            pi2s_app->running = 0;

            while ((drv_l1_i2s_rx_dbf_status_get(pi2s_app->channel) == 1) || \
                    (drv_l1_i2s_rx_dma_status_get(pi2s_app->channel) == 1) )
            {
                osDelay(1);
            }

            #if (K_I2S_RX_USE_INTERRUPT == 1)
            drv_l1_i2s_rx_disable_interrupt(pi2s_app->channel);
            #endif

			#if 0 && (K_I2S_RX_USE_INTERRUPT == 1)
            i2s_app_wait_rx_fifo_half_full(pi2s_app->channel, 16); // TEST, purposely to wait half full to let interrupt happen
            #endif

            drv_l1_i2s_rx_disable(pi2s_app->channel, 1);

            if (pi2s_app->output_target == K_DATA_AT_DISK)
            {
				i2s_rx_demo_data_consumer(pi2s_app->channel, MAX_PCM_BUF_NUM);

				// save remain data at ring buffer to file and close the file
				if (pi2s_app->fd_out_original >= 0)
				{
					fs_close(pi2s_app->fd_out_original);
				}

                if (pi2s_app->fd_out >= 0)
				{
                    fs_close(pi2s_app->fd_out);
				}
            }

            //wolfson_WM8988_power_down(&(pi2s_app->i2s_i2c));
            i2s_i2c_close(&(pi2s_app->i2s_i2c));

            drv_l1_i2s_rx_dbf_free(channel);
        }
    }

    return 0;
}

// input_source_flag, 2 bit for each channel
int i2s_rx_demo_record(INT32U channel_flag, INT32U input_source_flag, INT32U run_number, INT8U drv)
{
    i2s_app_t *pi2s_app;
    INT8U input_source;
    INT8U ch_total = 0;
    INT8U ch_start = 0xff;
    INT8U channel;
    int ret;

    for (channel=0; channel < 4; channel++)
    {
        input_source_list[channel] = 0xff;
        if ((channel_flag & (1<<channel)) != 0)
        {
            if (ch_start == 0xff)
            {
                ch_start = channel;
                DBG_PRINT("selected first channel %d\r\n", ch_start);
            }
            input_source = (input_source_flag >> (channel*2)) & 0x3;
            if (input_source > 3 || input_source == 0)
              input_source = K_INPUT_LINE_IN;
            input_source_list[channel] = input_source;
            ch_total++;
            DBG_PRINT("channel %d input source is %s\r\n", channel, input_source_name[input_source]);
        }
    }

    if (ch_total == 0)
    {
        for (channel=0; channel < 4; channel++)
            input_source_list[channel] = 0xff;

        input_source_list[0] = K_INPUT_LINE_IN;
        //input_source_list[1] = K_INPUT_LINE_IN;
        //input_source_list[2] = K_INPUT_LINE_IN;
        //input_source_list[3] = K_INPUT_LINE_IN;

        for (channel=0; channel < 4; channel++)
        {
            if (input_source_list[channel] != 0xff)
            {
                if (ch_start == 0xff)
                {
                    ch_start = channel;
                }
                ch_total++;
            }
        }

        if (ch_start != 0xff)
            DBG_PRINT("no channel selected, default to %d channel, start channel is %d, each input source is %s\r\n", ch_total, ch_start, input_source_name[input_source_list[ch_start]]);
    }

    for (channel = ch_start; channel < 4; channel++)
    {
        if (input_source_list[channel] != 0xff)
        {
            DBG_PRINT("\r\n\r\n======== start prepare channel %d ========\r\n\r\n", channel);
            if (channel == 0)
              R_SYSTEM_CODEC_CTRL1 |= (1<<14); // SEL_EXT_CODEC_ADC, RX0 pinmux with ADC code, so must set 1 to
                                                // 0: connect to internal CODEC ADC 1: connect to PAD(external CODEC ADC)

            pi2s_app = &i2s_apps[channel];
            ret = i2s_app_channel_prepare(channel, input_source_list[channel], run_number, drv);
            if (ret != 0)
                return ret;

            DBG_PRINT("\r\n\r\n======== end prepare channel %d ========\r\n\r\n", channel);
        }
    }

    i2s_rx_demo_running();

    // print summary message
    for (channel = ch_start; channel < 4; channel++)
    {
        if (input_source_list[channel] != 0xff)
        {
            i2s_dma_t *prx_dma;

            pi2s_app = &i2s_apps[channel];
            prx_dma = &pi2s_app->rx_dma;
            DBG_PRINT("rx channel =%d, TestState=%d, done_count=%d\r\n", pi2s_app->channel, pi2s_app->TestState, prx_dma->dma_done_count);
            DBG_PRINT("rx channel =%d, half_full=%d, overrun=%d, ovf=%d\r\n", pi2s_app->channel, prx_dma->half_full_count, prx_dma->overrun_count, prx_dma->ovf_count);
        }
    }

    return 0;
}

static void i2s_rx_demo_dma_isr(INT32U i2s_channel, INT8U dma_channel, INT8U dma_done_status)
{
    INT32S ret;
    i2s_app_t *pi2s_app;
    i2s_dma_t *prx_dma;
    INT32U k, empty_count;
    pcm_buf_entry_t *pcm_buf_entry;
    INT32U done_id;
    INT32U going_id;
    INT32U next_id;

    pi2s_app = &i2s_apps[i2s_channel];
    prx_dma = &pi2s_app->rx_dma;

    if (dma_done_status != C_DMA_STATUS_DONE)
    {
        DBG_PRINT("i2s rx dma not done. i2s%u dma%u done=%u\r\n", i2s_channel, dma_channel, dma_done_status);
    }

    done_id = producer_buf_idx_list[i2s_channel];
    if (!pcm_buf_list[i2s_channel][done_id].stopped)
    {
        #if 0 // DEBUG CODE
        if (pcm_buf_list[i2s_channel][done_id].filled == 1)
            DBG_PRINT("rx channel %u done_id buffer already filled. done_id=%u\r\n", i2s_channel, done_id);
        #endif

        pcm_buf_list[i2s_channel][done_id].filled = 1;

        producer_buf_idx_list[i2s_channel]++;
        if(producer_buf_idx_list[i2s_channel]>=MAX_PCM_BUF_NUM)
            producer_buf_idx_list[i2s_channel] = 0;

        going_id = producer_buf_idx_list[i2s_channel];

        next_id = producer_buf_idx_list[i2s_channel] + 1;
        if(next_id>=MAX_PCM_BUF_NUM)
            next_id = 0;
        pcm_buf_entry = &pcm_buf_list[i2s_channel][next_id];

        if (pi2s_app->running)
            drv_l1_i2s_rx_dbf_set(i2s_channel, (INT16S *)pcm_buf_entry->buf, pcm_buf_entry->buf_len*2);

        #if 0 // DEBUG CODE
        if (pcm_buf_list[i2s_channel][going_id].filled == 1)
            DBG_PRINT("rx channel %u going_id buffer already filled. going_id=%u\r\n", i2s_channel, going_id);

        if (pcm_buf_list[i2s_channel][next_id].filled == 1)
            DBG_PRINT("rx channel %u next buffer already filled. next_id=%u\r\n", i2s_channel, next_id);

        for (k = 0, empty_count = 0; k < MAX_PCM_BUF_NUM; k++)
        {
            if (pcm_buf_list[i2s_channel][k].filled == 0)
            {
                if (k != done_id && k != going_id && k != next_id)
                    empty_count++;
            }
        }
        if (empty_count < 3)
            DBG_PRINT("rx channel %u empty_count=%u\r\n", i2s_channel, empty_count);
        #endif
    }

}

static void i2s_rx0_demo_dma_isr(INT8U dma_channel, INT8U dma_done_status)
{
    i2s_rx_demo_dma_isr(0, dma_channel, dma_done_status);
}

static void i2s_rx1_demo_dma_isr(INT8U dma_channel, INT8U dma_done_status)
{
    i2s_rx_demo_dma_isr(1, dma_channel, dma_done_status);
}

static void i2s_rx2_demo_dma_isr(INT8U dma_channel, INT8U dma_done_status)
{
    i2s_rx_demo_dma_isr(2, dma_channel, dma_done_status);
}

static void i2s_rx3_demo_dma_isr(INT8U dma_channel, INT8U dma_done_status)
{
    i2s_rx_demo_dma_isr(3, dma_channel, dma_done_status);
}

void (*i2s_rx_demo_dma_user_isr[MAX_I2X_RX_NUM])(INT8U, INT8U) = {i2s_rx0_demo_dma_isr, i2s_rx1_demo_dma_isr, i2s_rx2_demo_dma_isr, i2s_rx3_demo_dma_isr};

static INT32S i2s_rx_demo_dma_start(INT32U ch, INT32U buf_A_id, INT32U buf_B_id)
{
    INT32S ret=1;
    i2s_app_t *pi2s_app;
    i2sRxReg_t *i2s;
    i2s_dma_t *prx_dma;
    pcm_buf_entry_t *pbuf_info_A;
    pcm_buf_entry_t *pbuf_info_B;

    pi2s_app = &i2s_apps[ch];
    prx_dma = &pi2s_app->rx_dma;
    i2s = drv_l1_i2s_rx_get_register_base_addr(ch);

    #if (K_I2S_RX_USE_INTERRUPT == 1)
    drv_l1_i2s_rx_enable_interrupt(ch);
    #endif

    DBG_PRINT("enable RX channel %d, ctr %08x\r\n", ch, i2s->RX_CTRL);

    pbuf_info_A = &pcm_buf_list[ch][buf_A_id];

    ret = drv_l1_i2s_rx_dbf_put(ch, (INT16S *)pbuf_info_A->buf, pbuf_info_A->buf_len*2, NULL, &prx_dma->notify_flag, i2s_rx_demo_dma_user_isr[ch]);

    if(ret != 0)
        return STATUS_FAIL;

    pbuf_info_B = &pcm_buf_list[ch][buf_B_id];

    drv_l1_i2s_rx_dbf_set(ch, (INT16S *)pbuf_info_B->buf, pbuf_info_B->buf_len*2);

    drv_l1_i2s_rx_enable(ch, 0);

    return STATUS_OK;
}

void i2s_rx_demo_buf_init(INT32U ch)
{
    INT32U i;
	i2s_app_t *pi2s_app;
    i2s_dma_t *prx_dma;

	pi2s_app = &i2s_apps[ch];
	prx_dma = &pi2s_app->rx_dma;

    producer_buf_idx_list[ch] = 0;
    consumer_buf_idx_list[ch] = 0;
    startup_done_flag[ch] = 0;

    for(i=0; i<MAX_PCM_BUF_NUM; i++)
    {
		pcm_buf_list[ch][i].buf_len = prx_dma->dma_bulk_size;
        pcm_buf_list[ch][i].buf = prx_dma->buffer + (i*pcm_buf_list[ch][i].buf_len*4L);
        pcm_buf_list[ch][i].filled = 0;
        pcm_buf_list[ch][i].stopped = 0;
    }
}

static INT32S i2s_rx_demo_data_consumer(INT32U ch, INT32U fill_loop)
{
    INT32S ret = 0;
    i2s_app_t *pi2s_app;
    INT32U k;

    pi2s_app = &i2s_apps[ch];

    for (k=0; k < fill_loop; k++)
    {
        if (pcm_buf_list[ch][consumer_buf_idx_list[ch]].filled == 1)
        {
            i2s_profile_t *prx_profile;
            INT16S *pcm_current;
            INT32S write_size;
			INT32S write_ret;
            INT32U pcm_len_in_word;

            prx_profile = &pi2s_app->rx_profile;

			pcm_current = pcm_buf_list[ch][consumer_buf_idx_list[ch]].buf;
			pcm_len_in_word = pcm_buf_list[ch][consumer_buf_idx_list[ch]].buf_len;

			if (pi2s_app->fd_out_original >= 0)
			{
				write_size = pcm_len_in_word * 4;
				write_ret = fs_write(pi2s_app->fd_out_original, (INT32U)pcm_current, write_size);
				if (write_ret != write_size)
				{
					DBG_PRINT("channel =%d, origin write_ret != write_size, write=%d but ret=%d byte\r\n", pi2s_app->channel, write_size, write_ret);
					ret = -1;
				}
			}

			if (pi2s_app->fd_out >= 0)
			{
				if (prx_profile->send_mode)
				{
					if (prx_profile->data_size == 16)
						traverse_16_bits((INT16U *)(pcm_current), pcm_len_in_word*2);
					else
						traverse_32_bits((INT32U *)(pcm_current), pcm_len_in_word, prx_profile->data_size);
				}

				if (prx_profile->data_size == 32)
				{
					i2s_app_truncate_32_to_24_bit((INT32U *)pcm_current, pcm_len_in_word);
					pi2s_app->write_size = pcm_len_in_word * 3;
				}
				else if (prx_profile->data_size == 24 || prx_profile->data_size == 20 || prx_profile->data_size == 22 || prx_profile->data_size == 18)
				{
					i2s_app_convert_32_to_24_bit((INT32U *)pcm_current, pcm_len_in_word, prx_profile->data_size);
					pi2s_app->write_size = pcm_len_in_word * 3;
				}
				else if (prx_profile->merge == K_I2S_NONMERGE_MODE && prx_profile->data_size == 16 && (prx_profile->frame_size == 16 || prx_profile->frame_size == 0))
				{
					i2s_app_convert_32_to_16_bit((INT32U *)pcm_current, pcm_len_in_word);
					pi2s_app->write_size = pcm_len_in_word * 2;
				}
				else
					pi2s_app->write_size = pcm_len_in_word * 4;

				pi2s_app->write_ret = fs_write(pi2s_app->fd_out, (INT32U)pcm_current, pi2s_app->write_size);

				if (pi2s_app->write_ret != pi2s_app->write_size)
				{
					DBG_PRINT("channel =%d, write_ret != write_size, write=%d but ret=%d byte\r\n", pi2s_app->channel, pi2s_app->write_size, pi2s_app->write_ret);
					ret = -1;
				}
			}

            if (ret < 0)
            {
                pcm_buf_list[ch][consumer_buf_idx_list[ch]].stopped = 1;
                DBG_PRINT("channel[%u] write err.\r\n", ch);
                return -1;
            }

            pcm_buf_list[ch][consumer_buf_idx_list[ch]].filled = 0;
            consumer_buf_idx_list[ch]++;
            if(consumer_buf_idx_list[ch]>=MAX_PCM_BUF_NUM)
				consumer_buf_idx_list[ch] = 0;
        }
    } // end for

    if (startup_done_flag[ch] == 0)
    {
        DBG_PRINT("channel %u startup done\r\n", ch);
        ret = i2s_rx_demo_dma_start(ch, consumer_buf_idx_list[ch], consumer_buf_idx_list[ch]+1);
        startup_done_flag[ch] = 1;
    }

    return 0;
}

extern osThreadId i2s_rx_demo_task_id;

void i2s_rx_demo_task(void const *param)
{
	int ret;
    int count = 0;

	prx_demo = ((i2s_rx_demo_t **)param);

	ret = _devicemount(prx_demo[0]->drv);
	while (ret == 0)
    {
        i2s_rx_demo_record(prx_demo[0]->channel_flag, 0, count, prx_demo[0]->drv);
		count++;
        if (count >= prx_demo[0]->total_run)
            break;
	}

	_deviceunmount(prx_demo[0]->drv);

    DBG_PRINT("I2S RX Task Quit!!! ret=%d\r\n\r\n", ret);
    i2s_rx_demo_task_id = NULL;
    osThreadTerminate(NULL);
}

#endif

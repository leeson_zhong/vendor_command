#ifndef _I2S_RX_DEMO_INCLUDE
#define _I2S_RX_DEMO_INCLUDE

#include "drv_l1_i2s_rx.h"
#include "drv_l1_clock.h"

#define K_AUDIO_BUF_SIZE        		(1200*1024)

typedef struct i2s_rx_demo_s
{
    INT32U channel_flag;
    INT32U mclk_selection;

    INT8U drv;
    INT8S amplitude;

    INT32U sample_rate;
    INT32U data_size;

    volatile INT32U total_run;

    char filename[256];
    char pathname[256];
} i2s_rx_demo_t;

#endif

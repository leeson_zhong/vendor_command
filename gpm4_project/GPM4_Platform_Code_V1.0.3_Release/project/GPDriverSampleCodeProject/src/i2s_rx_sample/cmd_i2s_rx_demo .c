
#include "project.h"
#include "drv_l1_cfg.h"
#include "gplib.h"
#include "i2s_rx_demo.h"

#if (_DRV_L1_I2S_RX == 1)

#include "console.h"

#define K_I2S_DRIVE    FS_SD

extern int i2s_rx_demo_record(INT32U channel_flag, INT32U input_source_flag, INT32U run_number, INT8U drv);

static int nf_atoi(char * str)
{
	int num = -1;

	if (STRCMP(str, "0") == 0)
		num = 0;
	else if (STRCMP(str, "1") == 0)
		num = 1;
	else if (STRCMP(str, "2") == 0)
		num = 2;
	else if (STRCMP(str, "3") == 0)
		num = 3;
	else if (STRCMP(str, "4") == 0)
		num = 4;
	else if (STRCMP(str, "5") == 0)
		num = 5;
	else if (STRCMP(str, "6") == 0)
		num = 6;
	else if (STRCMP(str, "7") == 0)
		num = 7;
	else if (STRCMP(str, "8") == 0)
		num = 8;
	else if (STRCMP(str, "9") == 0)
		num = 9;
	else if (STRCMP(str, "10") == 0)
		num = 10;
	else if (STRCMP(str, "11") == 0)
		num = 11;
	else if (STRCMP(str, "12") == 0)
		num = 12;
	else if (STRCMP(str, "13") == 0)
		num = 13;
	else if (STRCMP(str, "14") == 0)
		num = 14;
	else if (STRCMP(str, "15") == 0)
		num = 15;
	return num;
}

extern void i2s_rx_demo_task(void const *param);
extern void i2s_tx_demo_stop_run(void);

static i2s_rx_demo_t i2s_rx_demo_param[4]=
{
    {
        .channel_flag = 0x1,
        .mclk_selection = 5,
        .drv = K_I2S_DRIVE,
        .amplitude = -2,
        .sample_rate = 48000,
        .data_size = 16,
        .total_run = 1,
        .filename = {'\0'},
        .pathname = {'\0'}
    },
    {
        .channel_flag = 0x2,
        .mclk_selection = 5,
        .drv = K_I2S_DRIVE,
        .amplitude = -2,
        .sample_rate = 48000,
        .data_size = 16,
        .total_run = 1,
        .filename = {'\0'},
        .pathname = {'\0'}
    },
    {
        .channel_flag = 0x4,
        .mclk_selection = 5,
        .drv = K_I2S_DRIVE,
        .amplitude = -2,
        .sample_rate = 48000,
        .data_size = 16,
        .total_run = 1,
        .filename = {'\0'},
        .pathname = {'\0'}
    },
    {
        .channel_flag = 0x8,
        .mclk_selection = 5,
        .drv = K_I2S_DRIVE,
        .amplitude = -2,
        .sample_rate = 48000,
        .data_size = 16,
        .total_run = 1,
        .filename = {'\0'},
        .pathname = {'\0'}
    }
};
static i2s_rx_demo_t *pi2s_rx_demo_param[4] = {
                                                &i2s_rx_demo_param[0],\
                                                &i2s_rx_demo_param[1],\
                                                &i2s_rx_demo_param[2],\
                                                &i2s_rx_demo_param[3]
                                              };

volatile osThreadId i2s_rx_demo_task_id = NULL;

void i2s_rx_demo_launch(void)
{
    INT32U k;

    for (k=0; k < 4; k++)
        DBG_PRINT("pi2s_rx_demo_param[%u]=%08x\r\n", k, pi2s_rx_demo_param[k]);

    osThreadDef(i2s_rx_demo_task, osPriorityNormal, 0, 16384);
    i2s_rx_demo_task_id = osThreadCreate(&os_thread_def_i2s_rx_demo_task, (void *)(&pi2s_rx_demo_param[0]));
}

void i2s_rx_demo_stop(void)
{
    if (i2s_rx_demo_task_id != NULL)
    {
        INT32U save_total_run = i2s_rx_demo_param[0].total_run;

        i2s_rx_demo_param[0].total_run = 0;

        i2s_rx_demo_stop_run();
        while (i2s_rx_demo_task_id != NULL)
        {
            osDelay(1);
        }
        i2s_rx_demo_param[0].total_run = save_total_run;
    }
    DBG_PRINT("I2S RX DEMO STOP DONE !!!\r\n");
}

static void i2s_rx_demo_record_handler(int argc, char *argv[])
{
    INT32U channel_flag = 0;
    INT16S ret;
    unsigned int k, total_run = pi2s_rx_demo_param[0]->total_run;
    INT8U drv = K_I2S_DRIVE;

	if (argc > 1)
	{
		drv = nf_atoi(argv[1]);
		if (drv != FS_SD && \
            drv != FS_SD2 && \
            drv != FS_NAND1)
        {
            DBG_PRINT("illegal drive number [%s], should %u, %u, %u\r\n", argv[1], FS_SD, FS_SD2, FS_NAND1);
            return;
        }
	}

    if (argc > 2)
    {
		sscanf(argv[2], "%u", &total_run);
		if (total_run == 0)
        {
            DBG_PRINT("illegal run number [%s], should at least 1\r\n", argv[2]);
            return;
        }
    }

    if (argc > 3)
	{
        channel_flag = nf_atoi(argv[3]);

		if (channel_flag > 15)
		{
			DBG_PRINT("illegal channel flag [%s], should be 0~15\r\n", argv[3]);
			return;
		}
	}

	#if 0
    ret = _devicemount(drv);
    if (ret == 0)
	{
		for (k=0; k < total_run; k++)
        {
			ret = i2s_rx_demo_record(channel_flag, 0, k, drv);
			if (total_run > 1)
            {
				if (ret != 0)
				{
					DBG_PRINT("\nfail to run test at %u times of target %d times\r\n", k, total_run);
				}
				else
				{
					DBG_PRINT("\nsuccess to run test at %u times of target %d times\r\n", k, total_run);
				}
            }
		}
	}
    else
        DBG_PRINT("mount fail may be sd card not inserted\r\n");
    ret = _deviceunmount(drv);
	#else
	if (i2s_rx_demo_task_id != NULL)
    {
        DBG_PRINT("Please stop I2S RX DEMO to replay.\r\n");
        return;
    }
    pi2s_rx_demo_param[0]->total_run = total_run;
    pi2s_rx_demo_param[0]->channel_flag = channel_flag;
    pi2s_rx_demo_param[0]->drv = drv;
    i2s_rx_demo_launch();
	#endif
}

static void i2s_rx_demo_cmd_help(void)
{
    DBG_PRINT(
    	"\r\nUsage: i2s_rx_demo help\r\n"
		"\r\n       i2s_rx_demo record [<drive(10,2,0)> <run_count(1~)> <channel_flag(4 bit)>]\r\n"
    	"\r\n       i2s_rx_demo config\n"
    	);
}

static void i2s_rx_demo_cmd_handler(int argc, char *argv[])
{
    if (STRCMP(argv[1],"record") == 0)
    {
        i2s_rx_demo_record_handler(argc - 1, &argv[1]);
    }
    else if (STRCMP(argv[1],"stop") == 0)
    {
        i2s_rx_demo_stop();
    }
    else
    {
    	i2s_rx_demo_cmd_help();
    }
}

static cmd_t i2s_rx_demo_cmd_list[] =
{
	{"i2s_rx_demo",  i2s_rx_demo_cmd_handler,  NULL },
	{NULL,    NULL,   NULL}
};

void i2s_rx_demo_cmd_register(void)
{
	cmd_t *pcmd = &i2s_rx_demo_cmd_list[0];

	while (pcmd->cmd != NULL)
	{
		cmdRegister(pcmd);
		pcmd += 1;
	}
}

void I2S_RX_Demo(void)
{
    #if defined(GPLIB_CONSOLE_EN) && (GPLIB_CONSOLE_EN == 1)
    i2s_rx_demo_cmd_register();
    #endif
}

#endif // (_DRV_L1_I2S_RX == 1)

/**************************************************************************
 *                         Include File			                          *
 **************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "drv_l1.h"
#include "drv_l1_scaler.h"
#include "drv_l2_scaler.h"
#include "drv_l2_display.h"
#include "portable.h"
#include "TEXT_HDR.h"

/**************************************************************************
 *                              D E F I N E                               *
 **************************************************************************/
#define C_IMG_IN_W		            320
#define C_IMG_IN_H		            240
#define FRAME_BUF_ALIGN64           0x3F

/**************************************************************************
 *                              M A C R O S                               *
 **************************************************************************/
#define gpScale1Format_t 	        ScalerFormat_t
#define gpScale1Para_t		        ScalerPara_t

/**************************************************************************
 *                         G L O B A L    D A T A                         *
 **************************************************************************/
INT16U disp_width, disp_height;

static void wait_msec(INT32U msec)
{
#if 0
	INT32U cnt = msec * (MCLK >> 13); //0.64ms
	INT32U i;

	for(i=0; i<cnt; i++) {
		i = i;
	}
#else
	#if 1
	osDelay(msec);
	#else
	drv_msec_wait(msec);
	#endif
#endif
}

static INT32S scale1_simple_test(INT32U ScaleBufIn, INT32U ScaleBufOut)
{
	INT32S ret;
	gpScale1Format_t Scale1;
	gpScale1Para_t Matrix;

	DBG_PRINT("\r\n<< %s >>\r\n", __func__);
	memset((void *)&Scale1, 0x00, sizeof(Scale1));
	memset((void *)&Matrix, 0x00, sizeof(Matrix));

	// start test
	Scale1.input_format = C_SCALER_CTRL_IN_RGB565;
	Scale1.input_width = C_IMG_IN_W;
	Scale1.input_height = C_IMG_IN_H;
	Scale1.input_visible_width = C_IMG_IN_W;
	Scale1.input_visible_height = C_IMG_IN_H;
	Scale1.input_x_offset = C_IMG_IN_W / 4;
	Scale1.input_y_offset = C_IMG_IN_H / 4;
	Scale1.input_y_addr = (INT32U)ScaleBufIn;
	Scale1.input_u_addr = 0;
	Scale1.input_v_addr = 0;

	Scale1.output_format = C_SCALER_CTRL_OUT_RGB565;
	Scale1.output_width = disp_width;
	Scale1.output_height = disp_height;
	Scale1.output_buf_width = disp_width;
	Scale1.output_buf_height = disp_height;
	Scale1.output_x_offset = 0;

	Scale1.output_y_addr = ScaleBufOut;
	Scale1.output_u_addr = 0;
	Scale1.output_v_addr = 0;
	Scale1.fifo_mode = C_SCALER_CTRL_FIFO_DISABLE;
	Scale1.scale_mode = C_SCALER_BY_RATIO;
	Scale1.digizoom_m = 10;
	Scale1.digizoom_n = 10;

	Matrix.boundary_mode = 1;
	Matrix.boundary_color = 0x8080;
	Matrix.gamma_en = 0;
	Matrix.color_matrix_en = 0;
	Matrix.yuv_type = 0;

	ret = drv_l2_scaler_trigger(SCALER_0, 1, &Scale1, &Matrix);
	if(ret == C_SCALER_STATUS_DONE || ret == C_SCALER_STATUS_STOP) {
		drv_l2_scaler_stop(SCALER_0);
	}
	else {
		DBG_PRINT("Scale1 Fail\r\n");
		while(1);
	}

	//scale to display
    drv_l2_display_update(DISPLAY_DEVICE, ScaleBufOut);
	wait_msec(1000);

	return ret;
}

void scaler_demo(void)
{
    INT32U buf_out;

    DBG_PRINT("scaler_demo start\r\n");
    drv_l2_display_init();
    drv_l2_display_start(DISPLAY_DEVICE, DISP_FMT_RGB565);
    drv_l2_display_get_size(DISPLAY_DEVICE, &disp_width, &disp_height);

    buf_out = (INT32U)gp_malloc_align(((disp_width*disp_height*2)+64),64);
    buf_out = (INT32U)((buf_out + FRAME_BUF_ALIGN64) & ~FRAME_BUF_ALIGN64);
    scale1_simple_test((INT32U)_Text056_IMG0000_CellData, buf_out);

    DBG_PRINT("scaler_demo end\r\n");
    while(1)
        wait_msec(1);
}


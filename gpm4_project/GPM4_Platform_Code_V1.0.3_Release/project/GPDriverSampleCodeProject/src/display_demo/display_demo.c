#include "drv_l1_scaler.h"
#include "drv_l2_scaler.h"
#include "drv_l2_display.h"
#include "gp_stdlib.h"
#include "ui_QVGA_256.h"

/**************************************************************************
 *                              D E F I N E                               *
 **************************************************************************/
#define UI_IMG_IN_W		                        320
#define UI_IMG_IN_H		                        240
#define FRAME_BUF_ALIGN64                       0x3F
#if UI_DATA_EN == 1
#define UI_MODE_EN                              1
#else
#define UI_MODE_EN                              0
#endif

/**************************************************************************
 *                              M A C R O S                               *
 **************************************************************************/
#define gpScale1Format_t 	        ScalerFormat_t
#define gpScale1Para_t		        ScalerPara_t

/**************************************************************************
 *                         G L O B A L    D A T A                         *
 **************************************************************************/
static INT16U h_size,v_size;

static INT32S scaler_start(INT32U ScaleBufIn, INT32U ScaleBufOut)
{
	INT32S ret;
	gpScale1Format_t Scale1;
	gpScale1Para_t Matrix;

	DBG_PRINT("\r\n<< %s >>\r\n", __func__);
	gp_memset((void *)&Scale1, 0x00, sizeof(Scale1));
	gp_memset((void *)&Matrix, 0x00, sizeof(Matrix));

	// start test
	Scale1.input_format = C_SCALER_CTRL_IN_RGB1555;
	Scale1.input_width = UI_IMG_IN_W;
	Scale1.input_height = UI_IMG_IN_H;
	Scale1.input_visible_width = UI_IMG_IN_W;
	Scale1.input_visible_height = UI_IMG_IN_H;
	Scale1.input_x_offset = 0;
	Scale1.input_y_offset = 0;
	Scale1.input_y_addr = (INT32U)ScaleBufIn;
	Scale1.input_u_addr = 0;
	Scale1.input_v_addr = 0;

	Scale1.output_format = C_SCALER_CTRL_OUT_RGB1555;
	Scale1.output_width = h_size;
	Scale1.output_height = v_size;
	Scale1.output_buf_width = h_size;
	Scale1.output_buf_height = v_size;
	Scale1.output_x_offset = 0;

	Scale1.output_y_addr = ScaleBufOut;
	Scale1.output_u_addr = 0;
	Scale1.output_v_addr = 0;
	Scale1.fifo_mode = C_SCALER_CTRL_FIFO_DISABLE;
	Scale1.scale_mode = C_SCALER_FULL_SCREEN;
	Scale1.digizoom_m = 10;
	Scale1.digizoom_n = 10;

	Matrix.boundary_mode = 1;
	Matrix.boundary_color = 0x8080;
	Matrix.gamma_en = 0;
	Matrix.color_matrix_en = 0;
	Matrix.yuv_type = 0;

	ret = drv_l2_scaler_trigger(SCALER_0, 1, &Scale1, &Matrix);
	if(ret == C_SCALER_STATUS_DONE || ret == C_SCALER_STATUS_STOP) {
		drv_l2_scaler_stop(SCALER_0);
	}
	else {
		DBG_PRINT("Scale1 Fail\r\n");
		while(1);
	}

	return ret;
}

void display_demo(void)
{
 #if UI_MODE_EN == 1
    INT32U ui_buf;
 #endif
    INT32U i,buf_size,red_buf, green_buf, blue_buf;
    INT16U *src;

    DBG_PRINT("display_demo start \r\n");

    // Initialize display device
    drv_l2_display_init();

    drv_l2_display_start(DISPLAY_DEVICE,DISP_FMT_RGB565);
    drv_l2_display_get_size(DISPLAY_DEVICE,(INT16U *)&h_size,(INT16U *)&v_size);

    buf_size = h_size*v_size*2;
    red_buf = (INT32U)gp_malloc_align(((buf_size*3)+64),64);
    red_buf = (INT32U)((red_buf + FRAME_BUF_ALIGN64) & ~FRAME_BUF_ALIGN64);
    green_buf = (red_buf + buf_size);
    blue_buf = (green_buf + buf_size);
    src = (INT16U *)red_buf;
    for(i=0;i<buf_size/2;i++)
        *src++ = 0xF800;
    src = (INT16U *)green_buf;
    for(i=0;i<buf_size/2;i++)
        *src++ = 0x7E0;
    src = (INT16U *)blue_buf;
    for(i=0;i<buf_size/2;i++)
        *src++ = 0x1F;

#if UI_MODE_EN == 1
    if((DISPLAY_DEVICE == DISDEV_TFT) && (TPO_TD025THD1 == 1))
    {
        drv_l2_display_UI_init(DISPLAY_DEVICE,UI_FMT_PALETTE_256,0,(INT32U)ui_qvga_256_Data,(INT32U)ui_qvga_256_Palette0);
    }
    #if 0
    else
    {
        buf_size = h_size*v_size*2;
        ui_buf = (INT32U)gp_malloc_align((buf_size+64),32);
        ui_buf = (INT32U)((ui_buf + FRAME_BUF_ALIGN64) & ~FRAME_BUF_ALIGN64);
        scaler_start((INT32U)ui_qvga_1555_Data,(INT32U)ui_buf);
        drv_l2_display_UI_init(DISPLAY_DEVICE,UI_FMT_RGB1555,0,(INT32U)ui_buf,0);
    }
    #endif
#endif

    while(1)
    {
        DBG_PRINT("RGB565:red \r\n");
        drv_l2_display_update(DISPLAY_DEVICE,(INT32U)red_buf);
        osDelay(1000);
        DBG_PRINT("RGB565:green \r\n");
        drv_l2_display_update(DISPLAY_DEVICE,(INT32U)green_buf);
        osDelay(1000);
        DBG_PRINT("RGB565:blue \r\n");
        drv_l2_display_update(DISPLAY_DEVICE,(INT32U)blue_buf);
        osDelay(1000);
    }
}

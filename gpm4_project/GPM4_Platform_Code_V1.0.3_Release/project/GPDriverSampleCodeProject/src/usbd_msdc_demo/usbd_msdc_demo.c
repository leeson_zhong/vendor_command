/**************************************************************************
 *                                                                        *
 *         Copyright (c) 2014 by Generalplus Inc.                         *
 *                                                                        *
 *  This software is copyrighted by and is the property of Generalplus    *
 *  Inc. All rights are reserved by Generalplus Inc.                      *
 *  This software may only be used in accordance with the                 *
 *  corresponding license agreement. Any unauthorized use, duplication,   *
 *  distribution, or disclosure of this software is expressly forbidden.  *
 *                                                                        *
 *  This Copyright notice MUST not be removed or modified without prior   *
 *  written consent of Generalplus Technology Co., Ltd.                   *
 *                                                                        *
 *  Generalplus Inc. reserves the right to modify this software           *
 *  without notice.                                                       *
 *                                                                        *
 *  Generalplus Inc.                                                      *
 *  No.19, Industry E. Rd. IV, Hsinchu Science Park                       *
 *  Hsinchu City 30078, Taiwan, R.O.C.                                    *
 *                                                                        *
 **************************************************************************/
/*******************************************************
    Include file
*******************************************************/
#include "drv_l1_sfr.h"
#include "drv_l1_uart.h"
#include "drv_l1_usbd.h"
#include "drv_l2_usbd.h"
#include "drv_l2_usbd_msdc.h"

/******************************************************
    External variables & functions declaration
*******************************************************/
extern MDSC_LUN_STORAGE_DRV const gp_msdc_ramdisk;
extern MDSC_LUN_STORAGE_DRV const gp_msdc_nand0;
extern MDSC_LUN_STORAGE_DRV const gp_msdc_nandapp0;
extern MDSC_LUN_STORAGE_DRV const gp_msdc_sd0;
extern MDSC_LUN_STORAGE_DRV const gp_msdc_sd1;

extern INT8U Default_Device_Descriptor_TBL[];
extern INT8U Default_Qualifier_Descriptor_TBL[];
extern INT8U Default_Config_Descriptor_TBL[];
extern INT8U Default_String0_Descriptor[];
extern INT8U Default_String1_Descriptor[];
extern INT8U Default_String2_Descriptor[];
extern INT8U Default_scsi_inquirydata[];
extern INT8U Default_scsi_inquirydata_CDROM[];

INT8U *scsi_inquirydata_ptr[MAX_MSDC_LUN_NUM] = {NULL};
/******************************************************
    Definition and variable declaration
*******************************************************/
#define USBD_STORAGE_NO_WPROTECT	0
#define USBD_STORAGE_WPROTECT		1

#define USBD_MSDC_DMA_BUF_ORDER	4 // 6 // should not set more than 6
#define USBD_MSDC_DMA_BUF_SIZE	((1 << USBD_MSDC_DMA_BUF_ORDER)*512)	/* Support 512/1K/2K/4K/8K/16K/32K/64K/128K data length */

/******************************************************
    Functions declaration
*******************************************************/
void usbd_msdc_init(void)
{
    INT32S ret;
    INT8U lun_type;
    INT32U dma_buf_size = USBD_MSDC_DMA_BUF_SIZE;

    /* switch to USB device mode  bit8 = 0 */
 	rSYS_CTRL_NEW &= ~(1 << 8);

    /* Init USBD L2 protocol layer first, including control/bulk/ISO/interrupt transfers */
    /******************************* Control transfer ************************************/
    ret = drv_l2_usbd_ctl_init();
    if(ret == STATUS_FAIL)
    {
        DBG_PRINT("drv_l2_usbd_ctl_init failed!\r\n");
        return;
    }

    /* Register new descriptor table here, this action must be done after drv_l2_usbd_ctl_init() */
    drv_l2_usbd_register_descriptor(REG_DEVICE_DESCRIPTOR_TYPE, (INT8U*)Default_Device_Descriptor_TBL);
    drv_l2_usbd_register_descriptor(REG_CONFIG_DESCRIPTOR_TYPE, (INT8U*)Default_Config_Descriptor_TBL);
    drv_l2_usbd_register_descriptor(REG_DEVICE_QUALIFIER_DESCRIPTOR_TYPE, (INT8U*)Default_Qualifier_Descriptor_TBL);
    drv_l2_usbd_register_descriptor(REG_STRING0_DESCRIPTOR_TYPE, (INT8U*)Default_String0_Descriptor);
    drv_l2_usbd_register_descriptor(REG_STRING1_DESCRIPTOR_TYPE, (INT8U*)Default_String1_Descriptor);
    drv_l2_usbd_register_descriptor(REG_STRING2_DESCRIPTOR_TYPE, (INT8U*)Default_String2_Descriptor);

    /* Register RAM disk storage to MSDC */

    //lun_type = LUN_RAM_DISK_TYPE;
    //drv_l2_usbd_msdc_set_lun(LUN_RAM_DISK_TYPE, LUN_NUM_0, USBD_STORAGE_NO_WPROTECT, &gp_msdc_ramdisk);

    //lun_type = LUN_SDC_TYPE;
    //drv_l2_usbd_msdc_set_lun(LUN_SDC_TYPE, LUN_NUM_0, USBD_STORAGE_NO_WPROTECT, &gp_msdc_sd0);
    //drv_l2_usbd_msdc_set_lun(LUN_SDC_TYPE, LUN_NUM_0, USBD_STORAGE_NO_WPROTECT, &gp_msdc_sd1);

    lun_type = LUN_NF_TYPE;
    drv_l2_usbd_msdc_set_lun(LUN_NF_TYPE, LUN_NUM_0, USBD_STORAGE_NO_WPROTECT, &gp_msdc_nand0);
    #if 0 && (defined NAND_APP_EN) && (NAND_APP_EN == 1)
        #if (defined NAND_APP_WRITE_EN) && (NAND_APP_WRITE_EN == 1)
        drv_l2_usbd_msdc_set_lun(LUN_NF_TYPE, LUN_NUM_0, USBD_STORAGE_NO_WPROTECT, &gp_msdc_nandapp0);
        #else
        drv_l2_usbd_msdc_set_lun(LUN_NF_TYPE, LUN_NUM_0, USBD_STORAGE_WPROTECT, &gp_msdc_nandapp0);
        #endif
    #endif

	/* Set MSDC A/B buffer size */
	if (lun_type == LUN_NF_TYPE)
	{
        // when NF, for better performance, use 32K buffer size
        if (dma_buf_size < 32*1024)
            dma_buf_size = 32*1024;
	}
	drv_l2_usbd_msdc_set_dma_buffer_size(dma_buf_size);

   	/* Init MSDC driver */
    ret = drv_l2_usbd_msdc_init();
    if(ret == STATUS_FAIL)
    {
        /* Init failed, do uninit procedures */
        drv_l2_usbd_msdc_uninit();
        DBG_PRINT("drv_l2_usbd_msdc_uninit failed!\r\n");
        return;
    }
    scsi_inquirydata_ptr[0] = (INT8U *)Default_scsi_inquirydata;
#if MSDC_MULTI_LUN == 1
    scsi_inquirydata_ptr[1] = (INT8U *)Default_scsi_inquirydata2;
#endif
    /* Register SCSI inquiry data pointer, it must be done after drv_l2_usbd_msdc_init() */
    drv_l2_usbd_msdc_register_scsi_inquiry_data(scsi_inquirydata_ptr, (INT8U*)Default_scsi_inquirydata_CDROM);

    /* Init USBD L1 register layer */
    ret = drv_l1_usbd_init();
    if(ret == STATUS_FAIL)
    {
        DBG_PRINT("drv_l1_usbd_init failed!\r\n");
        return;
    }

	/* register USBD ISR handler */
	drv_l1_usbd_enable_isr();

    DBG_PRINT("USB MSDC device init completed!\r\n");
}

void usbd_msdc_uninit(void)
{
	drv_l2_usbd_ctl_uninit();
	drv_l2_usbd_msdc_uninit();
}


#include "gplib_spu_driver.h"

#define FRAME_BUF_ALIGN64                       0x3F

extern const unsigned int drm_beautiful[];

INT32S spu_malloc(INT32U size)
{
    INT32S temp;

    temp = (INT32S)gp_malloc_align(((size)+64),64);
    temp = (INT32S)((temp + FRAME_BUF_ALIGN64) & ~FRAME_BUF_ALIGN64);

    return temp;
}

INT32S spu_free(void *ptr)
{
    gp_free(ptr);

    return 0;
}

void spu_demo(void)
{

    DBG_PRINT("spu_demo start\r\n");

    SPU_Initial();
    spu_user_malloc_set(spu_malloc, spu_free);

    if(SPU_Get_SingleChannel_Status(0) == 0)
        SPU_PlayPCM_NoEnv_FixCH((INT32U *)&drm_beautiful[0], 64, 127, 0);

    DBG_PRINT("spu_demo end\r\n");
    while(1)
            vTaskDelay(1);
}

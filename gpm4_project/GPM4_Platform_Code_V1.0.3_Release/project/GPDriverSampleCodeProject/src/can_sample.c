#include "drv_l1_can.h"
#include "board_config.h"

INT32U *rdata3to0, *rdata7to4;

void can_rx_isr(void)
{
    INT32U status;
    status = drv_l1_can_rx_chk_status(RX_Buf1);
    drv_l1_can_rx_data_receive(rdata3to0, rdata7to4, RX_Buf1);
    drv_l1_can_rx_buf_free(TRUE, RX_Buf1);
    //drv_l1_can_rx_id_valid(FALSE, RX_Buf1);

    DBG_PRINT("(RX buf1 ISR)Data[0x%x] 0x%x,0x%x {0x%x}\r\n",(R_CAN_RXB1_CS & 0x0F),*rdata7to4,*rdata3to0,R_CAN_ERR_CNT);
}

void can_sample_demo(void)
{
    CAN_RX_Struct rx_can_set;
    CAN_TX_Struct tx_can_set;
    INT32U data_cnt=0;
	rdata3to0 = (INT32U*) gp_malloc(sizeof(rdata3to0));
	rdata7to4 = (INT32U*) gp_malloc(sizeof(rdata7to4));
	can_pinmux_set(MUX1);
    drv_l1_can_init(2,4,192);//193M 125K

    //Write single package data
    tx_can_set.Byte_Order = C_Byte_DLC;
    tx_can_set.TX_ExtendID_Set = C_Stand_ID; // standard ID format
    tx_can_set.TX_Buf_Src = TX_Buf0; // use TX buffer 0
    tx_can_set.D3toD0_data = 0x12345678;
    tx_can_set.D7toD4_data = data_cnt;
    tx_can_set.TX_Basic_ID = 0x22B;
    tx_can_set.TX_Extend_ID = 0x0;
    tx_can_set.DataLengthControl = 0x8;// 8 bytes data length
    tx_can_set.RemoteTransmitReq = 0x0;// normal data package


    //register RX ISR
    drv_l1_can_interrupt_en(C_RX1_COM);
    drv_l1_can_rx_ide_format(C_Extend_ID, RX_Buf1);
    drv_l1_can_register_handler(CAN_ISR_RX1COM,can_rx_isr);
    drv_l1_can_rx_id(0x22C ,0xb ,RX_Buf1);
    drv_l1_can_rx_byte_order(C_Byte_DLC, RX_Buf1);
    drv_l1_can_rx_id_valid(TRUE, RX_Buf1);

    //polling read single package data
    rx_can_set.Byte_Order = C_Byte_DLC;
    rx_can_set.RX_ExtendID_Set = C_Extend_ID;//Stand_ID; // standard ID format
    rx_can_set.RX_Buf_Src = RX_Buf0; // use RX buffer 0
    rx_can_set.D3toD0_Ptr = rdata3to0;
    rx_can_set.D7toD4_Ptr = rdata7to4;
    rx_can_set.RX_Basic_ID = 0x22D;
    rx_can_set.RX_Extend_ID = 0xb;
    rx_can_set.RX_Mask_BID = 0x0;
    rx_can_set.RX_Mask_EID = 0x0;

    while(1)
    {
        drv_l1_can_write(tx_can_set);
        osDelay(1000);
        drv_l1_can_read(rx_can_set);
        DBG_PRINT("rx_buf0 Data 0x%x, 0x%x \r\n",*rdata7to4,*rdata3to0);
    }
}


#include "project.h"
#include "drv_l1_cfg.h"
#include "gplib.h"
#include "i2s_tx_demo.h"

#if (_DRV_L1_I2S_TX == 1)

#include "console.h"

#define K_I2S_DRIVE    FS_SD

extern int i2s_tx_demo_play(INT32U channel_flag, INT32U spu_mclk_rate, INT32U spu_frame_rate, INT8U drv);

static int nf_atoi(char * str)
{
	int num = -1;

	if (STRCMP(str, "0") == 0)
		num = 0;
	else if (STRCMP(str, "1") == 0)
		num = 1;
	else if (STRCMP(str, "2") == 0)
		num = 2;
	else if (STRCMP(str, "3") == 0)
		num = 3;
	else if (STRCMP(str, "4") == 0)
		num = 4;
	else if (STRCMP(str, "5") == 0)
		num = 5;
	else if (STRCMP(str, "6") == 0)
		num = 6;
	else if (STRCMP(str, "7") == 0)
		num = 7;
	else if (STRCMP(str, "8") == 0)
		num = 8;
	else if (STRCMP(str, "9") == 0)
		num = 9;
	else if (STRCMP(str, "10") == 0)
		num = 10;
	else if (STRCMP(str, "11") == 0)
		num = 11;
	else if (STRCMP(str, "12") == 0)
		num = 12;
	else if (STRCMP(str, "13") == 0)
		num = 13;
	else if (STRCMP(str, "14") == 0)
		num = 14;
	else if (STRCMP(str, "15") == 0)
		num = 15;
	return num;
}

extern void i2s_tx_demo_task(void const *param);
extern void i2s_tx_demo_stop_run(void);
extern void i2s_tx_demo_adjust_volumn(INT32U channel);

static i2s_tx_demo_t i2s_tx_demo_param[4]=
{
    {
        .channel_flag = 0x1,
        .mclk_selection = 5,
        .drv = K_I2S_DRIVE,
        .play_list_mode = 0,
        .play_list_inited = 0,
        .amplitude = -2,
        .sample_rate = 48000,
        .data_size = 16,
        .total_run = 1,
        .filename = {'\0'},
        .pathname = {'\0'},
        .fixed_at_32_frame_size = 0,
        .fixed_at_32_frame_size_mclk_2x = 0,

        .channel_mode = I2S_CHANNEL_MODE_ONLY_I2S,
        .spu_play_list_inited = 0,
        .spu_filename = {'\0'},
        .spu_pathname = {'\0'},
        .spu_fs = 256,
        .spu_drm_buf = NULL
    },
    {
        .channel_flag = 0x2,
        .mclk_selection = 5,
        .drv = K_I2S_DRIVE,
        .play_list_mode = 0,
        .play_list_inited = 0,
        .amplitude = -2,
        .sample_rate = 48000,
        .data_size = 16,
        .total_run = 1,
        .filename = {'\0'},
        .pathname = {'\0'},
        .fixed_at_32_frame_size = 0,
        .fixed_at_32_frame_size_mclk_2x = 0,

        .channel_mode = I2S_CHANNEL_MODE_ONLY_I2S,
        .spu_play_list_inited = 0,
        .spu_filename = {'\0'},
        .spu_pathname = {'\0'},
        .spu_fs = 256,
        .spu_drm_buf = NULL
    },
    {
        .channel_flag = 0x4,
        .mclk_selection = 5,
        .drv = K_I2S_DRIVE,
        .play_list_mode = 0,
        .play_list_inited = 0,
        .amplitude = -2,
        .sample_rate = 48000,
        .data_size = 16,
        .total_run = 1,
        .filename = {'\0'},
        .pathname = {'\0'},
        .fixed_at_32_frame_size = 0,
        .fixed_at_32_frame_size_mclk_2x = 0,

        .channel_mode = I2S_CHANNEL_MODE_ONLY_I2S,
        .spu_play_list_inited = 0,
        .spu_filename = {'\0'},
        .spu_pathname = {'\0'},
        .spu_fs = 256,
        .spu_drm_buf = NULL
    },
    {
        .channel_flag = 0x8,
        .mclk_selection = 5,
        .drv = K_I2S_DRIVE,
        .play_list_mode = 0,
        .play_list_inited = 0,
        .amplitude = -2,
        .sample_rate = 48000,
        .data_size = 16,
        .total_run = 1,
        .filename = {'\0'},
        .pathname = {'\0'},
        .fixed_at_32_frame_size = 0,
        .fixed_at_32_frame_size_mclk_2x = 0,

        .channel_mode = I2S_CHANNEL_MODE_ONLY_I2S,
        .spu_play_list_inited = 0,
        .spu_filename = {'\0'},
        .spu_pathname = {'\0'},
        .spu_fs = 256,
        .spu_drm_buf = NULL
    }
};
static i2s_tx_demo_t *pi2s_tx_demo_param[4] = {
                                                &i2s_tx_demo_param[0],\
                                                &i2s_tx_demo_param[1],\
                                                &i2s_tx_demo_param[2],\
                                                &i2s_tx_demo_param[3]
                                              };

volatile osThreadId i2s_tx_demo_task_id = NULL;
volatile osThreadId i2s_tx_spu_demo_task_id = NULL;

void i2s_tx_demo_launch(void)
{
    INT32U k;

    for (k=0; k < 4; k++)
        DBG_PRINT("pi2s_tx_demo_param[%u]=%08x\r\n", k, pi2s_tx_demo_param[k]);

    osThreadDef(i2s_tx_demo_task, osPriorityNormal, 0, 16384);
    i2s_tx_demo_task_id = osThreadCreate(&os_thread_def_i2s_tx_demo_task, (void *)(&pi2s_tx_demo_param[0]));
}

void i2s_tx_demo_stop(void)
{
    INT32U save_total_run = i2s_tx_demo_param[0].total_run;

    i2s_tx_demo_stop_run();
    i2s_tx_demo_param[0].total_run = 0;

    while (i2s_tx_demo_task_id != NULL)
    {
        osDelay(1);
    }

    i2s_tx_demo_param[0].total_run = save_total_run;
    DBG_PRINT("I2S TX DEMO STOP DONE !!!\r\n");
}

static void i2s_tx_demo_play_handler(int argc, char *argv[])
{
    INT32U channel_flag = 1;
    INT16S ret;
    unsigned int total_run = pi2s_tx_demo_param[0]->total_run;
    INT8U drv = K_I2S_DRIVE;

	if (argc > 1)
	{
		drv = nf_atoi(argv[1]);
		if (drv != FS_SD && \
            drv != FS_SD2 && \
            drv != FS_NAND1)
        {
            DBG_PRINT("illegal drive number [%s], should %u, %u, %u\r\n", argv[1], FS_SD, FS_SD2, FS_NAND1);
            return;
        }
	}

    if (argc > 2)
    {
		sscanf(argv[2], "%u", &total_run);
		if (total_run == 0)
            total_run = 1;
    }

    if (argc > 3)
	{
        channel_flag = nf_atoi(argv[3]);

		if (channel_flag > 15)
		{
			DBG_PRINT("illegal channel flag [%s], should be 0~15\r\n", argv[3]);
			return;
		}
	}

    #if 0
    ret = _devicemount(drv);
    if (ret == 0)
	{
		ret = i2s_tx_demo_play(channel_flag, 0, 0, drv);
		if (ret == 0)
		{
			DBG_PRINT("i2sdemo tx play ...... pass on drv %u.\r\n", drv);
		}
	}
    else
        DBG_PRINT("drv %u mount fail may be sd card not inserted. \r\n", drv);

    ret = _deviceunmount(drv);
    #else
    if (i2s_tx_demo_task_id != NULL || i2s_tx_spu_demo_task_id != NULL)
    {
        DBG_PRINT("Please stop I2S TX DEMO to replay.\r\n");
        return;
    }
    pi2s_tx_demo_param[0]->total_run = total_run;
    pi2s_tx_demo_param[0]->channel_flag = channel_flag;
    pi2s_tx_demo_param[0]->drv = drv;
    i2s_tx_demo_launch();
    #endif
}

void i2s_tx_demo_config_handler(int argc, char *argv[])
{
    if (argc > 1)
	{
        if (STRCMP(argv[1],"samplerate") == 0)
        {
            if (argc > 2)
            {
                INT32U sample_rate;

                sscanf(argv[2], "%u", &sample_rate);
                if (sample_rate >= 8000 && sample_rate <= 96000)
                {
                    pi2s_tx_demo_param[0]->sample_rate = sample_rate/1000*1000;
					pi2s_tx_demo_param[0]->spu_sample_rate = sample_rate;
                    DBG_PRINT("sample_rate %u\r\n", sample_rate);
                }
                else
                    DBG_PRINT("sample_rate must >=8000 && <= 96000\r\n");
            }
        }
        else if (STRCMP(argv[1],"mclk") == 0)
        {
            if (argc > 2)
            {
                INT32U mclk_idx;

                sscanf(argv[2], "%u", &mclk_idx);
                if ((mclk_idx >= 1 && mclk_idx<= 5) || (mclk_idx >= 7 && mclk_idx<= 11))
                {
                    pi2s_tx_demo_param[0]->mclk_selection = mclk_idx;
                    DBG_PRINT("mclk %u\r\n", mclk_idx);
                }
                else
                    DBG_PRINT("mclk must between 1~5, 7~11\r\n");
            }
        }
        else if (STRCMP(argv[1],"vol") == 0)
        {
            if (argc > 2)
            {
                INT32S amplitude;

                sscanf(argv[2], "%d", &amplitude);
                if (amplitude >= -2 && amplitude <= 2)
                {
                    pi2s_tx_demo_param[0]->amplitude = amplitude;
                    i2s_tx_demo_adjust_volumn(0);
                    DBG_PRINT("volumn %d\r\n", amplitude);
                }
                else
                    DBG_PRINT("volumn must between -2~2\r\n");
            }
        }
        else if (STRCMP(argv[1],"playlistmode") == 0)
        {
            if (argc > 2)
            {
                INT32U play_list_mode;

                sscanf(argv[2], "%u", &play_list_mode);
                if (play_list_mode >=0 && play_list_mode <= 1)
                {
                    pi2s_tx_demo_param[0]->play_list_mode = play_list_mode;
                    pi2s_tx_demo_param[0]->play_list_inited = 0;
                    pi2s_tx_demo_param[0]->spu_play_list_inited = 0;
                    DBG_PRINT("playlistmode %u\r\n", play_list_mode);
                }
                else
                    DBG_PRINT("playlistmode must between 0~1\r\n");
            }
            else
                pi2s_tx_demo_param[0]->play_list_mode = 1;
        }
        else if (STRCMP(argv[1],"file") == 0)
        {
            if (argc > 2)
            {
                sscanf(argv[2], "%s", pi2s_tx_demo_param[0]->filename);
                DBG_PRINT("file name %s\r\n", pi2s_tx_demo_param[0]->filename);
            }
            else
                pi2s_tx_demo_param[0]->filename[0] = '\0'; // reset to default
        }
        else if (STRCMP(argv[1],"path") == 0)
        {
            if (argc > 2)
            {
                sscanf(argv[2], "%s", pi2s_tx_demo_param[0]->pathname);
                DBG_PRINT("path name %s\r\n", pi2s_tx_demo_param[0]->pathname);
            }
            else
                pi2s_tx_demo_param[0]->pathname[0] = '\0'; // reset to default
        }
	}
}
static void i2s_tx_demo_cmd_help(void)
{
    // internal test only: "\r\n       i2s_tx_demo dac sameplerate<48,44,32,24,22,16,12,11,8> double<0,1> bit<0,1>\r\n"

    DBG_PRINT(
    	"\r\nUsage: i2s_tx_demo help\r\n"
    	"\r\n       i2s_tx_demo 48K <clk(1:24.576M, 0:12.288M)>\r\n"
    	"\r\n       i2s_tx_demo 24K <clk(1:24.576M, 0:12.288M)>\r\n"
    	"\r\n       i2s_tx_demo 48K24B <clk(1:24.576M, 0:12.288M)>\r\n"
    	"\r\n       i2s_tx_demo 24K24B <clk(1:24.576M, 0:12.288M)>\r\n"

    	"\r\n       i2s_tx_demo next\r\n"
    	"\r\n       i2s_tx_demo stop\r\n"
    	"\r\n       i2s_tx_demo config vol <-2~2>\r\n"
    	"\r\n       i2s_tx_demo config samplerate <8000~96000>\r\n"

    	);
    DBG_PRINT(
        "\r\n       i2s_tx_demo play [<drive(10,2,0)> <run_count(1~)> <channel_flag(4 bit)>]\r\n"
        "\r\n       i2s_tx_demo playrec <samplerate(8000~96000)> <mclk(1~5, 7~11)>\r\n"
    	"\r\n       i2s_tx_demo config playlistmode <0, 1>\r\n"
    	"\r\n       i2s_tx_demo config file <name>\r\n"
    	"\r\n       i2s_tx_demo config path <name(""audio_input\\00\\"">\r\n"
    	"\r\n       i2s_tx_demo config mclk <1~5, 7~11, \
1: 36.864, 2: 24.576, 3: 18.432 4: 14.745 5: 12.288 \
7: 33.869 8: 22.579 9: 16.934 10: 13.547 11: 11.289 >\r\n"
    	);
    DBG_PRINT(
    	"\r\n       i2s_tx_demo drm <samplesel(48,44,32,24,22,16,12,11,8)>, \
<fssel(256,384,512,768)>, \
<mixed(0,1)>"
    	"\r\n       ex:i2s_tx_demo drm 48 256\r\n"
        );
}

extern void i2s_tx_spu_demo_task(void const *param);
extern void i2s_tx_spu_demo_stop_run(void);

void i2s_tx_spu_demo_launch(void)
{
    INT32U k;

    osThreadDef(i2s_tx_spu_demo_task, osPriorityNormal, 0, 16384);
    i2s_tx_spu_demo_task_id = osThreadCreate(&os_thread_def_i2s_tx_spu_demo_task, (void *)(&pi2s_tx_demo_param[0]));
}

void i2s_tx_spu_demo_stop(void)
{
    INT32U save_total_run = i2s_tx_demo_param[0].spu_total_run;

    i2s_tx_spu_demo_stop_run();

    i2s_tx_demo_param[0].spu_total_run = 0;

    while (i2s_tx_spu_demo_task_id != NULL)
    {
        osDelay(1);
    }
    i2s_tx_demo_param[0].spu_total_run = save_total_run;

    DBG_PRINT("I2S TX SPU DEMO STOP DONE !!!\r\n");
}

static char *i2s_tx_get_dir_name_by_sample_rate(INT32U sample_rate)
{
	if (sample_rate == 48000)
		return "audio_input\\48K\\";
	else if (sample_rate == 44100)
		return "audio_input\\44K\\";
	else if (sample_rate == 32000)
		return "audio_input\\32K\\";
    else if (sample_rate == 24000)
		return "audio_input\\24K\\";
	else if (sample_rate == 22500)
        return "audio_input\\22K\\";
	else if (sample_rate == 16000)
        return "audio_input\\16K\\";
	else if (sample_rate == 12000)
        return "audio_input\\12K\\";
    else if (sample_rate == 11025)
        return "audio_input\\11K\\";
	else if (sample_rate == 8000)
		return "audio_input\\8K\\";
    else
		return NULL; // unknown sample rate
}

static INT32U i2s_tx_sample_sel_to_sample_rate(INT32U sample_sel, INT8U *use_apll_67738MHZ)
{
    INT32U sample_rate;

	*use_apll_67738MHZ = 0;

	if (sample_sel == 48)
		sample_rate = 48000;
	else if (sample_sel == 44)
	{
		sample_rate = 44100;
        *use_apll_67738MHZ = 1;
	}
    else if (sample_sel == 32)
		sample_rate = 32000;
    else if (sample_sel == 24)
        sample_rate = 24000;
    else if (sample_sel == 22)
	{
		sample_rate = 22500;
        *use_apll_67738MHZ = 1;
	}
	else if (sample_sel == 16)
        sample_rate = 16000;
	else if (sample_sel == 12)
        sample_rate = 12000;
    else if (sample_sel == 11)
    {
        sample_rate = 11025;
        *use_apll_67738MHZ = 1;
    }
	else if (sample_sel == 8)
		sample_rate = 8000;
	else
		sample_rate = 0; // unknown sample rate

	return sample_rate;
}

static void i2s_tx_demo_cmd_handler(int argc, char *argv[])
{
    if (STRCMP(argv[1],"play") == 0)
    {
        pi2s_tx_demo_param[0]->fixed_at_32_frame_size = 0;
        i2s_tx_demo_play_handler(argc - 1, &argv[1]);
    }
    else if (STRCMP(argv[1],"stop") == 0)
    {
        i2s_tx_demo_stop();
        i2s_tx_spu_demo_stop();
        pi2s_tx_demo_param[0]->fixed_at_32_frame_size = 0;
        pi2s_tx_demo_param[0]->channel_mode = I2S_CHANNEL_MODE_ONLY_I2S;
    }
    else if (STRCMP(argv[1],"playrec") == 0) // special quick command for IC design to do test
    {
        i2s_tx_demo_stop();
        i2s_tx_spu_demo_stop();

        pi2s_tx_demo_param[0]->channel_mode = I2S_CHANNEL_MODE_ONLY_I2S;
        pi2s_tx_demo_param[0]->fixed_at_32_frame_size = 0;
        pi2s_tx_demo_param[0]->channel_flag = 1; // set to TX0 only
        pi2s_tx_demo_param[0]->play_list_mode = 1; // set to play whole folder
        pi2s_tx_demo_param[0]->play_list_inited = 0;
        pi2s_tx_demo_param[0]->spu_play_list_inited = 0;
        strcpy(pi2s_tx_demo_param[0]->pathname, "audio_output\\");
        pi2s_tx_demo_param[0]->data_size = 16; // ? future should add data_size specify by user
        if (argc > 2)
        {
            INT32U sample_rate;

            sscanf(argv[2], "%u", &sample_rate);
            if (sample_rate >= 8000 && sample_rate <= 96000)
            {
                pi2s_tx_demo_param[0]->sample_rate = sample_rate/1000*1000;
                DBG_PRINT("sample_rate %u\r\n", sample_rate);
            }
            else
                DBG_PRINT("sample_rate must >=8000 && <= 96000\r\n");
        }
        if (argc > 3)
        {
            INT32U mclk_idx;

            sscanf(argv[3], "%u", &mclk_idx);
            if ((mclk_idx >= 1 && mclk_idx<= 5) || (mclk_idx >= 7 && mclk_idx<= 11))
            {
                pi2s_tx_demo_param[0]->mclk_selection = mclk_idx;
                DBG_PRINT("mclk %u\r\n", mclk_idx);
            }
            else
                DBG_PRINT("mclk must between 1~5, 7~11\r\n");
        }

        i2s_tx_demo_launch();
    }
    else if (STRCMP(argv[1],"drm") == 0)
    {
        INT32U mixed_sel = 0;
        INT32U fs_sel = 256;

        //i2s_tx_demo drm sameplerate<48,44> fs<256, 384, 512, 768> mixed<0,1>

        i2s_tx_demo_stop();
        i2s_tx_spu_demo_stop();

        pi2s_tx_demo_param[0]->fixed_at_32_frame_size = 0;
        pi2s_tx_demo_param[0]->fixed_at_32_frame_size_mclk_2x = 0;
        pi2s_tx_demo_param[0]->channel_flag = 1; // set to TX0 only
        pi2s_tx_demo_param[0]->play_list_mode = 1; // set to play whole folder
        pi2s_tx_demo_param[0]->play_list_inited = 0;
        pi2s_tx_demo_param[0]->spu_play_list_inited = 0;
        strcpy(pi2s_tx_demo_param[0]->pathname, "audio_input\\48K\\");
        pi2s_tx_demo_param[0]->data_size = 16;

        pi2s_tx_demo_param[0]->spu_total_run = 1;
        strcpy(pi2s_tx_demo_param[0]->spu_pathname, "audio_input\\drm\\");
        pi2s_tx_demo_param[0]->channel_mode = I2S_CHANNEL_MODE_ONLY_SPU;
        pi2s_tx_demo_param[0]->spu_fs = fs_sel;

        if (argc > 4)
        {
             sscanf(argv[4], "%u", &mixed_sel);
             pi2s_tx_demo_param[0]->channel_mode = mixed_sel ? I2S_CHANNEL_MODE_BOTH_I2S_SPU : I2S_CHANNEL_MODE_ONLY_SPU;
        }
        if (argc > 3)
        {
            sscanf(argv[3], "%u", &fs_sel);
            if (fs_sel == 256 || fs_sel == 384 || fs_sel == 512 || fs_sel == 768)
                pi2s_tx_demo_param[0]->spu_fs = fs_sel;
            else
            {
                fs_sel = 256;
                pi2s_tx_demo_param[0]->spu_fs = fs_sel;
            }
        }

        if (argc > 2)
        {
            INT32U sameple_sel = 0;
            INT32U sample_rate;
            INT8U apll_67738MHZ = 0; // when 0, use 73728MHZ
            INT8U mclk_selection = I2S_MAIN_FREQ_12288MHZ;

            sscanf(argv[2], "%u", &sameple_sel);
            sample_rate = i2s_tx_sample_sel_to_sample_rate(sameple_sel, &apll_67738MHZ);
            if (sample_rate == 0 || sample_rate < 16000)
            {
                // illegal sample rate reset to 48K
                sameple_sel = 48;
                sample_rate = 48000;
                apll_67738MHZ = 0;
                fs_sel = 256;
                pi2s_tx_demo_param[0]->spu_fs = fs_sel;
            }

            if (pi2s_tx_demo_param[0]->channel_mode == I2S_CHANNEL_MODE_BOTH_I2S_SPU)
            {
                // when this mode, fps suport only 256
                fs_sel = 256;
                pi2s_tx_demo_param[0]->spu_fs = fs_sel;

                // when this mode, only 48K, 44.1K allowed
                if (sample_rate != 48000 && sample_rate != 44100)
                {
                    // reset to 48K
                    sameple_sel = 48;
                    sample_rate = 48000;
                    apll_67738MHZ = 0;
                }
            }

            strcpy(pi2s_tx_demo_param[0]->pathname, i2s_tx_get_dir_name_by_sample_rate(sample_rate));

            pi2s_tx_demo_param[0]->sample_rate = sample_rate/1000*1000;
            pi2s_tx_demo_param[0]->spu_sample_rate = sample_rate;

            if (apll_67738MHZ)
            {
                if (fs_sel == 768)
                    mclk_selection = I2S_MAIN_FREQ_33869MHZ;
                else if (fs_sel == 512)
                    mclk_selection = I2S_MAIN_FREQ_22579MHZ;
                else if (fs_sel == 384)
                    mclk_selection = I2S_MAIN_FREQ_16934MHZ;
                else
                    mclk_selection = I2S_MAIN_FREQ_11289MHZ;
            }
            else
            {
                if (fs_sel == 768)
                    mclk_selection = I2S_MAIN_FREQ_36864MHZ;
                else if (fs_sel == 512)
                    mclk_selection = I2S_MAIN_FREQ_24576MHZ;
                else if (fs_sel == 384)
                    mclk_selection = I2S_MAIN_FREQ_18432MHZ;
                else
                    mclk_selection = I2S_MAIN_FREQ_12288MHZ;
            }
            pi2s_tx_demo_param[0]->mclk_selection = mclk_selection;
        }
        if (pi2s_tx_demo_param[0]->channel_mode == I2S_CHANNEL_MODE_ONLY_SPU)
            i2s_tx_spu_demo_launch();
        else if (pi2s_tx_demo_param[0]->channel_mode == I2S_CHANNEL_MODE_BOTH_I2S_SPU)
        {
            i2s_tx_demo_launch();
            i2s_tx_spu_demo_launch();
        }
        else
            i2s_tx_demo_launch();
    }
#if 1
    else if (STRCMP(argv[1],"dac") == 0)
    {
        INT32U bit_sel = 0;
        INT32U doube_sel = 0;

        i2s_tx_demo_stop();
        i2s_tx_spu_demo_stop();

        pi2s_tx_demo_param[0]->channel_mode = I2S_CHANNEL_MODE_ONLY_I2S;
        pi2s_tx_demo_param[0]->fixed_at_32_frame_size = 1;
        pi2s_tx_demo_param[0]->channel_flag = 9; // set to TX0, TX3 only
        pi2s_tx_demo_param[0]->play_list_mode = 1; // set to play whole folder
        pi2s_tx_demo_param[0]->play_list_inited = 0;
        pi2s_tx_demo_param[0]->spu_play_list_inited = 0;
        strcpy(pi2s_tx_demo_param[0]->pathname, "audio_input\\48K\\");
        pi2s_tx_demo_param[0]->data_size = 16;
        if (argc > 4)
        {
             sscanf(argv[4], "%u", &bit_sel);
             pi2s_tx_demo_param[0]->data_size = bit_sel ? 24 : 16;
        }
        if (argc > 3)
        {
             sscanf(argv[3], "%u", &doube_sel);
        }
        pi2s_tx_demo_param[0]->fixed_at_32_frame_size_mclk_2x = doube_sel ? 1 : 0;
        if (argc > 2)
        {
            INT32U sameple_sel = 0;
            INT32U sample_rate;
            INT8U apll_67738MHZ = 0; // when 0, use 73728MHZ

            sscanf(argv[2], "%u", &sameple_sel);

            sample_rate = i2s_tx_sample_sel_to_sample_rate(sameple_sel, &apll_67738MHZ);
            if (sample_rate == 0)
            {
                // illegal sample rate reset to 48K
                sameple_sel = 48;
                sample_rate = 48000;
                apll_67738MHZ = 0;
            }

            strcpy(pi2s_tx_demo_param[0]->pathname, i2s_tx_get_dir_name_by_sample_rate(sample_rate));
            if (pi2s_tx_demo_param[0]->data_size != 16)
            {
                char num_str[32];
                char str_len = strlen(pi2s_tx_demo_param[0]->pathname);

                pi2s_tx_demo_param[0]->pathname[str_len - 1] = '\0'; // to redule the "\\"
                sprintf(num_str, "-%02uBit\\", pi2s_tx_demo_param[0]->data_size);
                strcat(pi2s_tx_demo_param[0]->pathname, num_str);
            }

            pi2s_tx_demo_param[0]->sample_rate = sample_rate/1000*1000;
            pi2s_tx_demo_param[0]->mclk_selection = (apll_67738MHZ) ? I2S_MAIN_FREQ_11289MHZ : I2S_MAIN_FREQ_12288MHZ;
        }
        i2s_tx_demo_launch();
    }
    else if (STRCMP(argv[1],"24K") == 0) // special quick command for IC design to do test
    {
        i2s_tx_demo_stop();
        i2s_tx_spu_demo_stop();

        pi2s_tx_demo_param[0]->channel_mode = I2S_CHANNEL_MODE_ONLY_I2S;
        pi2s_tx_demo_param[0]->fixed_at_32_frame_size = 0;
        pi2s_tx_demo_param[0]->channel_flag = 1; // set to TX0 only
        pi2s_tx_demo_param[0]->play_list_mode = 1; // set to play whole folder
        pi2s_tx_demo_param[0]->play_list_inited = 0;
        pi2s_tx_demo_param[0]->spu_play_list_inited = 0;
        strcpy(pi2s_tx_demo_param[0]->pathname, "audio_input\\24K\\");
        pi2s_tx_demo_param[0]->sample_rate = 24000;
        pi2s_tx_demo_param[0]->data_size = 16;
        if (argc > 2)
        {
            INT32U mclk_sel = 0;
            INT32U mclk_selection;

            sscanf(argv[2], "%u", &mclk_sel);
            mclk_selection = (mclk_sel == 1) ? I2S_MAIN_FREQ_24576MHZ : I2S_MAIN_FREQ_12288MHZ;
            pi2s_tx_demo_param[0]->mclk_selection = mclk_selection;
        }

        i2s_tx_demo_launch();
    }
    else if (STRCMP(argv[1],"48K") == 0) // special quick command for IC design to do test
    {
        i2s_tx_demo_stop();
        i2s_tx_spu_demo_stop();

        pi2s_tx_demo_param[0]->channel_mode = I2S_CHANNEL_MODE_ONLY_I2S;
        pi2s_tx_demo_param[0]->fixed_at_32_frame_size = 0;
        pi2s_tx_demo_param[0]->channel_flag = 1; // set to TX0 only
        pi2s_tx_demo_param[0]->play_list_mode = 1; // set to play whole folder
        pi2s_tx_demo_param[0]->play_list_inited = 0;
        pi2s_tx_demo_param[0]->spu_play_list_inited = 0;
        strcpy(pi2s_tx_demo_param[0]->pathname, "audio_input\\48K\\");
        pi2s_tx_demo_param[0]->sample_rate = 48000;
        pi2s_tx_demo_param[0]->data_size = 16;

        if (argc > 2)
        {
            INT32U mclk_sel = 0;
            INT32U mclk_selection;

            sscanf(argv[2], "%u", &mclk_sel);
            mclk_selection = (mclk_sel == 1) ? I2S_MAIN_FREQ_24576MHZ : I2S_MAIN_FREQ_12288MHZ;
            pi2s_tx_demo_param[0]->mclk_selection = mclk_selection;
        }

        i2s_tx_demo_launch();
    }
    else if (STRCMP(argv[1],"24K24B") == 0) // special quick command for IC design to do test
    {
        i2s_tx_demo_stop();
        i2s_tx_spu_demo_stop();

        pi2s_tx_demo_param[0]->channel_mode = I2S_CHANNEL_MODE_ONLY_I2S;
        pi2s_tx_demo_param[0]->fixed_at_32_frame_size = 0;
        pi2s_tx_demo_param[0]->channel_flag = 1; // set to TX0 only
        pi2s_tx_demo_param[0]->play_list_mode = 1; // set to play whole folder
        pi2s_tx_demo_param[0]->play_list_inited = 0;
        pi2s_tx_demo_param[0]->spu_play_list_inited = 0;
        strcpy(pi2s_tx_demo_param[0]->pathname, "audio_input\\24K-24Bit\\");
        pi2s_tx_demo_param[0]->sample_rate = 24000;
        pi2s_tx_demo_param[0]->data_size = 24;
        if (argc > 2)
        {
            INT32U mclk_sel = 0;
            INT32U mclk_selection;

            sscanf(argv[2], "%u", &mclk_sel);
            mclk_selection = (mclk_sel == 1) ? I2S_MAIN_FREQ_24576MHZ : I2S_MAIN_FREQ_12288MHZ;
            pi2s_tx_demo_param[0]->mclk_selection = mclk_selection;
        }

        i2s_tx_demo_launch();
    }
    else if (STRCMP(argv[1],"48K24B") == 0) // special quick command for IC design to do test
    {
        i2s_tx_demo_stop();
        i2s_tx_spu_demo_stop();

        pi2s_tx_demo_param[0]->channel_mode = I2S_CHANNEL_MODE_ONLY_I2S;
        pi2s_tx_demo_param[0]->fixed_at_32_frame_size = 0;
        pi2s_tx_demo_param[0]->channel_flag = 1; // set to TX0 only
        pi2s_tx_demo_param[0]->play_list_mode = 1; // set to play whole folder
        pi2s_tx_demo_param[0]->play_list_inited = 0;
        pi2s_tx_demo_param[0]->spu_play_list_inited = 0;
        strcpy(pi2s_tx_demo_param[0]->pathname, "audio_input\\48K-24Bit\\");
        pi2s_tx_demo_param[0]->sample_rate = 48000;
        pi2s_tx_demo_param[0]->data_size = 24;
        if (argc > 2)
        {
            INT32U mclk_sel = 0;
            INT32U mclk_selection;

            sscanf(argv[2], "%u", &mclk_sel);
            mclk_selection = (mclk_sel == 1) ? I2S_MAIN_FREQ_24576MHZ : I2S_MAIN_FREQ_12288MHZ;
            pi2s_tx_demo_param[0]->mclk_selection = mclk_selection;
        }

        i2s_tx_demo_launch();
    }
#endif
    else if (STRCMP(argv[1],"next") == 0)
    {
        i2s_tx_demo_stop();
        i2s_tx_spu_demo_stop();

        if (pi2s_tx_demo_param[0]->channel_mode == I2S_CHANNEL_MODE_ONLY_SPU)
            i2s_tx_spu_demo_launch();
        else if (pi2s_tx_demo_param[0]->channel_mode == I2S_CHANNEL_MODE_BOTH_I2S_SPU)
        {
            i2s_tx_demo_launch();
            i2s_tx_spu_demo_launch();
        }
        else
            i2s_tx_demo_launch();
    }
    else if (STRCMP(argv[1],"config") == 0)
    {
        i2s_tx_demo_config_handler(argc - 1, &argv[1]);
    }
    else
    {
    	i2s_tx_demo_cmd_help();
    }
}

static cmd_t i2s_tx_demo_cmd_list[] =
{
	{"i2s_tx_demo",  i2s_tx_demo_cmd_handler,  NULL },
	{NULL,    NULL,   NULL}
};

void i2s_tx_demo_cmd_register(void)
{
	cmd_t *pcmd = &i2s_tx_demo_cmd_list[0];

	while (pcmd->cmd != NULL)
	{
		cmdRegister(pcmd);
		pcmd += 1;
	}
}

void I2S_TX_Demo(void)
{
    #if defined(GPLIB_CONSOLE_EN) && (GPLIB_CONSOLE_EN == 1)
    i2s_tx_demo_cmd_register();
    #endif
}

#endif // (_DRV_L1_I2S_TX == 1)

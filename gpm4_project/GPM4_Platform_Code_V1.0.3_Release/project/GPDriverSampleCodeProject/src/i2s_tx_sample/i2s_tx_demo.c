#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "project.h"
#include "drv_l1_cfg.h"

#if (_DRV_L1_I2S_TX == 1)

#include "drv_l2.h"
#include "drv_l1_sfr.h"
#include "drv_l1_i2c.h"
#include "drv_l1_dma.h"
#include "drv_l1_gpio.h"
#include "gplib.h"
#include "gplib_cfg.h"
#include "drv_l2_sccb.h"
#include "i2s_tx_demo.h"

#define USE_I2C_GPIO            1

#define K_I2S_USE_REAL_BOARD    1

#define K_REPLAY_SPU            1
#define K_USE_TASK              0 // 1 or 0 only
#define K_REPEAT_TX_COUNT       5

#define K_HAVE_FILE_SYSTEM      GPLIB_FILE_SYSTEM_EN

#define K_TX_DMA_BULK_SIZE              4096 // (1200*1024/4) //153600 // 256 // word

#define K_DATA_AT_BUFFER                0
#define K_DATA_AT_DISK                  1

#if (K_HAVE_FILE_SYSTEM)
#define K_TX_DATA_SOURCE                K_DATA_AT_DISK //K_DATA_AT_BUFFER // K_DATA_AT_DISK
#else
#define K_TX_DATA_SOURCE                K_DATA_AT_BUFFER
#endif

#define K_AUDIO_STEREO                   0
#define K_AUDIO_MONO                     1

#define K_I2S_NONMERGE_MODE              0
#define K_I2S_MERGE_MODE                 1

#define K_AUDIO_CODEC_BCM_CLK_MODE       1
#define K_AUDIO_CODEC_DEFO_CLK_MODE      0

#define K_I2S_TX_USE_INTERRUPT           0

#define I2S_RX_SIG            0xbeafbeaf
#define I2S_TX_SIG            0xfaebfaeb

extern int merge_2ch(short *p, int times);
extern int merge_2ch_size_in_word(int times);

int i2s_tx_spu_play(void);

#define MAX_I2X_TX_NUM 		4
#define MAX_PCM_BUF_NUM 	20

typedef struct pcm_buf_entry_s
{
    INT16S *buf;
    volatile INT32U buf_len;
    volatile INT8U filled;
    volatile INT8U stopped;
} pcm_buf_entry_t;

static pcm_buf_entry_t pcm_buf_list[MAX_I2X_TX_NUM][MAX_PCM_BUF_NUM];
static volatile INT32U consumer_buf_idx_list[MAX_I2X_TX_NUM];
static volatile INT32U producer_buf_idx_list[MAX_I2X_TX_NUM];
static INT8U startup_done_flag[MAX_I2X_TX_NUM];

void i2s_tx_demo_buf_init(INT32U ch);

i2s_tx_demo_t **ptx_demo = NULL;

#define SLAVE_ID_0 0x34
#define SLAVE_ID_1 0x36

typedef drv_l1_i2c_bus_handle_t i2c_bus_handle_t;

typedef union i2s_i2c_config_def
{
    drv_l1_i2c_bus_handle_t    i2c_hw;
    sccb_config_t              i2c_gpio;
} i2s_i2c_config_t;

typedef struct i2s_i2c_def
{
    i2s_i2c_config_t config;
    INT8U use_gpio;
    void *handle;
} i2s_i2c_t;

typedef union _WM8988
{
    short info;
    char  data[2];
}WM8988;

typedef struct i2s_profile_s
{
    INT32U frame_size; // 16(0), 24(1), 32(2), 48(3), 64(4), 96(5), 128(6), 176(7), 192(8), 32(15, slave use)
    INT32U MCLK_clock_rate; // 12300000 = 12.3MHz
    INT32U sample_rate; // ex 8K=8000,48K=48000
                        // 目前FPGA MCLK=12.3MHz
                        // 所以1T BCLK :12.3M/4=3.075MHz
                        // 1T LRCK: 3.075/64=48KHz
                        // MCLK/1   BCLK = 12.3MHz,   LRCK = 192KHz
                        // MCLK/2,  BCLK = 6.15MHz,   LRCK =  96KHz
                        // MCLK/3,  BCLK = 4.1MHz,    LRCK =  64KHz
                        // MCLK/4,  BCLK = 3.075MHz,  LRCK =  48KHz
                        // MCLK/6,  BCLK = 2.05MHz,   LRCK =  32KHz
                        // MCLK/8,  BCLK = 1.5375MHz, LRCK =  24KHz
    INT32 MCLK_div;
    INT32U data_size;   // 16(0), 18(1), 20(2), 22(3), 24(4), 32(5), 32(6), 32(7)


    INT8U merge;
    INT8U mono;
    INT8U r_lsb;
    INT8U normal_mode_aligned;
    INT8U send_mode;
    INT8U edge_mode;
    INT8U frame_polar;
    INT8U first_frame_LR;
    INT8U framingmode; // 0 is I2S mode, 1 is Normal Mode, 2 is DSP mode, 3 is DSP mode
    INT8S amplitude; // -2, -1, 0, 1, 2
} i2s_profile_t;

static sccb_config_t i2c_0_2_config =
{   // I2C0 # 2
    .scl_port = IO_A8,
    .scl_drv = IOD_DRV_4mA,
    .sda_port = IO_A9,
    .sda_drv = IOD_DRV_4mA,
    .pwdn_port = (GPIO_ENUM)0, // when have_pwdn is 0, this filed dont care
    .pwdn_drv = IOD_DRV_4mA,   // not yet implemented
    .have_pwdn = 0,
    .RegBits = 8,
    .DataBits = 8,
    .slaveAddr = 0,
    .timeout = 2000,            // in ms unit, not yet implemented
    .clock_rate = 20            // in kHz unit, not yet implemented
};

static sccb_config_t i2c_2_2_config =
{   // I2C2 # 2
    .scl_port = IO_A10,
    .scl_drv = IOD_DRV_4mA,
    .sda_port = IO_A11,
    .sda_drv = IOD_DRV_4mA,
    .pwdn_port = (GPIO_ENUM)0, // when have_pwdn is 0, this filed dont care
    .pwdn_drv = IOD_DRV_4mA,   // not yet implemented
    .have_pwdn = 0,
    .RegBits = 8,
    .DataBits = 8,
    .slaveAddr = 0,
    .timeout = 2000,            // in ms unit, not yet implemented
    .clock_rate = 20            // in kHz unit, not yet implemented
};

static sccb_config_t i2c_1_0_config =
{   // I2C1 # 0
    .scl_port = IO_C0,
    .scl_drv = IOD_DRV_4mA,
    .sda_port = IO_C1,
    .sda_drv = IOD_DRV_4mA,
    .pwdn_port = (GPIO_ENUM)0, // when have_pwdn is 0, this filed dont care
    .pwdn_drv = IOD_DRV_4mA,   // not yet implemented
    .have_pwdn = 0,
    .RegBits = 8,
    .DataBits = 8,
    .slaveAddr = 0,
    .timeout = 2000,            // in ms unit, not yet implemented
    .clock_rate = 20            // in kHz unit, not yet implemented
};

static sccb_config_t i2c_IOA15_IOA14_config =
{
    .scl_port = IO_A15,
    .scl_drv = IOD_DRV_4mA,
    .sda_port = IO_A14,
    .sda_drv = IOD_DRV_4mA,
    .pwdn_port = (GPIO_ENUM)0, // when have_pwdn is 0, this filed dont care
    .pwdn_drv = IOD_DRV_4mA,   // not yet implemented
    .have_pwdn = 0,
    .RegBits = 8,
    .DataBits = 8,
    .slaveAddr = 0,
    .timeout = 2000,            // in ms unit, not yet implemented
    .clock_rate = 20            // in kHz unit, not yet implemented
};

static void i2s_delay(unsigned int num)
{
    volatile int i;

    for (i=0; i<num; ++i)
    {
        // nonsense , just delay
        //R_RANDOM0 = i;
        __asm("nop");
    }
}

static int i2c_write(i2s_i2c_t *pi2s_i2c, INT8U reg, INT8U value)
{
    int ret;
    int i=0;

    for (i=0;i<100;++i)
    {
        if (pi2s_i2c->use_gpio)
            ret = drv_l2_sccb_write(pi2s_i2c->handle , reg, value);
        else
            ret = drv_l1_reg_1byte_data_1byte_write(pi2s_i2c->handle, reg, value);
        if (ret != -1)
        {
          break;
        }
        else
        {
          DBG_PRINT("drv_l1_reg_1byte_data_1byte_write fail. reg=%d value=%0x02x\r\n", reg, value);
          i2s_delay(0x10000); // 0x4FFFF
        }
    }
    if ( ret == -1 ) {
      while (1) R_RANDOM0 = i;
    }

    //i2s_delay(0x10000); // 0x4FFFF

    return ret;
}

static short i2c_wolfson_WM8988(int addr, int data)
{
   int hi = addr << (8+1);
   int lo = data;
   int cmd_swap = (hi|lo);
   int cmd = ( ((cmd_swap>>8) & 0x000000ff) |  ((cmd_swap<<8) & 0x0000ff00) );

   return (short)cmd;
}

static void wolfson_WM8988_power_down(i2s_i2c_t *pi2s_i2c)
{
    WM8988 pack;

    pack.info = i2c_wolfson_WM8988(25,0x00);
    i2c_write(pi2s_i2c, pack.data[0] ,pack.data[1]);
}

static void wolfson_WM8988_tx_adjust_volumn(i2s_i2c_t *pi2s_i2c, INT8S amplitude)
{
    WM8988 pack;
    INT16U LOUT2;
    INT16U ROUT2;
    INT16U LOUT1;
    INT16U ROUT1;

    if (pi2s_i2c->handle == NULL)
        return;

    DBG_PRINT("amplitude=%d\r\n", amplitude);

    if (amplitude == 0)
        LOUT2 = 0x179;
    else if (amplitude == 1)
        LOUT2 = 0x17C;
    else if (amplitude >= 2)
        LOUT2 = 0x17f;
    else if (amplitude == -1)
        LOUT2 = 0x169;
    else if (amplitude <= -2)
        LOUT2 = 0x159;
    ROUT2 = LOUT2;
    LOUT1 = LOUT2;
    ROUT1 = LOUT1;

    pack.info = i2c_wolfson_WM8988(2,LOUT1);  // LOUT1
    i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);

    pack.info = i2c_wolfson_WM8988(3,ROUT1);   // ROUT1
    i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);

    pack.info = i2c_wolfson_WM8988(40,LOUT2);  // LOUT2 volumn , adjust left volumn, 0x179 is 0db, 0x17f is +6db, 0x130 is -67db, 0x13f is mute
    i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);

    pack.info = i2c_wolfson_WM8988(41,ROUT2); // ROUT2 volumn, 0x179 is 0db, 0x17f is +6db, 0x130 is -67db, 0x13f is mute
    i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);

}
static void wolfson_WM8988_tx_init(i2s_i2c_t *pi2s_i2c, i2s_profile_t *ptx_profile)
{
    WM8988 pack;
    INT16U bcm;
    INT16U WL;
    INT16U BCLKINV = 0;
    INT16U LRP = 0;
    INT16U format = 2; // I2S mode
    INT16U clkdiv2 = 0;
    INT16U mclk_44K = 0;
    INT16U higher_mclk = 0;
    INT16U SR;
    INT16U LOUT2;
    INT16U ROUT2;
    INT16U LOUT1;
    INT16U ROUT1;

    pack.info = i2c_wolfson_WM8988(15,0x0); //reset
    i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);

    i2s_delay(0x10000); // 0x1F

    pack.info = i2c_wolfson_WM8988(67,0x0);
    i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);

    pack.info = i2c_wolfson_WM8988(24,0x0);
    i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);

    pack.info = i2c_wolfson_WM8988(25,0xFC);	// Pwr Mgmt(1) // 0xEC not work, 0x0FC work.
    i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);

    pack.info = i2c_wolfson_WM8988(26,0x1F8);
    i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);

   // LOUT2 and ROUT2 set to same
    if (ptx_profile->amplitude == 0)
        LOUT2 = 0x179;
    else if (ptx_profile->amplitude == 1)
        LOUT2 = 0x17C;
    else if (ptx_profile->amplitude >= 2)
        LOUT2 = 0x17f;
    else if (ptx_profile->amplitude == -1)
        LOUT2 = 0x169;
    else if (ptx_profile->amplitude <= -2)
        LOUT2 = 0x159;
    ROUT2 = LOUT2;
    LOUT1 = LOUT2;
    ROUT1 = LOUT1;

    pack.info = i2c_wolfson_WM8988(2,0x179);  // LOUT1
    i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);

    pack.info = i2c_wolfson_WM8988(3,0x179);   // ROUT1
    i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);

    pack.info = i2c_wolfson_WM8988(40,LOUT2);  // LOUT2 volumn , adjust left volumn, 0x179 is 0db, 0x17f is +6db, 0x130 is -67db, 0x13f is mute
    i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);

    pack.info = i2c_wolfson_WM8988(41,ROUT2); // ROUT2 volumn, 0x179 is 0db, 0x17f is +6db, 0x130 is -67db, 0x13f is mute
    i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);

    pack.info = i2c_wolfson_WM8988(5,0x0); // ADC and DAC control, default is 0x08(DACMU=1 Digital Soft Mute=1)
    i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);

    if (ptx_profile->data_size == 32)
        WL = 3;
    else if (ptx_profile->data_size == 24)
        WL = 2;
    else if (ptx_profile->data_size == 20)
        WL = 1;
    else if (ptx_profile->data_size == 16)
        WL = 0; // data_size 16 bit
    else
        WL = 3; // data_size 32 bit

    if (ptx_profile->edge_mode)
    {
      BCLKINV = 1;
    }

    if (ptx_profile->framingmode == 1)
      format = 1; // left justified

    pack.info = i2c_wolfson_WM8988(7, format | (WL<<2) | (BCLKINV << 7) | (LRP << 4)); // Audio Interface,
                                                     //  [7]BCLKINV, 0 BLCK not inverted, 1 BLCK onverted
                                                     //  [6]MS, 1 Enable Master Mode, 0 Slave Mode
                                                     //  [5]LRSWAP 1 swap left and right, 0 no swap
                                                     //  [4]LRP 1 invert LRCLK polarity
                                                     //  [3:2] WL Audio Data word length, 00 16 bits, 01 20 bits, 10 24 bits, 11 32 bits
                                                     //  [1:0] Format Audio Data Format, 00 reserve, 01 Left justified, 10 I2S format 11 DSP Mode

    i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);

    pack.info = i2c_wolfson_WM8988(34,0x150);// Bit 8, LD2LO=1 enable left dac to left mixer
                                             // Bit 7, LI2LO=0, disable LMISEL Signal to LeftMixer
                                             // Bit 6:4, LI2LOVOL, 5, LMISEL Signal to LeftMixer
                                             // Bit 2:0, 0 LINPUT1, 1 LINPUT2, 3 left ADC Input(after PGA/MICNOAST), 4 Differential input
    i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);

    pack.info = i2c_wolfson_WM8988(35,0x50); // Bit 8, RD2LO=0 disable left dac to left mixer
                                             // Bit 7, RI2LO=0, disable RMISEL Signal to LeftMixer
                                             // Bit 6:4, RI2LOVOL, 5, RMISEL Signal to LeftMixer


    i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);

    pack.info = i2c_wolfson_WM8988(36,0x50); // Bit 8, LD2RO=0 disable left dac to right mixer
                                             // Bit 7, LI2RO=0, disable LMISEL Signal to RightMixer
                                             // Bit 6:4, LI2ROVOL, 5, LMISEL Signal to RightMixer
                                             // Bit 2:0, 0 RINPUT1, 1 RINPUT2, 3 right ADC Input(after PGA/MICNOAST), 4 Differential input

    i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);

    pack.info = i2c_wolfson_WM8988(37,0x150); // Bit 8, RD2RO=1 enable right dac to right mixer
                                              // Bit 7, RI2RO=0, disable RMISEL Signal to RightMixer
                                              // Bit 6:4, LI2ROVOL, 5, RMISEL Signal to RightMixer
    i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);

    bcm = 0; // tx use default clock mode instead of bcm(bit clock mode)

          // 1, MCLK/4
          // 2, MCLK/8
          // 3, MCLK/16
    if (ptx_profile->MCLK_clock_rate == 24576000 || \
        ptx_profile->MCLK_clock_rate == 22579000 || \
        ptx_profile->MCLK_clock_rate == 36864000 || \
        ptx_profile->MCLK_clock_rate == 33869000 )
        clkdiv2 = 1;

    if (ptx_profile->MCLK_clock_rate == 18432000 || \
        ptx_profile->MCLK_clock_rate == 36864000 || \
        ptx_profile->MCLK_clock_rate == 16934000 || \
        ptx_profile->MCLK_clock_rate == 33869000)
        higher_mclk = 1; // higher than

    if (ptx_profile->MCLK_clock_rate == 11289000 || \
        ptx_profile->MCLK_clock_rate == 22579000 || \
        ptx_profile->MCLK_clock_rate == 16934000 || \
        ptx_profile->MCLK_clock_rate == 33869000 )
      mclk_44K = 1; // 44K freq series

    if (ptx_profile->sample_rate == 24000)
    {
        SR = 0x1C; // SR[4:0]=11100=0x1C
        if (mclk_44K != 0)
          DBG_PRINT("sample rate 24K, mclk freq wrong. %u\r\n", ptx_profile->MCLK_clock_rate);
    }
    else if (ptx_profile->sample_rate == 32000)
    {
        SR = 0x0C; // SR[4:0]=01100=0x0C
        if (mclk_44K != 0)
          DBG_PRINT("sample rate 32K, mclk freq wrong. %u\r\n", ptx_profile->MCLK_clock_rate);
    }
    else if (ptx_profile->sample_rate == 96000)
    {
        SR = 0x0E; // SR[4:0]=01110=0x0E
        if (mclk_44K != 0)
          DBG_PRINT("sample rate 96K, mclk freq wrong. %u\r\n", ptx_profile->MCLK_clock_rate);
       //if (clkdiv2 == 0)
       //   DBG_PRINT("sample rate 96K, mclk freq must at >= 24576000 because mclk freq must >= 4xbclk freq. %u\r\n", ptx_profile->MCLK_clock_rate);
    }
    else if (ptx_profile->sample_rate == 88000)
    {
        SR = 0x0E; // SR[4:0]=01110=0x0E
        if (mclk_44K == 0)
          DBG_PRINT("sample rate 88.2K, mclk freq wrong. %u\r\n", ptx_profile->MCLK_clock_rate);
       if (clkdiv2 == 0)
          DBG_PRINT("sample rate 88.2K, mclk freq must at >= 22579000 because mclk freq must >= 4xbclk freq. %u\r\n", ptx_profile->MCLK_clock_rate);
    }
    else if (ptx_profile->sample_rate == 16000)
    {
        SR = 0x0A; // SR[4:0]=01010=0x0A
        if (mclk_44K != 0)
          DBG_PRINT("sample rate 16K, mclk freq wrong. %u\r\n", ptx_profile->MCLK_clock_rate);
    }
    else if (ptx_profile->sample_rate == 22000) // 22050
    {
        SR = 0x0A; // SR[4:0]=01010=0x0A
        if (mclk_44K == 0)
          DBG_PRINT("sample rate 22.05K, mclk freq wrong. %u\r\n", ptx_profile->MCLK_clock_rate);
    }
    else if (ptx_profile->sample_rate == 8000)
    {
        SR = 0x06; // SR[4:0]=00110=0x06
        if (mclk_44K != 0)
          DBG_PRINT("sample rate 8K, mclk freq wrong. %u\r\n", ptx_profile->MCLK_clock_rate);
    }
    else if (ptx_profile->sample_rate == 8017)
    {
        SR = 0x06; // SR[4:0]=00110=0x06
        if (mclk_44K == 0)
          DBG_PRINT("sample rate 8.0182K, mclk freq wrong. %u\r\n", ptx_profile->MCLK_clock_rate);
    }
    else if (ptx_profile->sample_rate == 12000)
    {
        SR = 0x08; // SR[4:0]=01000=0x08
        if (mclk_44K != 0)
          DBG_PRINT("sample rate 12K, mclk freq wrong. %u\r\n", ptx_profile->MCLK_clock_rate);
    }
    else if (ptx_profile->sample_rate == 11000) // 11025
    {
        SR = 0x08; // SR[4:0]=01000=0x08
        if (mclk_44K == 0)
          DBG_PRINT("sample rate 11.025K, mclk freq wrong. %u\r\n", ptx_profile->MCLK_clock_rate);
    }
    else if (ptx_profile->sample_rate == 44000)
    {
        SR = 0;   // SR[4:0]=00000=0x00
        if (mclk_44K == 0)
          DBG_PRINT("sample rate 44.1K, mclk freq wrong. %u\r\n", ptx_profile->MCLK_clock_rate);
    }
    else // 48K
    {
        SR = 0;   // SR[4:0]=00000=0x00
        if (mclk_44K != 0)
          DBG_PRINT("sample rate 48K, mclk freq wrong. %u\r\n", ptx_profile->MCLK_clock_rate);
    }

    pack.info = i2c_wolfson_WM8988(8, ((SR | (mclk_44K<<4) | higher_mclk)<<1) | (bcm << 7) | (clkdiv2 << 6) );

    i2c_write(pi2s_i2c,pack.data[0] ,pack.data[1]);
}

static char *in_file_path[4] =
    {
        "audio_input\\00\\",
        "audio_input\\01\\",
        "audio_input\\02\\",
        "audio_input\\03\\"
    };

static char *in_file_name[4] =
    {
        "input_ch_00.pcm",
        "input_ch_01.pcm",
        "input_ch_02.pcm",
        "input_ch_03.pcm"
    };

static INT32U pcm_audio_buf_size;
static INT8U *pcm_audio_buf;
static INT32U pcm_audio_buf2_size;
static INT8U *pcm_audio_buf2;
static INT32U pcm_audio_buf3_size;
static INT8U *pcm_audio_buf3;
static INT32U pcm_audio_buf4_size;
static INT8U *pcm_audio_buf4;

#define K_MCLK_RATE         12300000 //  目前FPGA MCLK=12.3MHz

//static INT8U mclk_divider[]     = {1, 2, 3, 4, 6, 8, 12, 24};   // fpga ever use
//static INT8U mclk_divider_idx[] = {0, 0, 1, 2, 3, 4, 5,  6}; // fpga ever use
static INT8U mclk_divider[]     = {2, 3, 4, 6, 8};
static INT8U mclk_divider_idx[] = {2, 3, 4, 6, 7}; // old

// giving mclk clock rate, frame size, calculate suitable mclk divider
// for the requested sample rate. When found, return 0 and MCLK_div,
// else return -1.
#if 0
static INT32S i2s_cal_sample_rate(i2s_profile_t *pprofile)
{
    INT8U k, mclk_div_total = sizeof(mclk_divider);
    INT32U bck_clock_rate;
    INT32U lrck_rate;

    for (k=0; k < mclk_div_total; k++)
    {
        bck_clock_rate = pprofile->MCLK_clock_rate / mclk_divider[k]; // 3075000
        lrck_rate = bck_clock_rate / (pprofile->frame_size * 2);
        lrck_rate = lrck_rate / 1000 * 1000;
        if (lrck_rate == pprofile->sample_rate)
        {
            pprofile->MCLK_div = mclk_divider_idx[k]; // mclk_divider[k];
            return 0;
        }
    }
    DBG_PRINT("Serious Error : Cant find suitable MCLK_div\r\n");

    return -1;
}
#else

static INT8U i2s_tx_frame_size_list[] = {16, 24, 32, 48, 64, 96, 128, 176, 192};

static INT32S i2s_cal_sample_rate(i2s_profile_t *pprofile)
{
    INT8U i, j, k, mclk_div_total = sizeof(mclk_divider);
    INT8U divider;
    INT8U divider_idx;
    INT8U max_divider;
    INT8U frame_size_total = sizeof(i2s_tx_frame_size_list);
    INT16U frame_size;
    INT16U fs;
    INT32U bclk, temp;
    INT32U MCLK_clock_rate;

    fs = pprofile->MCLK_clock_rate / pprofile->sample_rate;
    divider = fs / (2*pprofile->frame_size);
    max_divider = mclk_divider[mclk_div_total - 1];
    if (divider <=  max_divider)
    {
        // check if divider available
        for (k=0; k < mclk_div_total; k++)
        {
            if (divider == mclk_divider[k])
            {
                // found the divider supported, get it index
                pprofile->MCLK_div = mclk_divider_idx[k];
                DBG_PRINT("use divider %u, frame size %u, sample %u, clock %u\r\n", divider, pprofile->frame_size, pprofile->sample_rate, pprofile->MCLK_clock_rate);
                return 0;
            }
        }
        DBG_PRINT("Serious Error : Cant find suitable MCLK_div. divider=%u\r\n", divider);
        // find nearset divider
        divider = max_divider;
        divider_idx = mclk_div_total - 1;
        pprofile->MCLK_div = mclk_divider_idx[divider_idx];
    }
    else
    {
        DBG_PRINT("Serious Error : req divider over maximal divider 8. %u\r\n", divider);
        divider = max_divider;
        divider_idx = mclk_div_total - 1;
        pprofile->MCLK_div = mclk_divider_idx[divider_idx];
    }
    // else the request divider over maximal supported divider

    // use adjust frame size strategy

    bclk = pprofile->MCLK_clock_rate / divider;
    frame_size = (bclk / pprofile->sample_rate) / 2;
    if (frame_size < pprofile->data_size)
    {
        frame_size = pprofile->data_size; // frame size must greater and equal to data size
        divider = fs / (2*frame_size);
        for (k=0; k < mclk_div_total; k++)
        {
            if (divider == mclk_divider[k])
            {
                // found the divider supported, get it index
                pprofile->MCLK_div = mclk_divider_idx[k];
            }
        }
    }

    for (k=0; k < frame_size_total; k++)
    {
        if (frame_size == i2s_tx_frame_size_list[k])
        {
            pprofile->frame_size = frame_size;
            DBG_PRINT("update frame size. divider %u, frame size %u, sample %u, clock %u\r\n", divider, pprofile->frame_size, pprofile->sample_rate, pprofile->MCLK_clock_rate);
            return 0;
        }
    }

    // frame size
    DBG_PRINT("Serious Error : Cant find frame size. divider %u, frame size %u, sample %u, clock %u\r\n", divider, frame_size, pprofile->sample_rate, pprofile->MCLK_clock_rate);

    // use max frame size srategy
    for (k=0; k < frame_size_total; k++)
    {
        frame_size = i2s_tx_frame_size_list[frame_size_total - k - 1];
        if (frame_size >= pprofile->data_size)
        {
            if ((pprofile->MCLK_clock_rate % (frame_size*2)) == 0)
            {
                temp = pprofile->MCLK_clock_rate / (frame_size*2);
                if ((temp % pprofile->sample_rate) == 0)
                {
                    divider = temp / pprofile->sample_rate;
                    for (j=0; j < mclk_div_total; j++)
                    {
                        if (divider == mclk_divider[j])
                        {
                            // found the divider supported, get it index
                            pprofile->MCLK_div = mclk_divider_idx[j];
                            pprofile->frame_size = frame_size;
                            DBG_PRINT("use divider %u, frame size %u, sample %u, clock %u\r\n", divider, pprofile->frame_size, pprofile->sample_rate, pprofile->MCLK_clock_rate);
                            return 0;
                         }
                    } // end for j
                }
            }
        }
    } // end for k

    DBG_PRINT("Serious Error : Cant find divider and frame size. divider %u, frame size %u, sample %u, clock %u\r\n", divider, frame_size, pprofile->sample_rate, pprofile->MCLK_clock_rate);

    // use change MCLK strategy
    if ((pprofile->sample_rate % 8000) == 0)
    {
        I2S_MAIN_FREQ_E main_freq_list[] = {I2S_MAIN_FREQ_12288MHZ, I2S_MAIN_FREQ_18432MHZ, I2S_MAIN_FREQ_24576MHZ, I2S_MAIN_FREQ_36864MHZ};

        // 8k multiple
        for (i=0; i < 4; i++)
        {
            drv_l1_clock_set_i2s_main_mclk_freq(main_freq_list[i]);
            MCLK_clock_rate = drv_l1_clock_get_i2s_main_mclk_freq()*1000;
            for (k=0; k < frame_size_total; k++)
            {
                frame_size = i2s_tx_frame_size_list[frame_size_total - k - 1];
                if (frame_size >= pprofile->data_size)
                {
                    if ((MCLK_clock_rate % (frame_size*2)) == 0)
                    {
                        temp = MCLK_clock_rate / (frame_size*2);
                        if ((temp % pprofile->sample_rate) == 0)
                        {
                            divider = temp / pprofile->sample_rate;
                            for (j=0; j < mclk_div_total; j++)
                            {
                                if (divider == mclk_divider[j])
                                {
                                    // found the divider supported, get it index
                                    pprofile->MCLK_clock_rate = MCLK_clock_rate;
                                    pprofile->MCLK_div = mclk_divider_idx[j];
                                    pprofile->frame_size = frame_size;
                                    DBG_PRINT("use divider %u, frame size %u, sample %u, clock %u\r\n", divider, pprofile->frame_size, pprofile->sample_rate, pprofile->MCLK_clock_rate);
                                    return 0;
                                }
                            } // end for j
                        }
                    }
                }
            } // end for k
        } // end for i
    }

    return -1;
}
#endif

typedef struct i2s_dma_s
{
    INT32U buf_len; // word unit, buffer len
    short *buffer;
    INT32U dma_bulk_size;
    volatile INT8S notify_flag;
    INT32U dma_done_count;
    INT8S complete_flag;
    INT32U acc_dma_size;
    INT32U dma_req_count;
    INT32U underrun_count; // tx
    INT32U half_full_count;
    INT8U channel; // dma channel return by dma L1
} i2s_dma_t;

typedef struct i2s_app_s
{
    INT32U channel;
    unsigned int TestState;
    INT32U loop_max;
    int ret;
    char *task_name;

    i2s_i2c_t i2s_i2c;

    INT8U tx_data_source;   // K_TX_DATA_SOURCE
    volatile INT8U running;

    i2s_dma_t tx_dma;
    i2s_profile_t tx_profile;

    INT16S fd_in;
    INT32S read_size;
    INT32S read_ret;
	INT8U drv;
	INT64U f_size;
	INT64U acc_read_size;
    char in_file_name[256];

} i2s_app_t;

static i2s_app_t i2s_apps[4];

#if (K_I2S_TX_USE_INTERRUPT == 1)

static INT32U i2s_tx_sts_check(INT32U channel, INT32U bit_mask, char *msg)
{
    volatile i2sTxReg_t *i2s = drv_l1_i2s_tx_get_register_base_addr(channel);
    INT32U status;

    status = i2s->TX_STATUS;

    if ((status & bit_mask) == bit_mask)
    {
        if (msg != NULL)
            DBG_PRINT("%s, sts=%08x\r\n", msg, status);

        return 1;
    }
    else
        return 0;
}

static INT32U i2s_tx_ctl_check(INT32U channel, INT32U bit_mask, char *msg, INT32U bit_clear)
{
    volatile i2sTxReg_t *i2s = drv_l1_i2s_tx_get_register_base_addr(channel);
    INT32U ctl, ctl_org;

    ctl = i2s->TX_CTRL;
    if ((ctl & bit_mask) == bit_mask)
    {
        ctl_org = ctl;
        if (bit_clear == bit_mask)
        {
            ctl |= bit_clear;
            i2s->TX_CTRL = ctl;
            if (msg != NULL)
                DBG_PRINT("%s, ctl org=%08x clr=%08x, aft=%08x\r\n", msg, ctl_org,  ctl, i2s->TX_CTRL);
        }
        else if (bit_clear == (~bit_mask))
        {
            ctl &= bit_clear;
            i2s->TX_CTRL = ctl;
            if (msg != NULL)
                DBG_PRINT("%s, ctl org=%08x clr=%08x, aft=%08x\r\n", msg, ctl_org,  ctl, i2s->TX_CTRL);
        }
        else
        {
            if (msg != NULL)
                DBG_PRINT("%s, ctl_org=%08x\r\n", msg, ctl_org);
        }

        return 1;
    }
    else
        return 0;
}

static void i2s_tx_isr(INT32U channel, INT32U global)
{
    volatile i2sTxReg_t *i2s = drv_l1_i2s_tx_get_register_base_addr(channel);
    INT32U ctl, ctl_org;
    INT32U sts;
    i2s_app_t *pi2s_app;
    i2s_dma_t *ptx_dma;

    sts = i2s->TX_STATUS;
    ctl_org = i2s->TX_CTRL;

    ctl = ctl_org | (1<< K_I2S_TX_CTL_BIT_IRT_FLAG); //clear interrupt
    i2s->TX_CTRL = ctl;

    pi2s_app = &i2s_apps[channel];
    ptx_dma = &pi2s_app->tx_dma;

    ptx_dma->half_full_count++;

    if (i2s_tx_ctl_check(channel, (1<<K_I2S_TX_CTL_BIT_UNDERFLOW), NULL, (1<<K_I2S_TX_CTL_BIT_UNDERFLOW)))
    {
        ptx_dma->underrun_count++;
        i2s->TX_CTRL &= (~(1<< K_I2S_TX_CTL_BIT_EN_IRT)); // disable interrupt
        DBG_PRINT("txch=%d, sts=%08x ctrl=%08x, %d, %d\r\n", channel, sts, i2s->TX_CTRL, ptx_dma->half_full_count, ptx_dma->underrun_count);
    }

    if (ptx_dma->half_full_count < 5)
        DBG_PRINT("txch=%d, sts=%08x ctrl=%08x, %d\r\n", channel, sts, i2s->TX_CTRL, ptx_dma->half_full_count);
}

#endif

static int i2s_app_tx_buf_init(INT32U channel, i2s_dma_t *ptx_dma, INT32U aligned_size)
{
	INT32U buf_aligned_size = 4;

    if (channel == 1)
    {
		if (pcm_audio_buf2 == NULL)
		{
			pcm_audio_buf2_size = K_AUDIO_BUF_SIZE;
			pcm_audio_buf2 = pvPortMalloc(pcm_audio_buf2_size + buf_aligned_size);
			if (pcm_audio_buf2 == NULL)
				return -1;
		}
        ptx_dma->buffer = (short *)(((INT32U)pcm_audio_buf2 + (buf_aligned_size - 1)) & (~(buf_aligned_size - 1)));
        ptx_dma->buf_len = pcm_audio_buf2_size / 4;
    }
    else if (channel == 2)
    {
		if (pcm_audio_buf3 == NULL)
		{
			pcm_audio_buf3_size = K_AUDIO_BUF_SIZE;
			pcm_audio_buf3 = pvPortMalloc(pcm_audio_buf3_size + buf_aligned_size);
			if (pcm_audio_buf3 == NULL)
				return -1;
		}
        ptx_dma->buffer = (short *)(((INT32U)pcm_audio_buf3 + (buf_aligned_size - 1)) & (~(buf_aligned_size - 1)));
        ptx_dma->buf_len = pcm_audio_buf3_size / 4;
    }
    else if (channel == 3)
    {
		if (pcm_audio_buf4 == NULL)
		{
			pcm_audio_buf4_size = K_AUDIO_BUF_SIZE;
			pcm_audio_buf4 = pvPortMalloc(pcm_audio_buf4_size + buf_aligned_size);
			if (pcm_audio_buf4 == NULL)
				return -1;
		}
        ptx_dma->buffer = (short *)(((INT32U)pcm_audio_buf4 + (buf_aligned_size - 1)) & (~(buf_aligned_size - 1))); // aligned to 4 byte multiple
        ptx_dma->buf_len = pcm_audio_buf4_size / 4;
    }
    else
    {
		if (pcm_audio_buf == NULL)
		{
			pcm_audio_buf_size = K_AUDIO_BUF_SIZE;
			pcm_audio_buf = pvPortMalloc(pcm_audio_buf_size + buf_aligned_size);
			if (pcm_audio_buf == NULL)
				return -1;
		}
        ptx_dma->buffer = (short *)(((INT32U)pcm_audio_buf + (buf_aligned_size - 1)) & (~(buf_aligned_size - 1))); // aligned to 4 byte multiple
        ptx_dma->buf_len = pcm_audio_buf_size / 4;
    }

    if (aligned_size != 0 && aligned_size < ptx_dma->buf_len)
        ptx_dma->dma_bulk_size = aligned_size;
    else
        ptx_dma->dma_bulk_size = ptx_dma->buf_len;

    // rounded to aligned_size bulk size multiple
    ptx_dma->buf_len = (ptx_dma->buf_len / ptx_dma->dma_bulk_size) * ptx_dma->dma_bulk_size;

    ptx_dma->acc_dma_size = 0;

    return 0;
}

static void traverse_16_bits(INT16U *psample, INT32U count)
{
  INT32U k;
  INT16U j, orig, result;

  for (k = 0; k < count; k++)
  {
      orig = psample[k];
      result = 0;
      for (j = 0; j < 16; j++)
      {
          if ((orig & (1<<j)) != 0)
            result |= (1<< (15 - j));
      }
      psample[k] = result;
  }
}

static void traverse_32_bits(INT32U *psample, INT32U count, INT32U data_size)
{
  INT32U j, k, sample_bit_count;
  INT32U orig, result;

  sample_bit_count = data_size - 1;
  for (k = 0; k < count; k++)
  {
      orig = psample[k];
      result = 0;
      for (j = 0; j <= sample_bit_count; j++)
      {
          if ((orig & (1<<j)) != 0)
            result |= (1<< (sample_bit_count - j));
      }

      if ((data_size < 32) && ((result & (1 << sample_bit_count)) != 0))
      {
          // sign extension
          for (j = data_size; j < 32; j++)
            result |= (1<<j);
      }
      psample[k] = result;
  }
}

// convert 3 bytes sample to 4 bytes sample
static void covert_3_to_4_byte_sample(INT8U *psample, INT32U count, INT32U data_size)
{
    INT8U *psample_end = psample + (count * 3 - 1);
    INT32U origin_data;
    INT32U *psample_result = (INT32U *)psample;
    INT32U j, sample_bit_count;

    psample_result = psample_result + count - 1;
    sample_bit_count = data_size - 1;

    while (count > 0)
    {
        origin_data = ((*psample_end--) << 16);
        origin_data |= ((*psample_end--) << 8);
        origin_data |= (*psample_end--);

        if ((data_size < 32) && ((origin_data & (1 << sample_bit_count)) != 0))
        {
            // sign extension
            for (j = data_size; j < 32; j++)
                origin_data |= (1<<j);
        }

        *psample_result-- = origin_data;

        count--;
    }
}

static char *get_fs_root_path(INT8U drv)
{
    if (drv == FS_SD)
        return "C:\\";
    else if (drv == FS_SD2)
        return "K:\\";
    else if (drv == FS_NAND1)
        return "A:\\";
    else
    {
        DBG_PRINT("Unknown drive %u.\r\n", drv);
        return NULL;
    }
}

static int i2s_app_tx_buf_fill(i2s_app_t *pi2s_app)
{
    i2s_dma_t *ptx_dma;
    i2s_profile_t *ptx_profile;

    ptx_profile = &pi2s_app->tx_profile;
    ptx_dma = &pi2s_app->tx_dma;
    {
        if (pi2s_app->tx_data_source == K_DATA_AT_DISK)
        {
			char *root_path;

			root_path = get_fs_root_path(pi2s_app->drv);
			if (root_path != NULL)
				strcpy(pi2s_app->in_file_name, root_path);
			else
				strcpy(pi2s_app->in_file_name, "C:\\");

            if (ptx_demo != NULL)
            {
                INT32U channel = pi2s_app->channel;

                if (ptx_demo[0]->fixed_at_32_frame_size)
                    channel = 0;

                if (ptx_demo[channel]->pathname[0] != '\0')
                    strcat(pi2s_app->in_file_name, ptx_demo[channel]->pathname);
                else
                    strcat(pi2s_app->in_file_name, in_file_path[channel]);
                if (ptx_demo[channel]->filename[0] != '\0')
                    strcat(pi2s_app->in_file_name, ptx_demo[channel]->filename);
                else
                    strcat(pi2s_app->in_file_name, in_file_name[channel]);
            }
            else
            {
                strcat(pi2s_app->in_file_name, in_file_path[pi2s_app->channel]);
                strcat(pi2s_app->in_file_name, in_file_name[pi2s_app->channel]);
            }
            pi2s_app->acc_read_size = 0;
            pi2s_app->fd_in = fs_open(pi2s_app->in_file_name, O_RDONLY);
            if (pi2s_app->fd_in < 0)
            {
                DBG_PRINT("fail to open input file %s\r\n", pi2s_app->in_file_name);
                return -1;
            }
            else
                DBG_PRINT("success open input file %s\r\n", pi2s_app->in_file_name);
#if 0
#if 1
            if (ptx_profile->data_size > 16 && ptx_profile->data_size < 32)
                pi2s_app->read_size = ptx_dma->buf_len*3;
            else
#endif
                pi2s_app->read_size = ptx_dma->buf_len*4;
            pi2s_app->read_ret = fs_read(pi2s_app->fd_in, (INT32U)ptx_dma->buffer, pi2s_app->read_size);
            if (pi2s_app->read_ret != 0 && pi2s_app->read_ret != pi2s_app->read_size)
            {
                // should be end of file reached, refill from start of input file data
                lseek((INT16S)pi2s_app->fd_in, 0, SEEK_SET);
                pi2s_app->read_ret = fs_read(pi2s_app->fd_in, (INT32U)ptx_dma->buffer, pi2s_app->read_size);
            }
#endif
            if (ptx_dma->buf_len < 4)
                pi2s_app->loop_max = 0;
            lseek((INT16S)pi2s_app->fd_in, 0, SEEK_SET);
        }
        else
        {
            #if 0 // DEBUG USE
            {
                int k;
                //memset(ptx_dma->buffer, 0xAA, ptx_dma->buf_len*4);
                for (k=0; k < ptx_dma->buf_len; k++)
                    ( (INT32U *)ptx_dma->buffer)[k] = k+1;
            }
            #else
                ptx_dma->buf_len = merge_2ch((short *)ptx_dma->buffer,6);	// 48KHz PCM data
                if (ptx_dma->dma_bulk_size >= ptx_dma->buf_len)
                    ptx_dma->dma_bulk_size = ptx_dma->buf_len;
                else
                    // rounded to dma_bulk_size multiple
                    ptx_dma->buf_len = (ptx_dma->buf_len / ptx_dma->dma_bulk_size) * ptx_dma->dma_bulk_size;

                if (ptx_dma->buf_len < 4)
                    pi2s_app->loop_max = 0;
                DBG_PRINT("len_tx=%d\r\n", ptx_dma->buf_len);
            #endif
        }

        if (ptx_profile->data_size > 16 && ptx_profile->data_size < 32)
            covert_3_to_4_byte_sample((INT8U *)ptx_dma->buffer, ptx_dma->buf_len, ptx_profile->data_size);
#if 1
        if (ptx_profile->send_mode)
        {
            if (ptx_profile->data_size == 16)
                traverse_16_bits((INT16U *)(ptx_dma->buffer), ptx_dma->buf_len*2);
            else
                traverse_32_bits((INT32U *)(ptx_dma->buffer), ptx_dma->buf_len, ptx_profile->data_size);
        }
#endif
    }

    DBG_PRINT("tx buf addr=%08x, len=%d\r\n", (INT32U)(ptx_dma->buffer), ptx_dma->buf_len);

    return 0;
}

static void i2s_print_TX_source(i2s_app_t *pi2s_app)
{
    if (pi2s_app->tx_data_source == K_DATA_AT_DISK)
        DBG_PRINT("TX channel %d by file\r\n", pi2s_app->channel);
    else
        DBG_PRINT("TX channel %d by buffer\r\n", pi2s_app->channel);
}

static int i2s_i2c_init(INT32U devNumber, INT16U slaveAddr, i2s_i2c_t *pi2s_i2c)
{
    #if 0 // just for purpose to test B4, B5 connected, because this 2 bin connected to I2C
    if (devNumber == I2C_0)
    {
        gpio_init_io(IO_B4, GPIO_OUTPUT);
        gpio_init_io(IO_B5, GPIO_OUTPUT);
        gpio_set_port_attribute(IO_B4, ATTRIBUTE_HIGH);
        gpio_set_port_attribute(IO_B5, ATTRIBUTE_HIGH);
        gpio_write_io(IO_B4, DATA_HIGH);
        gpio_write_io(IO_B5, DATA_HIGH);
    }
    #endif

    if (pi2s_i2c->use_gpio)
    {
        pi2s_i2c->config.i2c_gpio.slaveAddr = slaveAddr;
        pi2s_i2c->config.i2c_gpio.clock_rate = 20;
        pi2s_i2c->handle = drv_l2_sccb_open_ext(&pi2s_i2c->config.i2c_gpio);
    }
    else
    {
        pi2s_i2c->config.i2c_hw.slaveAddr = slaveAddr;
        pi2s_i2c->config.i2c_hw.clkRate = 20; // 20; // from diag_i2C, 100
        pi2s_i2c->config.i2c_hw.devNumber = devNumber;
        drv_l1_i2c_init(pi2s_i2c->config.i2c_hw.devNumber);
        pi2s_i2c->handle = &pi2s_i2c->config.i2c_hw;
    }

    return 0;
}

static int i2s_i2c_close(i2s_i2c_t *pi2s_i2c)
{
    if (pi2s_i2c->use_gpio)
        drv_l2_sccb_close(pi2s_i2c->handle);
    else
        drv_l1_i2c_uninit(pi2s_i2c->config.i2c_hw.devNumber);
    pi2s_i2c->handle = NULL;

    return 0;
}

static void i2s_tx_init_for_spu(INT32U channel, INT32U mclk_rate, INT32U sample_rate, INT8U mono)
{
    INT32U devNumber;
    INT16U slaveAddr;
    i2s_profile_t tx_profile;
    i2s_profile_t *ptx_profile;
    i2s_i2c_t i2s_i2c;

    i2s_i2c.use_gpio = 0;

    if (channel == 1)
    {
        #if K_I2S_USE_REAL_BOARD == 1

        i2s_i2c.use_gpio = 1;
        i2s_i2c.config.i2c_gpio = i2c_0_2_config;
        R_FUNPOS0 = (R_FUNPOS0 & (~(0x3 << 25))) | (2 << 25); // williamyeo, for I2S board I2C0 #2
        devNumber = I2C_0;
        slaveAddr = SLAVE_ID_1;

        #else
        devNumber = I2C_0;
        slaveAddr = SLAVE_ID_1;
        #endif
    }
    else if (channel == 2)
    {
        #if K_I2S_USE_REAL_BOARD == 1
        i2s_i2c.use_gpio = 1;
        i2s_i2c.config.i2c_gpio = i2c_1_0_config;
        devNumber = I2C_1;
        slaveAddr = SLAVE_ID_1;
        #else
        devNumber = I2C_1;
        slaveAddr = SLAVE_ID_0;
        #endif
    }
    else if (channel == 3)
    {
        #if K_I2S_USE_REAL_BOARD == 1
        i2s_i2c.use_gpio = 1;
        i2s_i2c.config.i2c_gpio = i2c_IOA15_IOA14_config;
        devNumber = I2C_1;
        slaveAddr = SLAVE_ID_1;
        #else
        devNumber = I2C_1;
        slaveAddr = SLAVE_ID_1;
        #endif
    }
    else
    {
        #if K_I2S_USE_REAL_BOARD == 1
        i2s_i2c.use_gpio = 1;
        i2s_i2c.config.i2c_gpio = i2c_2_2_config;
        devNumber = I2C_2;
        slaveAddr = SLAVE_ID_1;
        R_FUNPOS0 = (R_FUNPOS0 & (~(0x3 << 16))) | (2 << 16); // williamyeo, for I2S board I2C2 #2
                                                              // document wrongly write I2C2 pinmux control at
                                                              // R_FUNPOS1[16:17], should be R_FUNPOS0[16:17]
                                                              // but R_FUNPOS0[16:17] also used by FIR...
        #else
        devNumber = I2C_0;
        slaveAddr = SLAVE_ID_0;
        #endif
    }

    i2s_i2c_init(devNumber, slaveAddr, &i2s_i2c);

    ptx_profile = &tx_profile;
    memset(ptx_profile, 0x00, sizeof(i2s_profile_t));

    #if (K_I2S_USE_REAL_BOARD == 1)
    drv_l1_clock_enable_apll_clock();
    drv_l1_clock_set_i2s_main_mclk_freq((I2S_MAIN_FREQ_E)mclk_rate);
    ptx_profile->MCLK_clock_rate = drv_l1_clock_get_i2s_main_mclk_freq()*1000;
    #else
    ptx_profile->MCLK_clock_rate = K_MCLK_RATE;
    #endif
    DBG_PRINT("i2s main clock initial frequency=%u\r\n", drv_l1_clock_get_i2s_main_mclk_freq());

    ptx_profile->mono = mono; //K_AUDIO_STEREO;
    ptx_profile->merge = K_I2S_MERGE_MODE;
    ptx_profile->data_size = 16;
    ptx_profile->r_lsb = 0; // 0 is most tested
    ptx_profile->normal_mode_aligned = 1; //1 is most tested, and must 1 for i2s. This flag have effect only when framingmode is Normal Mode
    ptx_profile->send_mode = 0; // 0 is most tested
    ptx_profile->edge_mode = 0;  // 0 is most tested, wolfson is ok for 0 and 1
    ptx_profile->frame_polar = 1; // 1 is most tested
    ptx_profile->first_frame_LR = 0; // 0 is most tested
    ptx_profile->framingmode = 0; // 0 is I2S mode, most tested, 1 is Normal Mode, 2 is DSP mode, 3 is DSP mode
    ptx_profile->sample_rate = sample_rate;
    ptx_profile->frame_size = 32;

    ptx_profile->data_size = 16;

    if (ptx_demo != NULL)
    {
        if (ptx_demo[channel]->sample_rate != 0)
            ptx_profile->sample_rate = ptx_demo[channel]->sample_rate;

        ptx_profile->amplitude = ptx_demo[channel]->amplitude;

        if (ptx_demo[channel]->data_size != 0)
        {
            ptx_profile->data_size = ptx_demo[channel]->data_size;
            if (ptx_profile->data_size > 16)
                ptx_profile->merge = K_I2S_NONMERGE_MODE;
        }
    }

    i2s_cal_sample_rate(ptx_profile);

    wolfson_WM8988_tx_init(&i2s_i2c, ptx_profile);

    drv_l1_i2s_tx_init(channel);

    // change main mclk freq have to set fs for spu
    drv_l1_i2s_tx_set_spu_fs(channel, ptx_profile->MCLK_clock_rate/ptx_profile->sample_rate);

    drv_l1_i2s_tx_set_framing_mode(channel, ptx_profile->framingmode);
    drv_l1_i2s_tx_set_mclk_divider(channel, ptx_profile->MCLK_div);
    drv_l1_i2s_tx_set_frame_size(channel, ptx_profile->frame_size, 0);
    drv_l1_i2s_tx_set_data_bit_length(channel, ptx_profile->data_size);
    drv_l1_i2s_tx_set_merge_mode(channel, ptx_profile->merge);
    drv_l1_i2s_tx_set_mono(channel, ptx_profile->mono);
    drv_l1_i2s_tx_set_right_channel_at_lsb_byte(channel, ptx_profile->r_lsb);
    drv_l1_i2s_tx_set_normal_mode_bit_align(channel, ptx_profile->normal_mode_aligned);
    drv_l1_i2s_tx_set_bit_send_mode(channel, ptx_profile->send_mode);
    drv_l1_i2s_tx_set_edge_mode(channel, ptx_profile->edge_mode);
    drv_l1_i2s_tx_set_frame_polar(channel, ptx_profile->frame_polar);
    drv_l1_i2s_tx_set_first_frame_left_right(channel, ptx_profile->first_frame_LR);

    drv_l1_i2s_tx_clear_fifo(channel);
    drv_l1_i2s_tx_enable(channel);
}

void i2s_set_fixed_at_32_frame_size_mode(i2s_profile_t *pprofile, INT8U x2)
{
    INT32U mclk_div;
    INT8U freq = 0;

    pprofile->frame_size = 32;

    if (pprofile->sample_rate == 48000)
    {
        // 12.288 MHz
        mclk_div = (x2) ? 3 : 6;
        freq = 0;
        drv_l1_clock_set_apll_freq(APLL_FREQ_73728MHZ);
        drv_l1_clock_set_apll_divider(mclk_div - 1);
        drv_l1_clock_set_rx_use_apll_clock(0);
    }
    else if (pprofile->sample_rate == 44000)
    {
        // 11.289 MHz
        mclk_div = (x2) ? 3 : 6;
        freq = 1;
        drv_l1_clock_set_apll_freq(APLL_FREQ_67738MHZ);
        drv_l1_clock_set_apll_divider(mclk_div - 1);
        drv_l1_clock_set_rx_use_apll_clock(0);
    }
    else if (pprofile->sample_rate == 32000)
    {
        // 8.192 MHz
        mclk_div = (x2) ? 4 : 9; // should be 4.5, but could not, so use 4
        freq = 0;
        drv_l1_clock_set_apll_freq(APLL_FREQ_73728MHZ);
        drv_l1_clock_set_apll_divider(mclk_div - 1);
        drv_l1_clock_set_rx_use_apll_clock(0);
    }
    else if (pprofile->sample_rate == 24000)
    {
        // 6.144 MHz
        mclk_div = (x2) ? 6 : 12;
        freq = 0;
        drv_l1_clock_set_apll_freq(APLL_FREQ_73728MHZ);
        drv_l1_clock_set_apll_divider(mclk_div - 1);
        drv_l1_clock_set_rx_use_apll_clock(0);
    }
    else if (pprofile->sample_rate == 22000)
    {
        // 5.644 MHz
        mclk_div = (x2) ? 6 : 12;
        freq = 1;
        drv_l1_clock_set_apll_freq(APLL_FREQ_67738MHZ);
        drv_l1_clock_set_apll_divider(mclk_div - 1);
        drv_l1_clock_set_rx_use_apll_clock(0);
    }
    else if (pprofile->sample_rate == 16000)
    {
        // 4.096 Mhz ?
        mclk_div = (x2) ? 9 : 18;
        freq = 0;
        drv_l1_clock_set_apll_freq(APLL_FREQ_73728MHZ);
        drv_l1_clock_set_apll_divider(mclk_div - 1);
        drv_l1_clock_set_rx_use_apll_clock(0);
    }
    else if (pprofile->sample_rate == 12000)
    {
        // 3.072 MHz
        mclk_div = (x2) ? 12 : 24;
        freq = 0;
        drv_l1_clock_set_apll_freq(APLL_FREQ_73728MHZ);
        drv_l1_clock_set_apll_divider(mclk_div - 1);
        drv_l1_clock_set_rx_use_apll_clock(0);
    }
    else if (pprofile->sample_rate == 11000)
    {
        // 2.8224 MHz
        mclk_div = (x2) ? 12 : 24;
        freq = 1;
        drv_l1_clock_set_apll_freq(APLL_FREQ_67738MHZ);
        drv_l1_clock_set_apll_divider(mclk_div - 1);
        drv_l1_clock_set_rx_use_apll_clock(0);
    }
    else
    {
        // 2.048 MHz
        x2 = 1; // since the max divider is 32, so force to double, then get 4MHz but not 2MHz
        mclk_div = (x2) ? 18 : 36;
        freq = 0;
        drv_l1_clock_set_apll_freq(APLL_FREQ_73728MHZ);
        drv_l1_clock_set_apll_divider(mclk_div - 1);
        drv_l1_clock_set_rx_use_apll_clock(0);
    }
    pprofile->MCLK_div = (x2 == 0 ? 4: 7);

    {
        char *str;

        str = (freq == 0) ? "73.728" : "67.738";
        DBG_PRINT("samplerate=%u, mclk=%s MHz, mclkdiv=%u\r\n", pprofile->sample_rate, str, mclk_div);
    }
}

static int i2s_tx_app_channel_prepare(INT32U channel, INT32U spu_mclk_rate, INT32U spu_frame_rate, INT8U drv)
{
    i2s_app_t *pi2s_app;
    i2s_dma_t *ptx_dma;
    INT32U repat_buffer_count;
    i2s_profile_t *ptx_profile;
    INT32U devNumber;
    INT16U slaveAddr;

    pi2s_app = &i2s_apps[channel];

    memset(pi2s_app, 0, sizeof(i2s_app_t));

    pi2s_app->channel = channel;
	pi2s_app->drv = drv;

    ptx_profile = &pi2s_app->tx_profile;
    #if (K_I2S_USE_REAL_BOARD == 1)
    drv_l1_clock_enable_apll_clock();
    if (spu_mclk_rate != 0)
        drv_l1_clock_set_i2s_main_mclk_freq((I2S_MAIN_FREQ_E)spu_mclk_rate);
    ptx_profile->MCLK_clock_rate = drv_l1_clock_get_i2s_main_mclk_freq()*1000;
    #else
    ptx_profile->MCLK_clock_rate = K_MCLK_RATE;
    #endif
    DBG_PRINT("i2s main clock initial frequency=%u\r\n", drv_l1_clock_get_i2s_main_mclk_freq());

    ptx_profile->mono = K_AUDIO_STEREO;
    ptx_profile->merge = K_I2S_MERGE_MODE;
    ptx_profile->data_size = 16;
    ptx_profile->r_lsb = 0; // 0 is most tested
    ptx_profile->normal_mode_aligned = 1; //1 is most tested, and must 1 for i2s. This flag have effect only when framingmode is Normal Mode
    ptx_profile->send_mode = 0; // 0 is most tested
    ptx_profile->edge_mode = 0;  // 0 is most tested, wolfson is ok for 0 and 1
    ptx_profile->frame_polar = 1; // 1 is most tested
    ptx_profile->first_frame_LR = 0; // 0 is most tested
    ptx_profile->framingmode = 0; // 0 is I2S mode, most tested, 1 is Normal Mode, 2 is DSP mode, 3 is DSP mode
    ptx_profile->amplitude = 0;

    if (channel == 3)
    {
        ptx_profile->amplitude = -2;
        #if 0
        ptx_profile->sample_rate = 32000;
        ptx_profile->frame_size = 64; // div = 3
        ptx_profile->data_size = 32;
        #endif
        #if 1
        ptx_profile->sample_rate = 48000;
        ptx_profile->frame_size = 32; // div = 4
        ptx_profile->data_size = 16;
        #endif
    }
    else if (channel == 2)
    {
        ptx_profile->amplitude = -2;

        #if 0
        ptx_profile->sample_rate = 24000;
        ptx_profile->frame_size = 128;  // div = 2
        ptx_profile->data_size = 16;
        #endif
        #if 1
        ptx_profile->sample_rate = 48000;
        ptx_profile->frame_size = 32; // div = 4
        ptx_profile->data_size = 16;
        #endif
    }
    else if (channel == 1)
    {
        ptx_profile->amplitude = -2;
#if 1
        ptx_profile->sample_rate = 48000;
        ptx_profile->frame_size = 32; // div = 2
        ptx_profile->data_size = 16;
#endif
#if 0
        ptx_profile->sample_rate = 16000;
        ptx_profile->frame_size = 48; // div = 8
        ptx_profile->data_size = 20;
#endif

    }
    else
    {
        ptx_profile->amplitude = -2;
      #if 1
        //ptx_profile->sample_rate = 8000;
        ptx_profile->sample_rate = 48000;
        //ptx_profile->sample_rate = 88000;
        //ptx_profile->sample_rate = 22000; // 22050
        //ptx_profile->sample_rate = 11000; // 11025
        //ptx_profile->sample_rate = 88000;

        ptx_profile->frame_size = 32; // div = 3, 36.864M
        ptx_profile->data_size = 16;
      #endif

      #if 0
        ptx_profile->sample_rate = 8000;
        ptx_profile->frame_size = 192; // div = 8, 24.6M
        ptx_profile->data_size = 16;
      #endif

      #if 0
        ptx_profile->sample_rate = 8000;
        ptx_profile->frame_size = 128; // div = 6
        ptx_profile->data_size = 24;
      #endif
      #if 0
        ptx_profile->sample_rate = 48000;
        ptx_profile->frame_size = 32; // div = 4
        ptx_profile->data_size = 16;
      #endif
      #if 0
        ptx_profile->sample_rate = 32000;
        ptx_profile->frame_size = 24; // div = 8
        ptx_profile->data_size = 16;
      #endif
      #if 0
        ptx_profile->sample_rate = 16000;
        ptx_profile->frame_size = 192; // div = 2
        ptx_profile->data_size = 16;
      #endif
      #if 0
        ptx_profile->sample_rate = 96000;
        ptx_profile->frame_size = 32; // div = 2, 12.288M
        ptx_profile->data_size = 16;
      #endif

    }
    if (ptx_demo != NULL)
    {
        if (ptx_demo[0]->fixed_at_32_frame_size)
        {
            if (ptx_demo[0]->sample_rate != 0)
                ptx_profile->sample_rate = ptx_demo[0]->sample_rate;
            ptx_profile->amplitude = ptx_demo[0]->amplitude;
            if (ptx_demo[0]->data_size != 0)
            {
                ptx_profile->data_size = ptx_demo[0]->data_size;
                if (ptx_profile->data_size > 16)
                    ptx_profile->merge = K_I2S_NONMERGE_MODE;
            }
        }
        else
        {
            if (ptx_demo[channel]->sample_rate != 0)
                ptx_profile->sample_rate = ptx_demo[channel]->sample_rate;

            ptx_profile->amplitude = ptx_demo[channel]->amplitude;

            if (ptx_demo[channel]->data_size != 0)
            {
                ptx_profile->data_size = ptx_demo[channel]->data_size;
                if (ptx_profile->data_size > 16)
                    ptx_profile->merge = K_I2S_NONMERGE_MODE;
            }
        }
    }

    i2s_cal_sample_rate(ptx_profile);

    // change main mclk freq have to set fs for spu
    if (spu_frame_rate != 0)
        drv_l1_i2s_tx_set_spu_fs(channel, ptx_profile->MCLK_clock_rate/spu_frame_rate);

    if (ptx_demo != NULL /*&& channel == 0*/)
    {
        if (ptx_demo[0]->fixed_at_32_frame_size)
            i2s_set_fixed_at_32_frame_size_mode(ptx_profile, ptx_demo[0]->fixed_at_32_frame_size_mclk_2x);
    }

    pi2s_app->tx_data_source = K_TX_DATA_SOURCE;
    i2s_print_TX_source(pi2s_app);

    ptx_dma = &pi2s_app->tx_dma;
    i2s_app_tx_buf_init(channel, ptx_dma, K_TX_DMA_BULK_SIZE);
    i2s_app_tx_buf_fill(pi2s_app);
	i2s_tx_demo_buf_init(channel);

    pi2s_app->loop_max = ptx_dma->buf_len / ptx_dma->dma_bulk_size;

    repat_buffer_count = K_REPEAT_TX_COUNT;
    if (pi2s_app->tx_data_source == K_DATA_AT_BUFFER)
        pi2s_app->loop_max = pi2s_app->loop_max * repat_buffer_count;
    #if (K_HAVE_FILE_SYSTEM)
    else
    {
        struct sfn_info file_info;
        INT32U f_word = 0;

        file_info.f_size = 0;
        sfn_stat(pi2s_app->fd_in, &file_info);
        DBG_PRINT("%s file size %lu bytes.\r\n", pi2s_app->in_file_name, (INT32U)file_info.f_size);
        pi2s_app->f_size = file_info.f_size;
        if (file_info.f_size != 0)
        {
            f_word = (file_info.f_size / 4);
            if (f_word < ptx_dma->buf_len)
            {
                ptx_dma->buf_len = f_word;
                pi2s_app->loop_max = ptx_dma->buf_len / ptx_dma->dma_bulk_size;
            }
            else
            {
                pi2s_app->loop_max = (f_word / ptx_dma->dma_bulk_size);
            }
        }
        pi2s_app->loop_max = pi2s_app->loop_max * repat_buffer_count;
    }
    #endif

    DBG_PRINT("repat_buffer_count = %d\r\n", repat_buffer_count);

    pi2s_app->i2s_i2c.use_gpio = 0;

    if (channel == 1)
    {
        #if K_I2S_USE_REAL_BOARD == 1

        pi2s_app->i2s_i2c.use_gpio = 1;
        pi2s_app->i2s_i2c.config.i2c_gpio = i2c_0_2_config;
        R_FUNPOS0 = (R_FUNPOS0 & (~(0x3 << 25))) | (2 << 25); // williamyeo, for I2S board I2C0 #2
        devNumber = I2C_0;
        slaveAddr = SLAVE_ID_1;

        #else
        devNumber = I2C_0;
        slaveAddr = SLAVE_ID_1;
        #endif
    }
    else if (channel == 2)
    {
        #if K_I2S_USE_REAL_BOARD == 1
        pi2s_app->i2s_i2c.use_gpio = 1;
        pi2s_app->i2s_i2c.config.i2c_gpio = i2c_1_0_config;
        devNumber = I2C_1;
        slaveAddr = SLAVE_ID_1;
        #else
        devNumber = I2C_1;
        slaveAddr = SLAVE_ID_0;
        #endif
    }
    else if (channel == 3)
    {
        #if K_I2S_USE_REAL_BOARD == 1
        pi2s_app->i2s_i2c.use_gpio = 1;
        pi2s_app->i2s_i2c.config.i2c_gpio = i2c_IOA15_IOA14_config;
        devNumber = I2C_1;
        slaveAddr = SLAVE_ID_1;
        #else
        devNumber = I2C_1;
        slaveAddr = SLAVE_ID_1;
        #endif
    }
    else
    {
        #if K_I2S_USE_REAL_BOARD == 1
        pi2s_app->i2s_i2c.use_gpio = 1;
        pi2s_app->i2s_i2c.config.i2c_gpio = i2c_2_2_config;
        devNumber = I2C_2;
        slaveAddr = SLAVE_ID_1;
        R_FUNPOS0 = (R_FUNPOS0 & (~(0x3 << 16))) | (2 << 16); // williamyeo, for I2S board I2C2 #2
                                                              // document wrongly write I2C2 pinmux control at
                                                              // R_FUNPOS1[16:17], should be R_FUNPOS0[16:17]
                                                              // but R_FUNPOS0[16:17] also used by FIR...
        #else
        devNumber = I2C_0;
        slaveAddr = SLAVE_ID_0;
        #endif
    }

    i2s_i2c_init(devNumber, slaveAddr, &pi2s_app->i2s_i2c);

    wolfson_WM8988_tx_init(&pi2s_app->i2s_i2c, ptx_profile);

    DBG_PRINT("0 TestState=%d\r\n", pi2s_app->TestState);

    // TX in master mode, 產生 BCLK, LRCK

    {
        drv_l1_i2s_tx_init(pi2s_app->channel);

        drv_l1_i2s_tx_set_framing_mode(pi2s_app->channel, ptx_profile->framingmode);
        drv_l1_i2s_tx_set_mclk_divider(pi2s_app->channel, ptx_profile->MCLK_div);
        drv_l1_i2s_tx_set_frame_size(pi2s_app->channel, ptx_profile->frame_size, 0);
        drv_l1_i2s_tx_set_data_bit_length(pi2s_app->channel, ptx_profile->data_size);
        drv_l1_i2s_tx_set_merge_mode(pi2s_app->channel, ptx_profile->merge);
        drv_l1_i2s_tx_set_mono(pi2s_app->channel, ptx_profile->mono);
        drv_l1_i2s_tx_set_right_channel_at_lsb_byte(pi2s_app->channel, ptx_profile->r_lsb);
        drv_l1_i2s_tx_set_normal_mode_bit_align(pi2s_app->channel, ptx_profile->normal_mode_aligned);
        drv_l1_i2s_tx_set_bit_send_mode(pi2s_app->channel, ptx_profile->send_mode);
        drv_l1_i2s_tx_set_edge_mode(pi2s_app->channel, ptx_profile->edge_mode);
        drv_l1_i2s_tx_set_frame_polar(pi2s_app->channel, ptx_profile->frame_polar);
        drv_l1_i2s_tx_set_first_frame_left_right(pi2s_app->channel, ptx_profile->first_frame_LR);

        drv_l1_i2s_tx_clear_fifo(pi2s_app->channel);
    }

    //drv_l1_i2s_tx_dma_transfer(pi2s_app->channel, (INT8U*)ptx_dma->buffer, ptx_dma->dma_bulk_size, &ptx_dma->notify_flag, &ptx_dma->channel);

    return 0;
}

static char *tx_source_name[2] =
  {
      "buffer",
      "file"
  };

static INT8U i2s_completed_channel_count(INT8U *tx_list, INT8U *tx_list_copy)
{
    int j, k;

    for (j = 0, k = 0; j < 4; j++)
    {
        if ((tx_list[j] != 0xff) && (tx_list_copy[j] == 0xff))
            k++;
    } // end for

    return k;
}

static void i2s_app_wait_fifo_empty(INT32U channel)
{
    INT32U leftFifoCount;

    leftFifoCount = drv_l1_i2s_tx_get_fifo_count(channel);
    if (leftFifoCount != 0)
    {
        DBG_PRINT("1 leftFifoCount=%d\r\n", leftFifoCount);
        while (leftFifoCount != 0)
        {
            leftFifoCount = drv_l1_i2s_tx_get_fifo_count(channel);
            DBG_PRINT("2 leftFifoCount=%d\r\n", leftFifoCount);
        }
    }
    return;
}

static INT8U tx_list_copy[4];
static INT8U tx_list[4];

static INT32S i2s_tx_demo_data_producer(INT32U ch);

void i2s_tx_demo_stop_run(void)
{
    INT32U channel;

    for (channel = 0; channel < 4; channel++)
        tx_list_copy[channel] = 0xff;
}

void i2s_tx_demo_adjust_volumn(INT32U channel)
{
    if (ptx_demo != NULL)
    {
        i2s_app_t *pi2s_app;
        i2s_profile_t *ptx_profile;

        pi2s_app = &i2s_apps[channel];
        ptx_profile = &pi2s_app->tx_profile;
        ptx_profile->amplitude = ptx_demo[channel]->amplitude;
        wolfson_WM8988_tx_adjust_volumn(&pi2s_app->i2s_i2c, ptx_profile->amplitude);
    }
}

static int i2s_tx_demo_running(void)
{
    i2s_app_t *pi2s_app;
    i2s_dma_t *ptx_dma;
    INT32U channel;

    INT8U stop_running = 0;
    INT8U ch_total = 0;

    for (channel = 0, ch_total = 0; channel < 4; channel++)
    {
        tx_list_copy[channel] = tx_list[channel];
        if (tx_list_copy[channel] != 0xff)
        {
            pi2s_app = &i2s_apps[channel];
            ch_total++;

            #if (K_I2S_TX_USE_INTERRUPT == 1)
            drv_l1_i2s_tx_resgiter_isr(channel, &i2s_tx_isr, 0);
            drv_l1_i2s_tx_enable_interrupt(pi2s_app->channel);
            #endif

            pi2s_app->running = 1;
        }
    }
    channel = 3;

    while (!stop_running)
    {
        channel = (channel + 1) % 4;

        if (i2s_completed_channel_count(tx_list, tx_list_copy) == ch_total)
        {
            stop_running = 1;
            DBG_PRINT("ALL STOP RUNNING!!!\r\n");
            break;
        }

        #if (K_REPLAY_SPU == 1)
        if (ptx_demo != NULL)
        {
            if (ptx_demo[0]->channel_mode == I2S_CHANNEL_MODE_BOTH_I2S_SPU)
                i2s_tx_spu_play();
        }
        #endif

        if (tx_list_copy[channel] != 0xff)
        {
            INT32S ret;

            pi2s_app = &i2s_apps[channel];
            ptx_dma = &pi2s_app->tx_dma;

            ret = i2s_tx_demo_data_producer(channel);
            if(ret < 0)
			{
				tx_list_copy[channel] = 0xff;
            }
        }
    } // end while

    for (channel = 0; channel < 4; channel++)
    {
        if (tx_list[channel] != 0xff)
        {
            pi2s_app = &i2s_apps[channel];

            pi2s_app->running = 0;

            while ((drv_l1_i2s_tx_dbf_status_get(pi2s_app->channel) == 1) || \
                    (drv_l1_i2s_tx_dma_status_get(pi2s_app->channel) == 1) )
            {
                osDelay(1);
            }

			if (ptx_demo[0]->channel_mode == I2S_CHANNEL_MODE_BOTH_I2S_SPU)
			{
				if (SPU_Get_SingleChannel_Status(0))
					SPU_StopChannel(0);
			}

            //i2s_app_wait_fifo_empty(pi2s_app->channel);
            #if (K_I2S_TX_USE_INTERRUPT == 1)
            drv_l1_i2s_tx_disable_interrupt(pi2s_app->channel);
            #endif

            drv_l1_i2s_tx_disable(pi2s_app->channel, 1);

            if (pi2s_app->tx_data_source == K_DATA_AT_DISK)
            {
                if (pi2s_app->fd_in >= 0)
                    fs_close(pi2s_app->fd_in);
            }

            //wolfson_WM8988_power_down(&(pi2s_app->i2s_i2c));
            i2s_i2c_close(&(pi2s_app->i2s_i2c));

            drv_l1_i2s_tx_dbf_free(channel);
        }
    }

    return 0;
}

int i2s_tx_demo_play(INT32U channel_flag, INT32U spu_mclk_rate, INT32U spu_frame_rate, INT8U drv)
{
    i2s_app_t *pi2s_app;
    INT8U ch_total = 0;
    INT8U ch_start = 0xff;
    INT8U channel;

    for (channel=0; channel < 4; channel++)
    {
        tx_list[channel] = 0xff;
        if ((channel_flag & (1<<channel)) != 0)
        {
            if (ch_start == 0xff)
            {
                ch_start = channel;
                DBG_PRINT("selected first channel %d\r\n", ch_start);
            }
            tx_list[channel] = K_TX_DATA_SOURCE;
            ch_total++;
            DBG_PRINT("TX channel %d play by %s\r\n", channel, tx_source_name[tx_list[channel]]);
        }
    }

    if (ch_total == 0)
    {
        for (channel=0; channel < 4; channel++)
            tx_list[channel] = 0xff; //K_TX_DATA_SOURCE;

        tx_list[0] = K_TX_DATA_SOURCE;
        tx_list[1] = K_TX_DATA_SOURCE;
        //tx_list[2] = K_TX_DATA_SOURCE; // JTAG also use TX2 pins, need using boot mode to test, also
        //tx_list[3] = K_TX_DATA_SOURCE; // conclict with SD1#0

        for (channel=0; channel < 4; channel++)
        {
            if (tx_list[channel] != 0xff)
            {
                if (ch_start == 0xff)
                {
                    ch_start = channel;
                }
                ch_total++;
            }
        }
        if (ch_start != 0xff)
            DBG_PRINT("no channel selected, default to %d channel, start channel is %d, each TX play by %s.\r\n", ch_total, ch_start, tx_source_name[tx_list[ch_start]]);
    }

    if (ptx_demo != NULL)
    {
        if (ptx_demo[0]->channel_mode == I2S_CHANNEL_MODE_BOTH_I2S_SPU)
            SPU_SetI2SSyncSampleRate(ptx_demo[0]->spu_sample_rate);
    }

    for (channel = ch_start; channel < 4; channel++)
    {
        if (tx_list[channel] != 0xff)
        {
            if (channel == 2)
            {
                if ((R_GPIOCTRL & 0x1) != 0)
                {
                    R_GPIOCTRL &= ~0x1;
                    DBG_PRINT("TX2 MCLK and LRCK conflict with JTAG, disable JTAG. R_GPIOCTRL=%08x\r\n", R_GPIOCTRL);
                }
            }
            DBG_PRINT("\r\n\r\n======== start prepare channel %d ========\r\n\r\n", channel);
            i2s_tx_app_channel_prepare(channel, spu_mclk_rate, spu_frame_rate, drv);
            DBG_PRINT("\r\n\r\n======== end prepare channel %d ========\r\n\r\n", channel);
            DBG_PRINT("R_GPIOCTRL=%08x\r\n", R_GPIOCTRL);
        }
    }

	i2s_tx_demo_running();

    return 0;
}

static void i2s_tx_demo_dma_isr(INT32U i2s_channel, INT8U dma_channel, INT8U dma_done_status)
{
    INT32S ret;
    i2s_app_t *pi2s_app;
    i2s_dma_t *ptx_dma;
    INT32U k, fill_count;
    pcm_buf_entry_t *pcm_buf_entry;
    INT32U done_id;
    INT32U going_id;
    INT32U next_id;

    pi2s_app = &i2s_apps[i2s_channel];
    ptx_dma = &pi2s_app->tx_dma;

    if (dma_done_status != C_DMA_STATUS_DONE)
    {
        DBG_PRINT("i2s tx dma not done. i2s%u dma%u done=%u\r\n", i2s_channel, dma_channel, dma_done_status);
    }

    done_id = consumer_buf_idx_list[i2s_channel];
    if (!pcm_buf_list[i2s_channel][done_id].stopped)
    {
        #if 0 // DEBUG CODE
        if (pcm_buf_list[i2s_channel][done_id].filled == 0)
            DBG_PRINT("channel %u done_id buffer not fill yet. done_id=%u\r\n", i2s_channel, done_id);
        #endif

        pcm_buf_list[i2s_channel][consumer_buf_idx_list[i2s_channel]].filled = 0;

        consumer_buf_idx_list[i2s_channel]++;
        if(consumer_buf_idx_list[i2s_channel]>=MAX_PCM_BUF_NUM)
            consumer_buf_idx_list[i2s_channel] = 0;

        going_id = consumer_buf_idx_list[i2s_channel];

        next_id = consumer_buf_idx_list[i2s_channel] + 1;
        if(next_id>=MAX_PCM_BUF_NUM)
            next_id = 0;
        pcm_buf_entry = &pcm_buf_list[i2s_channel][next_id];

        if (pi2s_app->running && pcm_buf_list[i2s_channel][next_id].filled)
            drv_l1_i2s_tx_dbf_set(i2s_channel, (INT16S *)pcm_buf_entry->buf, pcm_buf_entry->buf_len*2);

        #if 0 // DEBUG CODE
        if (pcm_buf_list[i2s_channel][going_id].filled == 0)
            DBG_PRINT("channel %u going_id buffer not fill yet. going_id=%u\r\n", i2s_channel, going_id);

        if (pcm_buf_list[i2s_channel][next_id].filled == 0)
            DBG_PRINT("channel %u next buffer not fill yet. next_id=%u\r\n", i2s_channel, next_id);

        for (k = 0, fill_count = 0; k < MAX_PCM_BUF_NUM; k++)
        {
            if (pcm_buf_list[i2s_channel][k].filled == 1)
            {
                if (k != done_id && k != going_id && k != next_id)
                    fill_count++;
            }
        }
        if (fill_count < 3)
            DBG_PRINT("channel %u fill_count=%u\r\n", i2s_channel, fill_count);
        #endif
    }

}

static void i2s_tx0_demo_dma_isr(INT8U dma_channel, INT8U dma_done_status)
{
    i2s_tx_demo_dma_isr(0, dma_channel, dma_done_status);
}

static void i2s_tx1_demo_dma_isr(INT8U dma_channel, INT8U dma_done_status)
{
    i2s_tx_demo_dma_isr(1, dma_channel, dma_done_status);
}

static void i2s_tx2_demo_dma_isr(INT8U dma_channel, INT8U dma_done_status)
{
    i2s_tx_demo_dma_isr(2, dma_channel, dma_done_status);
}

static void i2s_tx3_demo_dma_isr(INT8U dma_channel, INT8U dma_done_status)
{
    i2s_tx_demo_dma_isr(3, dma_channel, dma_done_status);
}

void (*i2s_tx_demo_dma_user_isr[MAX_I2X_TX_NUM])(INT8U, INT8U) = {i2s_tx0_demo_dma_isr, i2s_tx1_demo_dma_isr, i2s_tx2_demo_dma_isr, i2s_tx3_demo_dma_isr};

static INT32S i2s_tx_demo_dma_start(INT32U ch, INT32U buf_A_id, INT32U buf_B_id)
{
    INT32S ret=1;
    i2s_app_t *pi2s_app;
    i2sTxReg_t *i2s;
    i2s_dma_t *ptx_dma;
    pcm_buf_entry_t *pbuf_info_A;
    pcm_buf_entry_t *pbuf_info_B;

    pi2s_app = &i2s_apps[ch];
    ptx_dma = &pi2s_app->tx_dma;
    i2s = drv_l1_i2s_tx_get_register_base_addr(ch);

    #if (K_I2S_TX_USE_INTERRUPT == 1)
    drv_l1_i2s_tx_enable_interrupt(ch);
    #endif

    DBG_PRINT("enable TX channel %d, ctr %08x\r\n", ch, i2s->TX_CTRL);

    pbuf_info_A = &pcm_buf_list[ch][buf_A_id];

    ret = drv_l1_i2s_tx_dbf_put(ch, (INT16S *)pbuf_info_A->buf, pbuf_info_A->buf_len*2, NULL, &ptx_dma->notify_flag, i2s_tx_demo_dma_user_isr[ch]);

    if(ret != 0)
        return STATUS_FAIL;

    pbuf_info_B = &pcm_buf_list[ch][buf_B_id];

    drv_l1_i2s_tx_dbf_set(ch, (INT16S *)pbuf_info_B->buf, pbuf_info_B->buf_len*2);

    drv_l1_i2s_tx_enable(ch);

    return STATUS_OK;
}

void i2s_tx_demo_buf_init(INT32U ch)
{
    INT32U i;
	i2s_app_t *pi2s_app;
    i2s_dma_t *ptx_dma;

	pi2s_app = &i2s_apps[ch];
	ptx_dma = &pi2s_app->tx_dma;

    producer_buf_idx_list[ch] = 0;
    consumer_buf_idx_list[ch] = 0;
    startup_done_flag[ch] = 0;

    for(i=0; i<MAX_PCM_BUF_NUM; i++)
    {
		pcm_buf_list[ch][i].buf_len = ptx_dma->dma_bulk_size;
        pcm_buf_list[ch][i].buf = ptx_dma->buffer + (i*pcm_buf_list[ch][i].buf_len*4L);
        pcm_buf_list[ch][i].filled = 0;
        pcm_buf_list[ch][i].stopped = 0;
    }
}

static INT32S i2s_tx_demo_data_producer(INT32U ch)
{
    INT32S ret = 0;
    i2s_app_t *pi2s_app;
    INT32U fill_loop;
    INT32U k;

    pi2s_app = &i2s_apps[ch];

    fill_loop = (startup_done_flag[ch] == 0) ? 2 : 1;

    for (k=0; k < fill_loop; k++)
    {
        if (pcm_buf_list[ch][producer_buf_idx_list[ch]].filled == 0)
        {
            i2s_profile_t *ptx_profile;
            INT16S *pcm_current;
            INT32U read_len;
            INT32U produced_size;

            ptx_profile = &pi2s_app->tx_profile;
            if (ptx_profile->data_size > 16 && ptx_profile->data_size < 32)
                read_len = pcm_buf_list[ch][producer_buf_idx_list[ch]].buf_len*3L;
            else
                read_len = pcm_buf_list[ch][producer_buf_idx_list[ch]].buf_len*4L;

			pcm_current = pcm_buf_list[ch][producer_buf_idx_list[ch]].buf;
			ret = fs_read(pi2s_app->fd_in, (INT32U)pcm_current, read_len);
			if (ret > 0)
			{
                pi2s_app->acc_read_size += ret;

				if (ret != read_len)
                {
                    if (ptx_profile->data_size > 16 && ptx_profile->data_size < 32)
                        produced_size = read_len / 3 * 4;
                    else
                        produced_size = read_len / 4 * 4;
                    if (produced_size == 0)
						ret = -1;
                }
				else
					produced_size = pcm_buf_list[ch][producer_buf_idx_list[ch]].buf_len*4L;
            }
            else if (ret == 0)
                ret = -1;

            if (ret < 0)
            {
                pcm_buf_list[ch][producer_buf_idx_list[ch]].stopped = 1;
                DBG_PRINT("channel[%u] read err. end of file\r\n", ch);
                return -1;
            }
            if (produced_size > 0)
            {
                if (ptx_profile->data_size > 16 && ptx_profile->data_size < 32)
                    covert_3_to_4_byte_sample((INT8U *)pcm_current, pcm_buf_list[ch][producer_buf_idx_list[ch]].buf_len, ptx_profile->data_size);

                pcm_buf_list[ch][producer_buf_idx_list[ch]].filled = 1;
                pcm_buf_list[ch][producer_buf_idx_list[ch]].buf_len = produced_size/4;
                producer_buf_idx_list[ch]++;
                if(producer_buf_idx_list[ch]>=MAX_PCM_BUF_NUM)
                    producer_buf_idx_list[ch] = 0;
            }
        }
    } // end for

    if (startup_done_flag[ch] == 0)
    {
        DBG_PRINT("channel %u startup done\r\n", ch);
        ret = i2s_tx_demo_dma_start(ch, consumer_buf_idx_list[ch], consumer_buf_idx_list[ch]+1);
        startup_done_flag[ch] = 1;
    }

    return 0;
}

extern osThreadId i2s_tx_demo_task_id;

static unsigned char pcm_audio_type_name[4] = {'p', 'c', 'm', '\0'};

void i2s_tx_demo_task(void const *param)
{
    int ret;
    int count = 0;
    INT32U k;
	struct f_info finfo;
	static char tx_demo_pathname[256];
	static STDiskFindControl tx_demo_stDiskFindControl;
	struct sfn_info sfninfo;
	f_ppos cur_f_ppos;

    ptx_demo = ((i2s_tx_demo_t **)param);

    for (k=0; k < 4; k++)
        DBG_PRINT("ptx_demo[%u]=%08x\r\n", k, ptx_demo[k]);

    DBG_PRINT("I2S TX Task!!!\r\n");
    DBG_PRINT("channel_flag=%08x\r\n", ptx_demo[0]->channel_flag);
    DBG_PRINT("drv=%u\r\n", ptx_demo[0]->drv);

    ret = _devicemount(ptx_demo[0]->drv);
    if (ret == 0)
    {
        if (ptx_demo[0]->play_list_mode == 1 && !ptx_demo[0]->play_list_inited)
        {
            if (ptx_demo[0]->pathname[0] != '\0')
            {
				strcpy(tx_demo_pathname, get_fs_root_path(ptx_demo[0]->drv));
				strcat(tx_demo_pathname, ptx_demo[0]->pathname);
            }
            else
            {
				strcpy(tx_demo_pathname, get_fs_root_path(ptx_demo[0]->drv));
				strcat(tx_demo_pathname, in_file_path[0]);
            }
            if (ret == 0)
            {
				cur_f_ppos = get_first_file_in_folder(&tx_demo_stDiskFindControl, tx_demo_pathname, (INT8S *)pcm_audio_type_name, &sfninfo, D_FILE, 0, NULL);
                ret = GetFileInfo(cur_f_ppos, &finfo);
				if (ret == 0)
                {
                    strcpy(ptx_demo[0]->filename, finfo.f_name);
                    ptx_demo[0]->play_list_inited = 1;
                }
                else
                    DBG_PRINT("I2S TX Task search folder Fail!!! %s\r\n\r\n", tx_demo_pathname);
            }
        }
    }
    else
        DBG_PRINT("I2S TX Task Mount Fail!!! ret=%d\r\n\r\n", ret);

    while (ret == 0)
    {
        L_PLAY_NEXT:

        if (ptx_demo[0]->total_run == 0)
            break;

        ret = i2s_tx_demo_play(ptx_demo[0]->channel_flag, ptx_demo[0]->mclk_selection, ptx_demo[0]->sample_rate, ptx_demo[0]->drv);

        if (ptx_demo[0]->play_list_mode == 1 && ptx_demo[0]->play_list_inited)
        {
			cur_f_ppos = get_next_file_in_folder(&tx_demo_stDiskFindControl, tx_demo_pathname, (INT8S *)pcm_audio_type_name, &sfninfo, D_FILE, 0, NULL);
            ret = GetFileInfo(cur_f_ppos, &finfo);
			if (ret == 0)
            {
                strcpy(ptx_demo[0]->filename, finfo.f_name);
                goto L_PLAY_NEXT;
            }
            else
            {
				cur_f_ppos = get_first_file_in_folder(&tx_demo_stDiskFindControl, tx_demo_pathname, (INT8S *)pcm_audio_type_name, &sfninfo, D_FILE, 0, NULL);
                ret = GetFileInfo(cur_f_ppos, &finfo);
                if (ret == 0)
                    strcpy(ptx_demo[0]->filename, finfo.f_name);
            }
        }
        count++;
        if (count >= ptx_demo[0]->total_run)
            break;
    }
    i2s_tx_demo_stop_run();
    _deviceunmount(ptx_demo[0]->drv);

    DBG_PRINT("I2S TX Task Quit!!! ret=%d\r\n\r\n", ret);
    i2s_tx_demo_task_id = NULL;
    osThreadTerminate(NULL);
}

int i2s_set_spu_oparation_clock(void)
{
    // system clock must use 297MHz(before divide 2) to set below divider for spu
    // because 297Mhz/(10+1) = 27MHz, spu require 27MHz clock(0xd0000010[12]=1)
    INT32U temp;

    temp = R_SYSTEM_HDMI_CTRL;
    temp &= ~0x3F;
    temp |= 0xA;
    R_SYSTEM_HDMI_CTRL = temp;

    // check clock multiple divided ?
    temp = (R_SYSTEM_HDMI_CTRL & 0x3F) + 1;
    if (((SystemCoreClock*2) / temp) != 27000000)
    {
        DBG_PRINT("Warnning:SPU divider not multiple of system clock. divider=%u system rate=%u\r\n", temp, SystemCoreClock);
        return 1;
    }
    else
        return 0;
}

extern const unsigned int drm_beautiful[];
extern unsigned int drm_beautiful_size;
extern const unsigned int drm_blankspace[];
extern unsigned int drm_blankspace_size;

static char *drm_in_file_path[1] =
{
    "audio_input\\drm\\"
};
static char *drm_in_file_name[1] =
{
    "beautiful.drm"
    //"blankspace.drm"
};

void i2s_tx_spu_get_file_path(INT8U drv, char *in_file_name)
{
    char *root_path;

    root_path = get_fs_root_path(drv);
    if (root_path != NULL)
        strcpy(in_file_name, root_path);
    else
        strcpy(in_file_name, "C:\\");

    if (ptx_demo != NULL)
    {
        if (ptx_demo[0]->spu_pathname[0] != '\0')
            strcat(in_file_name, ptx_demo[0]->spu_pathname);
        else
            strcat(in_file_name, drm_in_file_path[0]);

        if (ptx_demo[0]->spu_filename[0] != '\0')
            strcat(in_file_name, ptx_demo[0]->spu_filename);
        else
            strcat(in_file_name, drm_in_file_name[0]);
    }
    else
    {
        strcat(in_file_name, drm_in_file_path[0]);
        strcat(in_file_name, drm_in_file_name[0]);
    }
}

int i2s_tx_spu_play(void)
{
    if(SPU_Get_SingleChannel_Status(0) == 0)
    {
        INT32U spu_mclk_rate =  SPU_GetI2SSyncSampleRate();
        INT32U *pdrm_song = (INT32U *)&drm_beautiful[0];
        char in_file_name[256];
        INT16S fd_in;

        if (ptx_demo[0]->spu_drm_buf != NULL)
        {
            vPortFree(ptx_demo[0]->spu_drm_buf);
            ptx_demo[0]->spu_drm_buf = NULL;
        }

        i2s_tx_spu_get_file_path(ptx_demo[0]->drv, &in_file_name[0]);

        fd_in = fs_open(in_file_name, O_RDONLY);
        if (fd_in < 0)
        {
			INT16S ret = 0;

            DBG_PRINT("fail to open drm input file %s to play spu\r\n", in_file_name);
            //return -1;
			#if 1 // create folder
			{
				ret = chdir("C:\\audio_input");
				if (ret != 0)
				{
					ret = mkdir("C:\\audio_input");
					ret = chdir("C:\\audio_input");
				}
				if (ret == 0)
				{
					ret = chdir("C:\\audio_input\\drm");
					if (ret != 0)
					{
						ret = mkdir("C:\\audio_input\\drm");
						ret = chdir("C:\\audio_input\\drm");
					}
				}
			}
			#endif

            #if 1 // TEST CODE
			if (ret == 0)
            {
                void *buf;
                INT32U drm_size = drm_beautiful_size;
                //INT32U drm_size = drm_blankspace_size;

                buf = (void *)pvPortMalloc(drm_size);
                if (buf != NULL)
                {
                    INT16S fd_out;
                    char *drm_file_name = "C:\\audio_input\\drm\\beautiful.drm";
                    //char *drm_file_name = "C:\\audio_input\\drm\\blankspace.drm";

                    memcpy(buf, &drm_beautiful[0], drm_size);
                    //memcpy(buf, &drm_blankspace[0], drm_size);

                    fd_out = fs_open(drm_file_name, O_CREAT|O_TRUNC|O_WRONLY);
                    if (fd_out >= 0)
                    {
                        INT32S write_ret;

                        write_ret = fs_write(fd_out, (INT32U)buf, drm_size);
                        if (write_ret != drm_size)
                            DBG_PRINT("fail to save file %s\r\n", drm_file_name);
                        fs_close(fd_out);
						fd_in = fs_open(drm_file_name, O_RDONLY);
                    }
                    vPortFree(buf);
                }
            }
            #endif
        }
        if (fd_in >= 0)
        {
            void *buf;
            struct sfn_info file_info;
            INT32U f_size;

            sfn_stat(fd_in, &file_info);
            f_size = (INT32U)file_info.f_size;
            if (f_size != 0)
            {
                buf = (void *)pvPortMalloc(f_size);
                if (buf != NULL)
                {
                    INT32S read_ret;

                    read_ret = fs_read(fd_in, (INT32U)buf, (INT32U)f_size);
                    if (read_ret != f_size)
                    {
                        DBG_PRINT("fail to read file %s\r\n", in_file_name);
                        vPortFree(buf);
                    }
                    else
                        ptx_demo[0]->spu_drm_buf = buf;
                }
            }
            DBG_PRINT("success open drm input file %s to play spu, size %u\r\n", in_file_name, f_size);

            fs_close(fd_in);
        }

        SPU_Initial();


        if (    spu_mclk_rate == 44100 || \
                spu_mclk_rate == 22500 || \
                spu_mclk_rate == 11025 )
            //pdrm_song = (INT32U *)&drm_blankspace[0]; // 22K, should use this but could packed pass
                pdrm_song = (INT32U *)&drm_beautiful[0]; // 16K

        if (ptx_demo[0]->spu_drm_buf != NULL)
            pdrm_song = ptx_demo[0]->spu_drm_buf; // use drm song from file

        SPU_PlayPCM_NoEnv_FixCH(pdrm_song, 64, 127, 0);

        return 0;
    }
    else
    {
        return 1;
    }
}

int i2s_tx_spu_demo_play(INT32U channel, INT32U spu_mclk_rate, INT32U spu_frame_rate, INT8U drv)
{
    int ret;
    INT32U k = 0;

    if (ptx_demo[0]->channel_mode == I2S_CHANNEL_MODE_ONLY_I2S)
    {
        ret = i2s_tx_demo_play(1, spu_mclk_rate, spu_frame_rate/1000*1000, drv);
        return ret;
    }

    //i2s_set_spu_oparation_clock(); // remark this because spu init code will adjust clock for spu
    SPU_SetI2SSyncSampleRate(spu_frame_rate);

    if (ptx_demo[0]->channel_mode == I2S_CHANNEL_MODE_ONLY_SPU)
        i2s_tx_init_for_spu(channel, spu_mclk_rate, spu_frame_rate/1000*1000, 0);

    //SPU_SetPostWaveClock_288K(); // not working, will cause no sound
    //SPU_EnablePostWaveDownSample(); // seem no effect ?

    SPU_EnablePostWaveLPF();
    SPU_EnablePostWaveI2SSync(); // when enable this, sample arte must at least 48K, 44.1K
    i2s_tx_spu_play();

    while (1)
    {
        osDelay(1);

        if (ptx_demo[0]->spu_total_run == 0)
		{
            if (SPU_Get_SingleChannel_Status(0))
                SPU_StopChannel(0);
            break;
		}
        if ( ( (ptx_demo[0]->channel_mode == I2S_CHANNEL_MODE_ONLY_SPU) && (SPU_Get_SingleChannel_Status(0) == 0)) || \
             ( (ptx_demo[0]->channel_mode != I2S_CHANNEL_MODE_ONLY_SPU) && (SPU_Get_SingleChannel_Status(0) == 0) && (i2s_completed_channel_count(tx_list, tx_list_copy) == 1) ) \
           )
        {
            DBG_PRINT("k=%u\r\n", k);
            break;
        }
        k++;
    }
    return 0;
}

void i2s_tx_spu_demo_stop_run(void)
{
    #if (K_I2S_TX_USE_INTERRUPT == 1)
    drv_l1_i2s_tx_disable_interrupt(0);
    #endif

    drv_l1_i2s_tx_disable(0, 1);

    SPU_DisablePostWaveI2SSync();
    SPU_DisablePostWaveLPF();
    // SPU_SetPostWaveClock_281K();
    //SPU_DisablePostWaveDownSample();
}

extern osThreadId i2s_tx_spu_demo_task_id;

static unsigned char drm_audio_type_name[4] = {'d', 'r', 'm', '\0'};

void i2s_tx_spu_demo_task(void const *param)
{
    int ret;
    int count = 0;
    INT32U k;
	struct f_info finfo;
	static char tx_spu_demo_pathname[256];
	static STDiskFindControl tx_spu_demo_stDiskFindControl;
	struct sfn_info sfninfo;
	f_ppos cur_f_ppos;

    ptx_demo = ((i2s_tx_demo_t **)param);

    for (k=0; k < 4; k++)
        DBG_PRINT("ptx_demo[%u]=%08x\r\n", k, ptx_demo[k]);

    DBG_PRINT("I2S TX SPU Task!!!\r\n");
    DBG_PRINT("channel_flag=%08x\r\n", ptx_demo[0]->channel_flag);
    DBG_PRINT("drv=%u\r\n", ptx_demo[0]->drv);

    ret = _devicemount(ptx_demo[0]->drv);
    if (ret == 0)
    {
        if (ptx_demo[0]->play_list_mode == 1 && !ptx_demo[0]->spu_play_list_inited)
        {
            if (ptx_demo[0]->spu_pathname[0] != '\0')
            {
				strcpy(tx_spu_demo_pathname, get_fs_root_path(ptx_demo[0]->drv));
				strcat(tx_spu_demo_pathname, ptx_demo[0]->spu_pathname);
            }
            else
            {
                strcpy(tx_spu_demo_pathname, get_fs_root_path(ptx_demo[0]->drv));
				strcat(tx_spu_demo_pathname, drm_in_file_path[0]);
            }

            if (ret == 0)
            {
				cur_f_ppos = get_first_file_in_folder(&tx_spu_demo_stDiskFindControl, tx_spu_demo_pathname, (INT8S *)drm_audio_type_name, &sfninfo, D_FILE, 0, NULL);
                ret = GetFileInfo(cur_f_ppos, &finfo);
                if (ret == 0)
                {
                    strcpy(ptx_demo[0]->spu_filename, finfo.f_name);
                    ptx_demo[0]->spu_play_list_inited = 1;
                }
                else
                    DBG_PRINT("I2S TX SPU Task search folder Fail!!! %s\r\n\r\n", tx_spu_demo_pathname);
                ret = 0; // skip error, since no drm file type avail yet
            }
        }
    }
    else
    {
        DBG_PRINT("I2S TX SPU Task Mount Fail!!! ret=%d\r\n\r\n", ret);
        ret = 0; // purposely make no error, so that at at least play drm in code resource
    }

    while (ret == 0)
    {
        L_PLAY_NEXT:

        if (ptx_demo[0]->spu_total_run == 0)
            break;

        ret = i2s_tx_spu_demo_play(0, ptx_demo[0]->mclk_selection, ptx_demo[0]->spu_sample_rate, ptx_demo[0]->drv);

        if (ptx_demo[0]->play_list_mode == 1 && ptx_demo[0]->spu_play_list_inited)
        {
            cur_f_ppos = get_next_file_in_folder(&tx_spu_demo_stDiskFindControl, tx_spu_demo_pathname, (INT8S *)drm_audio_type_name, &sfninfo, D_FILE, 0, NULL);
            ret = GetFileInfo(cur_f_ppos, &finfo);
            if (ret == 0)
            {
                strcpy(ptx_demo[0]->spu_filename, finfo.f_name);
                goto L_PLAY_NEXT;
            }
            else
            {
				cur_f_ppos = get_first_file_in_folder(&tx_spu_demo_stDiskFindControl, tx_spu_demo_pathname, (INT8S *)drm_audio_type_name, &sfninfo, D_FILE, 0, NULL);
                ret = GetFileInfo(cur_f_ppos, &finfo);
                if (ret == 0)
                    strcpy(ptx_demo[0]->spu_filename, finfo.f_name);
            }
        }
        count++;
        if (count >= ptx_demo[0]->total_run)
            break;
    }

    if (ptx_demo[0]->spu_drm_buf != NULL)
    {
        vPortFree(ptx_demo[0]->spu_drm_buf);
        ptx_demo[0]->spu_drm_buf = NULL;
    }
    i2s_tx_spu_demo_stop_run();
    _deviceunmount(ptx_demo[0]->drv);

    DBG_PRINT("I2S TX SPU Task Quit!!! ret=%d\r\n\r\n", ret);
    i2s_tx_spu_demo_task_id = NULL;
    osThreadTerminate(NULL);
}

#endif // (_DRV_L1_I2S_TX == 1)


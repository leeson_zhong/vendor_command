#ifndef _I2S_TX_DEMO_INCLUDE
#define _I2S_TX_DEMO_INCLUDE

#include "drv_l1_i2s_tx.h"
#include "drv_l1_clock.h"

#define K_AUDIO_BUF_SIZE        		(1200*1024) // byte

typedef enum {
    I2S_CHANNEL_MODE_ONLY_I2S       = 0, // I2S bus just for I2S data
    I2S_CHANNEL_MODE_ONLY_SPU       = 1, // I2S bus just for spu data
    I2S_CHANNEL_MODE_BOTH_I2S_SPU   = 2  // I2S bus for both spu and I2S data
} I2S_CHANNEL_MODE;


typedef struct i2s_tx_demo_s
{
    INT32U channel_flag;
    INT32U mclk_selection;

    INT8U drv;
    INT8U play_list_mode;
    INT8U play_list_inited;
    INT8S amplitude;

    INT32U sample_rate;
    INT32U data_size;

    volatile INT32U total_run;
    char filename[256];
    char pathname[256];

    INT8U fixed_at_32_frame_size; // special  mode for analog IC design to develop DAC
                                  // when this mode, dont set wolfson
    INT8U fixed_at_32_frame_size_mclk_2x; // double the mclk when this is 1

    I2S_CHANNEL_MODE channel_mode;

    INT8U spu_play_list_inited;
    INT16U spu_fs;
    char spu_filename[256];
    char spu_pathname[256];
    volatile INT32U spu_total_run;
    INT32U spu_sample_rate;
    void *spu_drm_buf;
} i2s_tx_demo_t;

#endif

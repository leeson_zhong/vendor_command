#ifndef __JPEG_API_H__
#define __JPEG_API_H__

#include "drv_l1.h"

// yuv format
#define JPEG_YUV422				0
#define JPEG_GPYUV420			1

typedef struct JpegEnc_s
{
	INT32U quality_value;
	INT32U yuv_fmt;
	INT16U yuv_sample_fmt;
	INT32U input_width;
	INT32U input_height;
	INT32U input_y_addr;
	INT32U input_u_addr;
	INT32U input_v_addr;
	INT32U input_y_len;
	INT32U input_uv_len;
	INT32U output_addr;

	INT32U jpeg_status;
	INT32U encode_size;
} JpegEnc_t;

typedef struct JpegDec_s
{
	INT32U raw_data_addr;
	INT32U raw_data_size;

	INT32U scaler_mode;
	INT32U jpeg_fifo;
	INT32U scaler_fifo;
	INT32U fifo_line;

	INT32U output_format;
	INT32U output_w;
	INT32U output_h;
	INT32U output_buffer_w;
	INT32U output_buffer_h;
	INT32U output_addr;
	INT32U output_boundary_color;

	// jpeg file read back infomation
	INT32U jpeg_yuv_mode;
	INT32U jpeg_valid_w;
	INT32U jpeg_valid_h;
	INT32U jpeg_extend_w;
	INT32U jpeg_extend_h;
} JpegDec_t;

extern INT32S jpeg_file_decode_output(JpegDec_t *pJpegDec);

extern INT32U jpeg_file_encode_one_frame(JpegEnc_t *pJpegEnc);

extern INT32S jpeg_file_encode_fifo_start(JpegEnc_t *pJpegEnc);
extern INT32S jpeg_file_encode_fifo_once(JpegEnc_t *pJpegEnc);
extern INT32S jpeg_file_encode_fifo_stop(JpegEnc_t *pJpegEnc);

#endif 		// __JPEG_API_H__

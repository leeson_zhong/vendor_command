#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "jpeg_api.h"
#include "drv_l1_cache.h"
#include "drv_l1_jpeg.h"
#include "drv_l1_scaler.h"
#include "drv_l2_scaler.h"

#include "gplib_jpeg.h"
#include "gplib_jpeg_encode.h"
#include "gplib_jpeg_decode.h"
#include "portable.h"

/**************************************************************************
 *                           C O N S T A N T S                            *
 **************************************************************************/
#define	C_READ_SIZE			40*1024
#define PRE_JPEG_SCALE_EN	1

/**************************************************************************
 *                              M A C R O S                               *
 **************************************************************************/
#ifndef DISABLE
#define DISABLE     0
#endif

#ifndef ENABLE
#define ENABLE      1
#endif

#define MSG					            DBG_PRINT

#define gp_malloc(size)                     pvPortMalloc(size)
#define gp_malloc_align(size, align)        gp_malloc(size)
#define gp_iram_malloc_align(size, align)   gp_malloc(size)
#define gp_free(ptr)                        vPortFree((void *)ptr);

/**************************************************************************
 *                              E X T E R N A L                           *
 **************************************************************************/
extern void gplib_jpeg_default_quantization_table_load(INT32U quality);
extern void gplib_jpeg_default_huffman_table_load(void);

/**************************************************************************
 *                         G L O B A L    D A T A                         *
 **************************************************************************/
static INT32U gJpegYAddr, gJpegCbAddr, gJpegCrAddr;


/**************************************************************************
 *                         D E C O D E			                          *
 **************************************************************************/
static INT32S jpeg_file_seek_to_jpeg_header(INT32U raw_data_addr, INT32U cblen)
{
	INT8U *pdata;
	INT8S cnt;
	INT16U wdata;
	INT32U i;

	cnt = 0;
	//seek to data
	pdata = (INT8U*)raw_data_addr;
	wdata = *pdata++;
	wdata <<= 8;
	wdata |= *pdata++;

	for(i=0; i<cblen; i++)
	{
		if(wdata == 0xFFD8) {
			break;
		}

		wdata <<= 8;
		wdata |= *pdata++;
		cnt++;
	}

	if(i == cblen) {
		return -1;
	}

	return cnt;
}

static INT32S jpeg_file_fifo_mem_alloc(JpegDec_t *pJpegDec)
{
	INT16U cbcr_shift;
	INT32U y_size=0, cb_cr_size=0;

	if (pJpegDec->jpeg_yuv_mode == C_JPG_CTRL_YUV420) {
		MSG("C_JPG_CTRL_YUV420\r\n");
		cbcr_shift = 2;
		//factor = 15;
	}
	else if (pJpegDec->jpeg_yuv_mode == C_JPG_CTRL_YUV411) {
		MSG("C_JPG_CTRL_YUV411\r\n");
		cbcr_shift = 2;
		//factor = 15;
	}
	else if (pJpegDec->jpeg_yuv_mode == C_JPG_CTRL_YUV422) {
		MSG("C_JPG_CTRL_YUV422\r\n");
		cbcr_shift = 1;
		//factor = 20;
	}
	else if (pJpegDec->jpeg_yuv_mode == C_JPG_CTRL_YUV422V) {
		MSG("C_JPG_CTRL_YUV422V\r\n");
		cbcr_shift = 1;
		//factor = 20;
	}
	else if (pJpegDec->jpeg_yuv_mode == C_JPG_CTRL_YUV444) {
		MSG("C_JPG_CTRL_YUV444\r\n");
		cbcr_shift = 0;
		//factor = 30;
	}
	else if (pJpegDec->jpeg_yuv_mode == C_JPG_CTRL_GRAYSCALE) {
		MSG("C_JPG_CTRL_GRAYSCALE\r\n");
		cbcr_shift = 32;
		//factor = 10;
	}
	else if (pJpegDec->jpeg_yuv_mode == C_JPG_CTRL_YUV411V) {
		MSG("C_JPG_CTRL_YUV411V\r\n");
		cbcr_shift = 2;
		//factor = 15;
	}
	else if (pJpegDec->jpeg_yuv_mode == C_JPG_CTRL_YUV420H2) {
		MSG("C_JPG_CTRL_YUV420H2\r\n");
		cbcr_shift = 1;
		//factor = 15;
	}
	else if (pJpegDec->jpeg_yuv_mode == C_JPG_CTRL_YUV420V2) {
		MSG("C_JPG_CTRL_YUV420V2\r\n");
		cbcr_shift = 1;
		//factor = 15;
	}
	else if (pJpegDec->jpeg_yuv_mode == C_JPG_CTRL_YUV411H2) {
		MSG("C_JPG_CTRL_YUV411H2\r\n");
		cbcr_shift = 1;
		//factor = 15;
	}
	else if (pJpegDec->jpeg_yuv_mode == C_JPG_CTRL_YUV411V2) {
		MSG("C_JPG_CTRL_YUV411V2\r\n");
		cbcr_shift = 1;
		//factor = 15;
	}
	else {
		return STATUS_FAIL;
	}

	if (pJpegDec->fifo_line) {
		switch(pJpegDec->jpeg_yuv_mode)
		{
		case C_JPG_CTRL_YUV444:
		case C_JPG_CTRL_YUV422:
		case C_JPG_CTRL_YUV422V:
		case C_JPG_CTRL_YUV420:
		case C_JPG_CTRL_YUV411:
			y_size = pJpegDec->jpeg_extend_w * pJpegDec->fifo_line * 2 * 2; //YUYV
			cb_cr_size = 0;
			break;

		case C_JPG_CTRL_GRAYSCALE:
		case C_JPG_CTRL_YUV411V:
		case C_JPG_CTRL_YUV420H2:
		case C_JPG_CTRL_YUV420V2:
		case C_JPG_CTRL_YUV411H2:
		case C_JPG_CTRL_YUV411V2:
			y_size = pJpegDec->jpeg_extend_w * pJpegDec->fifo_line*2;
			cb_cr_size = y_size >> cbcr_shift;
		}
	}
	else {
		y_size = pJpegDec->jpeg_extend_w * pJpegDec->jpeg_extend_h;
		cb_cr_size = y_size >> cbcr_shift;
	}

	// allocate internal first
	gJpegYAddr = (INT32U) gp_iram_malloc_align(y_size + cb_cr_size + cb_cr_size, 16);
	if (gJpegYAddr == 0) {
		gJpegYAddr = (INT32U) gp_malloc_align(y_size + cb_cr_size + cb_cr_size, 16);
		if (gJpegYAddr == 0) {
			return STATUS_FAIL;
		}
	}

	if(cb_cr_size) {
		gJpegCbAddr = gJpegYAddr + y_size;
		gJpegCrAddr = gJpegCbAddr + cb_cr_size;
	}

	return STATUS_OK;
}

static void jpeg_file_fifo_mem_free(void)
{
	if(gJpegYAddr) {
		gp_free((void*)gJpegYAddr);
	}

	gJpegYAddr = 0;
	gJpegCbAddr = 0;
	gJpegCrAddr = 0;
}

static void jpeg_file_set_scaler(JpegDec_t *pJpegDec, ScalerFormat_t *pScale, ScalerPara_t *pScalePara)
{
	if (pJpegDec->output_w == 0) {
	    pJpegDec->output_w = pJpegDec->output_buffer_w;
	}

	if (pJpegDec->output_h == 0) {
	    pJpegDec->output_h = pJpegDec->output_buffer_h;
	}

	switch(pJpegDec->jpeg_yuv_mode)
	{
	case C_JPG_CTRL_YUV444:
	case C_JPG_CTRL_YUV422:
	case C_JPG_CTRL_YUV420:
	case C_JPG_CTRL_YUV411:
	case C_JPG_CTRL_YUV422V:
		pScale->input_format = C_SCALER_CTRL_IN_YUYV;
		break;

	case C_JPG_CTRL_GRAYSCALE:
		pScale->input_format = C_SCALER_CTRL_IN_Y_ONLY;
		break;

	case C_JPG_CTRL_YUV411V:
		pScale->input_format = C_SCALER_CTRL_IN_YUV411V;
		break;

	case C_JPG_CTRL_YUV420H2:
		pScale->input_format = C_SCALER_CTRL_IN_YUV422V;
		break;

	case C_JPG_CTRL_YUV420V2:
		pScale->input_format = C_SCALER_CTRL_IN_YUV422;
		break;

	case C_JPG_CTRL_YUV411H2:
		pScale->input_format = C_SCALER_CTRL_IN_YUV422;
		break;

	case C_JPG_CTRL_YUV411V2:
		pScale->input_format = C_SCALER_CTRL_IN_YUV422V;
		break;

	default:
		while(1);
	}

	pScale->input_width = pJpegDec->jpeg_valid_w;
	pScale->input_height = pJpegDec->jpeg_valid_h;
	pScale->input_visible_width = pJpegDec->jpeg_valid_w;
	pScale->input_visible_height = pJpegDec->jpeg_valid_h;
	pScale->input_x_offset = 0;
	pScale->input_y_offset = 0;

	pScale->input_y_addr = gJpegYAddr;
	pScale->input_u_addr = gJpegCbAddr;
	pScale->input_v_addr = gJpegCrAddr;

	pScale->output_format = pJpegDec->output_format;
	pScale->output_width = pJpegDec->output_w;
	pScale->output_height = pJpegDec->output_h;
	pScale->output_buf_width = pJpegDec->output_buffer_w;
	pScale->output_buf_height = pJpegDec->output_buffer_h;
	pScale->output_x_offset = 0;

	pScale->output_y_addr = pJpegDec->output_addr;
	pScale->output_u_addr = 0;
	pScale->output_v_addr = 0;

	pScale->fifo_mode = pJpegDec->scaler_fifo;
	pScale->scale_mode = pJpegDec->scaler_mode;
	pScale->digizoom_m = 10;
	pScale->digizoom_n = 10;

	pScalePara->boundary_mode = 1;
	pScalePara->boundary_color = pJpegDec->output_boundary_color;
}

void jpeg_union_mode_pre_scale_down_set(JpegDec_t *pJpegDec)
{
#if PRE_JPEG_SCALE_EN == 1
	if((pJpegDec->output_w <= (pJpegDec->jpeg_valid_w>>2)) && (pJpegDec->output_h <= (pJpegDec->jpeg_valid_h>>2))) {
		MSG("PreScale = 1/4 in Union Mode\r\n");
		drv_l1_jpeg_decode_scale_down_set(ENUM_JPG_DIV4);	// Use 1/4 scale-down mode
		pJpegDec->jpeg_valid_w >>= 2;
		pJpegDec->jpeg_valid_h >>= 2;
		pJpegDec->jpeg_extend_w >>= 2;
		pJpegDec->jpeg_extend_h >>= 2;
	}
	else if((pJpegDec->output_w <= (pJpegDec->jpeg_valid_w>>1)) && (pJpegDec->output_h <= (pJpegDec->jpeg_valid_h>>1))) {
		MSG("PreScale = 1/2 in Union Mode\r\n");
		drv_l1_jpeg_decode_scale_down_set(ENUM_JPG_DIV2);	// Use 1/2 scale-down mode
		pJpegDec->jpeg_valid_w >>= 1;
		pJpegDec->jpeg_valid_h >>= 1;
		pJpegDec->jpeg_extend_w >>= 1;
		pJpegDec->jpeg_extend_h >>= 1;
	}
	else {
		drv_l1_jpeg_decode_scale_down_set(ENUM_JPG_NO_SCALE_DOWN);
	}
#else
	drv_l1_jpeg_decode_scale_down_set(ENUM_JPG_NO_SCALE_DOWN);
#endif
}

void jpeg_pre_scale_down_set(JpegDec_t *pJpegDec)
{
#if 0// not exist in GP15B and GP22
	if((pJpegDec->output_w <= (pJpegDec->jpeg_valid_w>>1)) && (pJpegDec->output_h <= (pJpegDec->jpeg_valid_h>>1))) {
		MSG("PreScale = 1/2\r\n");
		jpeg_decode_level2_scaledown_enable();	// Use 1/2 scale-down mode
		pJpegDec->jpeg_valid_w >>= 1;
		pJpegDec->jpeg_valid_h >>= 1;
		pJpegDec->jpeg_extend_w >>= 1;
		pJpegDec->jpeg_extend_h >>= 1;
	}
	else {
		drv_l1_jpeg_level2_scaledown_mode_disable();
	}
	drv_l1_jpeg_level2_scaledown_mode_disable();
#endif
}

static INT32S jpeg_decode_without_scaler(JpegDec_t *pJpegDec)
{
	INT8U *p_vlc;
	INT32U fly_len, header_len;
	INT32S ret;

	MSG("%s\r\n", __func__);

	ret = jpeg_decode_output_set(pJpegDec->output_addr, 0, 0, C_JPG_FIFO_DISABLE);
	if(ret < 0) {
		MSG("jpeg_decode_output_set() failed\r\n");
		ret = STATUS_FAIL;
		goto __exit;
	}

	drv_l1_jpeg_decode_scale_down_set(ENUM_JPG_NO_SCALE_DOWN);
	p_vlc = jpeg_decode_image_vlc_addr_get();
	header_len = ((INT32U) p_vlc) - pJpegDec->raw_data_addr;
	fly_len = pJpegDec->raw_data_size - header_len;

	ret = drv_l1_jpeg_vlc_addr_set((INT32U) p_vlc);
	if (ret < 0) {
		MSG("Calling to jpeg_vlc_addr_set() failed\r\n");
		ret = STATUS_FAIL;
		goto __exit;
	}

	ret = drv_l1_jpeg_vlc_maximum_length_set(fly_len);
	if (ret < 0) {
		MSG("Calling to jpeg_vlc_maximum_length_set() failed\r\n");
		ret = STATUS_FAIL;
		goto __exit;
	}

	ret = drv_l1_jpeg_decompression_start(NULL);
	if (ret < 0) {
		MSG("Calling to jpeg_decompression_start() failed\r\n");
		ret = STATUS_FAIL;
		goto __exit;
	}

	while (1) {
		ret = drv_l1_jpeg_status_wait_idle(TRUE);
		if(ret & C_JPG_STATUS_DECODE_DONE) {
			break;
		}

		if(ret & (C_JPG_STATUS_STOP|C_JPG_STATUS_TIMEOUT|C_JPG_STATUS_INIT_ERR)) {
			ret = STATUS_FAIL;
			goto __exit;
		}
	}

	ret = STATUS_OK;
__exit:
	drv_l1_jpeg_stop();
	return ret;
}

static INT32S jpeg_decode_and_scaler(JpegDec_t *pJpegDec)
{
	INT8U  *p_vlc;
	INT8U  scaler_done;
	INT32S jpeg_status, scaler_status;
	INT32S fly_len, header_len, ret;
	ScalerFormat_t scale;
	ScalerPara_t para;

	MSG("%s\r\n", __func__);

	ret = jpeg_file_fifo_mem_alloc(pJpegDec);
	if(ret < 0) {
		ret = STATUS_FAIL;
		goto __exit;
	}

	ret = jpeg_decode_output_set(gJpegYAddr, gJpegCbAddr, gJpegCrAddr, pJpegDec->jpeg_fifo);
	if(ret < 0) {
		ret = STATUS_FAIL;
		goto __exit;
	}

	p_vlc = jpeg_decode_image_vlc_addr_get();
	header_len = ((INT32U) p_vlc) - pJpegDec->raw_data_addr;
	fly_len = pJpegDec->raw_data_size - header_len;

	// Set maximum VLC length to prevent JPEG from hangging too long
	ret = jpeg_decode_vlc_maximum_length_set(fly_len);
	if(ret < 0) {
		ret = STATUS_FAIL;
		goto __exit;
	}

	// Now start JPEG decoding on the fly
	if(fly_len > C_READ_SIZE) {
		ret = ((INT32U)p_vlc) + C_READ_SIZE;
		ret &= ~0x0F;
		fly_len = ret - ((INT32U)p_vlc);	//16 align
	}

	ret = jpeg_decode_on_the_fly_start(p_vlc, fly_len);
	if(ret < 0) {
		ret = STATUS_FAIL;
		goto __exit;
	}

	// Setup Scaler
	scaler_done = 0;
	scaler_status = C_SCALER_STATUS_STOP;
	memset((void *)&scale, 0, sizeof(scale));
	memset((void *)&para, 0, sizeof(para));
	jpeg_file_set_scaler(pJpegDec, &scale, &para);

  	while(1) {
  		jpeg_status = jpeg_decode_status_query(1);

		if(jpeg_status & C_JPG_STATUS_DECODE_DONE) {
			// Wait until scaler finish its job
			while(1) {
				if(scaler_status == C_SCALER_STATUS_STOP) {
					scaler_status = drv_l2_scaler_trigger(SCALER_0, ENABLE, &scale, &para);
				}
				else if(scaler_status & C_SCALER_STATUS_INPUT_EMPTY) {
					scaler_status = drv_l2_scaler_retrigger(SCALER_0, &scale);
				}
				else if(scaler_status & C_SCALER_STATUS_TIMEOUT) {
					MSG("Scale1Timeout.\r\n");
					break;
				}
				else if(scaler_status & C_SCALER_STATUS_INIT_ERR) {
					MSG("Scale1InitErr.\r\n");
					break;
				}
				else {
				 	MSG("Scale1StatusErr = 0x%x\r\n", scaler_status);
				  	break;
				}

				if(scaler_status == C_SCALER_STATUS_STOP) {
					break;
				}
			}
			break;
		}

  		if(jpeg_status & C_JPG_STATUS_INPUT_EMPTY) {
			p_vlc += fly_len;
			ret = ((INT32U)p_vlc) + C_READ_SIZE;
			ret &= ~0x0F;
			fly_len = ret - ((INT32U)p_vlc);

			// Now restart JPEG decoding on the fly
			ret = jpeg_decode_on_the_fly_start(p_vlc, fly_len);
		  	if(ret < 0) {
		  		ret = STATUS_FAIL;
		  		goto __exit;
		  	}
		}

		if(jpeg_status & C_JPG_STATUS_OUTPUT_FULL) {
			// Start scaler to handle the full output FIFO now
		  	if(scaler_done == 0) {
				// scale1 run
		  		if(scaler_status == C_SCALER_STATUS_STOP) {
					scaler_status = drv_l2_scaler_trigger(SCALER_0, ENABLE, &scale, &para);
				}
				else if(scaler_status & C_SCALER_STATUS_INPUT_EMPTY) {
					scaler_status = drv_l2_scaler_retrigger(SCALER_0, &scale);
				}
				else if(scaler_status & C_SCALER_STATUS_TIMEOUT) {
					MSG("Scale1Timeout\r\n");
					break;
				}
				else if(scaler_status & C_SCALER_STATUS_INIT_ERR) {
					MSG("Scale1InitErr\r\n");
					break;
		  		}
		  		else {
			  		MSG("Scale1StatusErr = 0x%x\r\n", scaler_status);
			  		break;
			  	}

				if(scaler_status == C_SCALER_STATUS_STOP) {
					scaler_done = 1;
				}
			}
	  		// Now restart JPEG to output to next FIFO
	  		if(jpeg_decode_output_restart())
	  		{
	  			MSG("Failed to call jpeg_decode_output_restart()\r\n");
	  			break;
	  		}
		}

		if(jpeg_status & C_JPG_STATUS_STOP) {
			MSG("JPEG is not started!\r\n");
			break;
		}

		if(jpeg_status & C_JPG_STATUS_TIMEOUT) {
			MSG("JPEG execution timeout!\r\n");
			break;
		}

		if(jpeg_status & C_JPG_STATUS_INIT_ERR) {
			MSG("JPEG init error!\r\n");
			break;
		}

		if(jpeg_status & C_JPG_STATUS_RST_VLC_DONE) {
			MSG("JPEG Restart marker number is incorrect!\r\n");
			break;
		}

		if(jpeg_status & C_JPG_STATUS_RST_MARKER_ERR) {
			MSG("JPEG Restart marker sequence error!\r\n");
			break;
		}
  	}

  	ret = STATUS_OK;
__exit:
	jpeg_decode_stop();
  	jpeg_file_fifo_mem_free();
	return ret;
}

INT32S jpeg_file_decode_output(JpegDec_t *pJpegDec)
{
	INT32S ret;

	MSG("%s\r\n", __func__);

	ret = jpeg_file_seek_to_jpeg_header(pJpegDec->raw_data_addr, pJpegDec->raw_data_size);
	if(ret < 0) {
		return -1;
	}

	pJpegDec->raw_data_size -= ret;
	pJpegDec->raw_data_addr += ret;

	jpeg_decode_init();
	ret = jpeg_decode_parse_header((INT8U *) pJpegDec->raw_data_addr, pJpegDec->raw_data_size);
	if(ret != JPEG_PARSE_OK) {
		jpeg_decode_stop();
		ret = STATUS_FAIL;
		goto __exit;
	}

	if(pJpegDec->scaler_mode == 0) {
		pJpegDec->scaler_mode = C_SCALER_BY_RATIO;
	}

	if(pJpegDec->fifo_line == 0) {
		pJpegDec->jpeg_fifo = C_JPG_FIFO_32LINE;
		pJpegDec->scaler_fifo = C_SCALER_CTRL_FIFO_32LINE;
		pJpegDec->fifo_line = 32;
	}

	pJpegDec->jpeg_yuv_mode = jpeg_decode_image_yuv_mode_get();
	pJpegDec->jpeg_valid_w = jpeg_decode_image_width_get();
	pJpegDec->jpeg_valid_h = jpeg_decode_image_height_get();
	pJpegDec->jpeg_extend_w = jpeg_decode_image_extended_width_get();
	pJpegDec->jpeg_extend_h = jpeg_decode_image_extended_height_get();

	MSG("jpeg valid = %dx%d\r\n", pJpegDec->jpeg_valid_w, pJpegDec->jpeg_valid_h);
	MSG("jpeg extend = %dx%d\r\n", pJpegDec->jpeg_extend_w, pJpegDec->jpeg_extend_h);

	if(pJpegDec->output_w == 0) {
		pJpegDec->output_w = pJpegDec->jpeg_valid_w;
	}

	if(pJpegDec->output_buffer_w == 0) {
		pJpegDec->output_buffer_w = pJpegDec->output_w;
	}

	if(pJpegDec->output_h == 0) {
		pJpegDec->output_h = pJpegDec->jpeg_valid_h;
	}

	if(pJpegDec->output_buffer_h == 0) {
		pJpegDec->output_buffer_h = pJpegDec->output_h;
	}

	switch(pJpegDec->jpeg_yuv_mode)
	{
	case C_JPG_CTRL_YUV444:
	case C_JPG_CTRL_YUV422:
	case C_JPG_CTRL_YUV420:
	case C_JPG_CTRL_YUV411:
	case C_JPG_CTRL_YUV422V:
		// use union mode
		drv_l1_jpeg_using_union_mode_enable();

		jpeg_union_mode_pre_scale_down_set(pJpegDec);

		if(pJpegDec->output_w == pJpegDec->jpeg_valid_w && pJpegDec->output_h == pJpegDec->jpeg_valid_h) {
			ret = jpeg_decode_without_scaler(pJpegDec);
		}
		else {
			ret = jpeg_decode_and_scaler(pJpegDec);
		}
		break;

	case C_JPG_CTRL_GRAYSCALE:
	case C_JPG_CTRL_YUV411V:
	case C_JPG_CTRL_YUV420H2:
	case C_JPG_CTRL_YUV420V2:
	case C_JPG_CTRL_YUV411H2:
	case C_JPG_CTRL_YUV411V2:
		// use YUV seperate mode and fifo mode
		drv_l1_jpeg_using_union_mode_disable();

		jpeg_pre_scale_down_set(pJpegDec);

		ret = jpeg_decode_and_scaler(pJpegDec);
		break;
	}

__exit:
	return ret;
}

/**************************************************************************
 *                         E N C O D E			                          *
 **************************************************************************/
INT32U jpeg_file_encode_one_frame(JpegEnc_t *pJpegEnc)
{
	INT32S ret;

	jpeg_encode_init();
	gplib_jpeg_default_quantization_table_load(pJpegEnc->quality_value);	// Load default qunatization table(quality)
	gplib_jpeg_default_huffman_table_load();								// Load default huffman table
	ret = jpeg_encode_input_size_set(pJpegEnc->input_width, pJpegEnc->input_height);
	if(ret < 0) {
		goto __exit;
	}

	jpeg_encode_input_format_set(pJpegEnc->yuv_fmt);						// C_JPEG_FORMAT_YUYV / C_JPEG_FORMAT_YUV_SEPARATE
	switch(pJpegEnc->yuv_sample_fmt)
	{
	case JPEG_YUV422:
		ret = jpeg_encode_yuv_sampling_mode_set(C_JPG_CTRL_YUV422);
		break;

	case JPEG_GPYUV420:
		ret = drv_l1_jpeg_yuv_sampling_mode_set(C_JPG_CTRL_GP420 | C_JPG_CTRL_YUV420);
		break;

	default:
		ret = STATUS_FAIL;
	}

	if(ret < 0) {
		goto __exit;
	}

	ret = jpeg_encode_output_addr_set((INT32U) pJpegEnc->output_addr);
	if(ret < 0) {
		goto __exit;
	}

	ret = jpeg_encode_once_start(pJpegEnc->input_y_addr, pJpegEnc->input_u_addr, pJpegEnc->input_v_addr);
	if(ret < 0) {
		goto __exit;
	}

	while(1) {
		pJpegEnc->jpeg_status = jpeg_encode_status_query(TRUE);
		if(pJpegEnc->jpeg_status & C_JPG_STATUS_ENCODE_DONE) {
			// Get encode length
			pJpegEnc->encode_size = jpeg_encode_vlc_cnt_get();
			//MSG("Encode Size = %d\r\n", pJpegEnc->encode_size);
			cache_invalid_range(pJpegEnc->output_addr, pJpegEnc->encode_size);
			break;
		}
		else if(pJpegEnc->jpeg_status & C_JPG_STATUS_ENCODING) {
			continue;
		}
		else {
			MSG("JPEG encode error!\r\n");
			ret = STATUS_FAIL;
			goto __exit;
		}
	}

	ret = STATUS_OK;
__exit:
	jpeg_encode_stop();
	return ret;
}

INT32S jpeg_file_encode_fifo_start(JpegEnc_t *pJpegEnc)
{
	INT32S ret;

	jpeg_encode_init();
	gplib_jpeg_default_quantization_table_load(pJpegEnc->quality_value);		// Load default qunatization table(quality=50)
	gplib_jpeg_default_huffman_table_load();			        				// Load default huffman table
	ret = jpeg_encode_input_size_set(pJpegEnc->input_width, pJpegEnc->input_height);
	if(ret < 0) {
		goto __exit;
	}

	jpeg_encode_input_format_set(pJpegEnc->yuv_fmt);							// C_JPEG_FORMAT_YUYV / C_JPEG_FORMAT_YUV_SEPARATE
	switch(pJpegEnc->yuv_sample_fmt)
	{
	case JPEG_YUV422:
		ret = jpeg_encode_yuv_sampling_mode_set(C_JPG_CTRL_YUV422);
		break;

	case JPEG_GPYUV420:
		ret = drv_l1_jpeg_yuv_sampling_mode_set(C_JPG_CTRL_GP420 | C_JPG_CTRL_YUV420);
		break;

	default:
		ret = STATUS_FAIL;
	}

	if(ret < 0) {
		goto __exit;
	}

	ret = jpeg_encode_output_addr_set((INT32U)pJpegEnc->output_addr);
	if(ret < 0) {
		goto __exit;
	}

	ret = jpeg_encode_on_the_fly_start(pJpegEnc->input_y_addr,
									   pJpegEnc->input_u_addr,
									   pJpegEnc->input_v_addr,
									   pJpegEnc->input_y_len + pJpegEnc->input_uv_len + pJpegEnc->input_uv_len);
	if(ret < 0) {
		goto __exit;
	}

	// wait jpeg block encode done
	pJpegEnc->jpeg_status = jpeg_encode_status_query(TRUE);
	if(pJpegEnc->jpeg_status & (C_JPG_STATUS_TIMEOUT | C_JPG_STATUS_INIT_ERR |C_JPG_STATUS_RST_MARKER_ERR)) {
		ret = STATUS_FAIL;
		goto __exit;
	}

__exit:
	return ret;
}

INT32S jpeg_file_encode_fifo_once(JpegEnc_t *pJpegEnc)
{
	INT32S ret = STATUS_OK;

	while(1) {
		if(pJpegEnc->jpeg_status & C_JPG_STATUS_ENCODE_DONE) {
			goto __exit;
		}
		else if(pJpegEnc->jpeg_status & C_JPG_STATUS_INPUT_EMPTY) {
			ret = jpeg_encode_on_the_fly_start(pJpegEnc->input_y_addr,
											   pJpegEnc->input_u_addr,
											   pJpegEnc->input_v_addr,
											   pJpegEnc->input_y_len + pJpegEnc->input_uv_len + pJpegEnc->input_uv_len);
			if(ret < 0) {
				goto __exit;
			}

			// wait jpeg block encode done
			pJpegEnc->jpeg_status = jpeg_encode_status_query(TRUE);
			goto __exit;
		}
		else if(pJpegEnc->jpeg_status & C_JPG_STATUS_STOP) {
			MSG("\r\njpeg encode is not started!\r\n");
			ret = STATUS_FAIL;
			goto __exit;
		}
		else if(pJpegEnc->jpeg_status & C_JPG_STATUS_TIMEOUT) {
			MSG("\r\njpeg encode execution timeout!\r\n");
			ret = STATUS_FAIL;
			goto __exit;
		}
		else if(pJpegEnc->jpeg_status & C_JPG_STATUS_INIT_ERR) {
			MSG("\r\njpeg encode init error!\r\n");
			ret = STATUS_FAIL;
			goto __exit;
		}
		else {
			MSG("\r\nJPEG status error = 0x%x!\r\n", pJpegEnc->jpeg_status);
			ret = STATUS_FAIL;
			goto __exit;
		}
	}

__exit:
	return ret;
}

INT32S jpeg_file_encode_fifo_stop(JpegEnc_t *pJpegEnc)
{
	INT32S ret = STATUS_OK;

	while(1) {
		if(pJpegEnc->jpeg_status & C_JPG_STATUS_ENCODE_DONE) {
			// get jpeg encode size
			pJpegEnc->encode_size = jpeg_encode_vlc_cnt_get();
			//MSG("Encode Size = %d\r\n", pJpegEnc->encode_size);
			cache_invalid_range(pJpegEnc->output_addr, pJpegEnc->encode_size);
			goto __exit;
		}
		else if(pJpegEnc->jpeg_status & C_JPG_STATUS_INPUT_EMPTY) {
			ret = jpeg_encode_on_the_fly_start(pJpegEnc->input_y_addr,
											   pJpegEnc->input_u_addr,
											   pJpegEnc->input_v_addr,
											   pJpegEnc->input_y_len + pJpegEnc->input_uv_len + pJpegEnc->input_uv_len);
			if(ret < 0) {
				goto __exit;
			}

			pJpegEnc->jpeg_status = jpeg_encode_status_query(TRUE);	//wait jpeg block encode done
		}
		else if(pJpegEnc->jpeg_status & C_JPG_STATUS_STOP) {
			MSG("\r\njpeg encode is not started!\r\n");
			ret = STATUS_FAIL;
			goto __exit;
		}
		else if(pJpegEnc->jpeg_status & C_JPG_STATUS_TIMEOUT) {
			MSG("\r\njpeg encode execution timeout!\r\n");
			ret = STATUS_FAIL;
			goto __exit;
		}
		else if(pJpegEnc->jpeg_status & C_JPG_STATUS_INIT_ERR) {
			MSG("\r\njpeg encode init error!\r\n");
			ret = STATUS_FAIL;
			goto __exit;
		}
		else {
			MSG("\r\nJPEG status error = 0x%x!\r\n", pJpegEnc->jpeg_status);
			ret = STATUS_FAIL;
			goto __exit;
		}
	}

__exit:
	jpeg_encode_stop();
	return ret;
}

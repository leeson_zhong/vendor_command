#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "drv_l1_jpeg.h"
#include "drv_l2_display.h"
#include "drv_l1_scaler.h"
#include "drv_l2_scaler.h"
#include "gplib_jpeg_encode.h"
#include "jpeg_api.h"
#include "jpeg_header.h"
#include "portable.h"
#include "gplib_cfg.h"
#include "gplib.h"

/**************************************************************************
 *                           C O N S T A N T S                            *
 **************************************************************************/
#define JPEG_DEC_W	800
#define JPEG_DEC_H	600

#define JPEG_ENC_W	640
#define JPEG_ENC_H	480

#define ENCODE_FIFO_LINE	32
#define ENCODE_Q_VALUE		90


/**************************************************************************
 *                              M A C R O S                               *
 **************************************************************************/
#define MSG					            DBG_PRINT
#define gp_malloc(size)                 pvPortMalloc(size)
#define gp_malloc_align(size, align)    gp_malloc(size)
#define gp_free(ptr)                    vPortFree((void *)ptr);

/**************************************************************************
 *                         G L O B A L    D A T A                         *
 **************************************************************************/
#include "test_yuv422.h"
#include "FMT_YUYV_VGA.h"

static INT16U disp_width, disp_height;
static INT32U disp_buf;
static INT32U jpeg_buf;
static INT32U jpeg_bs_buf[4];


void display_update(INT32U buf, INT32U width, INT32U height)
{
	INT32U ret;
	ScalerFormat_t ScaleDisp;
	ScalerPara_t para;

	MSG("<<%s>>\r\n\r\n", __func__);

	//scaler to display
	memset((void *)disp_buf, 0x00, disp_width * disp_height * 2);
	osDelay(100);

	memset((void *)&ScaleDisp, 0x00, sizeof(ScaleDisp));
	ScaleDisp.input_format = C_SCALER_CTRL_IN_YUYV;
	ScaleDisp.input_width = width;
	ScaleDisp.input_height = height;
	ScaleDisp.input_visible_width = 0;
	ScaleDisp.input_visible_height = 0;
	ScaleDisp.input_x_offset = 0;
	ScaleDisp.input_y_offset = 0;

	ScaleDisp.input_y_addr = buf;
	ScaleDisp.input_u_addr = 0;
	ScaleDisp.input_v_addr = 0;

#if 0//DISPLAY_OUTPUT_FORMAT == IMAGE_OUTPUT_FORMAT_RGB565
	ScaleDisp.output_format = C_SCALER_CTRL_OUT_RGB565;
#else
	ScaleDisp.output_format = C_SCALER_CTRL_OUT_YUYV;
#endif

	ScaleDisp.output_width = disp_width;
	ScaleDisp.output_height = disp_height;
	ScaleDisp.output_buf_width = disp_width;
	ScaleDisp.output_buf_height = disp_height;
	ScaleDisp.output_x_offset = 0;

	ScaleDisp.output_y_addr = (INT32U)disp_buf;
	ScaleDisp.output_u_addr = 0;
	ScaleDisp.output_v_addr = 0;

	ScaleDisp.fifo_mode = C_SCALER_CTRL_FIFO_DISABLE;
	ScaleDisp.scale_mode = C_SCALER_BY_RATIO;
	ScaleDisp.digizoom_m = 10;
	ScaleDisp.digizoom_n = 10;

	memset((void *)&para, 0x00, sizeof(para));
	para.boundary_color = 0x008080;

	ret = drv_l2_scaler_trigger(SCALER_0, 1, &ScaleDisp, &para);
	if(ret == C_SCALER_STATUS_DONE || ret == C_SCALER_STATUS_STOP) {
		drv_l2_scaler_stop(SCALER_0);
	}
	else {
		MSG("Scale1 Fail\r\n");
		while(1);
	}

	drv_l2_display_update(DISDEV_TFT, disp_buf);
	osDelay(500);
}

static INT32S fifo_line_mode_set(JpegDec_t *pDecode, INT32U fifo_line)
{
	switch(fifo_line)
	{
	case 0:
		MSG("FIFO Line 32\r\n");
		pDecode->jpeg_fifo = C_JPG_FIFO_32LINE;
		pDecode->scaler_fifo = C_SCALER_CTRL_FIFO_32LINE;
		pDecode->fifo_line = 32;
		break;

	case 1:
		MSG("FIFO Line 64\r\n");
		pDecode->jpeg_fifo = C_JPG_FIFO_64LINE;
		pDecode->scaler_fifo = C_SCALER_CTRL_FIFO_64LINE;
		pDecode->fifo_line = 64;
		break;

	case 2:
		MSG("FIFO Line 128\r\n");
		pDecode->jpeg_fifo = C_JPG_FIFO_128LINE;
		pDecode->scaler_fifo = C_SCALER_CTRL_FIFO_128LINE;
		pDecode->fifo_line = 128;
		break;

	case 3:
		MSG("FIFO Line 256\r\n");
		pDecode->jpeg_fifo = C_JPG_FIFO_256LINE;
		pDecode->scaler_fifo = C_SCALER_CTRL_FIFO_256LINE;
		pDecode->fifo_line = 256;
		break;

    case 4:
		MSG("FIFO Line 16\r\n");
		pDecode->jpeg_fifo = C_JPG_FIFO_16LINE;
		pDecode->scaler_fifo = C_SCALER_CTRL_FIFO_16LINE;
		pDecode->fifo_line = 16;
		break;

	default:
		return -1;
	}

	return 0;
}

static INT32S jpeg_decode_frame_mode_test(void)
{
	JpegDec_t decode;

	MSG("<<%s>>\r\n\r\n", __func__);

	while(1) {
		memset((void *)jpeg_buf, 0, JPEG_DEC_W * JPEG_DEC_H * 2);
		memset((void *)&decode, 0, sizeof(decode));

		decode.raw_data_addr = (INT32U)PATTERN_YUV422_START;
		decode.raw_data_size = sizeof(PATTERN_YUV422_START);

		decode.output_format = C_SCALER_CTRL_OUT_YUYV;
		decode.output_w = JPEG_DEC_W;
		decode.output_h = JPEG_DEC_H;
		decode.output_buffer_w = JPEG_DEC_W;
		decode.output_buffer_h = JPEG_DEC_H;
		decode.output_addr = jpeg_buf;
		decode.output_boundary_color = 0x008080;

		if((decode.raw_data_addr == 0) || (decode.raw_data_size == 0)) {
			break;
		}

		if(jpeg_file_decode_output(&decode) >= 0) {
			display_update(jpeg_buf, JPEG_DEC_W, JPEG_DEC_H);
		}
		else {
			return -1;
		}
		break;
	}

	return 0;
}

static INT32S jpeg_decode_fifo_mode_test(void)
{
	JpegDec_t decode;

	MSG("<<%s>>\r\n\r\n", __func__);

	while(1) {
		memset((void *)&decode, 0, sizeof(decode));

		decode.raw_data_addr = (INT32U)PATTERN_YUV422_START;
		decode.raw_data_size = sizeof(PATTERN_YUV422_START);

		decode.scaler_mode = C_SCALER_BY_RATIO;

		if(fifo_line_mode_set(&decode, 0) < 0) {
			break;
		}

		decode.output_format = C_SCALER_CTRL_OUT_YUYV;
		decode.output_w = disp_width;
		decode.output_h = disp_height;
		decode.output_buffer_w = disp_width;
		decode.output_buffer_h = disp_height;
		decode.output_addr = jpeg_buf;
		decode.output_boundary_color = 0x008080;

		if((decode.raw_data_addr == 0) || (decode.raw_data_size == 0)) {
            break;
		}

		if(jpeg_file_decode_output(&decode) >= 0) {
			display_update(jpeg_buf, disp_width, disp_height);
		}
		else {
			return -1;
		}
		break;
	}

	return 0;
}

void jpeg_decode_test(void)
{
	drv_l2_display_init();
	drv_l2_display_get_size(DISDEV_TFT, &disp_width, &disp_height);
	drv_l2_display_start(DISDEV_TFT, DISP_FMT_YUYV);

	disp_buf = (INT32U) gp_malloc_align(disp_width * disp_height * 2, 32);
	if(disp_buf == 0) {
		MSG("disp buf alloc fail\r\n");
		while(1);
	}

	jpeg_buf = (INT32U) gp_malloc_align((JPEG_DEC_W + 16) * (JPEG_DEC_H + 16)* 2, 32);
	if(jpeg_buf == 0) {
		while(1);
	}

	if(jpeg_decode_frame_mode_test() < 0) {
		MSG("jpeg_decode_frame_mode_test fail\r\n");
		while(1);
	}

	if(jpeg_decode_fifo_mode_test() < 0) {
		MSG("jpeg_decode_fifo_mode_test fail\r\n");
		while(1);
	}

	gp_free((void *)jpeg_buf);
	gp_free((void *)disp_buf);
	drv_l2_display_uninit();
}

static INT32S jpeg_encode_frame_mode_test(void)
{
	INT32S ret;
	JpegEnc_t JpegEnc;

	MSG("<< %s >>\r\n\r\n", __func__);

	// YUYV encode as JPEG
	MSG("YUYV encode as JPEG\r\n");
	jpeg_header_generate(JPEG_IMG_FORMAT_422, ENCODE_Q_VALUE, JPEG_ENC_W, JPEG_ENC_H);
	memcpy((void *)jpeg_bs_buf[0], (void *)jpeg_header_get_addr(), jpeg_header_get_size());

	memset((void *)&JpegEnc, 0, sizeof(JpegEnc));
	JpegEnc.quality_value = ENCODE_Q_VALUE;
	JpegEnc.yuv_fmt = C_JPEG_FORMAT_YUYV;
	JpegEnc.yuv_sample_fmt = JPEG_YUV422;
	JpegEnc.input_width = JPEG_ENC_W;
	JpegEnc.input_height = JPEG_ENC_H;
	JpegEnc.input_y_addr = (INT32U) PATTERN_YUYV_START;
	JpegEnc.input_y_len = sizeof(PATTERN_YUYV_START);
    JpegEnc.output_addr = jpeg_bs_buf[0] + jpeg_header_get_size();

	ret = jpeg_file_encode_one_frame(&JpegEnc);
	if(ret < 0) {
		return -1;
	}

	JpegEnc.encode_size += jpeg_header_get_size();
	MSG("Jpeg File Size = %d\r\n", JpegEnc.encode_size);

	return 0;
}

static INT32S jpeg_encode_fifo_mode_test(void)
{
	INT32S ret;
	INT32U times, cnt;
	JpegEnc_t JpegEnc;

	MSG("<< %s >>\r\n\r\n", __func__);

	// YUYV encode as JPEG
	MSG("YUYV encode as JPEG\r\n");
	jpeg_header_generate(JPEG_IMG_FORMAT_422, ENCODE_Q_VALUE, JPEG_ENC_W, JPEG_ENC_H);
	memcpy((void *)jpeg_bs_buf[2], (void *)jpeg_header_get_addr(), jpeg_header_get_size());

	memset((void *)&JpegEnc, 0, sizeof(JpegEnc));
	JpegEnc.quality_value = ENCODE_Q_VALUE;
	JpegEnc.yuv_fmt = C_JPEG_FORMAT_YUYV;
	JpegEnc.yuv_sample_fmt = JPEG_YUV422;
	JpegEnc.input_width = JPEG_ENC_W;
	JpegEnc.input_height = JPEG_ENC_H;
	JpegEnc.input_y_addr = (INT32U) PATTERN_YUYV_START;
    JpegEnc.input_y_len = JPEG_ENC_W * ENCODE_FIFO_LINE;
	JpegEnc.input_uv_len = JpegEnc.input_y_len >> 1; //YUV422
	JpegEnc.output_addr = jpeg_bs_buf[2] + jpeg_header_get_size();

	cnt = 1;
	times = JPEG_ENC_H / ENCODE_FIFO_LINE;
	MSG("Times = %d\r\n", times);

	ret = jpeg_file_encode_fifo_start(&JpegEnc);
	if(ret < 0) {
		return -1;
	}

	while(1) {
		if(JpegEnc.jpeg_status & C_JPG_STATUS_INPUT_EMPTY) {
			cnt++;
			MSG("Cnt = %d\r\n", cnt);

			JpegEnc.input_y_addr += JpegEnc.input_y_len + JpegEnc.input_uv_len + JpegEnc.input_uv_len;
			ret = jpeg_file_encode_fifo_once(&JpegEnc);
			if(ret < 0) {
				return -1;
			}
		}

		if(JpegEnc.jpeg_status & C_JPG_STATUS_ENCODE_DONE) {
			ret = jpeg_file_encode_fifo_stop(&JpegEnc);
			if(ret < 0) {
				return -1;
			}
			break;
		}
	}

	JpegEnc.encode_size += jpeg_header_get_size();
	MSG("Jpeg File Size = %d\r\n", JpegEnc.encode_size);

	return 0;
}

void jpeg_encode_test(void)
{
	INT32U i;
	INT32U jpegBsBuf;

	jpegBsBuf = (INT32U) gp_malloc_align((JPEG_ENC_W * JPEG_ENC_H + 32) * 4, 32);
	if(jpegBsBuf == 0) {
		while(1);
	}

	memset((void *)jpegBsBuf, 0x00, (JPEG_ENC_W * JPEG_ENC_H + 32) * 4);
   	for(i=0; i<4; i++) {
        jpeg_bs_buf[i] = jpegBsBuf + i*(JPEG_ENC_W * JPEG_ENC_H) + 32;
        jpeg_bs_buf[i] &= ~0x1F;
		MSG("jpeg_bs_buf[%d] = 0x%x\r\n", i, jpeg_bs_buf[i]);
	}

	if(jpeg_encode_frame_mode_test() < 0) {
		MSG("jpeg_encode_frame_mode_test fail\r\n");
		while(1);
	}

	if(jpeg_encode_fifo_mode_test() < 0) {
		MSG("jpeg_encode_frame_mode_test fail\r\n");
		while(1);
	}

	gp_free((void *)jpegBsBuf);
	for(i=0; i<4; i++) {
        jpeg_bs_buf[i] = 0;
	}
}

#define PIC_WIDTH   480 //1920
#define PIC_HEIGHT  360 //2560
#define USE_DISK	FS_SD2
extern INT16S fstat(INT16S handle, struct stat_t *statbuf);

void jpeg_decode_encode_test(void)
{
    JpegDec_t decode;
    JpegEnc_t JpegEnc;
    INT32S nRet;
    INT16S srcfile_fd;
    struct stat_t srcfile_stat;
    INT8U* srcBuffer;

    while(1)
	{
		if( _devicemount(USE_DISK))
		{
			DBG_PRINT("Mount Disk Fail[%d]\r\n", USE_DISK);
	#if	USE_DISK == FS_NAND1
			nRet = DrvNand_lowlevelformat();
			DBG_PRINT("NAND LOW LEVEL FORMAT = %d \r\n", nRet);
			nRet = _format(FS_NAND1, FAT32_Type);
			DBG_PRINT("Format NAND = %d\r\n", nRet);
			DrvNand_flush_allblk();
	#endif
			_deviceunmount(USE_DISK);
		}
		else
		{
			DBG_PRINT("Mount Disk success[%d]\r\n", USE_DISK);
			break;
		}
	}

	jpeg_buf = (INT32U) gp_malloc_align((PIC_WIDTH + 16) * (PIC_HEIGHT + 16)* 2, 32);
	if(jpeg_buf == 0) {
		while(1);
	}

    srcfile_fd = fs_open((char *)"K:\\A_rev.jpg", O_RDONLY);
    if(srcfile_fd < 0){
        DBG_PRINT("Open file %s failed\r\n");
        while(1);
    }

    nRet = fstat(srcfile_fd, &srcfile_stat);
    if(nRet != 0){
        DBG_PRINT("Get file stat failed\r\n");
        while(1);
    }

    srcBuffer = (INT8U*)gp_malloc_align(srcfile_stat.st_size, 32);
    if(srcBuffer == NULL){
        DBG_PRINT("Malloc Buffer Failed\r\n");
        while(1);
    }

    fs_read(srcfile_fd, (INT32U)srcBuffer, srcfile_stat.st_size);

	while(1) {
		memset((void *)jpeg_buf, 0, PIC_WIDTH * PIC_HEIGHT * 2);
		memset((void *)&decode, 0, sizeof(decode));

		decode.raw_data_addr = (INT32U)srcBuffer;
		decode.raw_data_size = srcfile_stat.st_size;

		decode.output_format = C_SCALER_CTRL_OUT_YUYV;
		decode.output_w = PIC_WIDTH;
		decode.output_h = PIC_HEIGHT;
		decode.output_buffer_w = PIC_WIDTH;
		decode.output_buffer_h = PIC_HEIGHT;
		decode.output_addr = jpeg_buf;
		decode.output_boundary_color = 0x008080;

		if(jpeg_file_decode_output(&decode) >= 0) {
			break;
		}
	}

	//////////////////////////////////////////////////////////////
	fs_close(srcfile_fd);
	gp_free(srcBuffer);

    srcfile_fd = fs_open("K:\\pg_1_Q70.jpg",O_CREAT|O_RDWR);
    if(srcfile_fd < 0){
        DBG_PRINT("Open file %s failed\r\n");
        while(1);
    }

    srcBuffer = (INT8U*)gp_malloc_align(PIC_WIDTH * PIC_HEIGHT * 2, 32);
	if(srcBuffer == 0) {
		while(1);
	}

	jpeg_header_generate(JPEG_IMG_FORMAT_422, ENCODE_Q_VALUE, PIC_WIDTH, PIC_HEIGHT);
	memcpy((void *)srcBuffer, (void *)jpeg_header_get_addr(), jpeg_header_get_size());

	memset((void *)&JpegEnc, 0, sizeof(JpegEnc));
	JpegEnc.quality_value = ENCODE_Q_VALUE;
	JpegEnc.yuv_fmt = C_JPEG_FORMAT_YUYV;
	JpegEnc.yuv_sample_fmt = JPEG_YUV422;
	JpegEnc.input_width = PIC_WIDTH;
	JpegEnc.input_height = PIC_HEIGHT;
	JpegEnc.input_y_addr = (INT32U)jpeg_buf;
	JpegEnc.input_y_len = PIC_WIDTH * PIC_HEIGHT * 2;
    JpegEnc.output_addr = (INT32)srcBuffer + jpeg_header_get_size();

	nRet = jpeg_file_encode_one_frame(&JpegEnc);
	if(nRet < 0) {
		while(1);
	}

	JpegEnc.encode_size += jpeg_header_get_size();
	MSG("Jpeg File Size = %d\r\n", JpegEnc.encode_size);

	fs_write(srcfile_fd, (INT32U)srcBuffer, JpegEnc.encode_size);

	fs_close(srcfile_fd);
}

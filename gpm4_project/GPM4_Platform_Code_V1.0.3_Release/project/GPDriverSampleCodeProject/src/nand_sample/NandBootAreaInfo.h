#ifndef _NANDBOOTAREAINFO_H_
#define _NANDBOOTAREAINFO_H_

#include "project.h"
#include "gplib_print_string.h"

typedef struct
{
    INT16U startPage;          //The start page address that store a bootloader
    INT16U endPage;            //The end page address that store a bootloader
    INT16U errorPageNum;       //The page num that read failed
    INT16S maxCorrectedBitNum; //The max corrected bit num occurred during read a bootloader
                               //<maxCorrectedBitNum> equal to -1, means read bootloader failed
    INT32U checkSum;           //The checksum of a bootloader
}NandBtLoaderDataInfo_st;

typedef struct
{
    INT16U headerPage;        //The page address that store a bootHeader
    INT16S correctedBitNum;   //The max corrected bit num occurred during read a bootHeader
                              //<correctedBitNum> equal to -1, means read <headerPage> failed
    INT32U checkSum;          //The CheckSum of a bootHeader
}NandBtHeaderDataInfo_st;

typedef struct
{
    INT8U isGoodBlock;     // value 1 means this block is a good block, 0 means bad block
    INT8U isContainBtInfo; //value 0 means contain nothing, value 1 means this block contain BootHeader, 2 means contain BootLoader
}NandBtAreaBlockInfo_st;

typedef struct
{
    INT16U btAreaBlkNum;       //Block Num in Boot Area
    NandBtAreaBlockInfo_st* btAreaBlksInfoArr; //The array contains the block's status of blocks in BootArea
    INT16U btHeaderCopyNum;    //The copy num of BootHeader
    NandBtHeaderDataInfo_st* btHeaderInfoArr;  //The array contains all Boot Header status in BootArea
    INT16U btLoaderCopyNum;    //The copy num of BootLoader
    NandBtLoaderDataInfo_st* btLoaderInfoArr;  //The array contains all Boot Loader status in BootArea
}NandBootAreaDataInfo_st;

//Mallocate buffers for pBtAreaInfo, then fill the pBtAreaInfo by scan read the every page in BootArea
//Return 0 means OK, others means malloc buffer error.
INT32S NandBootAreaDataInfoGet(NandBootAreaDataInfo_st* pBtAreaInfo);

//Free the mallocated buffers in pBtAreaInfo
void NandBootAreaDataInfoRelease(NandBootAreaDataInfo_st* pBtAreaInfo);

//Uart Output the content of pBtAreaInfo
void NandBootAreaDataInfoDump(NandBootAreaDataInfo_st* pBtAreaInfo);

//Return 0, Means Scan Blocks Successfully, and <*pTotalBlkNum> <*pOrgBlkNum> <*pUsrMarkBadBlkNum> have VALID value
//Return Others, Means Error Occurred, and <*pTotalBlkNum> <*pOrgBlkNum> <*pUsrMarkBadBlkNum> have INVALID value
INT32S NandScanBlockStatus(INT16U* pTotalBlkNum, INT16U* pOrgBlkNum, INT16U* pUsrMarkBadBlkNum);

#endif //_NANDBOOTAREAINFO_H_

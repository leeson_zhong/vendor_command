#include "application.h"
#include "drv_l1_uart.h"
#include "console.h"
#include "drv_l2_nand.h"
#include "gplib.h"
#include "drv_l2_usbd_msdc.h"

#if (defined(NAND1_EN) && (NAND1_EN == 1)) || \
	(defined(NAND2_EN) && (NAND2_EN == 1)) || \
	(defined(NAND3_EN) && (NAND3_EN == 1)) || \
	((defined NAND_APP_EN) && (NAND_APP_EN == 1))

#define USBD_STORAGE_NO_WPROTECT	0
#define USBD_STORAGE_WPROTECT		1

extern MDSC_LUN_STORAGE_DRV const gp_msdc_nand0;
extern MDSC_LUN_STORAGE_DRV const gp_msdc_nandapp0;
extern void state_usb_entry(void* para1);

static void nand_cmd_handler(int argc, char *argv[]);

#if (defined(NAND1_EN) && (NAND1_EN == 1)) || \
	    (defined(NAND2_EN) && (NAND2_EN == 1)) || \
	    (defined(NAND3_EN) && (NAND3_EN == 1))
static void nand_cmd_data_handler(int argc, char *argv[]);
#endif

#if ((defined NAND_APP_EN) && (NAND_APP_EN == 1))
static void nand_cmd_app_handler(int argc, char *argv[]);
#endif

static cmd_t nand_cmd_list[] =
{
	{"nandemo",  nand_cmd_handler,  NULL },
	{NULL,    NULL,   NULL}
};

void nand_demo_cmd_register(void)
{
	cmd_t *pcmd = &nand_cmd_list[0];

	while (pcmd->cmd != NULL)
	{
		cmdRegister(pcmd);
		pcmd += 1;
	}
}

void Nand_Demo(void)
{
	#if defined(GPLIB_CONSOLE_EN) && (GPLIB_CONSOLE_EN == 1)
	nand_demo_cmd_register();
	#endif
}

static void nand_cmd_help(void)
{
    DBG_PRINT(
    	"\r\nUsage:\r\n"

 	#if (defined(NAND1_EN) && (NAND1_EN == 1)) || \
	    (defined(NAND2_EN) && (NAND2_EN == 1)) || \
	    (defined(NAND3_EN) && (NAND3_EN == 1))
    	"    nandemo data help\r\n"
    #endif

    #if (defined NAND_APP_EN) && (NAND_APP_EN == 1)
        "    nandemo app help\r\n"
    #endif

    );
}

static void nand_cmd_handler(int argc, char *argv[])
{
	if (STRCMP(argv[1],"help") == 0)
    {
    	nand_cmd_help();
    }

    #if (defined(NAND1_EN) && (NAND1_EN == 1)) || \
	    (defined(NAND2_EN) && (NAND2_EN == 1)) || \
	    (defined(NAND3_EN) && (NAND3_EN == 1))
    else if (STRCMP(argv[1],"data") == 0)
    {
        nand_cmd_data_handler(argc - 1, &argv[1]);
    }
    #endif

    #if (defined NAND_APP_EN) && (NAND_APP_EN == 1)
 	else if (STRCMP(argv[1],"app") == 0)
    {
        nand_cmd_app_handler(argc - 1, &argv[1]);
    }
    #endif

    else
    {
       	nand_cmd_help();
    }
}

static int nf_atoi(char * str)
{
	int num = -1;

	if (STRCMP(str, "0") == 0)
		num = 0;
	else if (STRCMP(str, "1") == 0)
		num = 1;
	else if (STRCMP(str, "2") == 0)
		num = 2;
	else if (STRCMP(str, "3") == 0)
		num = 3;
	else if (STRCMP(str, "4") == 0)
		num = 4;
	else if (STRCMP(str, "5") == 0)
		num = 5;
	else if (STRCMP(str, "6") == 0)
		num = 6;
	else if (STRCMP(str, "7") == 0)
		num = 7;
	else if (STRCMP(str, "8") == 0)
		num = 8;
	else if (STRCMP(str, "9") == 0)
		num = 9;
	else if (STRCMP(str, "10") == 0)
		num = 10;

	return num;
}

static void nf_cmd_dump_buffer(unsigned char *addr, unsigned long size)
{
	unsigned long i;

	for(i=0; i<size; i++)
    {
		if (i%16 == 0) DBG_PRINT("\r\n[%08lx] ",i);
			DBG_PRINT("%02x ",addr[i]);
	}
	DBG_PRINT("\r\n\n");
}
#else
void Nand_Demo(void)
{
}
#endif

/* ======================= NAND DATA ======================= */
#if (defined(NAND1_EN) && (NAND1_EN == 1)) || \
	(defined(NAND2_EN) && (NAND2_EN == 1)) || \
	(defined(NAND3_EN) && (NAND3_EN == 1))

static void nf_demo_data_mount_disk(INT8U drv)
{
	if(_devicemount(drv))					// Mount device nand
	{
		DBG_PRINT("Mount Disk Fail[%d]\r\n", drv);
		#if 0
		{
                  INT16S ret;

                  ret = _format(drv, FAT32_Type);
                  _deviceunmount(drv);
                  _devicemount(drv);
                  if (drv == FS_NAND1)
                  DrvNand_flush_allblk();
		}
        #endif
	}
	else
	{
		DBG_PRINT("Mount Disk success[%d]\r\n", drv);
	}
}

static void nf_demo_data_unmount_disk(INT8U drv)
{
    INT16S ret;

    ret = _deviceunmount(drv);
    if (ret != 0)
        DBG_PRINT("Unmount Disk Fail[%d]. ret=%d\r\n", drv, ret);

    if (drv == FS_NAND1)
    DrvNand_flush_allblk();
}

static void nf_demo_data_freespace(INT8U drv)
{
	INT64U vfs_disk_free_size;
    struct _diskfree_t st_free;
    INT16S ret;

    ret = _devicemount(drv);
    if (ret == 0)
    {
        ret = _getdiskfree(drv, &st_free);
        if (ret == 0)
        {
            DBG_PRINT("\r\ndisk total cluster %u\r\n", st_free.total_clusters);
            DBG_PRINT("disk free cluster %u\r\n", st_free.avail_clusters);
            vfs_disk_free_size = (INT64U)st_free.avail_clusters * (INT64U)st_free.sectors_per_cluster * (INT64U)st_free.bytes_per_sector;
            DBG_PRINT("disk free size 0x%lx byte\r\n", (INT32U)vfs_disk_free_size);
            vfs_disk_free_size = vfsFreeSpace(drv);
            DBG_PRINT("disk free size 0x%lx byte\r\n\r\n", (INT32U)vfs_disk_free_size);
        }
        else
            DBG_PRINT("_getdiskfree err %d\r\n", ret);
        ret = _deviceunmount(drv);
    }
    else
        DBG_PRINT("_devicemount err %d\r\n", ret);
}

static void nf_demo_data_flush_all(void)
{
	DrvNand_flush_allblk();
}

extern void FlushWorkbuffer(void);

static void nf_demo_data_flush(void)
{
	FlushWorkbuffer();
}

static void nf_demo_data_msdcon(INT8U drv)
{
	_deviceunmount(drv); // to avoid data inconsistency, unmount link with file system
    #if 0 // 19b use
    storages_init(0x20);//Nand

	state_usb_entry((void *)1);

	//stop detect
	storage_polling_stop();
    #else

        #if 0 // 15b use
            drv_l2_usbd_msdc_set_lun(LUN_NF_TYPE, LUN_NUM_0, USBD_STORAGE_NO_WPROTECT, &gp_msdc_nand0);
            usbd_register_class(&usbd_msdc_ctl_blk);
            drv_l1_usbd_init();
        #else
            usbd_msdc_init();
        #endif

    #endif
}

static void nf_demo_data_msdcoff(INT8U drv)
{
    usbd_msdc_uninit();
    if (drv == FS_NAND1)
        DrvNand_flush_allblk();
}

static void nf_demo_data_format_disk(INT8U drv, INT8U fstype)
{
	INT16S ret;

    _deviceunmount(drv); // to avoid data inconsistency, unmount link with file system
	_devicemount(drv); // to do format, disk must be mounted first

	ret = _format(drv, fstype); // FAT32_Type
	if (drv == FS_NAND1)
        DrvNand_flush_allblk();

	DBG_PRINT("format disk[%d] %s\r\n", drv, (ret == 0 ? "success":"fail"));
}

INT32S nf_demo_data_lowformat_disk(INT8U drv, INT8U fstype)
{
	INT16S ret;

	_deviceunmount(drv); // to avoid data inconsistency, unmount link with file system

	ret = DrvNand_lowlevelformat();
	if (ret != 0)
		return ret;
	ret = DrvNand_initial();
	DrvNand_flush_allblk();

	if (ret != 0)
	{
		DBG_PRINT("initial fail\r\n");
		return ret;
	}

    _devicemount(drv);
	ret = _format(drv, fstype); // FAT32_Type
    _deviceunmount(drv);
 	DrvNand_flush_allblk();

	DBG_PRINT("low format disk[%d] %s\r\n", drv, ((ret == 0) ? "success":"fail"));

    // test format success or not
    ret = _devicemount(drv);
	if (ret != 0)
		DBG_PRINT("mount fail\r\n"); // dont care fail, just printed
    else
    {
        ret = _deviceunmount(drv);
        if (ret != 0)
            DBG_PRINT("unmount fail\r\n"); // dont care fail, just printed
    }
    DrvNand_flush_allblk();

	return ret;
}

static void nf_demo_data_mount_test(INT8U drv, INT32U iteration)
{
	INT16S ret;
	INT32U k = 0;
	INT16S fp;
	CHAR *file_path = "A:\\gp326033_driver_test.mcp";

	DBG_PRINT("===== mount test start =====\r\n");
	ret = _devicemount(drv);
	if(ret)					// Mount device nand
	{
		DBG_PRINT("0 Mount Disk Fail[%d], iterarion[%d], ret[%d]\r\n", drv, k, ret);
		return;
	}
	else
	{
		DBG_PRINT("0 Mount Disk success[%d], iterarion[%d]\r\n", drv, k);
	}
	#if _OPERATING_SYSTEM != _OS_NONE
            #if _OPERATING_SYSTEM == _OS_UCOS2
                OSTimeDly(20);
            #elif _OPERATING_SYSTEM == _OS_FREERTOS
                osDelay(20);
            #endif
        #endif
	for (k=1; k <= iteration; k++)
	{
		ret = _deviceunmount(drv);
		if(ret)					// Unmount device nand
		{
			DBG_PRINT("Unmount Disk Fail[%d], iterarion[%d], ret[%d]\r\n", drv, k, ret);
		 	return;
		}
		else
		{
			DBG_PRINT("Unmount Disk success[%d], iterarion[%d]\r\n", drv, k);
		}
		ret = _devicemount(drv);
		if(ret)					// Mount device nand
		{
			DBG_PRINT("Mount Disk Fail[%d], iterarion[%d], ret[%d]\r\n", drv, k, ret);
		 	return;
		}
		else
		{
			DBG_PRINT("Mount Disk success[%d], iterarion[%d]\r\n", drv, k);
		}

                #if _OPERATING_SYSTEM != _OS_NONE
                    #if _OPERATING_SYSTEM == _OS_UCOS2
                        fp = open(file_path, O_RDONLY);
                    #elif _OPERATING_SYSTEM == _OS_FREERTOS
                        fp = fs_open(file_path, O_RDONLY);
                    #endif
                #endif
		if (fp == -1)
		{
			DBG_PRINT("open file [%s] fail. fp[%d]\r\n", file_path, fp);
		}
		else
			DBG_PRINT("open file [%s] success. fp[%d]\r\n", file_path, fp);
		#if _OPERATING_SYSTEM != _OS_NONE
                    #if _OPERATING_SYSTEM == _OS_UCOS2
                        close(fp);
                    #elif _OPERATING_SYSTEM == _OS_FREERTOS
                        fs_close(fp);
                    #endif
                #endif

		DrvNand_flush_allblk();
	}
	ret = _deviceunmount(drv);
	DrvNand_flush_allblk();
        #if _OPERATING_SYSTEM != _OS_NONE
            #if _OPERATING_SYSTEM == _OS_UCOS2
                OSTimeDly(20);
            #elif _OPERATING_SYSTEM == _OS_FREERTOS
                osDelay(20);
            #endif
        #endif
	if(ret)					// Unmount device nand
	{
		DBG_PRINT("0 Unmount Disk Fail[%d], iterarion[%d], ret[%d]\r\n", drv, k, ret);
		return;
	}
	else
	{
		DBG_PRINT("0 Unmount Disk success[%d], iterarion[%d]\r\n", drv, k);
	}
	DBG_PRINT("===== mount test end =====\r\n");
}

static void nand_cmd_data_mount(int argc, char *argv[])
{
	INT8U drv = 255;

	if (argc > 2)
		drv = nf_atoi(argv[2]);

	if ((drv != FS_NAND1) && (drv != FS_SD) && (drv != FS_SD2))
	{
		DBG_PRINT("illegal driver number [%s], should be %d\r\n", argv[2], FS_NAND1);
		return;
	}

	nf_demo_data_mount_disk(drv);
}

static void nand_cmd_data_unmount(int argc, char *argv[])
{
	INT8U drv = 255;

	if (argc > 2)
		drv = nf_atoi(argv[2]);

	if ((drv != FS_NAND1) && (drv != FS_SD) && (drv != FS_SD2))
	{
		DBG_PRINT("illegal driver number [%s], should be %d\r\n", argv[2], FS_NAND1);
		return;
	}

	nf_demo_data_unmount_disk(drv);
}

static void nand_cmd_data_disksize(int argc, char *argv[])
{
	INT8U drv = 255;

	if (argc > 2)
		drv = nf_atoi(argv[2]);

	if ((drv != FS_NAND1) && (drv != FS_SD) && (drv != FS_SD2))
	{
		DBG_PRINT("illegal driver number [%s], should be %d\r\n", argv[2], FS_NAND1);
		return;
	}

	nf_demo_data_freespace(drv);
}

static void nand_cmd_data_flush_all(int argc, char *argv[])
{
	nf_demo_data_flush_all();
}

static void nand_cmd_data_flush(int argc, char *argv[])
{
	nf_demo_data_flush();
}

static void nand_cmd_data_msdcon(int argc, char *argv[])
{
	INT8U drv = 255;

	if (argc > 2)
		drv = nf_atoi(argv[2]);

	if (drv != FS_NAND1 && drv != FS_SD && drv != FS_SD2)
	{
		DBG_PRINT("illegal driver number [%s], should be %d\r\n", argv[2], FS_NAND1);
		return;
	}
	nf_demo_data_msdcon(drv);
}

static void nand_cmd_data_msdcoff(int argc, char *argv[])
{
	INT8U drv = 255;

	if (argc > 2)
		drv = nf_atoi(argv[2]);

	if (drv != FS_NAND1 && drv != FS_SD && drv != FS_SD2)
	{
		DBG_PRINT("illegal driver number [%s], should be %d\r\n", argv[2], FS_NAND1);
		return;
	}
	nf_demo_data_msdcoff(drv);
}

static void nand_cmd_data_format(int argc, char *argv[])
{
	INT8U drv = 255;
	INT8U fstype = FAT32_Type;

	if (argc > 2)
	{
		drv = nf_atoi(argv[2]);
		if (drv == FS_NAND1)
			fstype = FAT16_Type; // when NF, default FAT16 for better performance
	}

	if (argc > 3)
	{
		INT32U user_fstype;

		user_fstype = nf_atoi(argv[3]);
		if (user_fstype == FAT16_Type || \
			user_fstype == FAT32_Type || \
			user_fstype == FAT12_Type || \
			user_fstype == EXFAT_Type || \
			user_fstype == FORCE_FAT32_Type || \
			user_fstype == FORCE_FAT16_Type || \
			user_fstype == FORCE_EXFAT_Type)
			fstype = user_fstype;
	}

	if (drv != FS_NAND1 && drv != FS_SD && drv != FS_SD2)
	{
		DBG_PRINT("illegal driver number [%s], should be %d\r\n", argv[2], FS_NAND1);
		return;
	}

	nf_demo_data_format_disk(drv, fstype);
}

static void nand_cmd_data_lowformat(int argc, char *argv[])
{
	INT8U drv = 255;
	INT8U fstype = FAT16_Type;

	if (argc > 2)
		drv = nf_atoi(argv[2]);

	if (argc > 3)
	{
		INT32U user_fstype;

		user_fstype = nf_atoi(argv[3]);
		if (user_fstype == FAT16_Type || \
			user_fstype ==FAT32_Type || \
			user_fstype == FAT12_Type || \
			user_fstype == EXFAT_Type || \
			user_fstype == FORCE_FAT32_Type || \
			user_fstype == FORCE_FAT16_Type || \
			user_fstype == FORCE_EXFAT_Type)
			fstype = user_fstype;
	}

	if (drv != FS_NAND1)
	{
		DBG_PRINT("illegal driver number [%s], should be %d\r\n", argv[2], FS_NAND1);
		return;
	}

	nf_demo_data_lowformat_disk(drv, fstype);
}

static void nand_cmd_data_mounttest(int argc, char *argv[])
{
	INT8U drv = 255;

	if (argc > 2)
		drv = nf_atoi(argv[2]);
	if (drv != FS_NAND1)
	{
		DBG_PRINT("illegal driver number [%s], should be %d\r\n", argv[2], FS_NAND1);
		return;
	}

	nf_demo_data_mount_test(drv, 100);
}

extern int nand_erase_blocks(unsigned int wBlkPageNum, unsigned int page_size, unsigned int total_block);

static void nand_cmd_data_eraseblk(int argc, char *argv[])
{
    unsigned int total_block = 1024;
    unsigned int page_per_blk = 64;
    unsigned int page_size = 2048;
    int ret;

    DrvNand_UnIntial();

	if (argc > 2)
		sscanf(argv[2], "%u", &total_block);

	if (argc > 3)
		sscanf(argv[3], "%u", &page_per_blk);

    if (argc > 4)
		sscanf(argv[4], "%u", &page_size);

    DBG_PRINT("erase nand: total_block=%u page_per_blk=%u page_size=%u\r\n", total_block, page_per_blk, page_size);
	ret = nand_erase_blocks(page_per_blk, page_size, total_block);
	DBG_PRINT("erase nand: done = %u\r\n", ret);
}

extern int nand_import_header(unsigned char *input_header);
extern char *get_fs_root_path(INT8U drv);

static void nand_cmd_data_inputheader(int argc, char *argv[])
{
    static unsigned char input_header[1024];
	INT8U drv = 255;
    char *root_path;
    char full_path[256];
    INT16S fd;
    INT16S ret;

	if (argc > 2)
		drv = nf_atoi(argv[2]);
	if (drv != FS_SD2 && drv != FS_SD)
	{
		DBG_PRINT("illegal driver number [%s], should be %d, %d\r\n", argv[2], FS_SD2, FS_SD);
		return;
	}
    ret = _devicemount(drv);
    if (ret == 0)
    {
        root_path = get_fs_root_path(drv);
        strcpy(full_path, root_path);
        strcat(full_path, "GPM47XXXA_NAND_BootHeader.bin");

        fd = fs_open(full_path, O_RDONLY);
        if (fd >= 0)
        {
            INT32S read_ret;

            read_ret = fs_read(fd, (INT32U)input_header, 1024);
            fs_close(fd);
            if (read_ret == 1024)
            {
                nand_import_header(input_header);
                DBG_PRINT("success import header file %s\r\n", full_path);
            }
            else
                DBG_PRINT("fail to read file %s. %d\r\n", full_path, read_ret);
        }
        else
            DBG_PRINT("fail to open file %s\r\n", full_path);
    }
    else
        DBG_PRINT("fail to mount drive %u\r\n", drv);

    ret = _deviceunmount(drv);
}

static void nand_cmd_data_help(void)
{
    DBG_PRINT(
    	"\r\nUsage: nandemo data help\r\n"
    	"\r\n       nandemo data mount <drive>\r\n"
    	"\r\n       nandemo data disksize <drive>\r\n"
        "\r\n       nandemo data flushall\r\n"
        "\r\n       nandemo data flush\r\n"
        "\r\n       nandemo data msdcon <drive>\r\n"
        "\r\n       nandemo data format <drive>\r\n"
        "\r\n       nandemo data lowformat <drive>\r\n"
        "\r\n       nandemo data mounttest <drive>\r\n"
        "\r\n       nandemo data eraseblk <total_block> <page_per_blk(32,64,128,256)> <page_size(512,2048,4096,8192)>\r\n"
        "\r\n       nandemo data inputheader <drive>\r\n"
    	);
}

void nand_file_test(INT8U drv)
{
    INT16S ret;

    ret = _devicemount(drv);
    if(ret)
    {
        DBG_PRINT("Mount Disk Fail[%d]\r\n", drv);
	#if 0
	{
            ret = _format(drv, FAT32_Type);
            if (ret)
              DBG_PRINT("Format Disk Fail[%d]\r\n", drv);
            ret = _deviceunmount(drv);
            if (ret)
              DBG_PRINT("UnMount Disk Fail[%d]\r\n", drv);
            ret = _devicemount(drv);
        }
        #endif
    }
    if (ret)
        DBG_PRINT("2 Mount Disk Fail[%d]\r\n", drv);
    else
    {
        INT16S fd, fd_out;
        struct sfn_info file_info;
        char *in_file_name = "K:\\image\\IMG0001.jpg";
	char *out_file_name = "A:\\output\\IMG0001.jpg";

        DBG_PRINT("Mount Disk success[%d]\r\n", drv);

        ret = chdir("A:\\output");
        if (ret == 0)
        {
           ret = _deleteall("A:\\output\\");
           if (ret != 0)
              DBG_PRINT("fail to delete all. %s\r\n", "A:\\output\\*");

           ret = rmdir("K:\\output");
           if (ret != 0)
              DBG_PRINT("fail to rmdir because not empty. %s\r\n", "A:\\output");

           ret = mkdir("A:\\output");
           if (ret != 0)
              DBG_PRINT("fail to mkdir because dir exist. %s\r\n", "A:\\output");

           ret = _deviceunmount(FS_NAND1);
           if (ret != 0)
              DBG_PRINT("fail to unmount disk A:\r\n");

           ret = _devicemount(FS_NAND1);
           if (ret != 0)
              DBG_PRINT("fail to mount disk A:\r\n");
        }
        else
        {
            ret = mkdir("A:\\output");
            if (ret != 0)
                DBG_PRINT("fail to mkdir. %s\r\n", "A:\\output");
        }

        fd = fs_open(in_file_name, O_RDONLY);
        if (fd < 0)
            DBG_PRINT("fail to open input file %s\r\n", in_file_name);

        //ret = fs_lfndelete(out_file_name);
        //if (ret != 0)
        //  DBG_PRINT("fail to delete output file %s because not exist\r\n", out_file_name);
        fd_out = fs_open(out_file_name, O_CREAT|O_TRUNC|O_WRONLY);
        if (fd_out < 0)
            DBG_PRINT("fail to create output file %s\r\n", out_file_name);

        if (fd >= 0 && fd_out >= 0)
        {
            // duplicate file

            void *buf;
            INT32U buf_size;

            sfn_stat(fd, &file_info);
            DBG_PRINT("%s file size %lu bytes.\r\n", in_file_name, (INT32U)file_info.f_size);

            buf_size = 256*1024;
            buf = (void *)pvPortMalloc(buf_size);
            if (buf != NULL)
            {
                INT32S read_ret;
                INT32U read_size;
                INT64U remain_f_size = file_info.f_size;
                INT32S write_ret;
                INT64U offset = 0;

                DBG_PRINT("buf=%08x\r\n", (INT32U)buf);
                if (((INT32U)buf & (0x3)) != 0)
                  DBG_PRINT("WARNING buf not aligned to 4=%08x\r\n", (INT32U)buf);

                while (remain_f_size > 0)
                {
                  read_size = remain_f_size < buf_size ? remain_f_size : buf_size;
                  read_ret = fs_read(fd, (INT32U)buf, read_size);
                  if (read_ret > 0)
                  {
                      if (read_ret != read_size)
                      {
                          DBG_PRINT("read_ret != read_size, %d, %d. offset=%d\r\n", read_ret, read_size, (INT32U)offset);
                          break;
                      }
                      else
                      {
                        write_ret = fs_write(fd_out, (INT32U)buf, read_ret);
                        if (write_ret != read_ret)
                        {
                          DBG_PRINT("write_ret != read_ret, %d, %d. offset=%d\r\n", write_ret, read_ret, (INT32U)offset);
                          break;
                        }
                        else
                        {
                          DBG_PRINT("done copy %d %% of total %d bytes.\r\n", (INT32U)(((offset + write_ret) * 100)/file_info.f_size), (INT32U)file_info.f_size);
                        }
                        offset += write_ret;
                      }
                  }
                  else
                    DBG_PRINT("read %d fail. ret=%d offset=%d\r\n", read_size, read_ret, (INT32U)offset);
                  remain_f_size -= read_size;
                }
                vPortFree(buf);
            }
        }

        if (fd >= 0)
            fs_close(fd);

        if (fd_out >= 0)
            fs_close(fd_out);

        ret = _deviceunmount(FS_NAND1);
        if (ret != 0)
            DBG_PRINT("2 fail to unmount disk A:\r\n");

        ret = _devicemount(FS_NAND1);
        if (ret != 0)
            DBG_PRINT("2 fail to mount disk A:\r\n");

        DrvNand_flush_allblk();

        ret = _copy("A:\\output\\IMG0001.jpg", "K:\\output\\IMG0001.jpg");
        if (ret == 0)
            DBG_PRINT("success copy nand file to sd card !!!");
        else
            DBG_PRINT("fail copy nand file to sd card !!!");
    }
    ret = _deviceunmount(drv);
    DBG_PRINT("UNMOUNTED SD COMPLETED. ATTETION: MUST DO BEFORE PLUGOUT SD CARD. ret=%d !!!\r\n", ret);
}

static void nand_cmd_data_handler(int argc, char *argv[])
{
	if (STRCMP(argv[1],"help") == 0)
    {
    	nand_cmd_data_help();
    }
    else if (STRCMP(argv[1],"mount") == 0)
    {
    	nand_cmd_data_mount(argc, argv);
        //nand_file_test(FS_SD2);
    }
    else if (STRCMP(argv[1],"unmount") == 0)
    {
    	nand_cmd_data_unmount(argc, argv);
    }
	else if (STRCMP(argv[1],"disksize") == 0)
    {
    	nand_cmd_data_disksize(argc, argv);
    }
   	else if (STRCMP(argv[1],"flushall") == 0)
    {
    	nand_cmd_data_flush_all(argc, argv);
    }
	else if (STRCMP(argv[1],"flush") == 0)
    {
    	nand_cmd_data_flush(argc, argv);
    }
   	else if (STRCMP(argv[1],"msdcon") == 0)
    {
    	nand_cmd_data_msdcon(argc, argv);
    }
    else if (STRCMP(argv[1],"msdcoff") == 0)
    {
    	nand_cmd_data_msdcoff(argc, argv);
    }
	else if (STRCMP(argv[1],"format") == 0)
    {
    	nand_cmd_data_format(argc, argv);
    }
	else if (STRCMP(argv[1],"lowformat") == 0)
    {
    	nand_cmd_data_lowformat(argc, argv);
    }
	else if (STRCMP(argv[1],"mounttest") == 0)
    {
    	nand_cmd_data_mounttest(argc, argv);
    }
    #if 1 // These command for development purpose only
    else if (STRCMP(argv[1],"eraseblk") == 0)
    {
    	nand_cmd_data_eraseblk(argc, argv);
    }
    else if (STRCMP(argv[1],"inputheader") == 0)
    {
    	nand_cmd_data_inputheader(argc, argv);
    }
    #endif
    else
    {
    	nand_cmd_data_help();
    }
}
#endif

/* ======================= NAND APP ======================= */
#if (defined NAND_APP_EN) && (NAND_APP_EN == 1)

static void nand_cmd_app_init(int argc, char *argv[])
{
	INT32S ret;

	ret = NandBootInit();
	if (ret == 0)
		DBG_PRINT("app init ok!\r\n");
	else
		DBG_PRINT("app init fail! ret=%d\r\n", ret);
}

static void showAppPartInfo(INT8U partNum, NF_APP_PART_INFO *partInfo)
{
	DBG_PRINT("App Partition %d Header's Info:\r\n", partNum);
	DBG_PRINT("->Size 0x%x Sectors\r\n", partInfo->part_size);
	DBG_PRINT("->CheckSum 0x%x \r\n", partInfo->checkSum);
	DBG_PRINT("->StartSector 0x%x \r\n", partInfo->startSector);
	DBG_PRINT("->ImageSize 0x%x Sectors\r\n", partInfo->imageSize);
	DBG_PRINT("->DestAddress 0x%x Sectors\r\n", partInfo->destAddress);
	DBG_PRINT("->Type 0x%x Sectors\r\n", partInfo->type);
}

static void nand_cmd_app_partinfo(int argc, char *argv[])
{
	INT32S ret;
	INT16U partTotal, k;
	INT32U partTotalSector;
	NF_APP_PART_INFO partInfo;

	ret = NandAppInitParts(&partTotal, &partTotalSector);
	if (ret == 0)
		DBG_PRINT("app parts init ok! part#=%d, size=%d sectors\r\n", partTotal, partTotalSector);
	else
	{
		DBG_PRINT("app parts init fail! ret=%d, part#=%d, size=%d sectors\r\n", ret, partTotal, partTotalSector);
		return;
	}
	for (k = 0; k < partTotal; k++)
	{
		ret = NandAppGetPartInfo(k, &partInfo);
		if (ret == 0)
		{
			showAppPartInfo(k, &partInfo);
		}
		else
			DBG_PRINT("app part info fail. k=%d ret=%d\r\n", k, ret);
	}
}

static void nand_cmd_app_findpart(int argc, char *argv[])
{
	INT32S ret;
	INT16U index = 0;
	INT8U type = 0;
	NF_APP_PART_INFO partInfo;

	if (argc > 2)
	{
		index = nf_atoi(argv[2]);
		if (index > 15)
		{
			DBG_PRINT("part index must between 0~15. %s\r\n", argv[2]);
			return;
		}
	}
	if (argc > 3)
	{
		type = nf_atoi(argv[3]);
		if (type > APP_KIND_MAX)
		{
			DBG_PRINT("part index must between %d~%d. %s\r\n", APP_KIND_MIN, APP_KIND_MAX, argv[3]);
			return;
		}
	}

	DBG_PRINT("part to be find: index=%d type=%d\r\n", index, type);

	ret = NandAppFindPart(index, type, &partInfo);
	if (ret == 0)
		showAppPartInfo(index, &partInfo);
	else
		DBG_PRINT("part not found. index=%d type=%d ret=%d\r\n", index, type, ret);
}

static void nand_cmd_app_readpart(int argc, char *argv[])
{
	INT32S ret;
	INT16U index = 0;
	INT8U type = 0;
	NF_APP_PART_INFO partInfo;

	if (argc > 2)
	{
		index = nf_atoi(argv[2]);
		if (index > 15)
		{
			DBG_PRINT("part index must between 0~15. %s\r\n", argv[2]);
			return;
		}
	}
	if (argc > 3)
	{
		type = nf_atoi(argv[3]);
		if (type > APP_KIND_MAX)
		{
			DBG_PRINT("part index must between %d~%d. %s\r\n", APP_KIND_MIN, APP_KIND_MAX, argv[3]);
			return;
		}
	}

	DBG_PRINT("part to be find: index=%d type=%d\r\n", index, type);

	ret = NandAppFindPart(index, type, &partInfo);
	if (ret == 0)
	{
		void *buf = NULL;
		INT32U buf_size = 32; // in sector unit, 32 mean 16K
		INT32U imageSize = partInfo.imageSize;
		INT16U req_size;
		INT32U startSector = partInfo.startSector;

		showAppPartInfo(index, &partInfo);

		buf = gp_malloc_align(buf_size*512, 32);

		if (buf != NULL)
		{
			do
			{
				req_size = (imageSize < buf_size) ? imageSize : buf_size;
				DBG_PRINT("read app sector = %d\r\n", startSector);
				ret = NandBootReadSector(startSector, req_size, (INT32U)buf);
				DBG_PRINT("read app sector done = %d, ret=%d\r\n", startSector, ret);
				DBG_PRINT("dump app sector start = %d, nr = %d\r\n", startSector, req_size);
				nf_cmd_dump_buffer(buf, req_size*512);
				if (ret != 0)
					break;
				DBG_PRINT("\r\n");
				startSector += req_size;
				imageSize -= req_size;
			} while (imageSize != 0);
			gp_free(buf);
			NandBootFlush();
		}
	}
	else
		DBG_PRINT("part not found. index=%d type=%d ret=%d\r\n", index, type, ret);
}

static void nand_cmd_app_writepart(int argc, char *argv[])
{
	INT32S ret;
	INT16U index = 0;
	INT8U type = 0;
	NF_APP_PART_INFO partInfo;

	if (argc > 2)
	{
		index = nf_atoi(argv[2]);
		if (index > 15)
		{
			DBG_PRINT("part index must between 0~15. %s\r\n", argv[2]);
			return;
		}
	}
	if (argc > 3)
	{
		type = nf_atoi(argv[3]);
		if (type > APP_KIND_MAX)
		{
			DBG_PRINT("part index must between %d~%d. %s\r\n", APP_KIND_MIN, APP_KIND_MAX, argv[3]);
			return;
		}
	}

	DBG_PRINT("part to be find: index=%d type=%d\r\n", index, type);

	ret = NandAppFindPart(index, type, &partInfo);
	if (ret == 0)
	{
		void *buf = NULL;
		INT32U buf_size = 32; // in sector unit, 32 mean 16K
		INT32U imageSize = partInfo.imageSize;
		INT16U req_size;
		INT32U startSector = partInfo.startSector;

		showAppPartInfo(index, &partInfo);

		buf = gp_malloc_align(buf_size*512, 32);

		if (buf != NULL)
		{
			gp_memset(buf, 0xAA, buf_size*512);
			NandBootEnableWrite();
			do
			{
				req_size = (imageSize < buf_size) ? imageSize : buf_size;
				DBG_PRINT("write app sector = %d\r\n", startSector);
				ret = NandBootWriteSector(startSector, req_size, (INT32U)buf);
				DBG_PRINT("write app sector done= %d, ret=%d\r\n", startSector, ret);
				if (ret != 0)
					break;
				startSector += req_size;
				imageSize -= req_size;
			} while (imageSize != 0);
			NandBootDisableWrite();
			gp_free(buf);
			NandBootFlush();
		}
	}
	else
		DBG_PRINT("part not found. index=%d type=%d ret=%d\r\n", index, type, ret);
}

static void nf_demo_app_msdcon(INT8U drv)
{
	_deviceunmount(drv); // to avoid data inconsistency, unmount link with file system
#if 0
        storages_init(0x20);//Nand

	state_usb_entry((void *)1);

	//stop detect
	storage_polling_stop();
#else
    #if 0
        // 15b use
        #if (defined NAND_APP_WRITE_EN) && (NAND_APP_WRITE_EN == 1)
        drv_l2_usbd_msdc_set_lun(LUN_NF_TYPE, LUN_NUM_0, USBD_STORAGE_NO_WPROTECT, &gp_msdc_nandapp0);
        #else
        drv_l2_usbd_msdc_set_lun(LUN_NF_TYPE, LUN_NUM_0, USBD_STORAGE_WPROTECT, &gp_msdc_nandapp0);
        #endif
        //usbd_register_class(&usbd_msdc_ctl_blk);
        drv_l1_usbd_init();
	#else
        usbd_msdc_init();
	#endif

#endif
}

static void nand_cmd_app_msdcon(int argc, char *argv[])
{
	INT8U drv = 255;

	if (argc > 2)
		drv = nf_atoi(argv[2]);

	if (drv != FS_NAND_APP)
	{
		DBG_PRINT("illegal driver number [%s], should be %d\r\n", argv[2], FS_NAND_APP);
		return;
	}
	nf_demo_app_msdcon(drv);
}

#if 0 // WARNING: APP area could not support file system and msdc write
static void nf_demo_app_format_disk(INT8U drv)
{
	INT16S ret;

    NandBootEnableWrite();
	_devicemount(drv); // to do format, disk must be mounted first
	ret = _format(drv, FAT32_Type);
    _deviceunmount(drv);
 	NandBootFlush();
    NandBootDisableWrite();

	DBG_PRINT("format disk[%d] %s\r\n", drv, (ret == 0 ? "success":"fail"));
}

static void nand_cmd_app_format(int argc, char *argv[])
{
	INT8U drv = 255;

	if (argc > 2)
		drv = nf_atoi(argv[2]);

	if (drv != FS_NAND_APP)
	{
		DBG_PRINT("illegal driver number [%s], should be %d\r\n", argv[2], FS_NAND_APP);
		return;
	}

	nf_demo_app_format_disk(drv);
}

static void nf_demo_app_mount_disk(INT8U drv)
{
	if(_devicemount(drv))					// Mount device nand
	{
		DBG_PRINT("Mount Disk Fail[%d]\r\n", drv);
		nf_demo_app_format_disk(drv);
        if(_devicemount(drv))
            DBG_PRINT("Mount Disk Fail after format[%d]\r\n", drv);
        else
            DBG_PRINT("Mount Disk success after format[%d]\r\n", drv);
	}
	else
	{
		DBG_PRINT("Mount Disk success[%d]\r\n", drv);
	}
}

static void nand_cmd_app_mount(int argc, char *argv[])
{
	INT8U drv = 255;

	if (argc > 2)
		drv = nf_atoi(argv[2]);

	if (drv != FS_NAND_APP)
	{
		DBG_PRINT("illegal driver number [%s], should be %d\r\n", argv[2], FS_NAND_APP);
		return;
	}

	nf_demo_app_mount_disk(drv);
}
#endif

static void nand_cmd_app_help(void)
{
    DBG_PRINT(
    	"\r\nUsage: nandemo app help\r\n"
    	#if 0 // WARNING: APP area could not support file system and msdc write
    	"\r\n       nandemo app mount <drive>\r\n"
    	"\r\n       nandemo app format <drive>\r\n"
    	#endif
    	"\r\n       nandemo app msdcon <drive>\r\n"
    	"\r\n       nandemo app init\r\n"
    	"\r\n       nandemo app partinfo\r\n"
    	"\r\n       nandemo app findpart <index> <type>\r\n"
    	"\r\n       nandemo app readpart <index> <type>\r\n"
    	"\r\n       nandemo app writepart <index> <type>\r\n"
    	);
}

static void nand_cmd_app_handler(int argc, char *argv[])
{
	if (STRCMP(argv[1],"help") == 0)
    {
    	nand_cmd_app_help();
    }
    #if 0 // WARNING: APP area could not support file system and msdc write
    else if (STRCMP(argv[1],"mount") == 0)
    {
    	nand_cmd_app_mount(argc, argv);
    }
	else if (STRCMP(argv[1],"format") == 0)
    {
    	nand_cmd_app_format(argc, argv);
    }
    #endif
    else if (STRCMP(argv[1],"init") == 0)
    {
    	nand_cmd_app_init(argc, argv);
    }
	else if (STRCMP(argv[1],"msdcon") == 0)
    {
    	nand_cmd_app_msdcon(argc, argv);
    }
	else if (STRCMP(argv[1],"partinfo") == 0)
    {
    	nand_cmd_app_partinfo(argc, argv);
    }
	else if (STRCMP(argv[1],"findpart") == 0)
    {
    	nand_cmd_app_findpart(argc, argv);
    }
 	else if (STRCMP(argv[1],"readpart") == 0)
    {
    	nand_cmd_app_readpart(argc, argv);
    }
 	else if (STRCMP(argv[1],"writepart") == 0)
    {
    	nand_cmd_app_writepart(argc, argv);
    }
    else
    {
    	nand_cmd_app_help();
    }
}
#endif

#include "NandBootAreaInfoScan.h"
#include "NandBootAreaInfo.h"

void dumpNandBootAreaBlockStatus(NandBootAreaInfo_st * pNandBtAreaInfo)
{
    INT32U i;

    DBG_PRINT("\r\nBoot Area Block Status: \r\n");
    for(i = 0; i < pNandBtAreaInfo->btAreaEndBlk; ++i)
    {
        DBG_PRINT("Block %3d: %s\r\n", i, (pNandBtAreaInfo->btAreaBlkFlagArr[i] == 0xFF)? "Good Block" : "Bad Block");
    }
}

void dumpNandBootAreaConfigureInfo(NandBootAreaInfo_st * pNandBtAreaInfo)
{
    INT32U i;
    INT32U errPageNum = 0;

    DBG_PRINT("\r\nBootHeader Blocks :\r\n");
    for(i = 0; i < pNandBtAreaInfo->btHeaderCopyNum; i++){
        if( (pNandBtAreaInfo->btAreaPageArr[i].pageAddr % pNandBtAreaInfo->blkPageNum) == 0){
            DBG_PRINT("Block %d\r\n", pNandBtAreaInfo->btAreaPageArr[i].pageAddr/pNandBtAreaInfo->blkPageNum);
        }

        if(pNandBtAreaInfo->btAreaPageArr[i].correctedBitNum < 0){
            errPageNum++;
            DBG_PRINT("Read Error Page: Block %d, Page %d\r\n",
                pNandBtAreaInfo->btAreaPageArr[i].pageAddr / pNandBtAreaInfo->blkPageNum,
                pNandBtAreaInfo->btAreaPageArr[i].pageAddr % pNandBtAreaInfo->blkPageNum );
        }
    }
    DBG_PRINT("BootHeader Page Read Error Num %d\r\n", errPageNum);

    errPageNum = 0;
    DBG_PRINT("\r\nBootLoader Blocks :\r\n");
    for(i = 0; i < pNandBtAreaInfo->btLoaderCopyNum; i++){
        if( (pNandBtAreaInfo->btAreaPageArr[i+pNandBtAreaInfo->btHeaderCopyNum].pageAddr % pNandBtAreaInfo->blkPageNum) == 0){
            DBG_PRINT("Block %d\r\n", pNandBtAreaInfo->btAreaPageArr[i+pNandBtAreaInfo->btHeaderCopyNum].pageAddr/pNandBtAreaInfo->blkPageNum);
        }

        if(pNandBtAreaInfo->btAreaPageArr[i+pNandBtAreaInfo->btHeaderCopyNum].correctedBitNum < 0){
            errPageNum++;
            DBG_PRINT("Read Error Page: Block %d, Page %d\r\n",
                pNandBtAreaInfo->btAreaPageArr[i+pNandBtAreaInfo->btHeaderCopyNum].pageAddr / pNandBtAreaInfo->blkPageNum,
                pNandBtAreaInfo->btAreaPageArr[i+pNandBtAreaInfo->btHeaderCopyNum].pageAddr % pNandBtAreaInfo->blkPageNum );
        }
    }
    DBG_PRINT("BootLoader Page Read Error Num %d\r\n", errPageNum);
    DBG_PRINT("\r\n");
}

void dumpNandBootAreaBtHeaderDetailInfo(NandBootAreaInfo_st * pNandBtAreaInfo)
{
    INT32U i;
    INT32U checkSum = pNandBtAreaInfo->btAreaPageArr[0].checksum;

    DBG_PRINT("\r\nBootArea BootHeaderPage Status :\r\n");
    for(i = 0; i < pNandBtAreaInfo->btHeaderCopyNum; i++){
        if( (pNandBtAreaInfo->btAreaPageArr[i].pageAddr % pNandBtAreaInfo->blkPageNum) == 0){
            DBG_PRINT("\r\nBlock %d :\r\n", pNandBtAreaInfo->btAreaPageArr[i].pageAddr/pNandBtAreaInfo->blkPageNum);
        }

        DBG_PRINT("Page 0x%x\t: CorrectedBitNum %d\t CheckSum 0x%x %s\r\n", pNandBtAreaInfo->btAreaPageArr[i].pageAddr,
            pNandBtAreaInfo->btAreaPageArr[i].correctedBitNum, pNandBtAreaInfo->btAreaPageArr[i].checksum,
            (pNandBtAreaInfo->btAreaPageArr[i].checksum == checkSum)? "correct" : "error");
    }
}

void dumpNandBootAreaBtLoaderInfo(NandBootAreaInfo_st * pNandBtAreaInfo)
{
    INT32U bootLoaderStartPage;
    INT32U bootLoaderPageNum;
    INT32U bootLoaderCopy;
    INT32U bootLoaderEndPage;
    INT32U bootLoaderCheckSum;
    INT32U firstCheckSum = 0;
    INT32U i;

    bootLoaderPageNum = 0;
    bootLoaderCopy = 1;
    bootLoaderStartPage = pNandBtAreaInfo->btAreaPageArr[pNandBtAreaInfo->btHeaderCopyNum].pageAddr;
    bootLoaderCheckSum = 0;

    DBG_PRINT("\r\nBootArea BootLoaderPage Status :\r\n");
    for(i = 0; i < pNandBtAreaInfo->btLoaderCopyNum; i++)
    {
        bootLoaderCheckSum += pNandBtAreaInfo->btAreaPageArr[i+pNandBtAreaInfo->btHeaderCopyNum].checksum;
        bootLoaderPageNum++;

        if(bootLoaderPageNum == pNandBtAreaInfo->btLoaderPageNum){
            bootLoaderPageNum = 0;
            bootLoaderEndPage = pNandBtAreaInfo->btAreaPageArr[i+pNandBtAreaInfo->btHeaderCopyNum].pageAddr;
            firstCheckSum = (firstCheckSum == 0)? bootLoaderCheckSum : firstCheckSum;

            DBG_PRINT("BootLoader Copy %2d: Range [Block %3d, Page %3d -> Block %3d, Page %3d], CheckSum 0x%08x %s\r\n",
                bootLoaderCopy, bootLoaderStartPage/pNandBtAreaInfo->blkPageNum, bootLoaderStartPage%pNandBtAreaInfo->blkPageNum,
                bootLoaderEndPage/pNandBtAreaInfo->blkPageNum, bootLoaderEndPage%pNandBtAreaInfo->blkPageNum, bootLoaderCheckSum,
                (bootLoaderCheckSum == firstCheckSum)? "correct" : "error");

            bootLoaderCheckSum = 0;
            bootLoaderStartPage = pNandBtAreaInfo->btAreaPageArr[i+1+pNandBtAreaInfo->btHeaderCopyNum].pageAddr;
            bootLoaderCopy++;
        }
    }
}

void gpNandBootAreaInfoDump(void)
{
    INT32S retval;

    NandBootAreaInfo_st nandBtAreaInfo;

    retval = NandBootAreaInfoGet(&nandBtAreaInfo);
    if(retval != 0){
        DBG_PRINT("Get Nand BootArea Info Failed %d\r\n", retval);
    }

    dumpNandBootAreaBlockStatus(&nandBtAreaInfo);
    dumpNandBootAreaConfigureInfo(&nandBtAreaInfo);
    dumpNandBootAreaBtHeaderDetailInfo(&nandBtAreaInfo);
    dumpNandBootAreaBtLoaderInfo(&nandBtAreaInfo);

    NandBootAreaInfoRelease(&nandBtAreaInfo);

    return;
}

static INT32S NandBootAreaDataInfoFill(NandBootAreaDataInfo_st* pBtAreaInfo, NandBootAreaInfo_st *pDetailInfo)
{
    INT16U i;
    INT32U bootLoaderCopy;
    INT32U bootLoaderPageNum;

    pBtAreaInfo->btAreaBlksInfoArr = NULL;
    pBtAreaInfo->btHeaderInfoArr   = NULL;
    pBtAreaInfo->btLoaderInfoArr   = NULL;

    pBtAreaInfo->btAreaBlkNum    = pDetailInfo->btAreaEndBlk;
    pBtAreaInfo->btHeaderCopyNum = pDetailInfo->btHeaderCopyNum;
    pBtAreaInfo->btLoaderCopyNum = pDetailInfo->btLoaderCopyNum / pDetailInfo->btLoaderPageNum;

    pBtAreaInfo->btAreaBlksInfoArr = (NandBtAreaBlockInfo_st *)gp_malloc_align(pBtAreaInfo->btAreaBlkNum * sizeof(NandBtAreaBlockInfo_st), 16);
    pBtAreaInfo->btHeaderInfoArr = (NandBtHeaderDataInfo_st *)gp_malloc_align(pBtAreaInfo->btHeaderCopyNum * sizeof(NandBtHeaderDataInfo_st), 16);
    pBtAreaInfo->btLoaderInfoArr = (NandBtLoaderDataInfo_st *)gp_malloc_align(pBtAreaInfo->btLoaderCopyNum * sizeof(NandBtLoaderDataInfo_st), 16);

    if(    (pBtAreaInfo->btAreaBlksInfoArr == NULL)
        || (pBtAreaInfo->btHeaderInfoArr == NULL)
        || (pBtAreaInfo->btLoaderInfoArr == NULL) )
    {
        return -1;
    }

    for(i = 0; i < pDetailInfo->btAreaEndBlk; ++i)
    {
        if(pDetailInfo->btAreaBlkFlagArr[i] == 0xFF){
            pBtAreaInfo->btAreaBlksInfoArr[i].isGoodBlock = 1;
        }
        else{
            pBtAreaInfo->btAreaBlksInfoArr[i].isGoodBlock = 0;
        }

        pBtAreaInfo->btAreaBlksInfoArr[i].isContainBtInfo = 0;
    }

    for(i = 0; i < pDetailInfo->btHeaderCopyNum; i++){
        if( (pDetailInfo->btAreaPageArr[i].pageAddr % pDetailInfo->blkPageNum) == 0){
            pBtAreaInfo->btAreaBlksInfoArr[pDetailInfo->btAreaPageArr[i].pageAddr/pDetailInfo->blkPageNum].isContainBtInfo = 1;
        }

        pBtAreaInfo->btHeaderInfoArr[i].headerPage = pDetailInfo->btAreaPageArr[i].pageAddr;
        pBtAreaInfo->btHeaderInfoArr[i].correctedBitNum = pDetailInfo->btAreaPageArr[i].correctedBitNum;
        pBtAreaInfo->btHeaderInfoArr[i].checkSum = pDetailInfo->btAreaPageArr[i].checksum;
    }

    bootLoaderCopy = 0;
    bootLoaderPageNum = 0;

    for(i = 0; i < pDetailInfo->btLoaderCopyNum; i++)
    {
        if( (pDetailInfo->btAreaPageArr[i+pDetailInfo->btHeaderCopyNum].pageAddr % pDetailInfo->blkPageNum) == 0){
            pBtAreaInfo->btAreaBlksInfoArr[pDetailInfo->btAreaPageArr[i+pDetailInfo->btHeaderCopyNum].pageAddr/pDetailInfo->blkPageNum].isContainBtInfo = 2;
        }

        if(bootLoaderPageNum == 0){
            pBtAreaInfo->btLoaderInfoArr[bootLoaderCopy].startPage = pDetailInfo->btAreaPageArr[i+pDetailInfo->btHeaderCopyNum].pageAddr;
            pBtAreaInfo->btLoaderInfoArr[bootLoaderCopy].maxCorrectedBitNum = 0;
            pBtAreaInfo->btLoaderInfoArr[bootLoaderCopy].errorPageNum = 0;
            pBtAreaInfo->btLoaderInfoArr[bootLoaderCopy].checkSum = 0;
        }

        if(pDetailInfo->btAreaPageArr[i+pDetailInfo->btHeaderCopyNum].correctedBitNum < 0){
            (pBtAreaInfo->btLoaderInfoArr[bootLoaderCopy].errorPageNum)++;
             pBtAreaInfo->btLoaderInfoArr[bootLoaderCopy].maxCorrectedBitNum = -1;
        }
        else{
            if(pBtAreaInfo->btLoaderInfoArr[bootLoaderCopy].maxCorrectedBitNum >= 0){
                if(pDetailInfo->btAreaPageArr[i+pDetailInfo->btHeaderCopyNum].correctedBitNum > pBtAreaInfo->btLoaderInfoArr[bootLoaderCopy].maxCorrectedBitNum)
                {
                    pBtAreaInfo->btLoaderInfoArr[bootLoaderCopy].maxCorrectedBitNum = pDetailInfo->btAreaPageArr[i+pDetailInfo->btHeaderCopyNum].correctedBitNum;
                }
            }
        }

        pBtAreaInfo->btLoaderInfoArr[bootLoaderCopy].checkSum += pDetailInfo->btAreaPageArr[i+pDetailInfo->btHeaderCopyNum].checksum;
        bootLoaderPageNum++;

        if(bootLoaderPageNum == pDetailInfo->btLoaderPageNum){
            bootLoaderPageNum = 0;
            pBtAreaInfo->btLoaderInfoArr[bootLoaderCopy].endPage = pDetailInfo->btAreaPageArr[i+pDetailInfo->btHeaderCopyNum].pageAddr;
            bootLoaderCopy++;
        }
    }
}

INT32S NandBootAreaDataInfoGet(NandBootAreaDataInfo_st* pBtAreaInfo)
{
    INT32S retval;

    NandBootAreaInfo_st nandBtAreaDetailInfo;

    retval = NandBootAreaInfoGet(&nandBtAreaDetailInfo);
    if(retval != 0){
        DBG_PRINT("Get Nand BootArea Info Failed %d\r\n", retval);
        return -1;
    }

    NandBootAreaDataInfoFill(pBtAreaInfo, &nandBtAreaDetailInfo);
    NandBootAreaInfoRelease(&nandBtAreaDetailInfo);

    return 0;
}

void NandBootAreaDataInfoRelease(NandBootAreaDataInfo_st* pBtAreaInfo)
{
    if(pBtAreaInfo->btAreaBlksInfoArr != NULL){
        gp_free(pBtAreaInfo->btAreaBlksInfoArr);
        pBtAreaInfo->btAreaBlksInfoArr = NULL;
    }

     if(pBtAreaInfo->btHeaderInfoArr != NULL){
        gp_free(pBtAreaInfo->btHeaderInfoArr);
        pBtAreaInfo->btHeaderInfoArr = NULL;
    }

     if(pBtAreaInfo->btLoaderInfoArr != NULL){
        gp_free(pBtAreaInfo->btLoaderInfoArr);
        pBtAreaInfo->btLoaderInfoArr = NULL;
    }
}

void NandBootAreaDataInfoDump(NandBootAreaDataInfo_st* pBtAreaInfo)
{
    INT16U i;
    INT8S*  strInfo;

    DBG_PRINT("\r\nBoot Area Block Status:\r\n");
    DBG_PRINT("Totoal Block Num %d\r\n", pBtAreaInfo->btAreaBlkNum);

    for(i = 0; i < pBtAreaInfo->btAreaBlkNum; i++){
        if(pBtAreaInfo->btAreaBlksInfoArr[i].isContainBtInfo == 1){
           strInfo = (INT8S*)"BootHeader";
        }
        else if(pBtAreaInfo->btAreaBlksInfoArr[i].isContainBtInfo == 2){
           strInfo = (INT8S*)"BootLoader";
        }
        else{
            strInfo = (INT8S*)"Nothing";
        }

        DBG_PRINT("Block %3d: %10s, contain %s\r\n", i, (pBtAreaInfo->btAreaBlksInfoArr[i].isGoodBlock)?"Good Block":"Bad Block", strInfo);
    }

    DBG_PRINT("\r\nBoot Header Copies Status: ");
    DBG_PRINT("Boot Header Copy Num %d\r\n", pBtAreaInfo->btHeaderCopyNum);

    for(i = 0; i < pBtAreaInfo->btHeaderCopyNum; i++){
        DBG_PRINT("Page 0x%03x: CheckSum 0x%08x, CorrectedErrorBitNum %d\r\n", pBtAreaInfo->btHeaderInfoArr[i].headerPage,
            pBtAreaInfo->btHeaderInfoArr[i].checkSum, pBtAreaInfo->btHeaderInfoArr[i].correctedBitNum);
    }

    DBG_PRINT("\r\nBoot Loader Copies Status: ");
    DBG_PRINT("Boot Loader Copy Num %d\r\n", pBtAreaInfo->btLoaderCopyNum);

    for(i = 0; i < pBtAreaInfo->btLoaderCopyNum; i++){
        DBG_PRINT("BootLoader Copy %d: Page 0x%03x ~ Page 0x%03x, Error Page Num %d, CheckSum 0x%08x\r\n", i+1, pBtAreaInfo->btLoaderInfoArr[i].startPage,
            pBtAreaInfo->btLoaderInfoArr[i].endPage, pBtAreaInfo->btLoaderInfoArr[i].errorPageNum, pBtAreaInfo->btLoaderInfoArr[i].checkSum);
    }
}

//Sample usage of the related API of NandBootAreaInfo.c
void NandBootAreaInfoDump_Demo(void)
{
    INT32S  retval = 0;
    NandBootAreaDataInfo_st BtAreaInfo;

    INT16U totalBlkNum;
    INT16U orgBlkNum;
    INT16U usrMarkBadBlkNum;

    retval = NandScanBlockStatus(&totalBlkNum, &orgBlkNum, &usrMarkBadBlkNum);
    if(retval != 0){
        DBG_PRINT("Scan Nand Block Status Error %d\r\n", retval);
    }

    DBG_PRINT("\r\nNand Block Status :\r\n");
    DBG_PRINT("-> Total Block Num        : %d\r\n", totalBlkNum);
    DBG_PRINT("-> Original Bad Block Num : %d\r\n", orgBlkNum);
    DBG_PRINT("-> User Mark Bad Block Num: %d\r\n", usrMarkBadBlkNum);

    retval = NandBootAreaDataInfoGet(&BtAreaInfo);
    if(retval != 0){
        NandBootAreaDataInfoRelease(&BtAreaInfo);
        return;
    }

    //Make usage of the data in BtAreaInfo
    NandBootAreaDataInfoDump(&BtAreaInfo);

    NandBootAreaDataInfoRelease(&BtAreaInfo);
}

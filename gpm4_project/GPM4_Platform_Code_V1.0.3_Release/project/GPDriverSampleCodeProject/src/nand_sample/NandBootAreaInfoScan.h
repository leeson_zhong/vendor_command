#ifndef _NANDBOOTAREAINFOSCAN_H_
#define _NANDBOOTAREAINFOSCAN_H_

#include "project.h"
#include "gplib_print_string.h"

#if !defined(_BCH_MODE_ENUM_) && !defined(_BCH_MODE_ENUM)
#define _BCH_MODE_ENUM_
#define _BCH_MODE_ENUM
typedef enum
{
	// BCH12 start
	BCH12_12_BITS_MODE=0x03,
	BCH12_8_BITS_MODE=0x04,
	BCH12_4_BITS_MODE=0x05,
    // BCH12 end

    BCH1K60_BITS_MODE=0x00,
    BCH1K40_BITS_MODE=0x01,
    BCH1K24_BITS_MODE=0x02,
    BCH1K16_BITS_MODE=0x03,
    BCH512B8_BITS_MODE=0x04,
    BCH512B4_BITS_MODE=0x05,
    ECC_1_BIT_MODE=0xFE,
    BCH_OFF=0xFF
} BCH_MODE_ENUM;
#endif  //_BCH_MODE_ENUM

typedef struct
{
	INT16U pageAddr;
	INT16S correctedBitNum;
	INT32U checksum;
}NandPageInfo_st;

typedef struct
{
    BCH_MODE_ENUM  btHeaderBCH;
    BCH_MODE_ENUM  btLoaderBCH;
    INT16U btHeaderPageSize;
    INT16U btLoaderPageSize;
    INT16U btHeaderCopyNum;
    INT16U btLoaderCopyNum;
    INT16U btLoaderPageNum;
    INT16U blkPageNum;
    INT16U btLoaderStartBlk;
    INT16U btAreaEndBlk;
    NandPageInfo_st *btAreaPageArr; // elementNum = <btAreaEndBlk> x <blkPageNum>
                                    // first <btHeaderCopyNum> elements are about Pages contain BootHeader;
                                    // then  <btLoaderCopyNum> eiements art about Pages contain BootLoader;
    INT8U  *btAreaBlkFlagArr;       // elementNum = <btAreaEndBlk>
}NandBootAreaInfo_st;

void dumNandBtAreaInfo(NandBootAreaInfo_st* pNandBtAreaInfo);
void NandBootAreaInfoRelease(NandBootAreaInfo_st *pNandAreaInfo);
INT32S NandBootAreaInfoGet(NandBootAreaInfo_st *pNandAreaInfo);

#endif // _NANDBOOTAREAINFOSCAN_H_

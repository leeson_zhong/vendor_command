#include "drv_l1_rotator.h"
#include "drv_l2_display.h"
#include "drv_l1_scaler.h"
#include "drv_l2_scaler.h"
#include "gp_stdlib.h"
#include "TEXT_HDR.h"

/**************************************************************************
 *                              D E F I N E                               *
 **************************************************************************/
#define IMAGE_SIZE_HPIXEL                       320
#define IMAGE_SIZE_VPIXEL                       240
#define FRAME_BUF_ALIGN64                       0x3F

/**************************************************************************
 *                              M A C R O S                               *
 **************************************************************************/
#define gpScale1Format_t 	                    ScalerFormat_t
#define gpScale1Para_t		                    ScalerPara_t

/**************************************************************************
 *                         G L O B A L    D A T A                         *
 **************************************************************************/
static INT16U h_size,v_size;

static INT32S scaler_start(INT32U ScaleBufIn, INT32U ScaleBufOut)
{
	INT32S ret;
	gpScale1Format_t Scale1;
	gpScale1Para_t Matrix;

	DBG_PRINT("\r\n<< %s >>\r\n", __func__);
	gp_memset((void *)&Scale1, 0x00, sizeof(Scale1));
	gp_memset((void *)&Matrix, 0x00, sizeof(Matrix));

	// start test
	Scale1.input_format = C_SCALER_CTRL_IN_RGB565;
	Scale1.input_width = IMAGE_SIZE_HPIXEL;
	Scale1.input_height = IMAGE_SIZE_VPIXEL;
	Scale1.input_visible_width = IMAGE_SIZE_HPIXEL;
	Scale1.input_visible_height = IMAGE_SIZE_VPIXEL;
	Scale1.input_x_offset = 0;
	Scale1.input_y_offset = 0;
	Scale1.input_y_addr = (INT32U)ScaleBufIn;
	Scale1.input_u_addr = 0;
	Scale1.input_v_addr = 0;

	Scale1.output_format = C_SCALER_CTRL_OUT_RGB565;
	Scale1.output_width = h_size;
	Scale1.output_height = v_size;
	Scale1.output_buf_width = h_size;
	Scale1.output_buf_height = v_size;
	Scale1.output_x_offset = 0;

	Scale1.output_y_addr = ScaleBufOut;
	Scale1.output_u_addr = 0;
	Scale1.output_v_addr = 0;
	Scale1.fifo_mode = C_SCALER_CTRL_FIFO_DISABLE;
	Scale1.scale_mode = C_SCALER_FULL_SCREEN;
	Scale1.digizoom_m = 10;
	Scale1.digizoom_n = 10;

	Matrix.boundary_mode = 1;
	Matrix.boundary_color = 0x8080;
	Matrix.gamma_en = 0;
	Matrix.color_matrix_en = 0;
	Matrix.yuv_type = 0;

	ret = drv_l2_scaler_trigger(SCALER_0, 1, &Scale1, &Matrix);
	if(ret == C_SCALER_STATUS_DONE || ret == C_SCALER_STATUS_STOP) {
		drv_l2_scaler_stop(SCALER_0);
	}
	else {
		DBG_PRINT("Scale1 Fail\r\n");
		while(1);
	}

	return ret;
}

void rotator_demo(void)
{
    INT32U tar_buf,src_buf;

    DBG_PRINT("rotator_demo start \r\n");

    // Initialize display device
    drv_l2_display_init();
    drv_l2_display_start(DISPLAY_DEVICE,DISP_FMT_RGB565);

    drv_l2_display_get_size(DISPLAY_DEVICE,(INT16U *)&h_size,(INT16U *)&v_size);

    tar_buf = (INT32U)gp_malloc_align(((h_size*v_size*2)+64),64);
    tar_buf = (INT32U)((tar_buf + FRAME_BUF_ALIGN64) & ~FRAME_BUF_ALIGN64);

    if((h_size == IMAGE_SIZE_HPIXEL) && (v_size == IMAGE_SIZE_VPIXEL))
        src_buf = (INT32U)_Text056_IMG0000_CellData;
    else
    {
        src_buf = (INT32U)gp_malloc_align(((h_size*v_size*2)+64),64);
        src_buf = (INT32U)((src_buf + FRAME_BUF_ALIGN64) & ~FRAME_BUF_ALIGN64);
        scaler_start((INT32U)_Text056_IMG0000_CellData,src_buf);
    }

    drv_l2_display_update(DISPLAY_DEVICE,(INT32U)src_buf);

    osDelay(1000);

    rotator_init();

    rotator_src_img_info(IMAGE_RGB565,h_size,v_size,(INT32U)src_buf);

    rotator_tar_img_addr(tar_buf);

    rotator_mode_set(ROTATOR_180);
    rotator_start();
    rotator_end_wait(1);
    drv_l2_display_update(DISPLAY_DEVICE,tar_buf);

    DBG_PRINT("rotator_demo end \r\n");
    while(1)
        osDelay(1);
}

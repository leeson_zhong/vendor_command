/*******************************************************
    Include file
*******************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "drv_l1_sfr.h"
#include "drv_l1_gpio.h"
#include "drv_l1_uart.h"
#include "drv_l1_spi.h"
#include "drv_l2_spi_flash.h"

#define TEST_SIZE	256

static INT8U wr_buf[TEST_SIZE] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
static INT8U rd_buf[TEST_SIZE];

/****************************************************
*		spi master mode demo						*
****************************************************/
INT32S spi_earse(INT32U spi_addr, INT32S size)
{
	INT32S ret;

	while(size > 0) {
		DBG_PRINT("Earse = 0x%x\r\n", spi_addr);
		ret = drv_l2_spiflash_sector_erase(spi_addr);
		if(ret < 0) {
			DBG_PRINT("earse Fail\r\n");
			goto __exit;
		}

		spi_addr += (1 << 12); //4K
		size -= (1 << 12);
	}

__exit:
	return ret;
}

INT32S spi_read(INT32U spi_addr, INT8U *buf, INT32S size)
{
	INT32S ret;

	while(size > 0) {
		DBG_PRINT("Read = 0x%x\r\n", spi_addr);
		ret = drv_l2_spiflash_read_page(spi_addr, buf);
		if(ret < 0) {
			DBG_PRINT("read Fail\r\n");
			goto __exit;
		}

		spi_addr += (1 << 8);
		buf += (1 << 8);
		size -= (1 << 8);
	}

__exit:
	return ret;
}

INT32S spi_write(INT32U spi_addr, INT8U *buf, INT32S size)
{
	INT32S ret;

	while(size > 0) {
		DBG_PRINT("Write = 0x%x\r\n", spi_addr);
		ret = drv_l2_spiflash_write_page(spi_addr, buf);
		if(ret < 0) {
			DBG_PRINT("read Fail\r\n");
			goto __exit;
		}

		spi_addr += (1 << 8);
		buf += (1 << 8);
		size -= (1 << 8);
	}

__exit:
	return ret;
}

INT32S spi_rw_test(INT8U *pWrBuf, INT8U *pRdBuf)
{
	INT32U spi_addr;
	INT32S ret;

	// earse
	spi_addr = 0x00;
	ret = spi_earse(spi_addr, TEST_SIZE);
	if(ret < 0) {
		DBG_PRINT("Earse Fail\r\n");
		goto __exit;
	}

	// verify erase
	ret = spi_read(spi_addr, pRdBuf, TEST_SIZE);
	if(ret < 0) {
		DBG_PRINT("Read Fail\r\n");
		goto __exit;
	}

	if(*pRdBuf == 0xFF) {
		DBG_PRINT("verify OK\r\n");
	} else {
		DBG_PRINT("verify Fail\r\n");
		while(1);
	}

	// write
	ret = spi_write(spi_addr, pWrBuf, TEST_SIZE);
	if(ret < 0) {
		DBG_PRINT("Read Fail\r\n");
		goto __exit;
	}

	// read
	ret = spi_read(spi_addr, pRdBuf, TEST_SIZE);
	if(ret < 0) {
		DBG_PRINT("Read Fail\r\n");
		goto __exit;
	}

	// verify
	ret = memcmp((INT8S *)pWrBuf, (INT8S *)pRdBuf, TEST_SIZE);
	if(ret == 0) {
		DBG_PRINT("verify OK\r\n");
	} else {
		DBG_PRINT("verify Fail\r\n");
		while(1);
	}

__exit:
	return ret;
}

void spi_demo(void)
{
	INT8U SPI_ID[3], temp;
	INT32S ret, i;

    DBG_PRINT("<< %s >>\r\n", __func__);

	for(i=0; i<sizeof(wr_buf); i++) {
		wr_buf[i] = i;
		rd_buf[i] = 0;
	}

	drv_l2_spiflash_init(12000000);

	while(1) {
		SPI_ID[0] = SPI_ID[1] = SPI_ID[2] = 0;
		ret = drv_l2_spiflash_read_id(SPI_ID);
		DBG_PRINT("spi flash id = %x,%x,%x\r\n", SPI_ID[0], SPI_ID[1], SPI_ID[2]);
		if(SPI_ID[0] != 0x00 && SPI_ID[0] != 0xFF) {
			break;
		}
	}

    spi_rw_test(wr_buf, rd_buf);

    while(1)
        osDelay(1);
}

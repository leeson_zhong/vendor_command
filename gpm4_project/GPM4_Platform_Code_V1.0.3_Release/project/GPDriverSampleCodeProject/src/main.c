#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "drv_l1.h"
#include "drv_l2.h"
#include "cmsis_os.h"
#include "gplib.h"
#if defined(GPLIB_CONSOLE_EN) && (GPLIB_CONSOLE_EN == 1)
#include "console.h"
#endif
#include "drv_l1_gpio.h"

#define I2C_DEMO                0
#define ROTATOR_DEMO            0
#define DISPLAY_DEMO            0
#define H264_ENCODE_DEMO        0
#define H264_DECODE_DEMO        0
#define USBD_MSDC_DEMO          0
#define SPU_DEMO                0
#define SCALER_DEMO             0
#define SPI_DEMO                0
#define JPEG_CODEC_DEMO         0
#define FS_DEMO                 0
#define I2S_TX_DEMO             0
#define I2S_RX_DEMO             0
#define NAND_DEMO               0

#define CEC_DEMO           0
#define CAN_DEMO           0

#define STACKSIZE       (32768)

/*-----------------------------------------------------------*/
#if 0
void vPortSetupTimerInterrupt()
{
/* Configure SysTick to interrupt at the requested rate. */
    SysTick_Config (32768 / configTICK_RATE_HZ);
}
#endif

/*-----------------------------------------------------------*/
void vApplicationTickHook( void )
{

}

/*-----------------------------------------------------------*/
void vApplicationMallocFailedHook( void )
{
	/* vApplicationMallocFailedHook() will only be called if
	configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h.  It is a hook
	function that will get called if a call to pvPortMalloc() fails.
	pvPortMalloc() is called internally by the kernel whenever a task, queue,
	timer or semaphore is created.  It is also called by various parts of the
	demo application.  If heap_1.c or heap_2.c are used, then the size of the
	heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
	FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
	to query the size of free heap space that remains (although it does not
	provide information on how the remaining heap might be fragmented). */
	taskDISABLE_INTERRUPTS();
	for( ;; );
}

/*-----------------------------------------------------------*/
void vApplicationIdleHook( void )
{
	/* vApplicationIdleHook() will only be called if configUSE_IDLE_HOOK is set
	to 1 in FreeRTOSConfig.h.  It will be called on each iteration of the idle
	task.  It is essential that code added to this hook function never attempts
	to block in any way (for example, call xQueueReceive() with a block time
	specified, or call vTaskDelay()).  If the application makes use of the
	vTaskDelete() API function (as this demo application does) then it is also
	important that vApplicationIdleHook() is permitted to return to its calling
	function, because it is the responsibility of the idle task to clean up
	memory allocated by the kernel to any task that has since been deleted. */
}

/*-----------------------------------------------------------*/
void vApplicationStackOverflowHook( TaskHandle_t pxTask, char *pcTaskName )
{
	( void ) pcTaskName;
	( void ) pxTask;
	DBG_PRINT("task name %s\r\n", pcTaskName);
	/* Run time stack overflow checking is performed if
	configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook
	function is called if a stack overflow is detected. */
	taskDISABLE_INTERRUPTS();
	for( ;; );
}

/*-----------------------------------------------------------*/
extern void i2c_demo(void);
extern void rotator_demo(void);
extern void display_demo(void);
extern void cec_sample_demo(void);
extern void can_sample_demo(void);
extern void spu_demo(void);
extern void scaler_demo(void);
extern void spi_demo(void);

void InitTask(void const *param)
{
    DBG_PRINT("\r\n%s ... \r\n",__func__);

    #if I2C_DEMO
    i2c_demo();
    #endif

    #if ROTATOR_DEMO
    rotator_demo();
    #endif

    #if DISPLAY_DEMO
    display_demo();
    #endif

    #if H264_ENCODE_DEMO
    H264_Encoder_Demo();
    #endif

    #if H264_DECODE_DEMO
    H264_Decoder_Demo();
    #endif

    #if (USBD_MSDC_DEMO == 1)
    usbd_msdc_init();
    #endif

    #if SPU_DEMO == 1
    spu_demo();
    #endif

    #if SCALER_DEMO == 1
    scaler_demo();
    #endif

    #if SPI_DEMO == 1
    spi_demo();
    #endif

    #if CEC_DEMO
    cec_sample_demo();
    #endif

    #if CAN_DEMO
    can_sample_demo();
    #endif

    #if JPEG_CODEC_DEMO
    jpeg_encode_test();
    jpeg_decode_test();
    #endif


    while(1){osDelay(1000);}
	osThreadTerminate(NULL);
}

void CmdTask(void const *param);
void TestTask(void const *param);
#if defined(GPLIB_CONSOLE_EN) && (GPLIB_CONSOLE_EN == 1)
xTaskHandle cmdTaskHandle = NULL;
#endif

/*-----------------------------------------------------------*/
int main(void)
{
    osThreadDef(InitTask, osPriorityNormal, 0, STACKSIZE);

    SystemInit();

    board_init();

    drv_l1_init();

    #if (GPLIB_FILE_SYSTEM_EN == 1)
    fs_init();              // Initiate file system module
    #endif

    #if defined(GPLIB_CONSOLE_EN) && (GPLIB_CONSOLE_EN == 1)
	osThreadDef(CmdTask, osPriorityAboveNormal, 0, STACKSIZE);
  	cmdTaskHandle = osThreadCreate(&os_thread_def_CmdTask, NULL);
    #endif

    osKernelStart(&os_thread_def_InitTask, NULL);

    /* Never go here */
    return 0;
}

#if defined(GPLIB_CONSOLE_EN) && (GPLIB_CONSOLE_EN == 1)

extern void OS_Cmd(void);
extern void Mem_Cmd(void);

extern void FS_Demo(void);
extern void Nand_Demo(void);
extern void I2S_TX_Demo(void);
extern void I2S_RX_Demo(void);

void CmdTask(void const *param)
{
    DBG_PRINT("%s ... \r\n",__func__);

    #if (CMD_OS == 1)
    OS_Cmd();
    #endif

    #if (CMD_MEM == 1)
    Mem_Cmd();
    #endif

    #if FS_DEMO
        #if !(  (defined(NAND1_EN) && (NAND1_EN == 1)) || \
                (defined(NAND2_EN) && (NAND2_EN == 1)) || \
                (defined(NAND3_EN) && (NAND3_EN == 1)) || \
                (defined(NAND_APP_EN) && (NAND_APP_EN == 1)) ||\
                (defined(SD_EN) && (SD_EN == 1)) \
            )
            #error "ERR:FS_DEMO require SD_EN or NAND_EN set to 1"
        #endif
        FS_Demo();
    #endif

    #if NAND_DEMO
        #if (GPLIB_FILE_SYSTEM_EN == 0)
            #error "ERR:NAND_DEMO require GPLIB_FILE_SYSTEM_EN set to 1"
        #endif
        Nand_Demo();
    #endif

    #if I2S_TX_DEMO
        #if (_DRV_L1_I2S_TX == 0)
            #error "ERR:I2S_TX_DEMO require _DRV_L1_I2S_TX set to 1"
        #endif
        I2S_TX_Demo();
    #endif

    #if I2S_RX_DEMO
        #if (_DRV_L1_I2S_RX == 0)
            #error "ERR:I2S_RX_DEMO require _DRV_L1_I2S_RX set to 1"
        #endif
        I2S_RX_Demo();
    #endif

    Cmd_Task((void *)param);
}
#endif // GPLIB_CONSOLE_EN

#include "drv_l1_cec.h"
#include "board_config.h"

#define RX_LEN 30
INT8U *cec_rx_buf;
INT32U broadcast = 0;
INT32U opcode1 = 0;

void RX_get_data_isr()
{
    INT32U idx=0;
    INT32U rcnt = drv_l1_cec_get_rx_count();;
    DBG_PRINT("RX_get_isr %d\r\n",rcnt);
    while(rcnt > 0)
    {
        DBG_PRINT("%x ",*(cec_rx_buf+idx));
        idx++;
        rcnt--;
    }
    if((*(cec_rx_buf+1)) == 0x83)
    {
        broadcast = 1;
    }
    DBG_PRINT("\r\n");
}

void cec_sample_demo(void)
{
    DBG_PRINT("CEC demo\r\n");
    cec_pinmux_set(MUX0);
    drv_l1_cec_init();
    drv_l1_cec_interrupt_enable(0x1FFF);
    //need pull high resister
    //enable CEC module, auto switch mux, but sensor priority  high need disable it
    drv_l1_cec_own_address_config(0x7F);
    drv_l1_cec_error_bit_config(BRE_STP | BRE_GEN | LBPE_GEN);
    drv_l1_cec_sft_set(SFT_BFRT,SFT_AUTO);
    cec_rx_buf = (INT8U*)gp_malloc(sizeof(INT8U)*RX_LEN);
    drv_l1_cec_assign_rx_buffer(cec_rx_buf,RX_LEN);
    drv_l1_cec_register_handler(CEC_ISR_RXEND,RX_get_data_isr);

    while(1)
    {
        if(broadcast) {
            drv_l1_cec_tx(0x4F, 0,1);//broadcast physical address
            drv_l1_cec_tx(0x84, 0,0);
            drv_l1_cec_tx(0x10, 0,0);//1.0.0.0
            drv_l1_cec_tx(0x00, 0,0);
            drv_l1_cec_tx(0x04, 1,0);
            broadcast=0;
        }
        osDelay(1);
    }
}

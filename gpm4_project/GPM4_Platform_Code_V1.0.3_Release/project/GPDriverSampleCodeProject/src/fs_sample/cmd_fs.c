
#include "gplib.h"

#if (GPLIB_FILE_SYSTEM_EN == 1)

#include "console.h"

extern void diag_fs(INT8U drv);

static int fs_atoi(char * str)
{
    int num = -1;

    if (STRCMP(str, "0") == 0)
        num = 0;
    else if (STRCMP(str, "1") == 0)
        num = 1;
    else if (STRCMP(str, "2") == 0)
        num = 2;
    else if (STRCMP(str, "3") == 0)
        num = 3;
    else if (STRCMP(str, "4") == 0)
        num = 4;
    else if (STRCMP(str, "5") == 0)
        num = 5;
    else if (STRCMP(str, "6") == 0)
        num = 6;
    else if (STRCMP(str, "7") == 0)
        num = 7;
    else if (STRCMP(str, "8") == 0)
        num = 8;
    else if (STRCMP(str, "9") == 0)
        num = 9;
    else if (STRCMP(str, "10") == 0)
        num = 10;
    else if (STRCMP(str, "11") == 0)
        num = 11;
    else if (STRCMP(str, "12") == 0)
        num = 12;
    else if (STRCMP(str, "13") == 0)
        num = 13;
    else if (STRCMP(str, "14") == 0)
        num = 14;
    else if (STRCMP(str, "15") == 0)
        num = 15;
    else if (STRCMP(str, "16") == 0)
        num = 16;

    return num;
}

static void fs_test_auto(INT8U drv)
{
     //unsigned int total= 2147483648;
     unsigned int total= 1;
     unsigned int k=0;

    for (k = 0; k < total; k++)
    {
        if (total > 1)
	    DBG_PRINT("=== test %u of tatal %u ===\r\n", k, total);
        diag_fs(drv);
    }
}

static void fs_test_auto_handler(int argc, char *argv[])
{
  INT8U drv = FS_SD2; // FS_NAND1(0):A; // FS_SD(2):C // FS_SD2(9):K

    if (argc > 2)
        drv = fs_atoi(argv[2]);

    if (drv >= MAX_DISK_NUM)
    {
        DBG_PRINT("illegal driver number [%s], should between %d and %d\r\n", argv[2], 0, MAX_DISK_NUM - 1);
	return;
    }

    fs_test_auto(drv);
}

static void fs_test_help(void)
{
    DBG_PRINT(
    	"\r\nUsage: fs test help\r\n"
    	"\r\n       fs test auto <drive(10,2,0)>\r\n"
    	);
}

static void fs_test_handler(int argc, char *argv[])
{
    if (STRCMP(argv[1],"auto") == 0)
    {
    	fs_test_auto_handler(argc, argv);
    }
    else
    {
    	fs_test_help();
    }
}

static void fs_cmd_help(void)
{
    DBG_PRINT(
    	"\r\nUsage: fs help\r\n"
    	"\r\n       fs test\r\n"
    	"\r\n       fs config\r\n"
    	);
}

static void fs_cmd_handler(int argc, char *argv[])
{
    if (STRCMP(argv[1],"test") == 0)
    {
        fs_test_handler(argc - 1, &argv[1]);
    }
    else
    {
    	fs_cmd_help();
    }
}

static cmd_t fs_cmd_list[] =
{
    {"fs",  fs_cmd_handler,  NULL },
    {NULL,    NULL,   NULL}
};

void fs_cmd_register(void)
{
    cmd_t *pcmd = &fs_cmd_list[0];

    while (pcmd->cmd != NULL)
    {
        cmdRegister(pcmd);
        pcmd += 1;
    }
}

void FS_Demo(void)
{
    #if defined(GPLIB_CONSOLE_EN) && (GPLIB_CONSOLE_EN == 1)
    fs_cmd_register();
    #endif
}

#endif // GPLIB_FILE_SYSTEM_EN == 1

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cmsis_os.h"
#include "gplib.h"
#include "drv_l1_gpio.h"

extern INT16S fs_lfndelete(INT8S * dirpath);
extern void sd_switch_pin_mux(INT8U drv);

static char *in_path = "image";
static char *in_file_name = "IMG0001.jpg";
static char *out_path = "output";
static char *out_file_name = "IMG0001.jpg";

char *get_fs_drive_letter(INT8U drv)
{
    if (drv == FS_SD)
        return "C:";
    else if (drv == FS_SD2)
        return "K:";
    else if (drv == FS_NAND1)
        return "A:";
    else
    {
        DBG_PRINT("Unknown drive %u.\r\n", drv);
        return NULL;
    }
}

char *get_fs_root_path(INT8U drv)
{
    if (drv == FS_SD)
        return "C:\\";
    else if (drv == FS_SD2)
        return "K:\\";
    else if (drv == FS_NAND1)
        return "A:\\";
    else
    {
        DBG_PRINT("Unknown drive %u.\r\n", drv);
        return NULL;
    }
}

void diag_fs(INT8U drv)
{
    INT16S ret;
    char *root_path;
    char full_path[256];

    root_path = get_fs_root_path(drv);
    if (root_path == NULL)
        return;

    //sd_switch_pin_mux(drv);

    ret = _devicemount(drv);
    if(ret)
    {
        DBG_PRINT("Mount Disk Fail[%d]\r\n", drv);
        #if 0
        {
            ret = _format(drv, FAT32_Type);
            if (ret)
              DBG_PRINT("Format Disk Fail[%d]\r\n", drv);
            ret = _deviceunmount(drv);
            if (ret)
              DBG_PRINT("UnMount Disk Fail[%d]\r\n", drv);
            ret = _devicemount(drv);
        }
        #endif
    }
    if (ret)
        DBG_PRINT("2 Mount Disk Fail[%d]\r\n", drv);
    else
    {
        INT16S fd, fd_out;
        struct sfn_info file_info;

        DBG_PRINT("Mount Disk success[%d]\r\n", drv);

        strcpy(full_path, root_path);
        strcat(full_path, in_path);
        strcat(full_path, "\\");
        strcat(full_path, in_file_name);

        fd = fs_open(full_path, O_RDONLY);
        if (fd < 0)
        {
            ret = _deviceunmount(drv);
            DBG_PRINT("fail to open input file %s. ret=%d\r\n", full_path, ret);
            return;
        }

        strcpy(full_path, root_path);
        strcat(full_path, out_path);
        ret = chdir(full_path);
        if (ret == 0)
        {
            ret = fs_lfndelete((INT8S *)out_file_name);
            if (ret != 0)
            {
                DBG_PRINT("fail to delete output file %s because not exist. ret=%d\r\n", out_file_name, ret);
                ret = 0; // so that code not flow to mkdir
            }

            #if 0
            ret = _deleteall(full_path);
            if (ret != 0)
                DBG_PRINT("fail to delete all %s. ret=%d\r\n", full_path, ret);

            ret = rmdir(full_path);
            if (ret != 0)
                DBG_PRINT("fail to rmdir because not empty %s. ret=%d\r\n", full_path, ret);
            else
                ret = 1; // so that code flow to mkdir
            #endif
        }
        if (ret != 0)
        {
            ret = mkdir(full_path);
            ret = chdir(full_path);
            if (ret != 0)
            {
                DBG_PRINT("fail to mkdir %s. ret=%d\r\n", full_path, ret);
                fs_close(fd);
                ret = _deviceunmount(drv);
                return;
            }
        }

        fd_out = fs_open(out_file_name, O_CREAT|O_TRUNC|O_WRONLY);
        if (fd_out < 0)
            DBG_PRINT("fail to create output file %s\r\n", out_file_name);

        if (fd_out >= 0)
        {
            // duplicate file
            void *buf;
            INT32U buf_size;

            sfn_stat(fd, &file_info);
            DBG_PRINT("%s file size %lu bytes.\r\n", in_file_name, (INT32U)file_info.f_size);

            buf_size = 256*1024;
            buf = (void *)pvPortMalloc(buf_size);
            if (buf != NULL)
            {
                INT32S read_ret;
                INT32U read_size;
                INT64U remain_f_size = file_info.f_size;
                INT32S write_ret;
                INT64U offset = 0;

                DBG_PRINT("buf=%08x\r\n", (INT32U)buf);
                if (((INT32U)buf & (0x3)) != 0)
                  DBG_PRINT("WARNING buf not aligned to 4=%08x\r\n", (INT32U)buf);

                while (remain_f_size > 0)
                {
                    read_size = remain_f_size < buf_size ? remain_f_size : buf_size;
                    read_ret = fs_read(fd, (INT32U)buf, read_size);
                    if (read_ret > 0)
                    {

                        if (read_ret != read_size)
                        {
                            DBG_PRINT("read_ret != read_size, %d, %d. offset=%d\r\n", read_ret, read_size, (INT32U)offset);
                            break;
                        }
                        else
                        {
                            write_ret = fs_write(fd_out, (INT32U)buf, read_ret);
                            if (write_ret != read_ret)
                            {
                                DBG_PRINT("write_ret != read_ret, %d, %d. offset=%d\r\n", write_ret, read_ret, (INT32U)offset);
                                break;
                            }
                            else
                            {
                                DBG_PRINT("done copy %d %% of total %d bytes.\r\n", (INT32U)(((offset + write_ret) * 100)/file_info.f_size), (INT32U)file_info.f_size);
                            }
                            offset += write_ret;
                        }
                    }
                    else
                        DBG_PRINT("read %d fail. ret=%d offset=%d\r\n", read_size, read_ret, (INT32U)offset);
                    remain_f_size -= read_size;
                } // end while
                vPortFree(buf);
            }
            fs_close(fd_out);
        }
        fs_close(fd);
    }
    ret = _deviceunmount(drv);
    DBG_PRINT("DONE UNMOUNTED SD COMPLETED. ATTETION: MUST DO BEFORE PLUGOUT SD CARD. ret=%d !!!\r\n", ret);
}

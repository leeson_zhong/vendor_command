#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cmsis_os.h"
#include "project.h"
#include "gplib.h"
#include "drv_l2_h264dec.h"
#include "drv_l2_h264enc.h"
#include "drv_l2_display.h"
#include "drv_l1_pscaler.h"

#define DISPLAY_OUTPUT  1
#define DISPLAY_DEVICE	DISDEV_TFT
#define BS_BUF_SIZE 0x500000
#define CIRCULAR_BUFFER 1
#define OUTPUT_TO_FILE  0
#define PIPELINE_ENCODE 1
#define OUTPUT_DIV  0
static INT16U frm_w = 352, frm_h = 288;
static INT16S fd, fd_out;
static INT8U *PscalerBufA;
static INT8U *PscalerBufB;
static int pscaler_done = 0;

extern INT32S vid_dec_parser_h264_bit_stream(INT8U *pdata, INT32U bs_size, INT16U *width, INT16U *height);

static void psca_callback(INT32U PScaler_Event)
{
    if (PScaler_Event & PIPELINE_SCALER_STATUS_BUF_A_DONE)
        pscaler_done = 1;
    if (PScaler_Event & PIPELINE_SCALER_STATUS_BUF_B_DONE)
        pscaler_done = 1;
    if (PScaler_Event & PIPELINE_SCALER_STATUS_MB420_FRAME_DONE)
        pscaler_done = 1;
}

static void PscaSetting(INT16S ow, INT16S oh)
{
    INT32U widthFactor,heightFactor;
    INT32U ww, hh;

    ww = frm_w;
    hh = frm_h;
#if OUTPUT_DIV
        ww = frm_w/OUTPUT_DIV;
        hh = frm_h/OUTPUT_DIV;
#endif

	widthFactor = ((ww*65536)/ow);
	heightFactor = ((hh*65536)/oh);

	drv_l1_pscaler_clk_ctrl(0,1);
    drv_l1_pscaler_init(0);
    drv_l1_pscaler_interrupt_set(0,PIPELINE_SCALER_INT_ENABLE_ALL);
    drv_l1_pscaler_callback_register(0, psca_callback);
    //drv_l1_pscaler_callback_register(0, NULL);
    drv_l1_pscaler_input_pixels_set(0, ww, hh);
    drv_l1_pscaler_output_pixels_set(0, widthFactor, ow, heightFactor, oh);
    drv_l1_pscaler_input_X_start_set(0, 0);
    drv_l1_pscaler_input_Y_start_set(0, 0);
    drv_l1_pscaler_output_fifo_line_set(0, oh, 0);
    drv_l1_pscaler_input_format_set(0, PIPELINE_SCALER_INPUT_FORMAT_VYUY);
#if DISPLAY_OUTPUT
    drv_l1_pscaler_output_format_set(0, PIPELINE_SCALER_OUTPUT_FORMAT_RGB565);
#else
    drv_l1_pscaler_output_format_set(0, PIPELINE_SCALER_OUTPUT_FORMAT_VYUY);
#endif
    drv_l1_pscaler_input_source_set(0, PIPELINE_SCALER_INPUT_SOURCE_MB420);
    drv_l1_pscaler_output_A_buffer_set(0, (INT32U)PscalerBufA);
    drv_l1_pscaler_output_B_buffer_set(0, (INT32U)PscalerBufB);

    drv_l1_pscaler_start(0);
}

static void PscalerOut()
{
	static INT8U* PscalerBuf = NULL;
	INT32S write_ret;

    while(!pscaler_done)
        osDelay(1);

    pscaler_done = 0;
#if OUTPUT_TO_FILE
	PscalerBuf = (PscalerBuf == PscalerBufA) ? PscalerBufB : PscalerBufA;

	cache_invalid_range(PscalerBuf,frm_w*frm_h*2);
	//DBG_PRINT("%p:\t%x\r\n", PscalerBuf, PscalerBuf[0]);
    write_ret = fs_write(fd_out, (INT32U)PscalerBuf, frm_w*frm_h*2);
    if(write_ret != frm_w*frm_h*2){
        DBG_PRINT("Pscaler write to sd err\r\n");
    }
#endif
}

static void YUYV_To_420MB(unsigned int in, unsigned int out, int width, int height)
{
    INT32U ret;

    R_SYSTEM_MISC_CTRL4 |= (0x0F|(0<<4));
    cache_drain_range(in, width*height*2);
	drv_l1_pscaler_clk_ctrl(0,1);
    drv_l1_pscaler_init(0);
    drv_l1_pscaler_callback_register(0, psca_callback);
    ret = drv_l1_pscaler_input_pixels_set(0, width, height);
    ret = drv_l1_pscaler_output_pixels_set(0, 0x10000, width, 0x10000, height);
    ret = drv_l1_pscaler_input_X_start_set(0, 0);
    ret = drv_l1_pscaler_input_Y_start_set(0, 0);
    //ret = drv_l1_pscaler_output_A_buffer_set(0, out);
    ret = drv_l1_pscaler_output_fifo_line_set(0, height, 0);
    ret = drv_l1_pscaler_interrupt_set(0, PIPELINE_SCALER_INT_ENABLE_MB420_FRAME_END);
    //ret = drv_l1_pscaler_interrupt_set(0, 0);
    ret = drv_l1_pscaler_input_format_set(0, PIPELINE_SCALER_INPUT_FORMAT_VYUY);
    ret = drv_l1_pscaler_output_format_set(0, PIPELINE_SCALER_OUTPUT_FORMAT_MB420);
    ret = drv_l1_pscaler_input_source_set(0, PIPELINE_SCALER_INPUT_SOURCE_DRAM);
    ret = drv_l1_pscaler_input_buffer_set(0, in);
    ret = drv_l1_pscaler_H264_output_A_buffer_set(0, out);
    //ret = PScaler_H264_Output_B_Buffer_Set(0, out);

    drv_l1_pscaler_start(0);
}

static void wait_YUV_To_420MB()
{
    while(!pscaler_done)
        osDelay(1);

    pscaler_done = 0;

    drv_l1_pscaler_stop(0);
    drv_l1_pscaler_clk_ctrl(0, 0);
}

void H264_Decoder_Demo()
{
    INT16S ret;
    struct sfn_info file_info;
    char *in_file_name     = "K:\\akiyo_cif_golden.264";
    char *out_file_name ="K:\\akiyo_out.bin";
    int frame_num = 120;
    INT32S  file_size, buf_size;
    INT32U TotalTick=0, t1=0, t2, fps;
    INT8U* src;
    int i, rp = 0, wp = 0, read_cnt = 0;
    int frame_count = 0;
    void* dec;
    INT16U disp_width, disp_height;
    gp_h264dec_cfg cfg;

/*==== Mount Disk ====*/
    ret = _devicemount(FS_SD2);
    if (ret)
    {
      DBG_PRINT("2 Mount Disk Fail[%d]\r\n", FS_SD2);
      return;
    }
/*==== Set Output ====*/
#if DISPLAY_OUTPUT
    drv_l2_display_init();
    drv_l2_display_get_size(DISPLAY_DEVICE, &disp_width, &disp_height);
    DBG_PRINT("display w=%d, h=%d\r\n", disp_width, disp_height);
    drv_l2_display_start(DISPLAY_DEVICE, DISP_FMT_RGB565);
    PscalerBufA = (INT8U *)((gp_malloc_align(disp_width*disp_height*2+0xF, 32))&(~0xF));
    PscalerBufB = PscalerBufA;
    drv_l2_display_update(DISPLAY_DEVICE, (INT32U)PscalerBufA);
#elif OUTPUT_TO_FILE
    PscalerBufA = (INT8U *)((gp_malloc_align(frm_w*frm_h*2+0xF, 32))&(~0xF));
    PscalerBufB = (INT8U *)((gp_malloc_align(frm_w*frm_h*2+0xF, 32))&(~0xF));
#else
    PscalerBufA = 0x50000000;
    PscalerBufB = 0x50000000;
#endif
/*==== Open Files ====*/
    fd = fs_open(in_file_name, O_RDONLY);
    if (fd < 0){
        DBG_PRINT("fail to open input file %s\r\n", in_file_name);
        goto end_test;
    }
#if OUTPUT_TO_FILE
    fd_out = fs_open(out_file_name, O_CREAT|O_TRUNC|O_WRONLY);
    if (fd_out < 0){
        DBG_PRINT("fail to open output file %s\r\n", out_file_name);
        goto end_test;
    }
#endif

    sfn_stat(fd, &file_info);
    file_size = file_info.f_size;

    DBG_PRINT("\r\nH.264 decode start. %s size=%d\r\n", in_file_name, file_size);
/*==== Star Read Bitstream ====*/
#if CIRCULAR_BUFFER
    if (file_size > BS_BUF_SIZE)
        buf_size = BS_BUF_SIZE;
    else
        buf_size = file_size;

    buf_size = (buf_size + 0x3F) & ~0x3F; // 64 byte alignment
    src = (INT8U*)gp_malloc_align(buf_size);

    fs_read(fd, (INT32U)src, buf_size);
    wp = read_cnt = buf_size;
#else
    if (file_size)
        src = (INT8U*)gp_malloc_align(file_size);

    fs_read(fd, (INT32U)src, file_size);
    buf_size = file_size;
#endif
/*==== Parse Bitstream  ====*/
    //vid_dec_parser_h264_bit_stream(src, buf_size, &frm_w, &frm_h);

/*==== Initial H.264 Decoder ====*/
    cfg.width = frm_w;
    cfg.height = frm_h;
    cfg.out_div = OUTPUT_DIV;
    cfg.HDMI = 0;
    dec = gp_h264dec_init(&cfg);

/*==== Set Pscaler Output ====*/
#if DISPLAY_OUTPUT
    PscaSetting(disp_width, disp_height);
#else
    PscaSetting(frm_w, frm_h);
#endif
/*==== Start Decode frame by frame ====*/
	{
		int read_size, pktstart = -1, pktend = -1, i, idx = 0, zero_count = 0;
		int header_ready = 0;
		int bufsize = 0;
		int pid = 0;

		for(i = 0; i < file_size;)
		{
			gp_h264dec_in param;
			int stream_len;

			param.BitstreamAddr =  (INT32U)(src + rp); // MUST be 8 byte alignment
			param.BitstreamBaseAddr =  (INT32U)src; //MUST be 64 byte alignment
			param.BitstreamMaxAddr =  (INT32U)(src + buf_size); //MUST be 64 byte alignment

			//while(xTaskGetTickCount() - t1 < 33)
				//osDelay(1);
			t1 = xTaskGetTickCount();
			gp_h264dec_decode(dec, &param);
			stream_len = gp_h264dec_decode_wait(dec, NULL, 0);
			stream_len &= ~0x7; //!! IMPORTANT !! align stream length to 8 byte.
#if DISPLAY_OUTPUT == 0
            if (frame_count > 0) // 1st frame will be output after decode 2nd frame.
                PscalerOut();
#endif
			t2 = xTaskGetTickCount();
			TotalTick += (t2-t1);
            if (frame_count % 90 == 0)
                DBG_PRINT("decode pkt[%d:%d](%d)\r\n", i, i+stream_len, pid++);

			i += stream_len;
			rp += stream_len;
#if CIRCULAR_BUFFER
			if (rp >= BS_BUF_SIZE)
                rp = rp - BS_BUF_SIZE;

			if ( (read_cnt < file_size) &&
                (((wp >= rp) && (wp -rp < 0x4B000)) ||
                ((wp < rp) && ((wp + BS_BUF_SIZE - rp) < 0x4B000))))
			{
                int read_size;
                if (wp >= rp)
                {
                    read_size = fs_read(fd, (INT32U)(src+wp), BS_BUF_SIZE - wp);
                    wp = wp + read_size;
                    read_cnt += read_size;
                    if (wp  >= BS_BUF_SIZE)
                        wp = 0;
                    //DBG_PRINT("read %d bytes, set wp to %d\r\n", read_size, wp);
                }
                if (wp < rp)
                {
                    read_size = fs_read(fd, (INT32U)(src+wp), rp-wp);
                    wp = wp + read_size;
                    read_cnt += read_size;
                    //DBG_PRINT("read %d bytes, set wp to %d\r\n", read_size, wp);
                }
			}
#endif
			frame_count++;
			if (frame_num && frame_count >= frame_num)
                break;
		}
	}

    //Flush out the last Frame.
    {
        gp_h264dec_in param;

		param.BitstreamAddr =  0;
		param.BitstreamBaseAddr =  0;
		param.BitstreamMaxAddr =  0;

		gp_h264dec_decode(dec, &param);
    }

#if DISPLAY_OUTPUT == 0
        PscalerOut();
#endif
/*==== End ====*/
  	DBG_PRINT(" H.264 decode done (%d frames)\r\n", frame_count);
  	fps = (frame_count*1000*100)/TotalTick;
    DBG_PRINT(" tick is %d, fps = %d.%02d \r\n", TotalTick, fps/100, fps - 100*(fps/100));
  	//DBG_PRINT(" tick is %d, fps = %f\r\n",TotalTick, (float)(frame_count*1000)/(float)TotalTick);

  	drv_l1_pscaler_stop(0);
  	drv_l1_pscaler_clk_ctrl(0, 0);

end_test:
    fs_close(fd);
#if OUTPUT_TO_FILE
    fs_close(fd_out);
    gp_free(PscalerBufA);
    gp_free(PscalerBufB);
#elif DISPLAY_OUTPUT
    gp_free(PscalerBufA);
#endif
    gp_h264dec_release(dec);
    gp_free(src);
  	ret = _deviceunmount(FS_SD2);
}

void H264_Encoder_Demo()
{
    int i;
    INT16S ret;
    int width = 352, height = 288, yuv_size;
    INT16S fd, fd_pat, fd_out;
    struct sfn_info file_info;
    int read_size, frame_count = 0, frame_num = 0, frame_size;
    char *in_yuv_name     = "K:\\akiyo_cif_422.yuv";
    char *out_file_name ="K:\\akiyo_out.264";
    INT32U yuv_frame, bitstream_out, mb_frame[2], tmp;
    int stream_size;
    void* handle;
    gp_h264_cfg cfg;
    gp_h264enc_in in;
    gp_h264enc_rate_ctrl rccfg;
    INT32U clk_1, clk_2, clk_3;
    INT32U encode_time = 0, total_size = 0;
    INT32U fps;

    ret = _devicemount(FS_SD2);
    if (ret)
    {
      DBG_PRINT("2 Mount Disk Fail[%d]\r\n", FS_SD2);
      return;
    }
    DBG_PRINT("open %s\r\n", in_yuv_name);
    fd = fs_open(in_yuv_name, O_RDONLY);

    if (fd >= 0)
    {
        sfn_stat(fd, &file_info);
        yuv_size = file_info.f_size;
    }
    else
    {
        DBG_PRINT("Open Fail\r\n");
        return;
    }

    DBG_PRINT("open %s\r\n", out_file_name);
    fd_out = fs_open(out_file_name, O_CREAT|O_TRUNC|O_WRONLY);

    if (fd_out < 0)
    {
        DBG_PRINT("Open Fail\r\n");
        fs_close(fd);
        return;
    }

    read_size = 0;

    cfg.width = width;
    cfg.height = height;
    cfg.level = 31;

    handle = gp_h264enc_init(&cfg);

    bitstream_out = (INT32U)gp_malloc_align(0x100000, 4);
    yuv_frame = (INT32U)gp_malloc_align(width*height*2 + 0xf, 4);
    mb_frame[0] = (INT32U)gp_malloc_align(width*height*2 + 0xf, 4);
#if PIPELINE_ENCODE
    mb_frame[1] = (INT32U)gp_malloc_align(width*height*2 + 0xf, 4);
#endif
    frame_size = width*height*2;

    rccfg.bitPerSeconds = 0;//7200*1000;
    rccfg.frameRate = 30;
    rccfg.gopLen = 15;
    rccfg.qpHdr = 27;//32;
    rccfg.qpMax = 51;
    rccfg.qpMin = 18;

    gp_h264enc_set_rc(handle, &rccfg);

    DBG_PRINT("Start H.264 encoding\r\n");

    while(read_size < yuv_size)
    {
        fs_read(fd, (INT32U)yuv_frame, frame_size);
        read_size += frame_size;
        clk_1 = xTaskGetTickCount();
        YUYV_To_420MB((INT32U)yuv_frame, (INT32U)mb_frame[0], width, height);
#if PIPELINE_ENCODE
        if (read_size == frame_size)
        {
            wait_YUV_To_420MB();
            tmp = mb_frame[1];
            mb_frame[1] = mb_frame[0];
            mb_frame[0] = tmp;
            continue;
        }
        in.RawFrameAddr = mb_frame[1];
#else
        wait_YUV_To_420MB();
        in.RawFrameAddr = mb_frame[0];
#endif
        in.BitstreamOutAddr = bitstream_out;

        if (frame_count % 15 == 0)
            in.PictureType = 0;
        else
            in.PictureType = 1;

        if (frame_count == 0)
            in.InsertHdr = 1;
        else
            in.InsertHdr = 0;

        gp_h264enc_encode(handle, &in);
        stream_size = gp_h264enc_wait(handle, 0);
#if PIPELINE_ENCODE
        wait_YUV_To_420MB();
        tmp = mb_frame[1];
        mb_frame[1] = mb_frame[0];
        mb_frame[0] = tmp;
#endif
        clk_3 = xTaskGetTickCount();

        cache_invalid_range(bitstream_out, stream_size);
        fs_write(fd_out, (INT32U)bitstream_out, stream_size);
        encode_time += (clk_3-clk_1);
        total_size += stream_size;
        //DBG_PRINT("write frame %d size = 0x%x time = %d \r\n", frame_count, stream_size, clk_3 - clk_1);
        DBG_PRINT(".");
        frame_count++;

        if (frame_num && frame_count >= frame_num)
            break;

    }

    fps = (frame_count*1000*100)/encode_time;
    //DBG_PRINT("\r\nfps = %f\r\n", fps);
    DBG_PRINT("fps = %d.%02d \r\n", fps/100, fps - 100*(fps/100));
    DBG_PRINT("data rate = %d Kbps\r\n", ((total_size * 6) / (frame_count* 25)));

    fs_close(fd);
    fs_close(fd_out);
    if (yuv_frame)        gp_free((void*)yuv_frame);
    if (mb_frame)         gp_free((void*)mb_frame[0]);
#if PIPELINE_ENCODE
    if (mb_frame)         gp_free((void*)mb_frame[1]);
#endif
    if (bitstream_out)    gp_free((void*)bitstream_out);

    gp_h264enc_release(handle);

    _deviceunmount(FS_SD2);
}

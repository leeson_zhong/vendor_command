#include "GPUSBMassStorageStdAgentDll.h"
#include "GPUSBMassStorageStdAgentDllExport.h"
#include "Define.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
C_GPUSBMassStorageStdAgentDll::C_GPUSBMassStorageStdAgentDll(void)
: m_hDll(NULL)
{
}

C_GPUSBMassStorageStdAgentDll::~C_GPUSBMassStorageStdAgentDll(void)
{
	if (NULL != m_hDll)
	{
		::FreeLibrary(m_hDll);
		m_hDll = NULL;
	}
}
//------------------------------------------------------------------------------------------------
BOOL C_GPUSBMassStorageStdAgentDll::Enumerate( int i32TableType,  int i32AuthorizeType,  int i32DeviceType )
{
	PFN_Enumerate pfnHandle = (PFN_Enumerate)GetProcAddress(GPUSB_FUN_ENUMERATE);
	if (NULL == pfnHandle)
		return FALSE;

	return (*pfnHandle)(i32TableType, i32AuthorizeType, i32DeviceType);
}
//------------------------------------------------------------------------------------------------
BOOL C_GPUSBMassStorageStdAgentDll::SetCustomerKey( LPCTSTR ptszCustomerKey )
{
	PFN_SetCustomerKey pfnHandle = (PFN_SetCustomerKey)GetProcAddress(GPUSB_FUN_SETCUSTOMERKEY);
	if (NULL == pfnHandle)
		return FALSE;

	return (*pfnHandle)(ptszCustomerKey);
}
//------------------------------------------------------------------------------------------------
int C_GPUSBMassStorageStdAgentDll::SetScsiVidPid( LPCTSTR ptszVID, LPCTSTR ptszPID )
{
	PFN_SetScsiVidPid pfnHandle = (PFN_SetScsiVidPid)GetProcAddress(GPUSB_FUN_SETSCSIVIDPID);
	if (NULL == pfnHandle)
		return Err_Invalid_Handle;

	return (*pfnHandle)(ptszVID, ptszPID);
}
//------------------------------------------------------------------------------------------------
int C_GPUSBMassStorageStdAgentDll::SetUSBVidPid( WORD wVID, WORD wPID, WORD wDeviceVer )
{
	PFN_SetUSBVidPid pfnHandle = (PFN_SetUSBVidPid)GetProcAddress(GPUSB_FUN_SETUSBVIDPID);
	if (NULL == pfnHandle)
		return Err_Invalid_Handle;

	return (*pfnHandle)(wVID, wPID, wDeviceVer);
}
//------------------------------------------------------------------------------------------------
int C_GPUSBMassStorageStdAgentDll::GetDeviceCount()
{
	PFN_GetDeviceCount pfnHandle = (PFN_GetDeviceCount)GetProcAddress(GPUSB_FUN_GETDEVICECOUNT);
	if (NULL == pfnHandle)
		return 0;

	return (*pfnHandle)();
}
//------------------------------------------------------------------------------------------------
LPCTSTR C_GPUSBMassStorageStdAgentDll::GetDeviceSerial( int i32DeviceIndex )
{
	PFN_GetDeviceSerial pfnHandle = (PFN_GetDeviceSerial)GetProcAddress(GPUSB_FUN_GETDEVICESERIAL);
	if (NULL == pfnHandle)
		return _T("");

	return (*pfnHandle)(i32DeviceIndex);
}
//------------------------------------------------------------------------------------------------
BOOL C_GPUSBMassStorageStdAgentDll::OpenDevice( LPCTSTR ptszSerial )
{
	PFN_OpenDevice pfnHandle = (PFN_OpenDevice)GetProcAddress(GPUSB_FUN_OPENDEVICE);
	if (NULL == pfnHandle)
		return FALSE;

	return (*pfnHandle)(ptszSerial);
}
//------------------------------------------------------------------------------------------------
BOOL C_GPUSBMassStorageStdAgentDll::CloseDevice( LPCTSTR ptszSerial )
{
	PFN_CloseDevice pfnHandle = (PFN_CloseDevice)GetProcAddress(GPUSB_FUN_CLOSEDEIVCE);
	if (NULL == pfnHandle)
		return FALSE;

	return (*pfnHandle)(ptszSerial);
}
//------------------------------------------------------------------------------------------------
int C_GPUSBMassStorageStdAgentDll::CmdPassThrough( LPCTSTR ptszSerial, BYTE* pbyCmd, BOOL bWriteData, BYTE* pbyData, UINT u32DataLen )
{
	PFN_CmdPassThrough pfnHandle = (PFN_CmdPassThrough)GetProcAddress(GPUSB_FUN_CMDPASSTHROUGH);
	if (NULL == pfnHandle)
		return Err_Invalid_Handle;

	return (*pfnHandle)(ptszSerial,pbyCmd,bWriteData,pbyData,u32DataLen);
}
//------------------------------------------------------------------------------------------------
int C_GPUSBMassStorageStdAgentDll::CmdPassThroughEx( LPCTSTR ptszSerial, BYTE* pbyCmd, BOOL bWriteData, BYTE* pbyData, UINT *pu32DataLen )
{
	return CmdPassThrough(ptszSerial,pbyCmd,bWriteData,pbyData,*pu32DataLen);
}
//------------------------------------------------------------------------------------------------
int C_GPUSBMassStorageStdAgentDll::GetLastIOErrorCode( LPCTSTR ptszSerial, DWORD* pdwIOErrorCode )
{
	PFN_GetLastIOErrorCode pfnHandle = (PFN_GetLastIOErrorCode)GetProcAddress(GPUSB_FUN_GETLASTIOERRORCODE);
	if (NULL == pfnHandle)
		return Err_Invalid_Handle;

	return (*pfnHandle)(ptszSerial,pdwIOErrorCode);
}
//------------------------------------------------------------------------------------------------
int C_GPUSBMassStorageStdAgentDll::GetLastSenseKeyValue( LPCTSTR ptszSerial, DWORD* pdwSenseKeyValue )
{
	PFN_GetLastSenseKeyValue pfnHandle = (PFN_GetLastSenseKeyValue)GetProcAddress(GPUSB_FUN_GETLASTSENSEKEYVALUE);
	if (NULL == pfnHandle)
		return Err_Invalid_Handle;

	return (*pfnHandle)(ptszSerial,pdwSenseKeyValue);
}
//------------------------------------------------------------------------------------------------
int C_GPUSBMassStorageStdAgentDll::CheckDeviceConnection( LPCTSTR ptszSerial )
{
	PFN_CheckDeviceConnection pfnHandle = (PFN_CheckDeviceConnection)GetProcAddress(GPUSB_FUN_CHECKDEVICECONNECTION);
	if (NULL == pfnHandle)
		return Err_Invalid_Handle;

	return (*pfnHandle)(ptszSerial);
}
//------------------------------------------------------------------------------------------------
FARPROC C_GPUSBMassStorageStdAgentDll::GetProcAddress( LPCSTR pszProcName )
{
	if (NULL == m_hDll)
	{
#ifdef	_UNICODE
		m_hDll = ::LoadLibrary(_T("GPUSBMassStorageStdAgentDll_U.dll"));
#else
		m_hDll = ::LoadLibrary(_T("GPUSBMassStorageStdAgentDll.dll"));
#endif
		if (NULL == m_hDll)
			return NULL;
	}

	return ::GetProcAddress(m_hDll, pszProcName);
}
//------------------------------------------------------------------------------------------------
// MassStorageCmdShowHelp.cpp: implementation of the C_MassStorageCmdShowHelp class.
//
//////////////////////////////////////////////////////////////////////

#include <string.h>
#include "MassStorageCmdShowHelp.h"
#include "Define.h"

static TCHAR	s_atszShortHelp[][128] =
{
	_T("Command List\n"),
	_T("\n"),
	_T("[Help Command]\n"),
	_T("\tH, Help          : Show command list.\n"),
	_T("[Device Connection Commands]\n"),
	_T("\tSetScsiVidPid    : Set device SCSI Vid/Pid.\n"),
	_T("\tSetDefaultVidPid : Set device SCSI Vid/Pid to default (USB101-001A).\n"),
	_T("\tSetCdromVidPid   : Set device SCSI Vid/Pid to CDROM.\n"),
	_T("\tSetTableType     : Set Table type for device enumeration.\n"),
	_T("\tSetAuthType      : Set Authorization type for device enumeration.\n"),
	_T("\tSetDiskType      : Set Disk type for device enumeration.\n"),
	_T("\tSetSetCustomerKey: Set Customer key for device enumeration.\n"),
	_T("\tFindDevSN        : Find device(s) serial number.\n"),
	_T("\tSelDevSN         : Select device serial number for further actions.\n"),
	_T("[Reserved Commands]\n"),
	_T("\tWriteData        : Send write data command.\n"),
	_T("\tReadData         : Send read data command.\n"),
	_T("\tWriteFromFile    : Send write data command. Data is read from file.\n"),
	_T("\tReadToFile       : Send read data command. Data is written to file.\n"),
	_T("[Other Commands]\n"),
	_T("\tEraseFlash       : Erash flash.\n"),
	_T("\tReadFlash        : Read flash.\n"),
	_T("\tWriteFalsh       : Write flash.\n"),
	_T("\tAutoTest         : Auto test Erase/Write/Read flash repeately.\n"),

};

S_HelpHandlerInfo C_MassStorageCmdShowHelp::m_asHelpHandlerInfo[] =
{
	{ _T("H"),					_FUNC_PTR(C_MassStorageCmdShowHelp::HelpHelp)				},
	{ _T("Help"),				_FUNC_PTR(C_MassStorageCmdShowHelp::HelpHelp)				},
	{ _T("SetScsiVidPid"),		_FUNC_PTR(C_MassStorageCmdShowHelp::HelpSetScsiVidPid)		},
	{ _T("SetDefaultVidPid"),	_FUNC_PTR(C_MassStorageCmdShowHelp::HelpSetDefaultVidPid)	},
	{ _T("SetCdromVidPid"),		_FUNC_PTR(C_MassStorageCmdShowHelp::HelpSetCdromVidPid)		},
	{ _T("SetSetCustomerKey"),	_FUNC_PTR(C_MassStorageCmdShowHelp::HelpSetCustomerKey)		},
	{ _T("FindDevSN"),			_FUNC_PTR(C_MassStorageCmdShowHelp::HelpFindDevSN)			},
	{ _T("SelDevSN"),			_FUNC_PTR(C_MassStorageCmdShowHelp::HelpSelectDevSN)		},
	{ _T("WriteData"),			_FUNC_PTR(C_MassStorageCmdShowHelp::HelpWriteData)			},
	{ _T("ReadData"),			_FUNC_PTR(C_MassStorageCmdShowHelp::HelpReadData)			},
	{ _T("WriteFromFile"),		_FUNC_PTR(C_MassStorageCmdShowHelp::HelpWriteDataFromFile)	},
	{ _T("ReadToFile"),			_FUNC_PTR(C_MassStorageCmdShowHelp::HelpReadDataToFile)		},
	{ _T("EraseFlash"),			_FUNC_PTR(C_MassStorageCmdShowHelp::HelpEraseFlash)			},
	{ _T("ReadFlash"),			_FUNC_PTR(C_MassStorageCmdShowHelp::HelpReadFlash)			},
	{ _T("WriteFlash"),			_FUNC_PTR(C_MassStorageCmdShowHelp::HelpWriteFlash)			},
	{ _T("AutoTest"),			_FUNC_PTR(C_MassStorageCmdShowHelp::HelpAutoTest)			},
	{ _T("SetTableType"),		_FUNC_PTR(C_MassStorageCmdShowHelp::HelpSetTableType)	    },
	{ _T("SetAuthType"),		_FUNC_PTR(C_MassStorageCmdShowHelp::HelpSetAuthType)		},
	{ _T("SetDiskType"),		_FUNC_PTR(C_MassStorageCmdShowHelp::HelpSetDiskType)		},

};

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

C_MassStorageCmdShowHelp::C_MassStorageCmdShowHelp()
{

}

C_MassStorageCmdShowHelp::~C_MassStorageCmdShowHelp()
{

}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdShowHelp::ShowAll()
{
	int i32ShortHelpCount = sizeof(s_atszShortHelp) / (128 * sizeof(TCHAR));

	for (int i32Idx = 0; i32Idx < i32ShortHelpCount; i32Idx++)
	{
		::_tprintf(_T("%s"), s_atszShortHelp[i32Idx]);
	}

	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdShowHelp::Show(
	LPCTSTR	ptszCommand
	)
{
	int i32CommandCount = sizeof(m_asHelpHandlerInfo) / sizeof(S_HelpHandlerInfo);

	for (int i32Idx = 0; i32Idx < i32CommandCount; i32Idx++)
	{
		if (0 == ::_tcsicmp(ptszCommand, m_asHelpHandlerInfo[i32Idx].tszCommand))
		{
			if (NULL == m_asHelpHandlerInfo[i32Idx].pfnHelpHandler)
				continue;
			return (this->*m_asHelpHandlerInfo[i32Idx].pfnHelpHandler)();
		}
	}

	::_tprintf(_T("No Help for command '%s' found!!\n"), ptszCommand);
	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdShowHelp::HelpHelp()
{
	::_tprintf(_T("\tCommand: Help [Command]\n"));
	::_tprintf(_T("\t         H [Command]\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\tShow command list or command usage.\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\tParameters:\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\t\tCommand - Ommitted to show all supported command list.\n"));
	::_tprintf(_T("\t\t        - Command name to show corresponding command usage.\n"));
	::_tprintf(_T("\n"));

	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdShowHelp::HelpSetScsiVidPid()
{
	::_tprintf(_T("\tCommand: SetScsiVidPid <Vid> <Pid>\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\tSet device SCSI Vid/Pid.\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\tParameters:\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\t\tVid - SCSI Vendor ID.\n"));
	::_tprintf(_T("\t\tPid - SCSI Product ID.\n"));
	::_tprintf(_T("\n"));

	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdShowHelp::HelpSetDefaultVidPid()
{
	::_tprintf(_T("\tCommand: SetDefaultVidPid\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\tSet device SCSI Vid to \"%s\" and Pid to \"%s\".\n"),
			   USB101ScsiVid, USB101ScsiPid);
	::_tprintf(_T("\n"));

	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdShowHelp::HelpSetTableType()
{
	::_tprintf(_T("\tCommand: SetTableType <TableType>\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\t 0x00 for MASS_PRODUCTION\n"));
	::_tprintf(_T("\t 0x01 for REAL_TABLE\n"));
	::_tprintf(_T("\t Default Value is 0x00.\n"));
	::_tprintf(_T("\n"));
	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdShowHelp::HelpSetAuthType()
{
	::_tprintf(_T("\tCommand: SetAuthorizeType <AuthorizeType>\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\t 0x00 for STANDARD authorization.\n"));
	::_tprintf(_T("\t 0x01 for EASY authorization.\n"));
	::_tprintf(_T("\t Default Value is 0x00.\n"));
	::_tprintf(_T("\n"));
	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdShowHelp::HelpSetDiskType()
{
	::_tprintf(_T("\tCommand: SetDiskType <DiskType>\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\t 0x00 indicates NON_MOUNT_POINT\n"));
	::_tprintf(_T("\t 0x01 indicates MOUNT_POINT\n"));
	::_tprintf(_T("\t Default Value is 0x00\n"));
	::_tprintf(_T("\n"));
	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdShowHelp::HelpSetCdromVidPid()
{
	::_tprintf(_T("\tCommand: SetCdromVidPid\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\tSet device SCSI Vid to \"%s\" and Pid to \"%s\".\n"),
			   CDROMScsiVid, CDROMScsiPid);
	::_tprintf(_T("\n"));

	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdShowHelp::HelpSetCustomerKey()
{
	::_tprintf(_T("\tCommand: SetCustomerKey <Key>\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\tSet customer key.\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\tParameters:\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\t\tKey- Customer key.\n"));
	::_tprintf(_T("\n"));

	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdShowHelp::HelpFindDevSN()
{
	::_tprintf(_T("\tCommand: FindDevSN\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\tFind device(s) serial number.\n"));
	::_tprintf(_T("\n"));

	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdShowHelp::HelpSelectDevSN()
{
	::_tprintf(_T("\tCommand: SelDevSN <Index>\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\tSelect device serial number for further actions.\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\tParameters:\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\t\tIndex - Serial number index.\n"));
	::_tprintf(_T("\n"));

	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdShowHelp::HelpWriteData()
{
	::_tprintf(_T("\tCommand: WriteData <Main> <Sub> <Byte 0> <Byte 1> ... <Byte N>\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\tSend write data command.\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\tParameters:\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\t\tMain      - Main command. Ex. 0xE0\n"));
	::_tprintf(_T("\t\tSub       - Sub command. Ex. 0x00\n"));
	::_tprintf(_T("\t\tByte 0..N - Data bytes to write.\n"));
	::_tprintf(_T("\n"));

	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdShowHelp::HelpReadData()
{
	::_tprintf(_T("\tCommand: ReadData <Main> <Sub> <Byte Count>\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\tSend read data command.\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\tParameters:\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\t\tMain       - Main command. Ex: 0xD0\n"));
	::_tprintf(_T("\t\tSub        - Sub command. Ex: 0x00\n"));
	::_tprintf(_T("\t\tByte Count - Number of data bytes to read.\n"));
	::_tprintf(_T("\n"));

	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdShowHelp::HelpWriteDataFromFile()
{
	::_tprintf(_T("\tCommand: WriteFromFile <Main> <Sub> <File Name>\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\tSend write data command. Data is read from file.\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\tParameters:\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\t\tMain      - Main command.\n"));
	::_tprintf(_T("\t\tSub       - Sub command.\n"));
	::_tprintf(_T("\t\tFile Name - The file name which the data is read from.\n"));
	::_tprintf(_T("\n"));

	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdShowHelp::HelpReadDataToFile()
{
	::_tprintf(_T("\tCommand: ReadToFile <Main> <Sub> <Byte Count> <File Name>\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\tSend read data command. Data is written to file.\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\tParameters:\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\t\tMain       - Main command.\n"));
	::_tprintf(_T("\t\tSub        - Sub command.\n"));
	::_tprintf(_T("\t\tByte Count - Number of data bytes to read.\n"));
	::_tprintf(_T("\t\tFile Name  - The file name which the data is written to.\n"));
	::_tprintf(_T("\n"));

	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdShowHelp::HelpEraseFlash()
{
	::_tprintf(_T("\tCommand: EraseFlash <Block Size> <Total Size> <Start Address>\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\tErase flash.\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\tParameters:\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\t\tBlock Size    - Block size. \"4K\", \"32K\", or \"64K\"\n"));
	::_tprintf(_T("\t\tTotal Size    - Total erase size.\n"));
	::_tprintf(_T("\t\tStart Address - The start address to erase flash.\n"));
	::_tprintf(_T("\t\t                Should be a multiple of Block Size.\n"));
	::_tprintf(_T("\n"));

	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdShowHelp::HelpReadFlash()
{
	::_tprintf(_T("\tCommand: ReadFlash <Size> <PC file path> <Start Address>\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\tRead flash.\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\tParameters:\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\t\tSize          - Read size.\n"));
	::_tprintf(_T("\t\tPC file path  - File path in PC to write.\n"));
	::_tprintf(_T("\t\tStart Address - The start address to read flash.\n"));
	::_tprintf(_T("\t\t                Should be a multiple of 512.\n"));
	::_tprintf(_T("\n"));

	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdShowHelp::HelpWriteFlash()
{
	::_tprintf(_T("\tCommand: WriteFlash <PC file path> <Start Address>\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\tWrite flash.\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\tParameters:\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\t\tPC file path - File path in PC to read.\n"));
	::_tprintf(_T("\t\tStart Address - The start address to write flash.\n"));
	::_tprintf(_T("\t\t                Should be a multiple of 512.\n"));
	::_tprintf(_T("\n"));

	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdShowHelp::HelpAutoTest()
{
	::_tprintf(_T("\tCommand: AutoTest <PC file path> <Erase block size> <Repeat time>\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\tAuto test Erase/Write/Read flash repeatly.\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\tParameters:\n"));
	::_tprintf(_T("\n"));
	::_tprintf(_T("\t\tPC file path     - File path in PC to read.\n"));
	::_tprintf(_T("\t\tErase Block Size - Block size. \"4K\", \"32K\", or \"64K\"\n"));
	::_tprintf(_T("\t\tRepeat time      - Times to repeat the test process.\n"));
	::_tprintf(_T("\n"));

	return TRUE;
}

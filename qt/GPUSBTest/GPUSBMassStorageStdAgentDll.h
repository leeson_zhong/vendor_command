#ifndef GPUSBMASSSTORAGESTDAGENTDLL
#define GPUSBMASSSTORAGESTDAGENTDLL

#include "GPType.h"

class C_GPUSBMassStorageStdAgentDll
{
public:
	C_GPUSBMassStorageStdAgentDll(void);
	virtual ~C_GPUSBMassStorageStdAgentDll(void);


	BOOL  Enumerate(
		int		i32TableType,		// TABLE_TYPE_MASS_PRODUCTION(0x00) or TABLE_TYPE_REAL_TABLE(0x01)
		int		i32AuthorizeType,	// AUTHORIZE_TYPE_STANDARD(0x00) or AUTHORIZE_TYPE_EASY(0x01)
		int		i32DeviceType		// NON_MOUNT_POINT_TYPE(0x00) or MOUNT_POINT_TYPE(0x01)
		);

	BOOL	SetCustomerKey(
		LPCTSTR	ptszCustomerKey
		);

	int  SetScsiVidPid(
		LPCTSTR	ptszVID,
		LPCTSTR	ptszPID
		);

	int  SetUSBVidPid(
		WORD	wVID,
		WORD	wPID,
		WORD	wDeviceVer
		);

	int  GetDeviceCount(
		);

	LPCTSTR  GetDeviceSerial(
		int		i32DeviceIndex
		);

	BOOL  OpenDevice(
		LPCTSTR	ptszSerial
		);

	BOOL  CloseDevice(
		LPCTSTR	ptszSerial
		);

	int  CmdPassThrough(
		LPCTSTR	ptszSerial,
		BYTE*	pbyCmd, 
		BOOL	bWriteData, 
		BYTE*	pbyData, 
		UINT	u32DataLen
		);

	int  CmdPassThroughEx(
		LPCTSTR	ptszSerial,
		BYTE*	pbyCmd, 
		BOOL	bWriteData, 
		BYTE*	pbyData, 
		UINT	*pu32DataLen
		);


	int  GetLastIOErrorCode(
		LPCTSTR	ptszSerial,
		DWORD*	pdwIOErrorCode
		);

	int  GetLastSenseKeyValue(
		LPCTSTR	ptszSerial,
		DWORD*	pdwSenseKeyValue
		);

	int  CheckDeviceConnection(
		LPCTSTR	ptszSerial
		);

private:
	FARPROC	GetProcAddress(
		LPCSTR	pszProcName
		);

private:
	HMODULE	m_hDll;


};


#endif
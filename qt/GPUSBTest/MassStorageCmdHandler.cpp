// MassStorageCmdHandler.cpp: implementation of the C_MassStorageCmdHandler class.
//
//////////////////////////////////////////////////////////////////////

#include <string.h>
#include "MassStorageCmdHandler.h"
#include "CmdlineInfo.h"
#include "Define.h"
#include "GPUSB101SPIFlashCommand.h"
#include "VCMigrate.h"

#ifdef	WIN32
    //#include <atlbase.h>
#else
	#define	USES_CONVERSION
	#define	A2T(sz)			sz
	#define A2CT(sz)		sz
	#define _MAX_PATH		257
#endif

#define	USB_CMD_FIRST_GEN_TAG	'G'
#define	USB_CMD_SECOND_GEN_TAG	'P'

#define BUF_SIZE 512	//add by Eric for writing SPI flash

S_CmdHandlerInfo C_MassStorageCmdHandler::m_asCmdHandlerInfo[] =
{
	{ _T("H"),					_FUNC_PTR(C_MassStorageCmdHandler::CmdHelp)				},
	{ _T("Help"),				_FUNC_PTR(C_MassStorageCmdHandler::CmdHelp)				},
	{ _T("SetScsiVidPid"),		_FUNC_PTR(C_MassStorageCmdHandler::CmdSetScsiVidPid)	},
	{ _T("SetDefaultVidPid"),	_FUNC_PTR(C_MassStorageCmdHandler::CmdSetDefaultVidPid)	},
	{ _T("SetCdromVidPid"),		_FUNC_PTR(C_MassStorageCmdHandler::CmdSetCdromVidPid)	},
	{ _T("SetSetCustomerKey"),	_FUNC_PTR(C_MassStorageCmdHandler::CmdSetSetCustomerKey)},
	{ _T("FindDevSN"),			_FUNC_PTR(C_MassStorageCmdHandler::CmdFindDevSN)		},
	{ _T("SelDevSN"),			_FUNC_PTR(C_MassStorageCmdHandler::CmdSelectDevSN)		},
	{ _T("WriteData"),			_FUNC_PTR(C_MassStorageCmdHandler::CmdWriteData)		},
	{ _T("ReadData"),			_FUNC_PTR(C_MassStorageCmdHandler::CmdReadData)			},
	{ _T("WriteFromFile"),		_FUNC_PTR(C_MassStorageCmdHandler::CmdWriteDataFromFile)},
	{ _T("ReadToFile"),			_FUNC_PTR(C_MassStorageCmdHandler::CmdReadDataToFile)	},
	{ _T("EraseFlash"),			_FUNC_PTR(C_MassStorageCmdHandler::CmdEraseFlash)		},
	{ _T("ReadFlash"),			_FUNC_PTR(C_MassStorageCmdHandler::CmdReadFlash)		},
	{ _T("WriteFlash"),			_FUNC_PTR(C_MassStorageCmdHandler::CmdWriteFlash)		},
	{ _T("AutoTest"),			_FUNC_PTR(C_MassStorageCmdHandler::CmdAutoTest)			},
	{ _T("SetTableType"),		_FUNC_PTR(C_MassStorageCmdHandler::CmdSetTableType)		},
	{ _T("SetAuthType"),		_FUNC_PTR(C_MassStorageCmdHandler::CmeSetAuthType)      },
	{ _T("SetDiskType"),		_FUNC_PTR(C_MassStorageCmdHandler::CmdSetDiskType)		},
};

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

C_MassStorageCmdHandler::C_MassStorageCmdHandler()
{
	::VCM_tcsncpy(m_tszScsiVid, USB101ScsiVid, 9);
	::VCM_tcsncpy(m_tszScsiPid, USB101ScsiPid, 16);
	::VCM_tcsncpy(m_tszCusKey, DEFAULT_CUSKEY, 39);

	m_tszCusKey[39] = 0;
	m_tszSerial[0] = 0;

	m_i32DiskType  = 0x00;	
	m_i32AuthType  = USB101AuthType;
	m_i32TableType = 0x00;
}

C_MassStorageCmdHandler::~C_MassStorageCmdHandler()
{

}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdHandler::IsQuitCommand(
	LPCTSTR	ptszCommandLine
	)
{
	C_CmdlineInfo CmdLineInfo(ptszCommandLine);
	LPCTSTR ptszCommand = CmdLineInfo.GetCommand();
	
	if (NULL == ptszCommand)
		return FALSE;

	return ((0 == ::_tcsicmp(ptszCommand, _T("quit"))) ||
			(0 == ::_tcsicmp(ptszCommand, _T("exit"))));
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdHandler::ExecuteCommand(
	LPCTSTR	ptszCommandLine
	)
{
	C_CmdlineInfo CmdLineInfo(ptszCommandLine);
	LPCTSTR ptszCommand = CmdLineInfo.GetCommand();
	
	if (NULL == ptszCommand)
		return FALSE;

	int i32CommandCount = sizeof(m_asCmdHandlerInfo) / sizeof(S_CmdHandlerInfo);

	for (int i32Idx = 0; i32Idx < i32CommandCount; i32Idx++)
	{
		if (0 == ::_tcsicmp(ptszCommand, 
							m_asCmdHandlerInfo[i32Idx].tszCommand))
		{
			if (NULL == m_asCmdHandlerInfo[i32Idx].pfnCmdHandler)
				continue;
			if (!(this->*m_asCmdHandlerInfo[i32Idx].pfnCmdHandler)(&CmdLineInfo))
				return m_ShowHelp.Show(CmdLineInfo.GetCommand());
			return TRUE;
		}
	}

	::_tprintf(_T("Unknown command!!\n"));
	return FALSE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdHandler::CmdHelp(
	C_CmdlineInfo*	pCmdLineInfo
	)
{
	if (0 == pCmdLineInfo->GetParamCount())
		return m_ShowHelp.ShowAll();
	else
		return m_ShowHelp.Show(pCmdLineInfo->GetParamItem(0));
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdHandler::CmdSetScsiVidPid(
	C_CmdlineInfo*	pCmdLineInfo
	)
{
	if (pCmdLineInfo->GetParamCount() < 2)
	{
		::_tprintf(_T("Incorrect parameters!!\n\n"));
		return FALSE;
	}

	::VCM_tcsncpy(m_tszScsiVid, pCmdLineInfo->GetParamItem(0), 9);
    ::VCM_tcsncpy(m_tszScsiPid, pCmdLineInfo->GetParamItem(1), 20);

	::_tprintf(_T("Scsi Vid set to \"%s\", Pid set to \"%s\".\n"),
			   m_tszScsiVid, m_tszScsiPid);

	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdHandler::CmdSetDefaultVidPid(
	C_CmdlineInfo*	pCmdLineInfo
	)
{
	::VCM_tcsncpy(m_tszScsiVid, USB101ScsiVid, 9);
	::VCM_tcsncpy(m_tszScsiPid, USB101ScsiPid, 16);

	m_i32DiskType  = 0x00;	
	m_i32AuthType  = USB101AuthType;
	m_i32TableType = 0x00;

	::_tprintf(_T("Scsi Vid set to \"%s\", Pid set to \"%s\".\n"),
			   m_tszScsiVid, m_tszScsiPid);

	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdHandler::CmdSetCdromVidPid(
	C_CmdlineInfo*	pCmdLineInfo
	)
{
	::VCM_tcsncpy(m_tszScsiVid, CDROMScsiVid, 9);
	::VCM_tcsncpy(m_tszScsiPid, CDROMScsiPid, 16);

	::_tprintf(_T("Scsi Vid set to \"%s\", Pid set to \"%s\".\n"),
			   m_tszScsiVid, m_tszScsiPid);

	return TRUE;
}
//------------------------------------------------------------------------------------------------
BOOL C_MassStorageCmdHandler::CmdSetSetCustomerKey( C_CmdlineInfo* pCmdLineInfo )
{
	if (pCmdLineInfo->GetParamCount() < 1)
	{
		::_tprintf(_T("Incorrect parameters!!\n\n"));
		return FALSE;
	}

	::VCM_tcsncpy(m_tszCusKey, pCmdLineInfo->GetParamItem(0), 39);
	m_tszCusKey[39] = 0;

	::_tprintf(_T("Set customer key to \"%s\".\n"),m_tszCusKey);

}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdHandler::CmdSetTableType(
			C_CmdlineInfo*	pCmdLineInfo
			)
{
	if(pCmdLineInfo->GetParamCount() > 1)
	{
		::_tprintf(_T("Incorrect parameters!!\n\n"));
		return FALSE;
	}

	return pCmdLineInfo->GetParamItemInt(0, &m_i32TableType);
}

BOOL C_MassStorageCmdHandler::CmeSetAuthType(
			C_CmdlineInfo*	pCmdLineInfo
			)
{
	if(pCmdLineInfo->GetParamCount() > 1)
	{
		::_tprintf(_T("Incorrect parameters!!\n\n"));
		return FALSE;
	}

	return pCmdLineInfo->GetParamItemInt(0, &m_i32AuthType);

}

BOOL C_MassStorageCmdHandler::CmdSetDiskType(
			C_CmdlineInfo*	pCmdLineInfo
			)
{
	if(pCmdLineInfo->GetParamCount() > 1)
	{
		::_tprintf(_T("Incorrect parameters!!\n\n"));
		return FALSE;
	}

	return pCmdLineInfo->GetParamItemInt(0, &m_i32DiskType);
}

BOOL C_MassStorageCmdHandler::CmdFindDevSN(
	C_CmdlineInfo*	pCmdLineInfo
	)
{
	::_tprintf(_T("Find devices serial number with:\n"));
	::_tprintf(_T("\tSCSI Vid      : %s.\n"), m_tszScsiVid);
	::_tprintf(_T("\tSCSI Pid      : %s.\n"), m_tszScsiPid);
	::_tprintf(_T("\n"));

	if (!m_DeviceAgent.SetScsiVidPid(m_tszScsiVid, m_tszScsiPid))
	{
		::_tprintf(_T("Set SCSI Vid/Pid failed!!\n"));
		return TRUE;
	}

	if(m_i32AuthType == AUTHORIZE_TYPE_SECURE_ID)
	{
		m_DeviceAgent.SetCustomerKey(m_tszCusKey);
	}

	if (!m_DeviceAgent.Enumerate(m_i32TableType, m_i32AuthType, m_i32DiskType))
	{
		::_tprintf(_T("Enumerate devices failed!!\n"));
		return TRUE;
	}

	int i32Count = m_DeviceAgent.GetDeviceCount();
	if (0 == i32Count)
	{
		::_tprintf(_T("No devices found.\n"));
		return TRUE;
	}

	::_tprintf(_T("%d device(s) found:\n"), i32Count);
	for (int i32Idx = 0; i32Idx < i32Count; i32Idx++)
	{
		::_tprintf(_T("[%02d] SN: %s\n"), i32Idx, 
				   m_DeviceAgent.GetDeviceSerial(i32Idx));
	}

	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdHandler::CmdSelectDevSN(
	C_CmdlineInfo*	pCmdLineInfo
	)
{
	if (pCmdLineInfo->GetParamCount() < 1)
	{
		::_tprintf(_T("Incorrect parameters!!\n\n"));
		return FALSE;
	}

	int i32Idx = -1;
	pCmdLineInfo->GetParamItemInt(0, &i32Idx);

	LPCTSTR ptszSerial = m_DeviceAgent.GetDeviceSerial(i32Idx);
	if (!m_DeviceAgent.OpenDevice(ptszSerial))
	{
		::_tprintf(_T("Failed to set device serial number!!\n"));
		return TRUE;
	}

	::_tprintf(_T("SN: \"%s\" is selected.\n"), ptszSerial);

	::_tcsncpy(m_tszSerial, ptszSerial, 17);
	m_tszSerial[16] = 0;

	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdHandler::CmdWriteData(
	C_CmdlineInfo*	pCmdLineInfo
	)
{
	DWORD dwParamCount = pCmdLineInfo->GetParamCount();
	if (dwParamCount < 2)
	{
		::_tprintf(_T("Incorrect parameters!!\n\n"));
		return FALSE;
	}

	BYTE abyCommand[16];
	::memset(abyCommand, 0, 16);

	int i32Value = -1;
	pCmdLineInfo->GetParamItemInt(0, &i32Value);
	abyCommand[0] = (BYTE)i32Value;

	pCmdLineInfo->GetParamItemInt(1, &i32Value);
	abyCommand[1] = (BYTE)i32Value;

	abyCommand[14] = USB_CMD_FIRST_GEN_TAG;
	abyCommand[15] = USB_CMD_SECOND_GEN_TAG;

	BYTE* pbyData = NULL;
	if (dwParamCount > 2)
	{
		pbyData = new BYTE [dwParamCount - 2];
		for (DWORD dwIdx = 2; dwIdx < dwParamCount; dwIdx++)
		{
			pCmdLineInfo->GetParamItemInt(dwIdx, &i32Value);
			pbyData[dwIdx - 2] = (BYTE)i32Value;
		}
	}

    ::memcpy(&abyCommand[3], pbyData, dwParamCount - 2);

    int i32Ret = m_DeviceAgent.CmdPassThrough(m_tszSerial, abyCommand, TRUE, NULL,
                                              0);
    //int i32Ret = m_DeviceAgent.CmdPassThrough(m_tszSerial, abyCommand, TRUE, pbyData,
    //										  dwParamCount - 2);

	if (NULL != pbyData)
		delete [] pbyData;

	if (Err_No_Err != i32Ret)
	{
		::_tprintf(_T("Send write data command failed!!\n"));
		return TRUE;
	}

	::_tprintf(_T("Send write data command successfully.\n"));
	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdHandler::CmdReadData(
	C_CmdlineInfo*	pCmdLineInfo
	)
{
	DWORD dwParamCount = pCmdLineInfo->GetParamCount();
	if (dwParamCount < 3)
	{
		::_tprintf(_T("Incorrect parameters!!\n\n"));
		return FALSE;
	}

	BYTE abyCommand[16];
	::memset(abyCommand, 0, 16);

	int i32Value = -1;
	pCmdLineInfo->GetParamItemInt(0, &i32Value);
	abyCommand[0] = (BYTE)i32Value;

	pCmdLineInfo->GetParamItemInt(1, &i32Value);
	abyCommand[1] = (BYTE)i32Value;

	abyCommand[14] = USB_CMD_FIRST_GEN_TAG;
	abyCommand[15] = USB_CMD_SECOND_GEN_TAG;

	pCmdLineInfo->GetParamItemInt(2, &i32Value);
	UINT u32DataLen = (UINT)i32Value;

	BYTE* pbyData = NULL;
	if (u32DataLen > 0)
		pbyData = new BYTE [u32DataLen];

	int i32Ret = m_DeviceAgent.CmdPassThroughEx(m_tszSerial, abyCommand, FALSE, pbyData, 
												&u32DataLen);

	if (Err_No_Err != i32Ret)
	{
		if (NULL != pbyData)
			delete [] pbyData;

		::_tprintf(_T("Send read data command failed!!\n"));
		return TRUE;
	}

	::_tprintf(_T("Send read data command successfully.\n"));
	::_tprintf(_T("Read data [%d bytes]:\n"), u32DataLen);
	for (DWORD dwIdx = 0; dwIdx < u32DataLen; dwIdx++)
		::_tprintf(_T("0x%02X "), pbyData[dwIdx]);
	::_tprintf(_T("\n"));

	if (NULL != pbyData)
		delete [] pbyData;

	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdHandler::CmdWriteDataFromFile(
	C_CmdlineInfo*	pCmdLineInfo
	)
{
	DWORD dwParamCount = pCmdLineInfo->GetParamCount();
	if (dwParamCount < 3)
	{
		::_tprintf(_T("Incorrect parameters!!\n\n"));
		return FALSE;
	}

	BYTE abyCommand[16];
	::memset(abyCommand, 0, 16);

	int i32Value = -1;
	pCmdLineInfo->GetParamItemInt(0, &i32Value);
	abyCommand[0] = (BYTE)i32Value;

	pCmdLineInfo->GetParamItemInt(1, &i32Value);
	abyCommand[1] = (BYTE)i32Value;

	abyCommand[14] = USB_CMD_FIRST_GEN_TAG;
	abyCommand[15] = USB_CMD_SECOND_GEN_TAG;

	LPCTSTR ptszFile = pCmdLineInfo->GetParamItem(2);
	FILE* fp = ::VCM_tfopen(ptszFile, _T("rb"));
	if (NULL == fp)
	{
		::_tprintf(_T("Open file \"%s\" failed!!"), ptszFile);
		return TRUE;
	}

	::fseek(fp, 0, SEEK_END);
	DWORD dwDataLen = (DWORD)::ftell(fp);
	::fseek(fp, 0, SEEK_SET);

	BYTE* pbyData = NULL;
	if (dwDataLen > 0)
	{
		pbyData = new BYTE [dwDataLen];
		if (1 != ::fread(pbyData, dwDataLen, 1, fp))
		{
			delete [] pbyData;
			::fclose(fp);
			::_tprintf(_T("Read file failed!!\n"));
			return TRUE;
		}
	}

	::fclose(fp);

	int i32Ret = m_DeviceAgent.CmdPassThrough(m_tszSerial, abyCommand, TRUE, pbyData, 
											  dwDataLen);

	if (NULL != pbyData)
		delete [] pbyData;

	if (Err_No_Err != i32Ret)
	{
		::_tprintf(_T("Send write data command failed!!\n"));
		return TRUE;
	}

	::_tprintf(_T("Send write data command successfully. (Write %d bytes)\n"),
			   dwDataLen);
	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdHandler::CmdReadDataToFile(
	C_CmdlineInfo*	pCmdLineInfo
	)
{
	DWORD dwParamCount = pCmdLineInfo->GetParamCount();
	if (dwParamCount < 4)
	{
		::_tprintf(_T("Incorrect parameters!!\n\n"));
		return FALSE;
	}

	BYTE abyCommand[16];
	::memset(abyCommand, 0, 16);

	int i32Value = -1;
	pCmdLineInfo->GetParamItemInt(0, &i32Value);
	abyCommand[0] = (BYTE)i32Value;

	pCmdLineInfo->GetParamItemInt(1, &i32Value);
	abyCommand[1] = (BYTE)i32Value;
	
	abyCommand[14] = USB_CMD_FIRST_GEN_TAG;
	abyCommand[15] = USB_CMD_SECOND_GEN_TAG;

	pCmdLineInfo->GetParamItemInt(2, &i32Value);
	DWORD dwDataLen = (DWORD)i32Value;

	BYTE* pbyData = NULL;
	if (dwDataLen > 0)
		pbyData = new BYTE [dwDataLen];

	int i32Ret = m_DeviceAgent.CmdPassThrough(m_tszSerial, abyCommand, FALSE, pbyData, 
											  dwDataLen);

	if (Err_No_Err != i32Ret)
	{
		if (NULL != pbyData)
			delete [] pbyData;

		::_tprintf(_T("Send read data command failed!!\n"));
		return TRUE;
	}

	LPCTSTR ptszFile = pCmdLineInfo->GetParamItem(3);
	FILE* fp = ::VCM_tfopen(ptszFile, _T("wb"));
	if (NULL == fp)
	{
		::_tprintf(_T("Open file \"%s\" failed!!"), ptszFile);
		return TRUE;
	}

	if (NULL != pbyData)
	{
		if (1 != ::fwrite(pbyData, dwDataLen, 1, fp))
		{
			delete [] pbyData;
			::fclose(fp);
			::_tprintf(_T("Write file failed!!\n"));
			return TRUE;
		}
	}

	::fclose(fp);

	::_tprintf(_T("Send read data command successfully. (Read %d bytes)\n"),
			   dwDataLen);
	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdHandler::CmdEraseFlash(
	C_CmdlineInfo*	pCmdLineInfo
	)
{
	if (pCmdLineInfo->GetParamCount() < 3)
	{
		::_tprintf(_T("Incorrect parameters!!\n\n"));
		return FALSE;
	}

	E_EraseMemorySizeType eEraseMemorySizeType = eERASE_MEMORY_SIZE_4K;
	DWORD dwSectorSize = 4096;
	LPCTSTR ptszSectorSize = pCmdLineInfo->GetParamItem(0);
	if (0 == ::_tcsicmp(ptszSectorSize, _T("4K")))
	{
		eEraseMemorySizeType = eERASE_MEMORY_SIZE_4K;
		dwSectorSize = 4 * 1024;
	}
	else if (0 == ::_tcsicmp(ptszSectorSize, _T("32K")))
	{
		eEraseMemorySizeType = eERASE_MEMORY_SIZE_32K;
		dwSectorSize = 32 * 1024;
	}
	else if (0 == ::_tcsicmp(ptszSectorSize, _T("64K")))
	{
		eEraseMemorySizeType = eERASE_MEMORY_SIZE_64K;
		dwSectorSize = 64 * 1024;
	}
	else
	{
		::_tprintf(_T("Incorrect parameters!!\n\n"));
		return FALSE;
	}

	int i32Value = -1;
	pCmdLineInfo->GetParamItemInt(1, &i32Value);
	DWORD dwEraseSize = (DWORD)i32Value;

	pCmdLineInfo->GetParamItemInt(2, &i32Value);
	DWORD dwStartAddr = (DWORD)i32Value;
	if (0 != (dwStartAddr % dwSectorSize))
	{
		::_tprintf(_T("Start Address should be a multiple of Sector Size!!\n\n"));
		return FALSE;
	}

	DWORD dwEraseCount = dwEraseSize / dwSectorSize;
	if ((dwEraseSize % dwSectorSize) > 0)
		dwEraseCount++;

	::_tprintf(_T("\n"));
	C_GPUSB101SPIFlashCommand Command;
	DWORD dwEraseAddr = dwStartAddr;
	for (DWORD dwIdx = 0; dwIdx < dwEraseCount; dwIdx++)
	{
		::_tprintf(_T("\rErase address: 0x%08X ..."), dwEraseAddr);
		::fflush(stdout);
		int i32Ret = Command.EraseMemory(m_DeviceAgent, m_tszSerial, dwEraseAddr, 
										 eEraseMemorySizeType);
		if (Err_No_Err != i32Ret)
		{
			::_tprintf(_T(" failed!!\n"));
			return TRUE;
		}

		dwEraseAddr += dwSectorSize;
	}

	::_tprintf(_T(" Done\n"));

	::_tprintf(_T("Successfully erase flash.\n"));

	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdHandler::CmdReadFlash(
	C_CmdlineInfo*	pCmdLineInfo
	)
{
	if (pCmdLineInfo->GetParamCount() < 3)
	{
		::_tprintf(_T("Incorrect parameters!!\n\n"));
		return FALSE;
	}

	int i32Value = -1;
	pCmdLineInfo->GetParamItemInt(0, &i32Value);
	UINT32 u32Size = (UINT32)i32Value;

	LPCTSTR ptszPCFilePath = pCmdLineInfo->GetParamItem(1);

	pCmdLineInfo->GetParamItemInt(2, &i32Value);
	UINT32 dwStartAddr = (UINT32)i32Value;
	if (0 != (dwStartAddr % 512))
	{
		::_tprintf(_T("Start Address should be a multiple of 512!!\n\n"));
		return FALSE;
	}

	// open file for writing
	FILE* fp = ::VCM_tfopen(ptszPCFilePath, _T("wb"));
	if (NULL == fp)
	{
		::_tprintf(_T("Uable to open PC file '%s' for writing!!\n"), ptszPCFilePath);
		return TRUE;
	}

	// Read file and save to PC
	::_tprintf(_T("\n"));
	C_GPUSB101SPIFlashCommand Command;
	BYTE abyData[4096];
	UINT32 u32Offset = dwStartAddr;
	UINT32 u32ReadSize = 0;
	while(u32Size > 0)
	{
		::_tprintf(_T("\rRead address: 0x%08X ..."), u32Offset);
		::fflush(stdout);

		u32ReadSize = (u32Size > 4096) ? 4096 : u32Size;

		int i32Ret = Command.ReadMemory(m_DeviceAgent, m_tszSerial, u32Offset, abyData, u32ReadSize);
		if (Err_No_Err != i32Ret)
		{
			::_tprintf(_T(" failed!!\n"));
			return TRUE;
		}

		::fwrite(abyData, u32ReadSize, 1, fp);

		u32Offset += u32ReadSize;
		u32Size -= u32ReadSize;
	}

	::fclose(fp);
	::_tprintf(_T(" Done\n"));

	::_tprintf(_T("Successfully read flash.\n"));

	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdHandler::CmdWriteFlash(
	C_CmdlineInfo*	pCmdLineInfo
	)
{
	if (pCmdLineInfo->GetParamCount() < 2)
	{
		::_tprintf(_T("Incorrect parameters!!\n\n"));
		return FALSE;
	}

	LPCTSTR ptszPCFilePath = pCmdLineInfo->GetParamItem(0);

	int i32Value;
	pCmdLineInfo->GetParamItemInt(1, &i32Value);
	UINT32 dwStartAddr = (UINT32)i32Value;
	if (0 != (dwStartAddr % 512))
	{
		::_tprintf(_T("Start Address should be a multiple of 512!!\n\n"));
		return FALSE;
	}

	// open file for reading
	FILE* fp = ::VCM_tfopen(ptszPCFilePath, _T("rb"));
	if (NULL == fp)
	{
		::_tprintf(_T("Uable to open PC file '%s' for reading!!\n"), ptszPCFilePath);
		return TRUE;
	}
	::fseek(fp, 0, SEEK_END);
	UINT32 u32FileSize = (UINT32)::ftell(fp);
	::fseek(fp, 0, SEEK_SET);

	// Read file from PC and save to device
	::_tprintf(_T("\n"));
	C_GPUSB101SPIFlashCommand Command;
	//BYTE abyData[4096];	//Del by Eric 2009/12/11
	BYTE abyData[BUF_SIZE];
	UINT32 u32Offset = dwStartAddr;
	UINT32 u32ReadSize = 0;
	while(u32FileSize > 0)
	{
		::_tprintf(_T("\rWrite address: 0x%08X ..."), u32Offset);
		::fflush(stdout);

		//u32ReadSize = (u32FileSize > 4096) ? 4096 : u32FileSize;		//del by Eric 2009/12/11
		u32ReadSize = (u32FileSize > BUF_SIZE) ? BUF_SIZE : u32FileSize; //add by Eric 2009/12/11

		::fread(abyData, u32ReadSize, 1, fp);

		int i32Ret = Command.WriteMemory(m_DeviceAgent, m_tszSerial, u32Offset, abyData, u32ReadSize);
		if (Err_No_Err != i32Ret)
		{
			::_tprintf(_T(" failed!!\n"));
			return TRUE;
		}

		u32Offset += u32ReadSize;
		u32FileSize -= u32ReadSize;
	}

	::fclose(fp);
	::_tprintf(_T(" Done\n"));

	::_tprintf(_T("Successfully wrote flash.\n"));

	return TRUE;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdHandler::CompareFile(
	LPCTSTR ptszFile1,
	LPCTSTR ptszFile2,
	DWORD	dwLength
	)
{
	FILE* fp1 = ::VCM_tfopen(ptszFile1, _T("rb"));
	if (NULL == fp1)
		return FALSE;

	FILE* fp2 = ::VCM_tfopen(ptszFile2, _T("rb"));
	if (NULL == fp2)
	{
		::fclose(fp1);
		return FALSE;
	}

	BYTE abyData1[4096];
	BYTE abyData2[4096];
	DWORD dwReadLen = 0;
	BOOL bRet = TRUE;
	while(dwLength > 0)
	{
		dwReadLen = (dwLength > 4096) ? 4096 : dwLength;
		if (1 != ::fread(abyData1, dwReadLen, 1, fp1))
		{
			bRet = FALSE;
			break;
		}
		if (1 != ::fread(abyData2, dwReadLen, 1, fp2))
		{
			bRet = FALSE;
			break;
		}

		if (0 != ::memcmp(abyData1, abyData2, dwReadLen))
		{
			bRet = FALSE;
			break;
		}

		dwLength -= dwReadLen;
	}

	::fclose(fp1);
	::fclose(fp2);
	return bRet;
}
//----------------------------------------------------------------------------
BOOL C_MassStorageCmdHandler::CmdAutoTest(
	C_CmdlineInfo*	pCmdLineInfo
	)
{
	if (pCmdLineInfo->GetParamCount() < 3)
	{
		::_tprintf(_T("Incorrect parameters!!\n\n"));
		return FALSE;
	}

	LPCTSTR ptszPCFilePath = pCmdLineInfo->GetParamItem(0);

	LPCTSTR ptszEraseBlockSize = pCmdLineInfo->GetParamItem(1);
	if ((0 != ::_tcsicmp(ptszEraseBlockSize, _T("4K"))) &&
		(0 != ::_tcsicmp(ptszEraseBlockSize, _T("32K"))) &&
		(0 != ::_tcsicmp(ptszEraseBlockSize, _T("64K"))))
	{
		::_tprintf(_T("Incorrect parameters!!\n\n"));
		return FALSE;
	}

	int i32Value = -1;
	pCmdLineInfo->GetParamItemInt(2, &i32Value);
	UINT32 u32Repeat = (UINT32)i32Value;

	// open file for reading
	FILE* fp = ::VCM_tfopen(ptszPCFilePath, _T("rb"));
	if (NULL == fp)
	{
		::_tprintf(_T("Uable to open PC file '%s' for reading!!\n"), ptszPCFilePath);
		return TRUE;
	}
	::fseek(fp, 0, SEEK_END);
	UINT32 u32FileSize = (UINT32)::ftell(fp);
	::fseek(fp, 0, SEEK_SET);

	::fclose(fp);

	TCHAR tszTempFilePath[_MAX_PATH];
	::VCM_stprintf(tszTempFilePath, _MAX_PATH, _T("%s.tmp"), ptszPCFilePath);

	TCHAR tszEraseCommand[512];
	TCHAR tszWriteCommand[512];
	TCHAR tszReadCommand[512];
	::VCM_stprintf(tszEraseCommand, 512, _T("EraseFlash %s 0x%X 0x0"), 
				   ptszEraseBlockSize, u32FileSize);
	::VCM_stprintf(tszWriteCommand, 512, _T("WriteFlash \"%s\" 0x0"), 
				   ptszPCFilePath);
	::VCM_stprintf(tszReadCommand, 512, _T("ReadFlash 0x%X \"%s\" 0x0"),
				   u32FileSize, tszTempFilePath);

	for (UINT32 u32Time = 0; u32Time < u32Repeat; u32Time++)
	{
		::_tprintf(_T("============================\n"));
		::_tprintf(_T("  AutoTest round %d.......\n"), u32Time);
		::_tprintf(_T("============================\n"));

		if (!ExecuteCommand(tszEraseCommand))
			return TRUE;
		if (!ExecuteCommand(tszWriteCommand))
			return TRUE;
		if (!ExecuteCommand(tszReadCommand))
			return TRUE;

		if (!CompareFile(ptszPCFilePath, tszTempFilePath, u32FileSize))
		{
			::_tprintf(_T("Data inconsistent!!\n"));
			return TRUE;
		}
	}

	::_tprintf(_T("AutoRun Finished.\n"));

	return TRUE;
}

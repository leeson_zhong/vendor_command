// MassStorageCmdHandler.h: interface for the C_MassStorageCmdHandler class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MASSSTORAGECMDHANDLER_H__E55DC160_EB13_406A_837B_4C92FC5F8AE2__INCLUDED_)
#define AFX_MASSSTORAGECMDHANDLER_H__E55DC160_EB13_406A_837B_4C92FC5F8AE2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifdef	WIN32
#pragma warning(disable:4786)
#endif

#include "MassStorageCmdShowHelp.h"
#include "GPUSB101SPIFlashCommand.h"


#define	MAX_DEVICE_NUM	32

class C_CmdlineInfo;
class C_MassStorageCmdHandler;

typedef BOOL (C_MassStorageCmdHandler::*PFN_CmdHandler)(C_CmdlineInfo*);

typedef struct	tagCmdHandlerInfo
{
	TCHAR			tszCommand[32];
	PFN_CmdHandler	pfnCmdHandler;
} S_CmdHandlerInfo;

class C_MassStorageCmdHandler  
{
public:
			C_MassStorageCmdHandler();
	virtual ~C_MassStorageCmdHandler();

	BOOL	IsQuitCommand(
			LPCTSTR	ptszCommand
			);

	BOOL	ExecuteCommand(
			LPCTSTR	ptszCommand
			);

protected:
	// Help command
	BOOL	CmdHelp(
			C_CmdlineInfo*	pCmdLineInfo
			);
	// Device connection commands
	BOOL	CmdSetScsiVidPid(
			C_CmdlineInfo*	pCmdLineInfo
			);
	BOOL	CmdSetDefaultVidPid(
			C_CmdlineInfo*	pCmdLineInfo
			);
	BOOL	CmdSetCdromVidPid(
			C_CmdlineInfo*	pCmdLineInfo
			);
	BOOL	CmdSetTableType(
			C_CmdlineInfo*	pCmdLineInfo
			);
	BOOL	CmeSetAuthType(
			C_CmdlineInfo*	pCmdLineInfo
			);
	BOOL	CmdSetDiskType(
			C_CmdlineInfo*	pCmdLineInfo
			);
	BOOL	CmdSetSetCustomerKey(
			C_CmdlineInfo*	pCmdLineInfo
			);
	BOOL	CmdFindDevSN(
			C_CmdlineInfo*	pCmdLineInfo
			);
	BOOL	CmdSelectDevSN(
			C_CmdlineInfo*	pCmdLineInfo
			);
	// Reserved commands
	BOOL	CmdWriteData(
			C_CmdlineInfo*	pCmdLineInfo
			);
	BOOL	CmdReadData(
			C_CmdlineInfo*	pCmdLineInfo
			);
	BOOL	CmdWriteDataFromFile(
			C_CmdlineInfo*	pCmdLineInfo
			);
	BOOL	CmdReadDataToFile(
			C_CmdlineInfo*	pCmdLineInfo
			);
	// Flash commands
	BOOL	CmdEraseFlash(
			C_CmdlineInfo*	pCmdLineInfo
			);
	BOOL	CmdReadFlash(
			C_CmdlineInfo*	pCmdLineInfo
			);
	BOOL	CmdWriteFlash(
			C_CmdlineInfo*	pCmdLineInfo
			);
	BOOL	CmdAutoTest(
			C_CmdlineInfo*	pCmdLineInfo
			);

	BOOL	CompareFile(
			LPCTSTR ptszFile1,
			LPCTSTR ptszFile2,
			DWORD	dwLength
			);

private:
	static S_CmdHandlerInfo	m_asCmdHandlerInfo[];

	C_MassStorageCmdShowHelp	m_ShowHelp;

	C_GPUSBMassStorageStdAgent	m_DeviceAgent;

	int		m_i32DiskType;	
	int		m_i32AuthType;
	int		m_i32TableType;

	TCHAR	m_tszSerial[17];
	TCHAR	m_tszScsiVid[9];
    TCHAR	m_tszScsiPid[20];
	TCHAR	m_tszCusKey[40];

};

#endif // !defined(AFX_MASSSTORAGECMDHANDLER_H__E55DC160_EB13_406A_837B_4C92FC5F8AE2__INCLUDED_)

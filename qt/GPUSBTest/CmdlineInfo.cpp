// _CmdlineInfo.cpp: implementation of the C_CmdlineInfo class.
//
//////////////////////////////////////////////////////////////////////

#include "GPType.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "CmdlineInfo.h"
#include "VCMigrate.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

C_CmdlineInfo::C_CmdlineInfo(LPCTSTR ptszCmdline)
{
	::VCM_tcsncpy(m_ptszCmdline, ptszCmdline, 512);
	m_ptszCmdline[511]=0;

	//============= Init val =====================
	m_dwParamCount = 0 ;
	m_ptszCmd = NULL ;
	for(int i=0;i<MAX_PARAM_COUNT;i++)
	{
		m_aptszParamTable[i] = NULL ;
	}
	//============= Init val =====================

	_ParseCmdline();
}

C_CmdlineInfo::~C_CmdlineInfo()
{
	_Release();
}

LPCTSTR C_CmdlineInfo::GetCommand()
{
	return m_ptszCmd ;
}

LPCTSTR C_CmdlineInfo::GetCmdline()
{
	m_ptszCmdline[0] = 0 ;

	::VCM_stprintf(m_ptszCmdline, 512, _T("\"%s\" "),m_ptszCmd);

	for (DWORD i = 0; i < m_dwParamCount; i++)
	{
		TCHAR pszItem[256] ;

		::VCM_stprintf(pszItem, 256, _T("\"%s\" "),m_aptszParamTable[i]);

		::VCM_tcscat(m_ptszCmdline, 512, pszItem);
	}

	return m_ptszCmdline ;
}

DWORD C_CmdlineInfo::GetParamCount()
{
	return m_dwParamCount ;
}

BOOL C_CmdlineInfo::SetParamCount(DWORD dwNewCount)
{
	m_dwParamCount = dwNewCount ;
	return TRUE ;
}

LPCTSTR	C_CmdlineInfo::GetParamItem(DWORD dwIndex)
{
	if(dwIndex >= MAX_PARAM_COUNT)
	{
		return NULL ;
	}

	return m_aptszParamTable[dwIndex] ;
}

BOOL C_CmdlineInfo::GetParamItemInt(DWORD dwIndex, int* pi32Value)
{
	if (NULL == pi32Value)
		return FALSE;

	LPCTSTR ptszParamItem = GetParamItem(dwIndex);
	if (NULL == ptszParamItem)
		return FALSE;

	LPTSTR ptszEnd = NULL;
	int i32Len = ::_tcslen(ptszParamItem);
	if (i32Len >= 2)
	{
		if ((ptszParamItem[0] == _T('0')) && 
			((ptszParamItem[1] == _T('x')) || (ptszParamItem[1] == _T('X'))))
		{	// hexadecimal
			*pi32Value = ::_tcstol(&ptszParamItem[2], &ptszEnd, 16);
			return TRUE;
		}
		if ((ptszParamItem[i32Len - 1] == _T('h')) ||
			(ptszParamItem[i32Len - 1] == _T('H')))
		{	// hexadecimal
			*pi32Value = ::_tcstol(ptszParamItem, &ptszEnd, 16);
			return TRUE;
		}
	}
	*pi32Value = ::_tcstol(ptszParamItem, &ptszEnd, 10);
	return TRUE;
}

BOOL C_CmdlineInfo::SetParamItem(DWORD dwIndex,LPCTSTR ptszNewItem)
{
	if(ptszNewItem == FALSE)
	{
		return FALSE ;
	}

	if(dwIndex >= m_dwParamCount)
	{
		return FALSE ;
	}

	int nLen = _tcslen(ptszNewItem);

	TCHAR * pszTemp = new TCHAR [nLen + 1];
	if(pszTemp == NULL)
	{
		return FALSE ;
	}

	::VCM_tcsncpy(pszTemp, ptszNewItem, nLen + 1);

	if(m_aptszParamTable[dwIndex] != NULL)
	{
		delete[] m_aptszParamTable[dwIndex] ;
	}

	m_aptszParamTable[dwIndex] = pszTemp ;

	return TRUE ;
}

BOOL C_CmdlineInfo::_ParseCmdline()
{
	if(m_ptszCmdline == NULL)
	{
		return FALSE ;
	}

	//===== m_ptszCmdline ================================
	int i32Length = _tcslen(m_ptszCmdline);
	if(i32Length == 0) 
	{
		return FALSE ;
	}

	m_dwParamCount = 0 ;

	int nIndex = -1 ;
	int nStart = -1 ;
	BOOL bStartQuote = TRUE ;

	BOOL bFindFirst = TRUE ;

	for(nIndex=0;nIndex<i32Length;nIndex++)
	{
		if(bFindFirst)
		{
			if(m_ptszCmdline[nIndex]!=' ')
			{
				bFindFirst = FALSE ;
				nStart = nIndex ;
				if(m_ptszCmdline[nIndex] == '\"')
					bStartQuote = TRUE ;
				else
					bStartQuote = FALSE ;
			}
		}
		else
		{
			if(bStartQuote == TRUE)
			{
				if(m_ptszCmdline[nIndex] == '\"')
				{
					bFindFirst = TRUE ;
					// [nStart+1, nIndex-1]
					if(nIndex-nStart>0)
					{
						if(m_ptszCmd == NULL)
						{
							m_ptszCmd = new TCHAR[nIndex-nStart] ;
							if(m_ptszCmd == NULL)
							{
								_Release();
								return FALSE ;
							}

							memset(m_ptszCmd,0,sizeof(TCHAR)*(nIndex-nStart));
							if(nIndex-nStart>1)
							{
								memcpy(m_ptszCmd,m_ptszCmdline+nStart+1,sizeof(TCHAR)*(nIndex-nStart-1));
							}
						}
						else
						{
							m_aptszParamTable[m_dwParamCount] = new TCHAR[nIndex-nStart] ;
							if(m_aptszParamTable[m_dwParamCount] == NULL)
							{
								_Release();
								return FALSE ;
							}

							memset(m_aptszParamTable[m_dwParamCount],0,sizeof(TCHAR)*(nIndex-nStart));
							if(nIndex-nStart>1)
							{
								memcpy(m_aptszParamTable[m_dwParamCount],m_ptszCmdline+nStart+1,sizeof(TCHAR)*(nIndex-nStart-1));
							}

							m_dwParamCount ++ ;

							if(m_dwParamCount>=MAX_PARAM_COUNT)
							{
								return TRUE ;
							}
						}
					}
					
				}
			}
			else
			{
				if(m_ptszCmdline[nIndex] == ' ')
				{
					bFindFirst = TRUE ;
					// [ nStart , (nIndex-1) ]
					if(nIndex-nStart>0)
					{
						if(m_ptszCmd == NULL)
						{
							m_ptszCmd = new TCHAR[nIndex-nStart+1] ;
							if(m_ptszCmd == NULL)
							{
								_Release();
								return FALSE ;
							}

							memset(m_ptszCmd,0,sizeof(TCHAR)*(nIndex-nStart+1));
							memcpy(m_ptszCmd,m_ptszCmdline+nStart,sizeof(TCHAR)*(nIndex-nStart));
						}
						else
						{
							m_aptszParamTable[m_dwParamCount] = new TCHAR[nIndex-nStart+1] ;
							if(m_aptszParamTable[m_dwParamCount] == NULL)
							{
								_Release();
								return FALSE ;
							}

							memset(m_aptszParamTable[m_dwParamCount],0,sizeof(TCHAR)*(nIndex-nStart+1));
							memcpy(m_aptszParamTable[m_dwParamCount],m_ptszCmdline+nStart,sizeof(TCHAR)*(nIndex-nStart));

							m_dwParamCount ++ ;

							if(m_dwParamCount>=MAX_PARAM_COUNT)
							{
								return TRUE ;
							}
						}
					}
				}
				else if(m_ptszCmdline[nIndex] == '\"')
				{
					bFindFirst = TRUE ;	
					// [nStart , (nIndex-1)]
					if(nIndex-nStart>0)
					{
						if(m_ptszCmd == NULL)
						{
							m_ptszCmd = new TCHAR[nIndex-nStart+1] ;
							if(m_ptszCmd == NULL)
							{
								_Release();
								return FALSE ;
							}

							memset(m_ptszCmd,0,sizeof(TCHAR)*(nIndex-nStart+1));
							memcpy(m_ptszCmd,m_ptszCmdline+nStart,sizeof(TCHAR)*(nIndex-nStart));
						}
						else
						{
							m_aptszParamTable[m_dwParamCount] = new TCHAR[nIndex-nStart+1] ;
							if(m_aptszParamTable[m_dwParamCount] == NULL)
							{
								_Release();
								return FALSE ;
							}

							memset(m_aptszParamTable[m_dwParamCount],0,sizeof(TCHAR)*(nIndex-nStart+1));
							memcpy(m_aptszParamTable[m_dwParamCount],m_ptszCmdline+nStart,sizeof(TCHAR)*(nIndex-nStart));

							m_dwParamCount ++ ;

							if(m_dwParamCount>=MAX_PARAM_COUNT)
							{
								return TRUE ;
							}
						}
					}

					nIndex--;
				}
			}
		}
	}

	if(bFindFirst == FALSE)
	{
		int i32Start ;
		int i32End ;
		if(bStartQuote == TRUE)
		{
			i32Start = nStart +1 ;
			i32End = i32Length-1 ;
		}
		else
		{
			i32Start = nStart  ;
			i32End = i32Length-1 ;
		}

		if(i32End-i32Start>=0)
		{
			if(m_ptszCmd == NULL)
			{
				m_ptszCmd = new TCHAR[i32End-i32Start+2] ;
				if(m_ptszCmd == NULL)
				{
					_Release();
					return FALSE ;
				}
				
				memset(m_ptszCmd,0,sizeof(TCHAR)*(i32End-i32Start+2));
				memcpy(m_ptszCmd,m_ptszCmdline+i32Start,sizeof(TCHAR)*(i32End-i32Start+1));
			}
			else
			{
				m_aptszParamTable[m_dwParamCount] = new TCHAR[i32End-i32Start+2] ;
				if(m_aptszParamTable[m_dwParamCount] == NULL)
				{
					_Release();
					return FALSE ;
				}
				
				memset(m_aptszParamTable[m_dwParamCount],0,sizeof(TCHAR)*(i32End-i32Start+2));
				memcpy(m_aptszParamTable[m_dwParamCount],m_ptszCmdline+i32Start,sizeof(TCHAR)*(i32End-i32Start+1));
				
				m_dwParamCount ++ ;
				
				if(m_dwParamCount>=MAX_PARAM_COUNT)
				{
					return TRUE ;
				}
			}
		}
	}

	return TRUE ;
}

BOOL C_CmdlineInfo::_Release()
{
	m_dwParamCount = 0 ;

	if(m_ptszCmd != NULL)
	{
		delete[] m_ptszCmd ;
		m_ptszCmd = NULL ;
	}

	for(int i=0;i<MAX_PARAM_COUNT;i++)
	{
		if(m_aptszParamTable[i] != NULL)
		{
			delete[] m_aptszParamTable[i] ;
			m_aptszParamTable[i] = NULL ;
		}
	}	

	return TRUE ;
}

BOOL C_CmdlineInfo::Release()
{
	_Release();

	delete this ;

	return TRUE ;
}

// GPUSB101SPIFlashCommand.cpp: implementation of the C_GPUSB101SPIFlashCommand class.
//
//////////////////////////////////////////////////////////////////////

//#include "stdafx.h"
#include "GPType.h"
#include <assert.h>
#include "GPUSB101SPIFlashCommand.h"
#include "Define.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

C_GPUSB101SPIFlashCommand::C_GPUSB101SPIFlashCommand()
{

}

C_GPUSB101SPIFlashCommand::~C_GPUSB101SPIFlashCommand()
{

}
//----------------------------------------------------------------------------
int C_GPUSB101SPIFlashCommand::EraseMemory(
	C_GPUSBMassStorageStdAgent&		DeviceAgent,
	LPCTSTR								ptszSerial,
	DWORD								dwMemoryAddress,
	E_EraseMemorySizeType				eEraseSizeType		// 0x08: 4KB, 0x40: 32KB, 0x80: 64KB
	)
{
#if _DEBUG
	switch(eEraseSizeType)
	{
	case eERASE_MEMORY_SIZE_4K:
		assert(0 == (dwMemoryAddress % 4096));
		break;
	case eERASE_MEMORY_SIZE_32K:
		assert(0 == (dwMemoryAddress % 32768));
		break;
	case eERASE_MEMORY_SIZE_64K:
		assert(0 == (dwMemoryAddress % 65536));
		break;
	default:
		assert(FALSE);
	}
#endif

#if	TEST_WITHOUT_DEVICE

	assert(NULL != g_fpTest);

	BYTE abyData[4096];
	::memset(abyData, 0xFF, 4096);

	::fseek(g_fpTest, dwMemoryAddress, SEEK_SET);

	int i32Times = ((int)eEraseSizeType) / 8;
	for(int i32Idx = 0; i32Idx < i32Times; i32Idx++)
		::fwrite(abyData, 4096, 1, g_fpTest);

#else

	BYTE abyCmd[16] = { 0xFD, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
						0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  'G',  'P' };

	abyCmd[2] = HIBYTE(HIWORD(dwMemoryAddress));
	abyCmd[3] = LOBYTE(HIWORD(dwMemoryAddress));
	abyCmd[4] = HIBYTE(LOWORD(dwMemoryAddress));
	abyCmd[5] = LOBYTE(LOWORD(dwMemoryAddress));
	abyCmd[6] = 0x06;	// SPI flash
	abyCmd[8] = (BYTE)eEraseSizeType;

	// Erase Memory
	int i32Ret = DeviceAgent.CmdPassThrough(ptszSerial, abyCmd, TRUE, NULL, 0);
	if (Err_No_Err != i32Ret)
		return i32Ret;

#endif

	return Err_No_Err;
}
//----------------------------------------------------------------------------
int C_GPUSB101SPIFlashCommand::ReadMemory(
	C_GPUSBMassStorageStdAgent&		DeviceAgent,
	LPCTSTR								ptszSerial,
	DWORD								dwMemoryAddress,
	BYTE*								pbyReadBuffer,
	WORD								wReadSize
	)
{
#if	TEST_WITHOUT_DEVICE

	assert(NULL != g_fpTest);

	::fseek(g_fpTest, dwMemoryAddress, SEEK_SET);

	::fread(pbyReadBuffer, wReadSize, 1, g_fpTest);

#else

	BYTE abyCmd[16] = { 0xFD, 0x28, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
						0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  'G',  'P' };

	abyCmd[2] = HIBYTE(HIWORD(dwMemoryAddress));
	abyCmd[3] = LOBYTE(HIWORD(dwMemoryAddress));
	abyCmd[4] = HIBYTE(LOWORD(dwMemoryAddress));
	abyCmd[5] = LOBYTE(LOWORD(dwMemoryAddress));
	abyCmd[6] = 0x06;	// SPI flash
	abyCmd[7] = HIBYTE(wReadSize);
	abyCmd[8] = LOBYTE(wReadSize);

	int i32Ret = DeviceAgent.CmdPassThrough(
						ptszSerial, abyCmd, FALSE, pbyReadBuffer, wReadSize);
	if (Err_No_Err != i32Ret)
		return i32Ret;

#endif

	return Err_No_Err;
}
//----------------------------------------------------------------------------
int C_GPUSB101SPIFlashCommand::WriteMemory(
	C_GPUSBMassStorageStdAgent&			DeviceAgent,
	LPCTSTR								ptszSerial,
	DWORD								dwMemoryAddress,
	BYTE*								pbyReadBuffer,
	WORD								wWriteSize
	)
{
#if	TEST_WITHOUT_DEVICE

	assert(NULL != g_fpTest);

	::fseek(g_fpTest, dwMemoryAddress, SEEK_SET);

	::fwrite(pbyReadBuffer, wReadSize, 1, g_fpTest);

#else

	BYTE abyCmd[16] = { 0xFD, 0x2A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
						0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  'G',  'P' };

	abyCmd[2] = HIBYTE(HIWORD(dwMemoryAddress));
	abyCmd[3] = LOBYTE(HIWORD(dwMemoryAddress));
	abyCmd[4] = HIBYTE(LOWORD(dwMemoryAddress));
	abyCmd[5] = LOBYTE(LOWORD(dwMemoryAddress));
	abyCmd[6] = 0x06;	// SPI flash
	abyCmd[7] = HIBYTE(wWriteSize);
	abyCmd[8] = LOBYTE(wWriteSize);

	int i32Ret = DeviceAgent.CmdPassThrough(
						ptszSerial, abyCmd, TRUE, pbyReadBuffer, wWriteSize);
	if (Err_No_Err != i32Ret)
		return i32Ret;

#endif

	return Err_No_Err;
}

#ifndef _GPType_h_
#define _GPType_h_

#include <stdio.h>

#ifdef	WIN32
	#ifndef	_WINSOCKAPI_
		#define _WINSOCKAPI_
		#include <wtypes.h>
		#include <tchar.h>
	
		#ifndef _INC_WINSOCK2
			#include <winsock2.h>
		#endif
	#else
		#include <wtypes.h>
		#include <tchar.h>
	#endif 

	#ifndef	INT
		#define	INT		int
	#endif
	#ifndef	UINT
		#define	UINT	unsigned int
	#endif

	#if	_MSC_VER < 1310
		#define	_FUNC_PTR(func)	func
	#else
		#define	_FUNC_PTR(func)	&func
	#endif
#endif

#ifdef	__MACH__
	#include <CoreFoundation/CoreFoundation.h>
	#include <sys/stat.h>
	#include <mach-o/dyld.h>
	#include <wchar.h>
	
	#define __stdcall

	typedef UInt8	BYTE;
	typedef UInt16	WORD;
	typedef UInt16	USHORT;
	typedef unsigned int DWORD;
	typedef SInt32	INT;
	typedef unsigned int UINT;
	
	typedef UInt32	UINT32;
	typedef SInt32	LONG;
	typedef UInt32	ULONG;
	
	typedef SInt32	HRESULT;

	#define	S_OK	((HRESULT)0x00000000L)
	#define E_FAIL	((HRESULT)0x80004005L)
	
	#define _MAX_DRIVE 3
	#define _MAX_DIR  256
	#define _MAX_FNAME 64
	#define _MAX_EXT 256
	typedef	unsigned short	WCHAR;
	
#endif

#ifdef	__unix__

	#include <wchar.h>
	#include <string.h>

	typedef	unsigned char	BYTE;
	typedef	unsigned short	USHORT;
	typedef	USHORT			WORD;
	typedef	unsigned int	UINT;
	typedef	UINT			UINT32;
	typedef	UINT32			DWORD;
	typedef	wchar_t			WCHAR;
	
#endif
	
#if	defined(__MACH__) || defined(__unix__)

	typedef bool	BOOL;
#ifdef _UNICODE
	typedef wchar_t				TCHAR;
	typedef unsigned wchar_t	UCHAR;
	typedef wchar_t*			LPTSTR;
	typedef wchar_t*			LPSTR;
	typedef const wchar_t*		LPCSTR;
	typedef	const wchar_t*		LPCTSTR;
	typedef	const wchar_t*		LPCWSTR;
#else
	typedef char			TCHAR;
	typedef unsigned char	UCHAR;
	typedef char*			LPTSTR;
	typedef char*			LPSTR;
	typedef const char*		LPCSTR;
	typedef	const char*		LPCTSTR;
	typedef	const WCHAR*	LPCWSTR;
#endif

	typedef UCHAR*			PUCHAR;
	typedef BYTE*			LPBYTE;
	typedef	LPBYTE			PBYTE;
	typedef	LPTSTR			PTSTR;
	
	typedef USHORT*			PUSHORT;
	typedef UINT32*			PUINT32;
	typedef DWORD*			PDWORD;
	
	typedef void*			HANDLE;
	typedef	void*			PVOID;
	typedef void*			LPVOID;
	
	#define	WINAPI
	
	#define	CONST			const
	#ifndef	FALSE
		#define	FALSE		false
	#endif
	#ifndef	TRUE
		#define	TRUE		true
	#endif
	
	#define	MAX_PATH		1024
	#define	_MAX_PATH		MAX_PATH
	
	#define	HIWORD(dwValue)	((dwValue >> 16) & 0xFFFF)
	#define	LOWORD(dwValue)	(dwValue & 0xFFFF)
	#define HIBYTE(wValue)	((wValue >> 8) & 0xFF)
	#define	LOBYTE(wValue)	(wValue & 0xFF)
	
	#define	MAKEWORD(byLow, byHigh)	((WORD)byLow | ((WORD)byHigh << 8))
	#define	MAKELONG(wLow, wHigh)	((DWORD)wLow | ((DWORD)wHigh << 16))
	
#ifdef _UNICODE
	#include <wctype.h>
	#define	_T(x)			L##x

	inline int wcscicmp(LPCSTR tszStr1, LPCSTR tszStr2)
	{
		while(towlower(*tszStr1)==towlower(*tszStr2))
		{
			if(*tszStr1 == 0)
				return 0;
			tszStr1++;
			tszStr2++;
		}
		return towlower(*tszStr1) - towlower(*tszStr2);
	}

	inline int wcnscicmp(LPCSTR tszStr1, LPCSTR tszStr2,size_t cmpsize)
	{
		size_t cursize = 0;
		while(towlower(*tszStr1)==towlower(*tszStr2))
		{
			if(cursize==cmpsize)
				break;
			
			if(*tszStr1 == 0)
				return 0;
			tszStr1++;
			tszStr2++;
			cursize++;	
		}
		return towlower(*tszStr1) - towlower(*tszStr2);
	}

	inline int Macswprintf(LPTSTR ptszBuf,LPTSTR ptszFormat,...)
	{
		va_list args;
		va_start(args,ptszFormat);
		int i32ret = swprintf(ptszBuf,wcslen(ptszBuf),ptszFormat,args);
		va_end(args);
		return i32ret;
	}

	inline wchar_t* getws(LPTSTR ptszBuf)
	{
		fgetws(ptszBuf, BUFSIZ, stdin);
		int i32len = wcslen(ptszBuf);
		if(i32len > 0 && ptszBuf[i32len-1]==_T('\n')) //remove new line
			ptszBuf[i32len-1]=_T('\0');
		
		return ptszBuf;
	}

	inline FILE* wfopen(LPCSTR ptszBuf,LPCSTR ptszMode)
	{
		char szPath[MAX_PATH],szMode[32];
		
		wcstombs(szPath,ptszBuf,MAX_PATH); 
		wcstombs(szMode,ptszMode,32); 
		
		return fopen(szPath,szMode);
	}

	#define	_tcslen			wcslen
	#define	_tcscpy			wcscpy
	#define	_tcsncpy		wcsncpy
	#define _tcscat			wcscat
	#define	_tcscmp			wcscmp
	#define	_tcsncmp		wcsncmp
	#define	_tcsicmp		wcscicmp
	#define	_tcsnicmp		wcnscicmp
	#define _tcstol			wcstol
	#define	_tcstoul		wcstoul
	#define	_tprintf		wprintf
	#define _stprintf		Macswprintf
	#define	_sntprintf		swprintf
	#define _getts			getws
	#define	_ttoi			atoi
	#define	_tcsdup			strdup
	#define	_tcsrchr		wcsrchr
	#define	_tcstok			wcstok
	#define	_tcsstr			wcsstr
	#define	_tcschr			wcschr
	#define _tfopen			wfopen
	#define	_fgetts			fgetws
	#define	_ftprintf		fwprintf

	#define	_vstprintf		vswprintf

#else
	#define	_T
	#define	_tcslen			strlen
	#define	_tcscpy			strcpy
	#define	_tcsncpy		strncpy
	#define _tcscat			strcat
	#define	_tcscmp			strcmp
	#define	_tcsncmp		strncmp
	#define	_tcsicmp		strcasecmp
	#define	_tcsnicmp		strncasecmp
	#define _tcstol			strtol
	#define	_tcstoul		strtoul
	#define	_tprintf		printf
	#define _stprintf		sprintf
	#define	_sntprintf		snprintf
	#define _getts			gets
	#define	_ttoi			atoi
	#define	_tcsdup			strdup
	#define	_tcsrchr		strrchr
	#define	_tcstok			strtok
	#define	_tcsstr			strstr
	#define	_tcschr			strchr
	#define _tfopen			fopen
	#define	_fgetts			fgets
	#define	_ftprintf		fprintf
	
	#define	_vstprintf		vsprintf
	
#endif
	#define	_tmkdir(path)	mkdir(path, S_IWGRP | S_IWOTH)
	
	#define	IN
	#define OUT
	
	#define	OutputDebugString(str)	
	#define	GetTickCount(x)			(0)
	
	#define	_FUNC_PTR(func)			&func
	
	#define	USES_CONVERSION
	#define	T2A(x)					x
	#define	T2CA(x)					x
	
	#define	Sleep(x)				usleep(x * 1000)
	#define	ZeroMemory(ptr, size)	memset(ptr, 0, size)
	
#endif

#ifdef	__MACH__

	inline int GetModuleFileName(HANDLE hHandle, LPTSTR tszPath, DWORD dwSize)
	{
		uint32_t u32Size = dwSize;
		::_NSGetExecutablePath((char *)tszPath, &u32Size);
		return ::_tcslen(tszPath);
	}
	
#endif

#endif	// _GPType_h_
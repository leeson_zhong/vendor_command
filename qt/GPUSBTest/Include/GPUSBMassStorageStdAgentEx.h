// GPUSBMassStorageNewStdAgent.h: interface for the CGPUSBMassStorageNewStdAgent class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GPUSBMASSSTORAGENEWSTDAGENT_H__DD5B7899_8B3C_4956_857C_C5A099C44671__INCLUDED_)
#define AFX_GPUSBMASSSTORAGENEWSTDAGENT_H__DD5B7899_8B3C_4956_857C_C5A099C44671__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class C_GPUSBMassStorageStdAgentEx  
{
public:
	C_GPUSBMassStorageStdAgentEx();
	virtual ~C_GPUSBMassStorageStdAgentEx();

	BOOL	SetScsiVidPid(
			LPCTSTR	ptszVID,
			LPCTSTR	ptszPID
			);

	BOOL	Enumerate(
			int		i32TableType,		// TABLE_TYPE_MASS_PRODUCTION(0x00) or TABLE_TYPE_REAL_TABLE(0x01)
			int		i32AuthorizeType,	// AUTHORIZE_TYPE_STANDARD(0x00) or AUTHORIZE_TYPE_EASY(0x01)
			int		i32DeviceType		// NON_MOUNT_POINT_TYPE(0x00) or MOUNT_POINT_TYPE(0x01)
			);

	BOOL	SetCustomerKey(
			LPCTSTR	ptszCustomerKey
			);

	int		GetDeviceCount();
	LPCTSTR	GetDeviceSerial(
			int		i32DeviceIndex
			);

	BOOL	OpenDevice(
			int		i32DeviceIndex
			);
	BOOL	OpenDevice(
			LPCTSTR	ptszSerial
			);

	BOOL	CloseDevice(
			int		i32DeviceIndex
			);
	BOOL	CloseDevice(
			LPCTSTR	ptszSerial
			);

	int		CmdPassThrough(
			int		i32DeviceIndex,
			BYTE*	pbyCmd, 
			BOOL	bWriteData, 
			BYTE*	pbyData, 
			UINT	u32DataLen
			);
	int		CmdPassThrough(
			LPCTSTR	ptszSerial,
			BYTE*	pbyCmd, 
			BOOL	bWriteData, 
			BYTE*	pbyData, 
			UINT	u32DataLen
			);
	int		CmdPassThroughEx(
			int		i32DeviceIndex,
			BYTE*	pbyCmd, 
			BOOL	bWriteData, 
			BYTE*	pbyData, 
			UINT*	pu32DataLen
			);
	int		CmdPassThroughEx(
			LPCTSTR	ptszSerial,
			BYTE*	pbyCmd, 
			BOOL	bWriteData, 
			BYTE*	pbyData, 
			UINT*	pu32DataLen
			);

	int		GetLastIOErrorCode(
			int		i32DeviceIndex,
			DWORD*	pdwIOErrorCode
			);
	int		GetLastIOErrorCode(
			LPCTSTR	ptszSerial,
			DWORD*	pdwIOErrorCode
			);
	int		GetLastSenseKeyValue(
			int		i32DeviceIndex,
			DWORD*	pdwSenseKeyValue
			);
	int		GetLastSenseKeyValue(
			LPCTSTR	ptszSerial,
			DWORD*	pdwSenseKeyValue
			);

protected:
	int		FindIndexBySerial(
			LPCTSTR	ptszSerial
			);
	BOOL	CreateMassStorageDeviceAgent();
	void	CloseAllDevice();

private:

	TCHAR	m_tszScsiVID[32];
	TCHAR	m_tszScsiPID[32];
};

#endif // !defined(AFX_GPUSBMASSSTORAGENEWSTDAGENT_H__DD5B7899_8B3C_4956_857C_C5A099C44671__INCLUDED_)

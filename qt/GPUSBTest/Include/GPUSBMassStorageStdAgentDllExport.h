#ifndef GPUSBMASSSTORAGESTDAGENTDLLEXPORT
#define GPUSBMASSSTORAGESTDAGENTDLLEXPORT

//APIs-------------------------------------------
extern "C"
{

#define GPUSB_FUN_ENUMERATE				"Enumerate"
	typedef BOOL	(WINAPI *PFN_Enumerate)(
		int		i32TableType,		// TABLE_TYPE_MASS_PRODUCTION(0x00) or TABLE_TYPE_REAL_TABLE(0x01)
		int		i32AuthorizeType,	// AUTHORIZE_TYPE_STANDARD(0x00) or AUTHORIZE_TYPE_EASY(0x01)
		int		i32DeviceType		// NON_MOUNT_POINT_TYPE(0x00) or MOUNT_POINT_TYPE(0x01)
		);

#define GPUSB_FUN_SETCUSTOMERKEY		"SetCustomerKey"
	typedef BOOL	(WINAPI *PFN_SetCustomerKey)(
		LPCTSTR	ptszCustomerKey
		);

#define GPUSB_FUN_SETSCSIVIDPID			"SetScsiVidPid"
	typedef int	(WINAPI *PFN_SetScsiVidPid)(
		LPCTSTR	ptszVID,
		LPCTSTR	ptszPID
		);

#define GPUSB_FUN_SETUSBVIDPID			"SetUSBVidPid"
	typedef int	(WINAPI *PFN_SetUSBVidPid)(
		WORD	wVID,
		WORD	wPID,
		WORD	wDeviceVer
		);

#define GPUSB_FUN_GETDEVICECOUNT		"GetDeviceCount"
	typedef int	(WINAPI *PFN_GetDeviceCount)(
		);

#define GPUSB_FUN_GETDEVICESERIAL		"GetDeviceSerial"
	typedef LPCTSTR	(WINAPI *PFN_GetDeviceSerial)(
		int		i32DeviceIndex
		);

#define GPUSB_FUN_OPENDEVICE			"OpenDevice"
	typedef BOOL	(WINAPI *PFN_OpenDevice)(
		LPCTSTR	ptszSerial
		);

#define GPUSB_FUN_CLOSEDEIVCE			"CloseDevice"
	typedef BOOL	(WINAPI *PFN_CloseDevice)(
		LPCTSTR	ptszSerial
		);

#define GPUSB_FUN_CMDPASSTHROUGH		"CmdPassThrough"
	typedef int	(WINAPI *PFN_CmdPassThrough)(
		LPCTSTR	ptszSerial,
		BYTE*	pbyCmd, 
		BOOL	bWriteData, 
		BYTE*	pbyData, 
		UINT	u32DataLen
		);

#define GPUSB_FUN_GETLASTIOERRORCODE	"GetLastIOErrorCode"
	typedef int	(WINAPI *PFN_GetLastIOErrorCode)(
		LPCTSTR	ptszSerial,
		DWORD*	pdwIOErrorCode
		);

#define GPUSB_FUN_GETLASTSENSEKEYVALUE	"GetLastSenseKeyValue"
	typedef int	(WINAPI *PFN_GetLastSenseKeyValue)(
		LPCTSTR	ptszSerial,
		DWORD*	pdwSenseKeyValue
		);

#define GPUSB_FUN_CHECKDEVICECONNECTION	"CheckDeviceConnection"
	typedef int	(WINAPI *PFN_CheckDeviceConnection)(
		LPCTSTR	ptszSerial
		);

};


#endif
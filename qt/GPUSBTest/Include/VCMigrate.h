#ifndef	_VCMigrate_h_
#define	_VCMigrate_h_

#include <stdio.h>
#include <stdlib.h>
#ifdef	WIN32
#include <tchar.h>
#endif

#define _COUNT_OF(_ARRAY)	(sizeof(_ARRAY)/sizeof(_ARRAY[0]))	//Single line added by longwq on 2009-8-1 15:46:41

//---------------------------------------------------------------------------
inline TCHAR* VCM_tcsncpy(
	LPTSTR	ptszDest,
	LPCTSTR	ptszSrc,
	size_t	sizeCount
	)
{
#if	_MSC_VER < 1310
	return ::_tcsncpy(ptszDest, ptszSrc, sizeCount);
#else
	if (sizeCount <= ::_tcslen(ptszSrc))
		::memcpy(ptszDest, ptszSrc, sizeof(TCHAR) * sizeCount);
	else
		errno_t err = ::_tcsncpy_s(ptszDest, sizeCount, ptszSrc, _TRUNCATE);
	return ptszDest;
#endif
}

//---------------------------------------------------------------------------
#if !(defined(__MACH__) && defined(_UNICODE))
inline char* VCM_strncpy(
	LPSTR	pszDest,
	LPCSTR	pszSrc,
	size_t	sizeCount
	)
{
#if	_MSC_VER < 1310
	return ::strncpy(pszDest, pszSrc, sizeCount);
#else
	if (sizeCount <= ::strlen(pszSrc))
		::memcpy(pszDest, pszSrc, sizeof(TCHAR) * sizeCount);
	else
		errno_t err = ::strncpy_s(pszDest, sizeCount, pszSrc, _TRUNCATE);
	return pszDest;
#endif
}
#endif
//---------------------------------------------------------------------------
inline TCHAR* VCM_tcscat(
	LPTSTR	ptszDest,
	size_t	sizeCount,
	LPCTSTR	ptszSrc
	)
{
#if	_MSC_VER < 1310
	return ::_tcscat(ptszDest, ptszSrc);
#else
	errno_t err = ::_tcscat_s(ptszDest, sizeCount, ptszSrc);
	return ptszDest;
#endif
}
//---------------------------------------------------------------------------
#if !(defined(__MACH__) && defined(_UNICODE)) 
inline char* VCM_strcat(
	LPSTR	ptszDest,
	size_t	sizeCount,
	LPCSTR	ptszSrc
	)
{
#if	_MSC_VER < 1310
	return ::strcat(ptszDest, ptszSrc);
#else
	errno_t err = ::strcat_s(ptszDest, sizeCount, ptszSrc);
	return ptszDest;
#endif
}
#endif
//---------------------------------------------------------------------------
inline TCHAR* VCM_getts(
	LPTSTR	ptszBuf,
	size_t	sizeCount
	)
{
#if	_MSC_VER < 1310
	return ::_getts(ptszBuf);
#else
	return ::_getts_s(ptszBuf, sizeCount);
#endif
}
//---------------------------------------------------------------------------
inline int  VCM_stprintf(
	LPTSTR	ptszBuf,
	size_t	sizeBuf,
	LPCTSTR ptszFormat,
	...
	)
{
#if	_MSC_VER < 1310
	
#if defined(__MACH__)
	va_list args;
	va_start(args, ptszFormat);
	
#ifdef _UNICODE
	return ::_vstprintf(ptszBuf,sizeBuf, ptszFormat, args);	
#else
	return ::_vstprintf(ptszBuf, ptszFormat, args);
#endif	
	
#else
    return ::_vstprintf(ptszBuf, ptszFormat, (va_list)(&ptszFormat + 1));
#endif
	
#else
	int 	i32Ret;
	va_list args;
	va_start(args, ptszFormat);
	i32Ret = ::_vstprintf_s(ptszBuf, sizeBuf, ptszFormat, args);
	va_end(args);
	return i32Ret;
#endif

}

//---------------------------------------------------------------------------
#ifdef	WIN32
inline int __cdecl VCM_sntprintf(
	 LPTSTR	ptszBuf,
	 size_t	sizeOfBuffer,
	 size_t	count,
	 LPCTSTR ptszFormat,
	...
	)
{
#if	_MSC_VER < 1310
    return ::_vsntprintf(ptszBuf,sizeOfBuffer,ptszFormat, (va_list)(&ptszFormat + 1));
#else
	return ::_vsntprintf_s(ptszBuf, sizeOfBuffer, count, ptszFormat, (va_list)(&ptszFormat + 1));
#endif
}

inline int __cdecl VCM_snprintf(
	 LPSTR	ptszBuf,
	 size_t	sizeOfBuffer,
	 size_t	count,
	 LPCSTR ptszFormat,
	va_list argptr
	)
{
#if	_MSC_VER < 1310
    return ::_vsnprintf(ptszBuf,sizeOfBuffer,ptszFormat, argptr);
#else
	return ::_vsnprintf_s(ptszBuf, sizeOfBuffer, count, ptszFormat, argptr);
#endif
}

#endif	// WIN32
//---------------------------------------------------------------------------
#ifdef WIN32
inline int __cdecl VCM_vstprintf(
	LPTSTR	ptszBuf,
	size_t	sizeBuf,
	LPCTSTR ptszFormat,
	va_list argptr
	)
{
#if	_MSC_VER < 1310
    return ::_vstprintf(ptszBuf, ptszFormat, argptr);	//del by Eric 2009/08/05
#else
	return ::_vstprintf_s(ptszBuf, sizeBuf, ptszFormat, argptr);
#endif
}
#endif
//---------------------------------------------------------------------------
#ifdef WIN32
inline int __cdecl VCM_sprintf(
	LPSTR	pszBuf,
	size_t	sizeBuf,
	LPCSTR	pszFormat,
	...
	)
{
#if	_MSC_VER < 1310
    return ::vsprintf(pszBuf, pszFormat, (va_list)(&pszFormat + 1));
#else
	int	    i32Ret;
	va_list args;
	va_start(args, pszFormat);
	i32Ret = ::vsprintf_s(pszBuf, sizeBuf, pszFormat, args);
	va_end(args);
	return i32Ret;
#endif
}
#endif


#ifdef __MACH__
//function: split the full path to components.
//path: input arguments, _MAX_PATH 256
//drive: NULL;
//dir: output arguments  _MAX_DIR 32
//fname: file name--->output arguments _MAX_FNAME 64
//ext: extend name--->output arguments
inline void MacOSSplitPath(LPCTSTR path, LPSTR drive, LPSTR dir, LPSTR  fname, LPSTR ext)
{
	if(NULL == path || path[0] == _T('\0'))
		return ;
	if(NULL != drive)
		return ;
		
	TCHAR *szFile = _tcsrchr(path, _T('/'));
	int i32DirLen = szFile - path;
	_tcsncpy(dir, path, i32DirLen);
	
	int i32FileLen = _tcslen(szFile);
	TCHAR *szFileName = _tcsrchr(szFile, _T('.'));
	
	int i32FileNameLen = szFileName - szFile;
	
	if(NULL != fname)
		_tcsncpy(fname, szFile+1, i32FileNameLen - 1);
		
	if(NULL != ext)
		_tcsncpy(ext, szFileName, i32FileLen - (szFileName - szFile -1));

}

inline void MACOSMakePath(LPSTR path, LPCSTR drive, LPCSTR dir, LPCSTR fname, LPCSTR ext)
{
	if(NULL == path)
		return ;
		
	if(NULL != drive)
		return ;
		
	_tcscpy(path, dir);
	_tcscat(path, _T("/"));
	_tcscat(path, fname);
	if(NULL != ext)
	{
		_tcscat(path, ext);
	}
}
#endif
//---------------------------------------------------------------------------
inline void	VCM_tsplitpath(
	LPCTSTR	ptszPath,
	LPTSTR	ptszDrive,
	size_t	sizeDriveBuf,
	LPTSTR	ptszDir,
	size_t	sizeDirBuf,
	LPTSTR	ptszFName,
	size_t	sizeFNameBuf,
	LPTSTR	ptszExt,
	size_t	sizeExtBuf
	)
{
#ifdef WIN32
#if	_MSC_VER < 1310
	::_tsplitpath(ptszPath, ptszDrive, ptszDir, ptszFName, ptszExt);
#else
	errno_t err = ::_tsplitpath_s(ptszPath,
								  ptszDrive, sizeDriveBuf,
								  ptszDir, sizeDirBuf,
								  ptszFName, sizeFNameBuf,
								  ptszExt, sizeExtBuf);
#endif
#elif __MACH__
	memset(ptszDrive, 0, sizeof(TCHAR)*sizeDriveBuf);
	memset(ptszDir, 0, sizeof(TCHAR)*sizeDirBuf);
	memset(ptszFName, 0, sizeof(TCHAR)*sizeFNameBuf);
	memset(ptszExt, 0, sizeof(TCHAR)*sizeExtBuf); 
	MacOSSplitPath(ptszPath, NULL, ptszDir, ptszFName, ptszExt);
#endif
}
//---------------------------------------------------------------------------
inline void	VCM_tmakepath(
	LPTSTR	ptszPath,
	size_t	sizePathBuf,
	LPCTSTR	ptszDrive,
	LPCTSTR	ptszDir,
	LPCTSTR	ptszFName,
	LPCTSTR	ptszExt
	)
{
#ifdef WIN32
#if	_MSC_VER < 1310
	::_tmakepath(ptszPath, ptszDrive, ptszDir, ptszFName, ptszExt);
#else
	errno_t err = ::_tmakepath_s(ptszPath, sizePathBuf,
								 ptszDrive, ptszDir, ptszFName, ptszExt);
#endif
#elif __MACH__
	memset(ptszPath, 0, sizeof(TCHAR)*sizePathBuf);

	MACOSMakePath(ptszPath, NULL, ptszDir, ptszFName, ptszExt);
#endif
}

#ifdef WIN32
//---------------------------------------------------------------------------
inline TCHAR* VCM_tcsupr(
	TCHAR* pszString,
	size_t numberOfElements
	)
{
#if	_MSC_VER < 1310
    return ::_tcsupr(pszString);
#else
	errno_t err = ::_tcsupr_s(pszString, numberOfElements);
    return pszString;
#endif   
}
//---------------------------------------------------------------------------
inline TCHAR* VCM_tcslwr(
	TCHAR* pszString,
	size_t numberOfElements
	)
{
#if	_MSC_VER < 1310
    return ::_tcslwr(pszString);
#else
	errno_t err = ::_tcslwr_s(pszString, numberOfElements);
    return pszString;
#endif   
}

//---------------------------------------------------------------------------
inline TCHAR* VCM_ultot(unsigned long value, TCHAR*	str, size_t  sizeOfstr,int radix)
{
#if	_MSC_VER < 1310
    return ::_ultot(value, str, radix);
#else
	errno_t err = ::_ultot_s(value, str, sizeOfstr, radix);
    return str;
#endif   
}

 inline char* VCM_ultoa(unsigned long value, char* str, size_t  sizeOfstr,int radix)
{
#if	_MSC_VER < 1310
    return ::_ultoa(value, str, radix);
#else
	errno_t err = ::_ultoa_s(value, str, sizeOfstr, radix);
    return str;
#endif   
}

	
//#if	_MSC_VER < 1310
//	#define VCM_sscanf ::sscanf
//#else
//	#define VCM_sscanf ::sscanf_s
//#endif

inline FILE * VCM_fopen(const char *filename,const char *mode)
{
#if	_MSC_VER < 1310
    return ::fopen(filename,mode);
#else
    FILE* pFile = NULL;
	errno_t err = ::fopen_s(&pFile,filename,mode);
    return pFile;
#endif  	
}

#endif	// WIN32

//---------------------------------------------------------------------------
inline FILE* VCM_tfopen(const TCHAR* filename, const TCHAR* mode)
{
#if	_MSC_VER < 1310
    return ::_tfopen(filename,mode);
#else
    FILE* pFile = NULL;
	errno_t err = ::_tfopen_s(&pFile,filename,mode);
    return pFile;
#endif   
}
//---------------------------------------------------------------------------
#if !(defined(__MACH__) && defined(_UNICODE)) 
inline TCHAR* VCM_tcstok(
   LPTSTR	ptszString,
   LPCTSTR	ptszDelimit,
   LPTSTR*	pptszContext
   )
{
#if	_MSC_VER < 1310
    return ::_tcstok(ptszString, ptszDelimit);
#else
	return ::_tcstok_s(ptszString, ptszDelimit, pptszContext);
#endif   
}
#endif 
//---------------------------------------------------------------------------
#if !(defined(__MACH__) && defined(_UNICODE)) 
inline char* VCM_strtok(
	LPSTR	pszString,
	LPCSTR	pszDelimit,
	LPSTR*	ppszContext
	)
{
#if	_MSC_VER < 1310
	return ::strtok(pszString, pszDelimit);
#else
	return ::strtok_s(pszString, pszDelimit, ppszContext);
#endif   
}
#endif   
//---------------------------------------------------------------------------
#if	_MSC_VER < 1310
	#define	VCM_strnicmp	strnicmp
//	#define	VCM_ftscanf		_ftscanf
//	#define VCM_stscanf		_stscanf
#else
	#define	VCM_strnicmp	_strnicmp
//	#define	VCM_ftscanf		_ftscanf_s
//	#define VCM_stscanf		_stscanf_s
#endif


//---------------------------------------------------------------------------
#ifdef	__cplusplus_cli
inline LPCSTR VCMSystemStringToLPCSTR(System::String^ str)
{
	LPCSTR pszConverted = 
		static_cast<LPCSTR>(const_cast<void*>(static_cast<const void*>(
			System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(
				str))));
	return pszConverted;
}

inline LPCWSTR VCMSystemStringToLPCWSTR(System::String^ str)
{
	LPCWSTR pwszConverted = 
		static_cast<LPCWSTR>(const_cast<void*>(static_cast<const void*>(
			System::Runtime::InteropServices::Marshal::StringToHGlobalUni(
				str))));
	return pwszConverted;
}

inline LPCTSTR VCMSystemStringToLPCTSTR(System::String^ str)
{
#ifdef _UNICODE
	return VCMSystemStringToLPCWSTR(str);
#else
	return VCMSystemStringToLPCSTR(str);
#endif
}

inline void VCMReleaseConvertedLPCTSTR(LPCTSTR ptszConverted)
{
	System::Runtime::InteropServices::Marshal::FreeHGlobal(
		static_cast<System::IntPtr>(const_cast<void*>(static_cast<const void*>(
			ptszConverted))));
}
#endif
//---------------------------------------------------------------------------

#endif	// _VCMigrate_h_
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//LRunasHdr.h
//Auth: longwq
//Date:	2008.5.23
//
#if !defined(INCLUDE__LRUNAS_20080523_HEADER)
#define		INCLUDE__LRUNAS_20080523_HEADER

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef		LRUNAS_EXPORT
#define		LRUNAS_API_(TYPE)		__declspec( dllimport ) TYPE WINAPI
#else
#define		LRUNAS_API_(TYPE)		__declspec( dllexport ) TYPE WINAPI
#endif

#define		RET_LRUNAS_FAILED			0	//Run application failed
#define		RET_LRUNAS_SUCCESSED		1	//Run application successed
//#define		RET_LRUNAS_ALREADY_ADMIN	2	//Now already an administrator privilege user,not need restart the application.


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
LRUNAS_API_(BOOL) IsUserAdminPrivilege();
LRUNAS_API_(int) RunasUserW(const WCHAR* lpwszUserName=NULL, const WCHAR* lpwszUserPsw=NULL, const WCHAR* lpwszCmdLine=NULL, const WCHAR* lpwszParam=NULL, BOOL bAdminPrivilege=NULL, BOOL bRemember=FALSE, BOOL bForceDisplayInterface=FALSE);
LRUNAS_API_(int) RunasUserA(const CHAR*  lpszUserName=NULL,  const CHAR*  lpszUserPsw=NULL,  const CHAR*  lpszCmdLine=NULL,  const CHAR*  lpszParam=NULL,  BOOL bAdminPrivilege=NULL, BOOL bRemember=FALSE, BOOL bForceDisplayInterface=FALSE);

#ifdef _UNICODE
typedef int(WINAPI *PRUNASUSER)(const WCHAR*, const WCHAR*, const WCHAR*, const WCHAR*, BOOL, BOOL, BOOL); 
#define RunasUser	RunasUserW
#else
typedef int(WINAPI *PRUNASUSER)(const CHAR*,  const CHAR*,  const CHAR*,  const CHAR*,  BOOL, BOOL, BOOL); 
#define RunasUser	RunasUserA
#endif

typedef BOOL(WINAPI* PISUSERANADMIN)();

#define USE_LRUNAS(EXIT_CODE)	\
{ \
	OutputDebugString(GetCommandLine()); \
	CLRunas LRunas ; \
	LRunas.LRunasUserIDE(EXIT_CODE);\
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//This part is load the LRunas DLL,offer an CLRunas class to convenience use
#ifndef  LRUNAS_EXPORT

class CLRunas
{
public:
	CLRunas()
	{
		TCHAR		szModule[MAX_PATH*2] = _T("");
		TCHAR		szPath[MAX_PATH*2] = _T("");
		TCHAR*		pszBias = NULL;

		m_hModule = NULL;
		m_fRunasUser = NULL;
		m_fIsUserAnAdmin = NULL;

		if(GetModuleFileName(NULL,szModule,MAX_PATH*2) > 0)
		{
			pszBias = _tcsrchr(szModule, _T('\\'));
			if(pszBias == NULL)
				pszBias = _tcsrchr(szModule,_T('/'));

			if(pszBias)
				pszBias[0] = _T('\0');


#if	_MSC_VER < 1310
			_sntprintf(szPath, sizeof(szPath)/sizeof(TCHAR), _T("%s\\LRunas.dll"),szModule);
#else
			_sntprintf_s(szPath, sizeof(szPath)/sizeof(TCHAR), _TRUNCATE, _T("%s\\LRunas.dll"),szModule);
#endif
		}
		else
		{
#if	_MSC_VER < 1310
			_sntprintf(szPath, sizeof(szPath)/sizeof(TCHAR), _T(".\\LRunas.dll"));
#else
			_sntprintf_s(szPath, sizeof(szPath)/sizeof(TCHAR), _TRUNCATE, _T(".\\LRunas.dll"));
#endif
		}

		m_hModule = LoadLibrary(szPath);
		if(m_hModule)
		{
			#ifdef _UNICODE
			m_fRunasUser = (PRUNASUSER)GetProcAddress(m_hModule,"RunasUserW");
			#else
			m_fRunasUser = (PRUNASUSER)GetProcAddress(m_hModule,"RunasUserA");
			#endif

			m_fIsUserAnAdmin = (PISUSERANADMIN)GetProcAddress(m_hModule,"IsUserAdminPrivilege");
		}
		else
		{
			OutputDebugString(_T("Load LRunas.dll failed"));
		}
	}

	virtual ~CLRunas()
	{
		if(m_hModule)
			FreeLibrary(m_hModule);

		m_hModule = NULL;
		m_fRunasUser = NULL;
		m_fIsUserAnAdmin = NULL;
	}

	int LRunasUser(LPCTSTR lpszUserName = NULL, LPCTSTR lpszUserPsw = NULL, LPCTSTR lpszCmdLine = NULL, LPCTSTR lpszParam = NULL, BOOL bAdminPrivilege = NULL, BOOL bRemember=FALSE, BOOL bForceDisplayInterface=FALSE)
	{
		if(m_fRunasUser)
		{
			return m_fRunasUser(lpszUserName, lpszUserPsw, lpszCmdLine, lpszParam, bAdminPrivilege, bRemember, bForceDisplayInterface);
		}
		else
			return RET_LRUNAS_FAILED;
	}

	BOOL IsUserAdminPrivilege()
	{
		if(m_fIsUserAnAdmin)
			return m_fIsUserAnAdmin();
		else
			return FALSE;
	}

	BOOL CheckMutex()
	{
		m_hMutex = ::CreateMutex(NULL,TRUE,_T("___LRunas_Mutex___"));

		if(GetLastError()==ERROR_ALREADY_EXISTS)
			return FALSE;
		else
			return TRUE;
	}

	void CloseMutex()
	{
		if (m_hMutex)
		{
			::ReleaseMutex(m_hMutex);
			CloseHandle(m_hMutex);
			m_hMutex = NULL;
		}
	}
	void LRunasUserIDE(int i32ExitCode = 0)
	{
		if(!IsUserAdminPrivilege())
		{
			if(FALSE == CheckMutex())
			{
				::MessageBox(NULL, _T("Elevate privileges as administrator failed, can not execute the programme!"), _T("Elevate privileges failed..."), MB_ICONEXCLAMATION | MB_OK);
				exit(0);
			}

			TCHAR szModule[MAX_PATH*2] = _T("");
			GetModuleFileName(NULL,szModule,MAX_PATH*2);	
		//	if(RET_LRUNAS_FAILED == LRunasUser(NULL,NULL,szModule,AfxGetApp()->m_lpCmdLine,TRUE,TRUE,FALSE))
			if(RET_LRUNAS_FAILED == LRunasUser(NULL,NULL,szModule,GetCommandLine(),true,TRUE,FALSE))
				OutputDebugString(_T("Call LRunas failed, can't elevat privilege to run!"));

			CloseMutex();
			exit(i32ExitCode);
		}
	}
private:
	HANDLE			m_hMutex;
	HMODULE			m_hModule;
	PRUNASUSER		m_fRunasUser;
	PISUSERANADMIN	m_fIsUserAnAdmin;
};

#endif	//LRUNAS_EXPORT

#endif	// !defined(INCLUDE__LRUNAS_20080523_HEADER)
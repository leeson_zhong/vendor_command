#ifndef ___ERROR_DEFINE___
#define ___ERROR_DEFINE___




//Return value definition
#define Err_No_Err							0x0000
#define Err_IO_FAIL							0x0001	//If get this value, refer to pdwErrCode and the meaning must search MSDN error define.
#define Err_No_Device						0x0002
#define Err_Invalid_Handle					0x0003
#define Err_Err_With_SenseKey				0x0004
#define Err_Buffer_Not_Enough				0x0005
#define Err_Invalid_Param					0x0006	
#define Err_Reserved_Empty					0x0007
#define Err_SRB_Err							0x0008	//If get this value, refer to pdwErrCode.
#define	Err_Internal_Error					0x0009	//Internal Error. Should no occure

#define Err_DeviceType_UnSupport			0x000A	//Device type is not support.   //added by fredchou 2007/11/23
#define Err_Password_UnSupport				0x000B	//Password is not support.      //added by fredchou 2007/11/23
#define Err_Table_Type_UnSupport			0x000C	//Table Type is not support.    //added by fredchou 2007/12/28
#define Err_Authorize_Type_UnSupport		0x000D	//Authorise Type is not support //added by fredchou 2007/12/28

#define	Err_Enum_USB_Device_Failed			0x0010	//Enumerate USB device failed.	//added by wiselychien 2008/03/12
#define	Err_Device_Is_Not_Opened			0x0011	//Device is not opened yet.		//added by wiselychien 2008/03/12

#define USB_INTERFACE_SUCCESS				0x0000
#define USB_INTERFACE_USB_HANDLE_FAILURE	0x0021
#define USB_INTERFACE_GET_DEVICE_ERROR		0x0022
#define USB_INTERFACE_SPI_ERASE_ERROR		0x0023
#define USB_INTERFACE_SPI_READ_ERROR		0x0024
#define USB_INTERFACE_SPI_WRITE_ERROR		0x0025
#define USB_INTERFACE_RESERVED_CMD_ERROR	0x0026

#define	USB_INTERFACE_INVALIDE_DLL_HANDLE	0x0030
#define	USB_INTERFACE_INVALIDE_PROC_POINTER	0x0031

#define	Err_Memory_Insufficient				0x0101	//System memory insufficient.	//added by wiselychien 2008/03/12
#define	Err_Not_Initialized					0x0102	//Library is not initialized.	//added by wiselychien 2008/06/02
#define	Err_Open_File_Failed				0x0103	//Open file failed.				//added by wiselychien 2008/06/13
#define	Err_Read_File_Failed				0x0104	//Read file failed.				//added by wiselychien 2008/06/13
#define	Err_Write_File_Failed				0x0105	//Write file failed.			//added by wiselychien 2008/06/13
#define	Err_Seek_File_Failed				0x0106	//Seek file failed.				//added by wiselychien 2008/06/13

#define Err_XML_NoError						0x0000
#define Err_XML_PathNotMatch				0x0201
#define Err_XML_BufferNotEnough				0x0202

//Sense key definition

#define SenseKey_No_Sense					0x000000	//0x1000
#define SenseKey_Media_No_Present			0x023A00	//0x1001
#define SenseKey_Invalid_OPcode				0x052000	//0x1002
#define SenseKey_Invalid_Cdb				0x052400	//0x1003
#define SenseKey_Media_Change				0x062800	//0x1004
#define SenseKey_Lun_Invalid				0x063F0E	//0x1005
#define SenseKey_Write_Protect				0x072700	//0x1006
#define SenseKey_Tag						0x0FFFFF	//0x1007				

//Scsi Vid Pid
#define DefaultScsiVid						"GENPLUS"
#define	DefaultScsiPid						"USB-MSDC_DISK_A"

#define NON_MOUNT_POINT_TYPE				0x00
#define MOUNT_POINT_TYPE					0x01

//USB Table Type ( Mass production / Real Table ).
#define TABLE_TYPE_MASS_PRODUCTION			0x00
#define TABLE_TYPE_REAL_TABLE				0x01

//Authorise Table Type ( Standard / Easy ).
#define AUTHORIZE_TYPE_STANDARD             0x00
#define AUTHORIZE_TYPE_EASY					0x01

#endif


QT -= gui

CONFIG += c++11
#CONFIG += console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS _USEGPUSBAGENTDLL

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

DESTDIR = Bin

SOURCES += \
    CmdlineInfo.cpp \
    GPUSB101SPIFlashCommand.cpp \
    GPUSBMassStorageStdAgentDll.cpp \
    GPUSBMassStorageStdSample.cpp \
    MassStorageCmdHandler.cpp \
    MassStorageCmdShowHelp.cpp
INCLUDEPATH = Include

HEADERS += \
    CmdlineInfo.h \
    GPUSB101SPIFlashCommand.h \
    GPUSBMassStorageStdAgentDll.h \
    MassStorageCmdHandler.h \
    MassStorageCmdShowHelp.h

DEFINES -= UNICODE

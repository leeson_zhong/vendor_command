// _CmdlineInfo.h: interface for the C_CmdlineInfo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX__CMDLINEINFO_H__745C7383_645F_41CB_97AB_ABE1C15111CF__INCLUDED_)
#define AFX__CMDLINEINFO_H__745C7383_645F_41CB_97AB_ABE1C15111CF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GPType.h"

#define		MAX_PARAM_COUNT			128

class C_CmdlineInfo// : public I_CmdlineInfo  
{
public:
			C_CmdlineInfo(LPCTSTR ptszCmdline);
	virtual ~C_CmdlineInfo();

public:
	virtual LPCTSTR GetCommand();
	virtual DWORD	GetParamCount();
	virtual BOOL	SetParamCount(DWORD dwNewCount);
	virtual LPCTSTR	GetParamItem(DWORD dwIndex);
	virtual BOOL	GetParamItemInt(DWORD dwIndex, int* pi32Value);
	virtual BOOL	SetParamItem(DWORD dwIndex,LPCTSTR ptszNewItem);
	virtual BOOL	Release();

	virtual LPCTSTR GetCmdline();

private:
	BOOL _Release();
	BOOL _ParseCmdline();

	TCHAR		m_ptszCmdline[512] ;

	DWORD		m_dwParamCount ;
	LPTSTR		m_ptszCmd ;
	LPTSTR		m_aptszParamTable[MAX_PARAM_COUNT] ;
};

#endif // !defined(AFX__CMDLINEINFO_H__745C7383_645F_41CB_97AB_ABE1C15111CF__INCLUDED_)

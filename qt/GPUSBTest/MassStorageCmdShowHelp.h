// MassStorageCmdShowHelp.h: interface for the C_MassStorageCmdShowHelp class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MASSSTORAGECMDSHOWHELP_H__05069DBA_854A_4C6F_8567_6F47808CD155__INCLUDED_)
#define AFX_MASSSTORAGECMDSHOWHELP_H__05069DBA_854A_4C6F_8567_6F47808CD155__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GPType.h"

#define AUTHORIZE_TYPE_STANDARD             0x00
#define AUTHORIZE_TYPE_EASY					0x01
#define AUTHORIZE_TYPE_SECURE_ID			0x02

#define	USB101ScsiVid	_T("GENPLUS")	
#define	USB101ScsiPid	_T("USB-MSDC_DISK_A")	
#define	USB101AuthType	0x01

#define	CDROMScsiVid	_T("GENERAL")
#define	CDROMScsiPid	_T("PLUS_CDROM")

#define DEFAULT_CUSKEY	_T("NNKP-NKCC-IRTF-1P7F-N3RD-DLEV-4335-NFCG")

class C_MassStorageCmdShowHelp;

typedef BOOL (C_MassStorageCmdShowHelp::*PFN_HelpHandler)();

typedef struct	tagHelpHandlerInfo
{
	TCHAR			tszCommand[32];
	PFN_HelpHandler	pfnHelpHandler;
} S_HelpHandlerInfo;

class C_MassStorageCmdShowHelp  
{
public:
			C_MassStorageCmdShowHelp();
	virtual ~C_MassStorageCmdShowHelp();

	BOOL	ShowAll();
	BOOL	Show(
			LPCTSTR	ptszCommand
			);

protected:
	BOOL	HelpHelp();
	BOOL	HelpSetScsiVidPid();
	BOOL	HelpSetDefaultVidPid();
	BOOL	HelpSetCdromVidPid();
	BOOL	HelpSetCustomerKey();
	BOOL	HelpFindDevSN();
	BOOL	HelpSelectDevSN();
	BOOL	HelpWriteData();
	BOOL	HelpReadData();
	BOOL	HelpWriteDataFromFile();
	BOOL	HelpReadDataToFile();
	BOOL	HelpEraseFlash();
	BOOL	HelpReadFlash();
	BOOL	HelpWriteFlash();
	BOOL	HelpAutoTest();
	BOOL	HelpSetTableType();
	BOOL	HelpSetAuthType();
	BOOL	HelpSetDiskType();
	BOOL	HelpReadCameraToFile();
private:
	static S_HelpHandlerInfo	m_asHelpHandlerInfo[];
};

#endif // !defined(AFX_MASSSTORAGECMDSHOWHELP_H__05069DBA_854A_4C6F_8567_6F47808CD155__INCLUDED_)

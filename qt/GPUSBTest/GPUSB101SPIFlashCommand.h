// GPUSB101SPIFlashCommand.h: interface for the C_GPUSB101SPIFlashCommand class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GPUSB101SPIFLASHCOMMAND_H__6824889B_D3D0_4EB7_8F8A_757C5D2559D6__INCLUDED_)
#define AFX_GPUSB101SPIFLASHCOMMAND_H__6824889B_D3D0_4EB7_8F8A_757C5D2559D6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GPUSBMassStorageStdAgentEx.h"

#ifdef _USEGPUSBAGENTDLL
	#include "GPUSBMassStorageStdAgentDll.h"
	#define C_GPUSBMassStorageStdAgent C_GPUSBMassStorageStdAgentDll
#else
	#define C_GPUSBMassStorageStdAgent C_GPUSBMassStorageStdAgentEx
#endif

enum E_EraseMemorySizeType
{
	eERASE_MEMORY_SIZE_4K	= 0x08,
	eERASE_MEMORY_SIZE_32K	= 0x40,
	eERASE_MEMORY_SIZE_64K	= 0x80
};

class C_GPUSB101SPIFlashCommand  
{
public:
			C_GPUSB101SPIFlashCommand();
	virtual ~C_GPUSB101SPIFlashCommand();

	int		EraseMemory(
			C_GPUSBMassStorageStdAgent&		DeviceAgent,
			LPCTSTR								ptszSerial,
			DWORD								dwMemoryAddress,
			E_EraseMemorySizeType				eEraseSizeType		// 0x08: 4KB, 0x40: 32KB, 0x80: 64KB
			);
	int		ReadMemory(
			C_GPUSBMassStorageStdAgent&		DeviceAgent,
			LPCTSTR								ptszSerial,
			DWORD								dwMemoryAddress,
			BYTE*								pbyReadBuffer,
			WORD								wReadSize
			);
	int		WriteMemory(
			C_GPUSBMassStorageStdAgent&		DeviceAgent,
			LPCTSTR								ptszSerial,
			DWORD								dwMemoryAddress,
			BYTE*								pbyReadBuffer,
			WORD								wWriteSize
			);
};

#endif // !defined(AFX_GPUSB101SPIFLASHCOMMAND_H__6824889B_D3D0_4EB7_8F8A_757C5D2559D6__INCLUDED_)

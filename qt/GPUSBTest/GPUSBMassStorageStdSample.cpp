// GPUSBMassStorageStdSample.cpp : Defines the entry point for the console application.
//
#include "QDebug"

#include "MassStorageCmdHandler.h"
#include "VCMigrate.h"

#ifdef WIN32
#include "LRunas\LRunasHdr.h"
#endif

int main(int argc, char* argv[])
{

#ifdef WIN32
    CLRunas *CRunWithPrivilege = new CLRunas();
    CRunWithPrivilege->LRunasUserIDE();
    delete CRunWithPrivilege;
#endif

	C_MassStorageCmdHandler CmdHandler;
	TCHAR tszCommandLine[1024];

    ::_tprintf(_T("Type \"h\" for help.\n\n"));

	while(1)
    {
		::_tprintf(_T("> "));
		::VCM_getts(tszCommandLine, 1024);

		if (CmdHandler.IsQuitCommand(tszCommandLine))
			break;
        ::strcpy(tszCommandLine,"FindDevSN");
        CmdHandler.ExecuteCommand(tszCommandLine);
	}

	return 0;
}

